{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell   #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE QuasiQuotes       #-}
{-# LANGUAGE PartialTypeSignatures #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE RankNTypes #-}
module Builders where

import HEA hiding (readAnno)

import Statistics.Quantile
import Data.Conduit.Zlib
import Data.Tuple (swap)
import Control.Arrow
import qualified Taiji.Utils.DataFrame as DF
import qualified Data.Vector as V
import qualified Data.Vector.Unboxed as U
import qualified Data.Text as T
import qualified Data.Text.IO as T
import Bio.RealWorld.GENCODE
import           Data.CaseInsensitive  (original)
import Data.Char (toUpper)
import Bio.Data.Bed.Types
import Bio.Data.Bed
import Control.Monad.ST (runST)
import qualified Data.Matrix as Mat
import qualified Data.Map.Strict as Map
import qualified Data.ByteString.Char8 as B
import qualified Data.HashMap.Strict as M
import qualified Data.HashSet as HS
import Data.List.Ordered (nubSort)
import Bio.Utils.Functions
import Taiji.Utils
import Shelly hiding (FilePath, path)
import Data.Binary
import System.Random.MWC.Distributions (uniformShuffle)
import System.Random.MWC (create)
import Statistics.Sample (mean)
import Data.Int
import Data.Matrix.Static.Sparse (toTriplet, SparseMatrix(..))
import Data.Matrix.Static.IO (fromMM', toMM)
import Data.Matrix.Dynamic (fromTriplet, Dynamic(..))

import Taiji.Prelude
import qualified Language.R                        as R
import           Language.R.QQ
import Language.R.HExp
import qualified Data.Vector.SEXP as S

{-
1.Home page: few words to describe HEA dataset: http://catlas.org/mousebrain/
2.Cell Browser: /mnt/silencer2/home/ziz361/data/catlas_input_example/cell_browser/umap_example
3.Cell Cluster: /projects/ps-renlab/yangli/projects/CEMBA/01.joint_dat/rs1cemba/rs1cemba.full.taxonomyLable.tsv
4.Tracks: /projects/ps-renlab/yangli/projects/CEMBA/01.joint_dat/rs1cemba/tracks/
-}

{-
color.tsv
Color specified for metafield.
One row per metafiled.

-}

readAnno :: FilePath -> IO [(B.ByteString, B.ByteString)]
readAnno fl = do
    anno <- map ((\(a:b:_:_) -> (a, b)) . B.split '\t') . B.lines <$>
        B.readFile fl
    return $ map swap $ M.toList $ M.fromListWith errMsg $ map swap anno
  where
    errMsg x y = error $ show x

track = do
    node "Bigwig" [| \() -> do
        bw <- lookupConfig "bigwig"
        annoFl <- lookupConfig "annotation"
        dir <- (<> "/tracks/bw/") <$> lookupConfig "output_dir"
        liftIO $ do
            shelly $ mkdir_p dir
            fls <- shelly $ lsT bw
            anno <- readAnno annoFl
            forM_ fls $ \fl -> do
                let cl = B.pack $ T.unpack $ fst $ T.breakOn ".bw" $ snd $ T.breakOnEnd "/" fl
                    output = dir <> B.unpack (fromMaybe (error $ show cl) $ lookup cl anno) <> ".bw"
                print output
                shelly $ cp (fromText fl) (fromText $ T.pack output)
        |] $ return ()
    node "cCRE" [| \() -> do
        creFl <- lookupConfig "cre"
        cluster <- lookupConfig "cluster_peak"
        annoFl <- lookupConfig "annotation"
        dir <- (<> "/tracks/cCREs/") <$> lookupConfig "output_dir"
        liftIO $ do
            let suffix = ".narrowPeak.gz"
            shelly $ mkdir_p $ fromText $ T.pack dir
            anno <- readAnno annoFl
            peaks <- fmap (filter (suffix `T.isSuffixOf`)) $ shelly $
                lsT $ fromText $ T.pack cluster
            let f :: Int -> BED3 -> BED
                f i p = name .~ Just ("cCRE" <> B.pack (show i)) $ convert p
            cCREs <- fmap (zipWith f [1..]) $ runResourceT $ runConduit $ streamBedGzip creFl .| sinkList
            forM_ peaks $ \pkFl -> do
                let nm = fromJust $ lookup (B.pack $ T.unpack $ fst $ T.breakOn suffix $ snd $ T.breakOnEnd "/" pkFl) anno
                    output = dir <> B.unpack nm <> ".cCRE.bed"
                pk <- runResourceT $ runConduit $ streamBedGzip (T.unpack pkFl) .| sinkList :: IO [BED3]
                runResourceT $ runConduit $ yieldMany cCREs .| intersectBed pk .| sinkFileBed output
        |] $ return ()

cell_cluster = do
    node "Cell_Cluster" [| \() -> do
        meta <- lookupConfig "metadata"
        annoFl <- lookupConfig "annotation"
        dir <- lookupConfig "output_dir"
        let header = B.intercalate "\t"
                [ "MajorType", "MajorTypeColor", "Description", "ProbableLocation"
                , "NucleiCounts", "avgTSSe", "avgUQ"]
            output = dir <> "/HEA.full.taxonomyLable.tsv"
        liftIO $ do
            shelly $ mkdir_p $ fromText $ T.pack dir
            df <- DF.readTableWith id meta
            anno <- readAnno annoFl
            let cl = V.toList $ df `DF.cindex` ("Cluster" :: T.Text)
                tsse = map readDouble $ V.toList $ df `DF.cindex` ("TSSe" :: T.Text)
                frag = map readDouble $ V.toList $ df `DF.cindex` ("Num_fragments" :: T.Text)
                colorMap = zip (map fst anno) colors
                f x = let c = fromMaybe (error $ show $ head x ^._1) $ lookup (head x ^._1) anno
                          n = B.pack $ show $ length x
                          tsse = B.pack $ show $ mean $ U.fromList $ map (^._2) x
                          ue = B.pack $ show $ truncate $ mean $ U.fromList $ map (^._3) x
                      in B.intercalate "\t" [c, fromMaybe (error $ show $ head x ^._1) $ lookup (head x ^. _1) colorMap, c, "NA", n, tsse, ue]
            B.writeFile output $ B.unlines $ (header:) $ map f $ groupBy ((==) `on` (^._1)) $ sortBy (comparing (^._1)) $ zip3 cl tsse frag
        |] $ return ()

cell_browser :: Builder ()
cell_browser = do
    -- gene.tsv: Unique gene name. One name per row.
    -- gmat.mtx: Sparse matrix for expression value in gene by cell matrix.

    node "Gene_Matrix" [| \cells -> do
        dir <- (<> "/Cell_Browser") <$> lookupConfig "output_dir"
        gencode <- lookupConfig "gencode"
        geneFl <- lookupConfig "gene"
        matFl <- lookupConfig "gene_matrix"
        liftIO $ do
            codingGenes <- HS.fromList . map (B.map toUpper . original . geneName) .
                filter (any ((==Coding) . transType) . geneTranscripts) <$> readGenes gencode
            shelly $ mkdir_p dir
            genes <- runResourceT $ runConduit $ sourceFile geneFl .| multiple ungzip .|
                linesUnboundedAsciiC .| mapC (fst . B.break (==':')) .| sinkList
            let output1 = dir <> "/HEA.gene.tsv"
                output2 = dir <> "/HEA.gmat.mtx"
                geneGroup = zip [0..] $ groupBy ((==) `on` fst) $
                    sortBy (comparing fst) $
                    filter ((`HS.member` codingGenes) . fst) $ zip genes [0..]
                idxMap = M.fromListWith undefined $ flip concatMap geneGroup $ \(i, xs) -> zip (map snd xs) $ repeat i
                g xs = M.toList $ M.fromListWith max $ mapMaybe (\(i, x) -> case M.lookup i idxMap of
                    Nothing -> Nothing
                    Just i' -> Just (i', x) ) xs

            B.writeFile output1 $ B.unlines $ map (fst . head . snd) geneGroup
            mat <- (\x -> x{_num_col = length geneGroup}) . mapRows (second g) <$> mkSpMatrix readInt matFl

            bcs <- B.lines <$> B.readFile cells
            let bcIdx = M.fromList $ zip bcs [0..]
                f (nm, xs) = case M.lookup nm bcIdx of
                    Nothing -> Nothing
                    Just i -> Just $ map (\(j,x) -> (j,i,x)) xs
            vec <- runResourceT $ runConduit $ streamRows mat .| 
                concatMapC f .| concatC .| sinkVector :: IO (U.Vector (Int,Int,Int))
            case fromTriplet (_num_col mat, M.size bcIdx) vec of
                Dynamic m -> runResourceT $ runConduit $
                    toMM (m :: SparseMatrix _ _ U.Vector Int) .| sinkFile output2
            return (output1, output2)
        |] $ return ()
    ["Cell_TSV"] ~> "Gene_Matrix"

    -- cell.tsv: Unique identifier for cell. One identifer per row.
    node "Cell_TSV" [| \() -> do
        metadata <- lookupConfig "metadata"
        dir <- (<> "/Cell_Browser") <$> lookupConfig "output_dir"
        let output = dir <> "/HEA.cell.tsv"
        liftIO $ do
            shelly $ mkdir_p $ fromText $ T.pack dir
            x <- B.unlines . map (head . B.split '\t') . tail . B.lines <$>
                B.readFile metadata
            B.writeFile output x
            return output
        |] $ return ()

    -- meta.tsv: First row is the name of meta fields. First column must be the cell identifier.
    node "Meta_TSV" [| \() -> do
        meta <- lookupConfig "metadata"
        annoFl <- lookupConfig "annotation"
        dir <- (<> "/Cell_Browser") <$> lookupConfig "output_dir"
        let output = dir <> "/HEA.meta.tsv"
            header = "cellID\tsample\treplicate\tlogUMI\ttsse\ttissue\tcell type"
        liftIO $ do
            shelly $ mkdir_p dir
            anno <- map (\(x,y) -> (T.pack $ B.unpack x, T.pack $ B.unpack y)) <$> readAnno annoFl
            df <- DF.readTableWith (T.pack . B.unpack) meta
            let content = [ DF.rowNames df
                    , map (fst . T.breakOn "+") $ DF.rowNames df
                    , replicate (fst $ DF.dim df) "1"
                    , map (T.pack . show . logBase 10 . read . T.unpack) $ V.toList $ df `DF.cindex` ("Num_fragments" :: T.Text)
                    , V.toList $ df `DF.cindex` ("TSSe" :: T.Text)
                    , map (T.init . fst . T.breakOnEnd "_" . fst . T.breakOn "+") $ DF.rowNames df
                    , map (\x -> fromMaybe (error $ show x) $ lookup ("C" <> x) anno) $ V.toList $ df `DF.cindex` ("Cluster" :: T.Text) ]
            T.writeFile output $ T.unlines $ header : map (T.intercalate "\t") (transpose content)
            return output
        |] $ return ()

    -- coords.tsv: Coordinate file that stores the x and y coordinate.
    --             First column is cell identifier, second is x and third is y.
    node "Coord_TSV" [| \() -> do
        meta <- lookupConfig "metadata"
        dir <- (<> "/Cell_Browser") <$> lookupConfig "output_dir"
        let output = dir <> "/HEA.coords.tsv"
            header = "cellID\tx\ty"
        liftIO $ do
            shelly $ mkdir_p $ fromText $ T.pack dir
            df <- DF.readTableWith (T.pack . B.unpack) meta
            let content = [ DF.rowNames df
                    , V.toList $ df `DF.cindex` ("UMAP1" :: T.Text)
                    , V.toList $ df `DF.cindex` ("UMAP2" :: T.Text) ]
            T.writeFile output $ T.unlines $ header : map (T.intercalate "\t") (transpose content)
        return output
        |] $ return ()

colors :: [B.ByteString]
colors = ["#ff8a94",
    "#00fe64",
    "#100cb5",
    "#53dc12",
    "#5f22cf",
    "#aeeb00",
    "#5545f0",
    "#d8ff2a",
    "#00049d",
    "#abff47",
    "#8911cd",
    "#00cf25",
    "#ae0cd1",
    "#78d900",
    "#a351ff",
    "#e0ef01",
    "#013dd8",
    "#fff235",
    "#0251e0",
    "#e1ff52",
    "#da37ec",
    "#5dff7a",
    "#ff51fd",
    "#01b01d",
    "#e35aff",
    "#80ff6e",
    "#9200ab",
    "#01ec76",
    "#e600bd",
    "#02d363",
    "#c262ff",
    "#01b83d",
    "#8460ff",
    "#ffd92a",
    "#001483",
    "#cdff74",
    "#7a009f",
    "#51ff9d",
    "#ff2eae",
    "#499e00",
    "#002ea3",
    "#babd00",
    "#10005e",
    "#a4ff8c",
    "#b40097",
    "#01dd95",
    "#fc0070",
    "#72ffc1",
    "#8a008b",
    "#c9ff8c",
    "#6d007f",
    "#fff369",
    "#00105f",
    "#d7b900",
    "#0057c7",
    "#76a500",
    "#a37cff",
    "#328a00",
    "#ff76f2",
    "#4f8900",
    "#ff61d6",
    "#00b063",
    "#b40086",
    "#01d7aa",
    "#fc0044",
    "#00ceb3",
    "#dd000d",
    "#02e0f0",
    "#ea3200",
    "#0183f7",
    "#f55100",
    "#7180ff",
    "#97a800",
    "#e884ff",
    "#3c7a00",
    "#4d006c",
    "#b5ffb1",
    "#0a0044",
    "#ffd35b",
    "#0166cc",
    "#f58500",
    "#027fdb",
    "#e89200",
    "#7790ff",
    "#b0a200",
    "#004998",
    "#ffad3f",
    "#001d5a",
    "#ffe679",
    "#100028",
    "#f1ffa5",
    "#750064",
    "#7cffd6",
    "#ff1157",
    "#02b57b",
    "#e40075",
    "#019144",
    "#e20064",
    "#01cfc7",
    "#d82600",
    "#00d2e8",
    "#ff313f",
    "#00b8f5",
    "#c30013",
    "#90ffe5",
    "#cd0024",
    "#01bcea",
    "#dc6300",
    "#57b1ff",
    "#d26900",
    "#78a6ff",
    "#c08f00",
    "#a19aff",
    "#778f00",
    "#ff8eed",
    "#017427",
    "#ff4e98",
    "#005d0c",
    "#ff76c6",
    "#017f4f",
    "#b80071",
    "#b0ffd9",
    "#ce0037",
    "#00a78a",
    "#cd004b",
    "#a9fffe",
    "#930800",
    "#5fd3ff",
    "#b03300",
    "#7fdcff",
    "#ff7035",
    "#009fcf",
    "#ff5e43",
    "#016daa",
    "#ffa045",
    "#002e59",
    "#ffe68d",
    "#410039",
    "#e1ffb9",
    "#89005f",
    "#d4ffd0",
    "#a60054",
    "#c0ffec",
    "#9f002e",
    "#aff0ff",
    "#841400",
    "#a3c7ff",
    "#aa4a00",
    "#d5a3ff",
    "#9e8200",
    "#dfb3ff",
    "#576700",
    "#ff99e2",
    "#005628",
    "#ff6db0",
    "#017859",
    "#ff555b",
    "#00858a",
    "#ff6d54",
    "#00558b",
    "#ff934e",
    "#001a35",
    "#ffc469",
    "#0b0000",
    "#f9ffe4",
    "#33001b",
    "#ffe39c",
    "#25000d",
    "#f0fffb",
    "#2c0002",
    "#ffe8ba",
    "#4d002d",
    "#dce9ff",
    "#760011",
    "#cad1ff",
    "#b47500",
    "#d3bfff",
    "#385200",
    "#ffaff3",
    "#003817",
    "#ff6093",
    "#005333",
    "#ff6e74",
    "#002314",
    "#f8caff",
    "#061d00",
    "#ffc0f0",
    "#253200",
    "#f3d8ff",
    "#1f2000",
    "#e1d0ff",
    "#a07400",
    "#00809e",
    "#ff875e",
    "#001c21",
    "#ffba87",
    "#002f33",
    "#ff95b1",
    "#003a28",
    "#ffb2de",
    "#221700",
    "#ffd9cf",
    "#2b0f00",
    "#ffbc9f",
    "#014e6e",
    "#9f6100",
    "#00495a",
    "#ff9f97",
    "#006b68",
    "#810025",
    "#ffb5bd",
    "#3e3c00",
    "#6b0044",
    "#7f4d00",
    "#78003e",
    "#5a3800",
    "#640026",
    "#7e3900",
    "#3e0f00",
    "#5d2b00",
    "#630012",
    "#3c2700"]