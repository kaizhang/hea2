{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell   #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE QuasiQuotes       #-}
{-# LANGUAGE PartialTypeSignatures #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE RankNTypes #-}
import HEA

import Taiji.Prelude
import qualified Language.R                        as R
import           Language.R.QQ
import Language.R.HExp
import qualified Data.Vector.SEXP as S

import Builders

--------------------------------------------------------------------------------
-- Workflow
--------------------------------------------------------------------------------
build "wf" [t| SciFlow ENV |] $ do
    cell_browser
    cell_cluster
    track

main :: IO ()
main = mainFun wf