-------------------------------------------------------------------------------
-- Parameters
-------------------------------------------------------------------------------

let plink_prefix : Text = "LDSC_hg38/plink_files/1000G.EUR.hg38."
let baseline_prefix : Text = "LDSC_hg38/baseline_v1.2/baseline."
let weights_prefix : Text = "LDSC_hg38/weights/weights.hm3_noMHC."

let chrList : List Natural = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22]
let annotation_ids : List Text = ["A_C10", "A_C1.1", "A_C11", "A_C12.1", "A_C12.2", "A_C12.3", "A_C12.4", "A_C1.2", "A_C13.1", "A_C13.2", "A_C13.3", "A_C13.4", "A_C13.5", "A_C13.6", "A_C13.7", "A_C13.8", "A_C13.9", "A_C1.3", "A_C1.4", "A_C14", "A_C15.1", "A_C15.2", "A_C15.3", "A_C15.4", "A_C15.5", "A_C15.6", "A_C1.5", "A_C16.1", "A_C16.2", "A_C16.3", "A_C16.4", "A_C16.5", "A_C1.6", "A_C17.1", "A_C17.2", "A_C17.3", "A_C17.4", "A_C17.5", "A_C17.6", "A_C17.7", "A_C1.7", "A_C18.1", "A_C18.2", "A_C18.3", "A_C1.8", "A_C19.1", "A_C19.2", "A_C1.9", "A_C20.1", "A_C20.2", "A_C20.3", "A_C20.4", "A_C20.5", "A_C21.1", "A_C21.2", "A_C2.1", "A_C22.1", "A_C22.2", "A_C22.3", "A_C22.4", "A_C2.2", "A_C23.1", "A_C23.2", "A_C2.3", "A_C24", "A_C25.1", "A_C25.2", "A_C26", "A_C27.1", "A_C27.2", "A_C27.3", "A_C28", "A_C29", "A_C30", "A_C3.1", "A_C3.2", "A_C4.1", "A_C4.2", "A_C4.3", "A_C4.4", "A_C4.5", "A_C4.6", "A_C4.7", "A_C4.8", "A_C4.9", "A_C5.10", "A_C5.11", "A_C5.1", "A_C5.2", "A_C5.3", "A_C5.4", "A_C5.5", "A_C5.6", "A_C5.7", "A_C5.8", "A_C5.9", "A_C6.1", "A_C6.2", "A_C7.1", "A_C7.2", "A_C8.1", "A_C8.2", "A_C9.1", "A_C9.2", "A_C9.3", "A_C9.4", "A_C9.5", "A_C9.6", "A_C9.7", "A_C9.8", "A_C9.9", "F_C10", "F_C11.1", "F_C11.2", "F_C11.3", "F_C11.4", "F_C11.5", "F_C11.6", "F_C1.1", "F_C12.1", "F_C12.2", "F_C12.3", "F_C12.4", "F_C12.5", "F_C12.6", "F_C12.7", "F_C12.8", "F_C12.9", "F_C1.2", "F_C13", "F_C14", "F_C15.1", "F_C15.2", "F_C15.3", "F_C15.4", "F_C15.5", "F_C15.6", "F_C15.7", "F_C15.8", "F_C16.1", "F_C16.2", "F_C16.3", "F_C16.4", "F_C16.5", "F_C17.10", "F_C17.11", "F_C17.12", "F_C17.1", "F_C17.2", "F_C17.3", "F_C17.4", "F_C17.5", "F_C17.7", "F_C17.8", "F_C17.9", "F_C18.1", "F_C18.2", "F_C18.3", "F_C18.4", "F_C18.5", "F_C18.6", "F_C18.7", "F_C18.8", "F_C19.1", "F_C19.2", "F_C19.3", "F_C19.4", "F_C19.5", "F_C20.1", "F_C20.2", "F_C20.3", "F_C20.4", "F_C21.1", "F_C21.2", "F_C22.1", "F_C22.2", "F_C22.3", "F_C22.4", "F_C22.5", "F_C23.1", "F_C23.2", "F_C24.1", "F_C24.2", "F_C24.3", "F_C24.4", "F_C24.5", "F_C24.6", "F_C24.7", "F_C24.8", "F_C24.9", "F_C25.1", "F_C25.2", "F_C26.1", "F_C27", "F_C29", "F_C2", "F_C30", "F_C3.1", "F_C31", "F_C32", "F_C3.3", "F_C33", "F_C34", "F_C35", "F_C36", "F_C4", "F_C5", "F_C6.1", "F_C6.2", "F_C7.10", "F_C7.1", "F_C7.2", "F_C7.3", "F_C7.4", "F_C7.5", "F_C7.6", "F_C7.7", "F_C7.8", "F_C8.1", "F_C8.2", "F_C9.1", "F_C9.2"]

-------------------------------------------------------------------------------
-- Function definition
-------------------------------------------------------------------------------

let List/map =
      https://prelude.dhall-lang.org/v11.1.0/List/map sha256:dd845ffb4568d40327f2a817eb42d1c6138b929ca758d50bc33112ef3c885680

let Plink : Type = 
    { chromosome : Text
    , prefix : Text }
let makePlink = List/map Natural Plink ( \(i : Natural) ->
      let chromosome : Text = Natural/show i
      let prefix : Text = "${plink_prefix}${chromosome}"
      in { chromosome, prefix } )
let SNP : Type = 
    { snp_chromosome : Text
    , snp_file : Text }
let makeSNP = List/map Natural SNP ( \(i : Natural) ->
      let snp_chromosome : Text = Natural/show i
      let snp_file : Text = "print_snps.txt"
      in { snp_chromosome, snp_file } )
let Annotation : Type = 
    { annotation_name : Text
    , annotation_file : Text }
let makeAnno = List/map Text Annotation ( \(id : Text) ->
      let annotation_file : Text = "/home/kaizhang/data/public_html/HEA/data/Peaks/${id}.narrowPeak.gz"
      let annotation_name : Text = id
      in { annotation_name, annotation_file } )

-------------------------------------------------------------------------------
-- Body
-------------------------------------------------------------------------------

in { output_dir = "output"
   , ldsc_python_path = "/home/kaizhang/data/software/ldsc-1.0.1/"

   , plink_files = makePlink chrList
   , snp_files = makeSNP chrList
   , baseline_ldscore = baseline_prefix
   , weights = weights_prefix

   , summary_statistics = ./sumstats.dhall
   -- , summary_statistics = [{ trait = "Atrial Fibrillation", trait_file = "all_sumstats/PASS_AtrialFibrillation_Nielsen2018.sumstats" }]

   , annotations = makeAnno annotation_ids

   , submit_params = "-q glean -l walltime=8:00:00"
   , submit_command = "qsub"
   , submit_cpu_format = "-l nodes=1:ppn=%d"
   , submit_memory_format = "-l mem=%dG"
   }
