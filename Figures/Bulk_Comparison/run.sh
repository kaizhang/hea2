set -e
cd src
stack ghc main -- -threaded -rtsopts
cd ..
src/main delete $1
src/main run --config config.yml --select $1
