{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell   #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE QuasiQuotes       #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE GADTs #-}
import HEA
import qualified Data.Text as T
import qualified Data.Text.IO as T
import Data.Conduit.Zlib (multiple, ungzip, gzip)
import qualified Data.Vector.Unboxed as U
import qualified Data.ByteString.Char8 as B
import Bio.Utils.Functions
import qualified Data.Matrix as Mat
import Control.Concurrent.Async (mapConcurrently)
import Data.Either
import Bio.Data.Bed
import Bio.Data.Bed.Utils
import Bio.Data.Bed.Types
import Bio.Data.Bam
import Shelly hiding (path)
import Taiji.Prelude
import qualified Data.HashMap.Strict as M
import qualified Data.Set as S
import qualified Data.Map.Strict as Map
import Control.DeepSeq
import Statistics.Correlation (pearson, spearman)
import Statistics.Distribution.Normal (standard)
import Data.List.Ordered
import Statistics.Sample
import Statistics.Distribution (complCumulative)
import Data.Binary
import qualified Data.Vector as V
import qualified Taiji.Utils.DataFrame as DF

import Taiji.Prelude hiding (groupBy)
import qualified Language.R                        as R
import           Language.R.QQ
import Language.R.HExp

sampleMeta = 
    [ ("adipose_omentum_SM-ADYHB", ("ENCDO271OUW", "omental fat pad"))
    , ("artery_aorta_SM-JF1NU", ("ENCDO271OUW", "ascending aorta"))
    , ("colon_transverse_SM-A9VP4", ("ENCDO271OUW", "transverse colon"))
    , ("esophagus_mucosa_SM-A9VPA", ("ENCDO271OUW", "esophagus squamous epithelium"))
    , ("esophagus_muscularis_SM-CSSCV", ("ENCDO271OUW", "esophagus muscularis mucosa"))
    , ("heart_atrial_appendage_SM-JF1NX", ("ENCDO271OUW", "right atrium auricular region"))
    , ("heart_lv_SM-JF1NY", ("ENCDO271OUW", "heart left ventricle"))
    , ("lung_SM-JF1NZ", ("ENCDO271OUW", "upper lobe of left lung"))
    , ("mammary_tissue_SM-JF1NV", ("ENCDO271OUW", "breast epithelium"))
    , ("muscle_SM-C1PWV", ("ENCDO271OUW", "gastrocnemius medialis"))
    , ("nerve_tibial_SM-CP2V6", ("ENCDO271OUW", "tibial nerve"))
    , ("pancreas_SM-ADRUQ", ("ENCDO271OUW", "body of pancreas"))
    , ("skin_SM-JF1O1", ("ENCDO271OUW", "suprapubic skin"))
    , ("skin_sun_exposed_SM-ADYHK", ("ENCDO271OUW", "lower leg skin"))
    , ("small_intestine_SM-JF1O2", ("ENCDO271OUW", "Peyer's patch"))
    , ("stomach_SM-JF1O3", ("ENCDO271OUW", "stomach"))
    , ("thyroid_SM-JF1O4", ("ENCDO271OUW", "thyroid gland"))
    , ("uterus_SM-A87A2", ("ENCDO271OUW", "uterus"))
    , ("adipose_omentum_SM-CHZRM", ("ENCDO451RUA", "omental fat pad"))
    , ("artery_aorta_SM-CR89M", ("ENCDO451RUA", "thoracic aorta"))
    , ("artery_tibial_SM-CHLWW", ("ENCDO451RUA", "tibial artery"))
    , ("colon_transverse_SM-CSSDA", ("ENCDO451RUA", "transverse colon"))
    , ("esophagus_ge_junction_SM-CTD24", ("ENCDO451RUA", "gastroesophageal sphincter"))
    , ("esophagus_mucosa_SM-AZPYJ", ("ENCDO451RUA", "esophagus squamous epithelium"))
    , ("esophagus_muscularis_SM-IQYD1", ("ENCDO451RUA", "esophagus muscularis mucosa"))
    , ("lung_SM-A62E9", ("ENCDO451RUA", "upper lobe of left lung"))
    , ("muscle_SM-C1MKW", ("ENCDO451RUA", "gastrocnemius medialis"))
    , ("nerve_tibial_SM-CHLWU", ("ENCDO451RUA", "tibial nerve"))
    , ("pancreas_SM-JF1NS", ("ENCDO451RUA", "body of pancreas"))
    , ("skin_sun_exposed_SM-JF1NT", ("ENCDO451RUA", "lower leg skin"))
    , ("small_intestine_SM-ADA5F", ("ENCDO451RUA", "Peyer's patch"))
    , ("stomach_SM-CHLWL", ("ENCDO451RUA", "stomach"))
    , ("thyroid_SM-C1MKY", ("ENCDO451RUA", "thyroid gland"))
    , ("adipose_omentum_SM-IOBHJ", ("ENCDO793LXB", "omental fat pad"))
    , ("adrenal_gland_SM-A8WNO", ("ENCDO793LXB", "adrenal gland"))
    , ("artery_aorta_SM-C1MLC", ("ENCDO793LXB", "ascending aorta"))
    , ("artery_tibial_SM-IOBHK", ("ENCDO793LXB", "tibial artery"))
    , ("colon_sigmoid_SM-AZPYO", ("ENCDO793LXB", "sigmoid colon"))
    , ("colon_transverse_SM-A9HOW", ("ENCDO793LXB", "transverse colon"))
    , ("colon_transverse_SM-BZ2ZS", ("ENCDO793LXB", "transverse colon"))
    , ("esophagus_ge_junction_SM-IOERG", ("ENCDO793LXB", "gastroesophageal sphincter"))
    , ("esophagus_mucosa_SM-A9HOR", ("ENCDO793LXB", "esophagus squamous epithelium"))
    , ("esophagus_muscularis_SM-IOBHM", ("ENCDO793LXB", "esophagus muscularis mucosa"))
    , ("heart_atrial_appendage_SM-IOBHN", ("ENCDO793LXB", "right atrium auricular region"))
    , ("heart_lv_SM-IOBHO", ("ENCDO793LXB", "heart left ventricle"))
    , ("liver_SM-A8WNZ", ("ENCDO793LXB", "right lobe of liver"))
    , ("lung_SM-A8WNH", ("ENCDO793LXB", "upper lobe of left lung"))
    , ("mammary_tissue_SM-IOBHL", ("ENCDO793LXB", "breast epithelium"))
    , ("muscle_SM-ADA6L", ("ENCDO793LXB", "gastrocnemius medialis"))
    , ("muscle_SM-IOBHP", ("ENCDO793LXB", "gastrocnemius medialis"))
    , ("nerve_tibial_SM-IOBHQ", ("ENCDO793LXB", "tibial nerve"))
    , ("ovary_SM-IOBHR", ("ENCDO793LXB", "ovary"))
    , ("pancreas_SM-IOBHS", ("ENCDO793LXB", "body of pancreas"))
    , ("skin_SM-IOBHU", ("ENCDO793LXB", "suprapubic skin"))
    , ("skin_sun_exposed_SM-IOBHT", ("ENCDO793LXB", "lower leg skin"))
    , ("small_intestine_SM-A62GO", ("ENCDO793LXB", "Peyer's patch"))
    , ("stomach_SM-IOBHV", ("ENCDO793LXB", "stomach"))
    , ("thyroid_SM-IOBHW", ("ENCDO793LXB", "thyroid gland"))
    , ("uterus_SM-IOBHX", ("ENCDO793LXB", "uterus"))
    , ("vagina_SM-A9HOS", ("ENCDO793LXB", "vagina"))
    , ("adipose_omentum_SM-CSSD4", ("ENCDO845WKR", "omental fat pad"))
    , ("artery_aorta_SM-C1PX3", ("ENCDO845WKR", "thoracic aorta"))
    , ("colon_sigmoid_SM-JF1O8", ("ENCDO845WKR", "sigmoid colon"))
    , ("colon_transverse_SM-ACCQ1", ("ENCDO845WKR", "transverse colon"))
    , ("esophagus_muscularis_SM-A8CPH", ("ENCDO845WKR", "esophagus muscularis mucosa"))
    , ("lung_SM-ACCPU", ("ENCDO845WKR", "upper lobe of left lung"))
    , ("muscle_SM-JF1O9", ("ENCDO845WKR", "gastrocnemius medialis"))
    , ("pancreas_SM-JF1O6", ("ENCDO845WKR", "body of pancreas"))
    , ("skin_sun_exposed_SM-IQYCP", ("ENCDO845WKR", "lower leg skin"))
    , ("stomach_SM-JF1NP", ("ENCDO845WKR", "stomach")) ]

mergePeaks :: FilePath -> ConduitT NarrowPeak NarrowPeak (ResourceT IO) ()
mergePeaks tmpdir = do
    mapC resize .| sinkFileBed tmp1
    liftIO $ shelly $ escaping False $ bashPipeFail bash_ "cat" $
        [T.pack tmp1, "|", "sort", "-k1,1", "-k2,2n", "-k3,3n", ">", T.pack tmp2]
    streamBed tmp2 .| mergeSortedBedWith iterativeMerge .| concatC
  where
    tmp1 = tmpdir <> "/tmp1"
    tmp2 = tmpdir <> "/tmp2"
    iterativeMerge [] = []
    iterativeMerge peaks = bestPeak : iterativeMerge rest
      where
        rest = filter (\x -> sizeOverlapped x bestPeak == 0) peaks
        bestPeak = maximumBy (comparing (^.npSignal)) peaks
    resize pk = chromStart .~ max 0 (summit - halfWindowSize) $
        chromEnd .~ summit + halfWindowSize$
        npPeak .~ Just halfWindowSize $ pk
      where
        summit = pk^.chromStart + fromJust (pk^.npPeak)
    halfWindowSize = 200


build "wf" [t| SciFlow ENV |] $ do
    node "SCATAC_Peaks" [| \() -> do
        dir <- (<> "/scATAC/") <$> lookupConfig "output_dir"
        peakdir <- lookupConfig "scatac_peaks"
        liftIO $ withTempDir Nothing $ \tmp -> do
            shelly $ mkdir_p dir
            samples <- fmap (filter ("SM-" `T.isInfixOf`)) $ shelly $ lsT peakdir
            forM samples $ \s -> do
                let nm = T.init $ fst $ T.breakOnEnd "_" $ snd $ T.breakOnEnd "/" s
                    output = T.unpack $ T.pack dir <> nm <> ".bed.gz"
                peaks <- shelly $ lsT $ T.unpack s
                runResourceT $ runConduit $ mapM_ (streamBedGzip . T.unpack) peaks .|
                    mergePeaks tmp .| sinkFileBedGzip output
                beds <- runResourceT $ runConduit $ mapM_ (streamBedGzip . T.unpack) peaks .| sinkList :: IO [BED3]
                n <- runConduit $ mergeBed beds .| mapC size .| sumC :: IO Int
                return (nm, output, n)
        |] $ return ()

    node "SCATAC_Depth" [| \() -> do
        cldir <- lookupConfig "scatac_clusters"
        liftIO $ do
            samples <- fmap (filter ("SM-" `T.isInfixOf`)) $ shelly $ lsT cldir
            forM samples $ \s -> do
                let nm = fst $ T.breakOn "_rep1" $ snd $ T.breakOnEnd "/" s
                cls <- decodeFile $ T.unpack s
                let d = foldl1' (+) $ map (foldl1' (+) . map _cell_coverage . _cluster_member) cls
                return (nm, d)
        |] $ return ()

    uNode "DNase_Peaks_Prep" [| \() -> do
        peakdir <- lookupConfig "dnase_peaks"
        liftIO $ do
            samples <- fmap (filter (".narrowPeak.gz" `T.isSuffixOf`)) $ shelly $ lsT peakdir
            return $ flip map samples $ \s ->
                let [nm, i, _] = T.splitOn "+" $ T.init $ fst $ T.breakOnEnd "_" $ snd $ T.breakOnEnd "/" s
                in (nm, i, T.unpack s)
        |]
    nodePar "DNase_Peaks" [| \(nm, i, fl) -> do
        dir <- (<> "/DNase/") <$> lookupConfig "output_dir"
        blk <- lookupConfig "blacklist"
        liftIO $ withTempDir Nothing $ \tmp -> do
            tree <- bedToTree undefined . (\x -> zip x $ repeat ()) <$> (readBed blk :: IO [BED3])
            shelly $ mkdir_p dir
            let output = T.unpack $ T.pack dir <> nm <> "_" <> i <> ".bed.gz"
            runResourceT $ runConduit $ streamBedGzip fl .|
                filterC (not . isIntersected tree) .|
                mergePeaks tmp .| sinkFileBedGzip output
            beds <- runResourceT $ runConduit $ streamBedGzip fl .|
                filterC (not . isIntersected tree) .| sinkList :: IO [BED3]
            n <- runConduit $ mergeBed beds .| mapC size .| sumC :: IO Int
            return (nm, i, output, n)
        |] $ return ()
    path ["DNase_Peaks_Prep", "DNase_Peaks"]

    uNode "DNase_Depth_Prep" [| \() -> do
        beddir <- lookupConfig "dnase_beds"
        liftIO $ do
            samples <- fmap (filter (".bed.gz" `T.isSuffixOf`)) $ shelly $ lsT beddir
            return $ flip map samples $ \s ->
                let [nm, i, _] = T.splitOn "+" $ fst $ T.breakOn "_rep1" $ snd $ T.breakOnEnd "/" s
                in (nm, i, T.unpack s)
        |]
    nodePar "DNase_Depth" [| \(nm, i, fl) -> liftIO $ do
        d <- runResourceT $ runConduit $ sourceFile fl .| multiple ungzip .| linesUnboundedAsciiC .| lengthC
        return (nm, i, d :: Int)
        |] $ return ()
    path ["DNase_Depth_Prep", "DNase_Depth"]

    uNode "ATAC_Peaks_Prep" [| \() -> do
        peakdir <- lookupConfig "atac_peaks"
        liftIO $ do
            samples <- fmap (filter (".narrowPeak.gz" `T.isSuffixOf`)) $ shelly $ lsT peakdir
            return $ flip map samples $ \s ->
                let [nm, i, _] = T.splitOn "+" $ T.init $ fst $ T.breakOnEnd "_" $ snd $ T.breakOnEnd "/" s
                in (nm, i, T.unpack s)
        |]
    nodePar "ATAC_Peaks" [| \(nm, i, fl) -> do
        dir <- (<> "/ATAC/") <$> lookupConfig "output_dir"
        blk <- lookupConfig "blacklist"
        liftIO $ withTempDir Nothing $ \tmp -> do
            tree <- bedToTree undefined . (\x -> zip x $ repeat ()) <$> (readBed blk :: IO [BED3])
            shelly $ mkdir_p dir
            let output = T.unpack $ T.pack dir <> nm <> "_" <> i <> ".bed.gz"
            runResourceT $ runConduit $ streamBedGzip fl .|
                filterC (not . isIntersected tree) .|
                mergePeaks tmp .| sinkFileBedGzip output
            beds <- runResourceT $ runConduit $ streamBedGzip fl .|
                filterC (not . isIntersected tree) .| sinkList :: IO [BED3]
            n <- runConduit $ mergeBed beds .| mapC size .| sumC :: IO Int
            return (nm, i, output, n)
        |] $ return ()
    path ["ATAC_Peaks_Prep", "ATAC_Peaks"]

    uNode "ATAC_Depth_Prep" [| \() -> do
        beddir <- lookupConfig "atac_beds"
        liftIO $ do
            samples <- fmap (filter (".bed.gz" `T.isSuffixOf`)) $ shelly $ lsT beddir
            return $ flip map samples $ \s ->
                let [nm, i, _] = T.splitOn "+" $ fst $ T.breakOn "_rep1" $ snd $ T.breakOnEnd "/" s
                in (nm, i, T.unpack s)
        |]
    nodePar "ATAC_Depth" [| \(nm, i, fl) -> liftIO $ do
        d <- runResourceT $ runConduit $ sourceFile fl .| multiple ungzip .| linesUnboundedAsciiC .| lengthC
        return (nm, i, d :: Int)
        |] $ return ()
    path ["ATAC_Depth_Prep", "ATAC_Depth"]

    node "Output_Result" [| \(sc_p, sc_d, d_p, d_d, a_p, a_d) -> do
        dir <- lookupConfig "output_dir"
        let output = dir <> "/stat.tsv"
        liftIO $ do
            sc_p' <- forM sc_p $ \(nm, fl, cov) -> do
                n <- fmap length $ (runResourceT $ runConduit $ streamBedGzip fl .| sinkList :: IO [BED3])
                let (i, t) = fromJust $ lookup nm sampleMeta
                return $ ((t, i), (n, cov))
            d_p' <- fmap M.fromList $ forM d_p $ \(nm, i, fl, cov) -> do
                n <- fmap length $ (runResourceT $ runConduit $ streamBedGzip fl .| sinkList :: IO [BED3])
                return $ ((T.unwords $ T.splitOn "_" nm, i), (n, cov))
            a_p' <- fmap M.fromList $ forM a_p $ \(nm, i, fl, cov) -> do
                n <- fmap length $ (runResourceT $ runConduit $ streamBedGzip fl .| sinkList :: IO [BED3])
                return $ ((T.unwords $ T.splitOn "_" nm, i), (n, cov))
            let sc_d' = M.fromList $ flip map sc_d $ \(nm, n) -> let (i, t) = fromJust $ lookup nm sampleMeta in ((t, i), n)
                d_d' = M.fromList $ flip map d_d $ \(nm, i, d) -> ((T.unwords $ T.splitOn "_" nm, i), d)
                a_d' = M.fromList $ flip map a_d $ \(nm, i, d) -> ((T.unwords $ T.splitOn "_" nm, i), d)
                header = T.intercalate "\t"
                    [ "Sample"
                    , "Donor"
                    , "scATAC #reads"
                    , "scATAC #peaks"
                    , "scATAC coverage"
                    , "ATAC #reads"
                    , "ATAC #peaks"
                    , "ATAC coverage"
                    , "DNase #reads"
                    , "DNase #peaks"
                    , "DNase coverage" ]
            T.writeFile output $ T.unlines $ (header:) $ flip map sc_p' $ \((t, i), (n, cov)) ->
                let d = T.pack $ show $ M.lookupDefault undefined (t, i) sc_d'
                    (n', cov') = case M.lookup (t, i) d_p' of
                        Nothing -> ("NA", "NA")
                        Just (a, b) -> (T.pack $ show a, T.pack $ show b)
                    d' = case M.lookup (t, i) d_d' of
                        Nothing -> "NA"
                        Just a -> T.pack $ show a
                    (n'', cov'') = case M.lookup (t, i) a_p' of
                        Nothing -> ("NA", "NA")
                        Just (a, b) -> (T.pack $ show a, T.pack $ show b)
                    d'' = case M.lookup (t, i) a_d' of
                        Nothing -> "NA"
                        Just a -> T.pack $ show a
                in T.intercalate "\t"
                    [ t, i, d, T.pack $ show n, T.pack $ show cov
                    , d'', n'', cov''
                    , d', n', cov' ]
        |] $ return ()
    [ "SCATAC_Peaks", "SCATAC_Depth"
        , "DNase_Peaks", "DNase_Depth"
        , "ATAC_Peaks", "ATAC_Depth" ] ~> "Output_Result"

main :: IO ()
main = mainFun wf
