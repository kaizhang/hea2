{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell   #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE QuasiQuotes       #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE RankNTypes #-}

module Tissue (tissueBuilder) where

import HEA

import Statistics.Quantile
import qualified Taiji.Utils.DataFrame as DF
import qualified Data.Vector as V
import qualified Data.Text as T
import Bio.Data.Bed.Types
import Bio.Data.Bed
import Bio.HTS.BAM
import Control.Monad.ST (runST)
import Control.Arrow
import Data.Int
import qualified Data.Matrix as Mat
import qualified Data.ByteString.Char8 as B
import qualified Data.HashMap.Strict as M
import Data.List.Ordered (nubSort)
import Bio.Utils.Functions
import Taiji.Utils.Plot
import Data.Binary
import System.Random.MWC.Distributions (uniformShuffle)
import System.Random.MWC (create)
import Data.Conduit.Internal (zipSinks)
import Shelly hiding (FilePath, path)

import Taiji.Prelude
import qualified Language.R                        as R
import           Language.R.QQ
import Language.R.HExp

splitClusterByTissue :: [CellCluster] -> [(String, [(B.ByteString, B.ByteString)])]
splitClusterByTissue cls = map (\x -> (B.unpack $ extractTissueName $ fst $ head x, x)) $
    groupBy ((==) `on` (extractTissueName . fst)) $
    sortBy (comparing (extractTissueName . fst)) bcMap
  where
    bcMap = flip concatMap cls $ \cl -> zip (map _cell_barcode $ _cluster_member cl) $
        repeat $ _cluster_name cl

getUMAP :: [B.ByteString] -> M.HashMap B.ByteString B.ByteString -> IO [(Double, Double)]
getUMAP bcs bcMap = withTemp Nothing $ \tmp1 -> withTemp Nothing $ \tmp2 -> do 
    B.writeFile tmp1 $ B.unlines $ map (\x -> M.lookupDefault (error $ show x) x bcMap) bcs
    shelly $ run_ "taiji-utils" ["viz", T.pack tmp1, T.pack tmp2]
    map ((\[x, y] -> (readDouble x, readDouble y)) . B.split '\t') . B.lines <$>
        B.readFile tmp2

extractTissueName :: B.ByteString -> B.ByteString
extractTissueName = f . B.init . fst . B.breakEnd (=='_') . B.init . fst .
    B.breakEnd (=='_')
  where
    f x | x == "LungMap" = "lung"
        | "CARE" `B.isInfixOf` x = B.intercalate "_" $ take 2 $ B.split '_' x
        | otherwise = x

plotCells :: [((B.ByteString, B.ByteString), (Double, Double))] -> R.R s (R.SomeSEXP s)
plotCells input = do
    myTheme
    [r| library("ggplot2")
        library("ggrastr")
        library("gridExtra")
        df <- data.frame(cls=cls_hs, x=xs_hs, y=ys_hs)
        ggplot(df, aes(x,y)) +
            geom_point_rast(size=0.1, alpha=1, stroke=0, shape=20, aes(colour = factor(cls))) +
            geom_text(data=data.frame(cx=cx_hs, cy=cy_hs),
                aes(cx, cy, label = label_hs), size=1.7
            ) +
            myTheme() + myFill() +
            labs(x = "UMAP-1", y = "UMAP-2") +
            theme(
                plot.margin=unit(c(2,0,2,2),"mm"),
                panel.grid.major = element_blank(),
                axis.ticks = element_blank(),
                axis.text = element_blank(),
                axis.line = element_blank(),
                panel.border = element_blank(),
                legend.position = "none"
            ) + # + scale_color_manual(values=colors_hs) +
            annotate("text", x = min(xs_hs) + 0.1 * (max(xs_hs) - min(xs_hs)),
                y = max(ys_hs), size = 2.1,
                label = paste(as.character(nrow(df)), " nuclei")
            )
    |]
  where
    (reps, cls, xs, ys) = unzip4 $ flip map input $ \((bc, cl) , (x, y)) ->
        (B.unpack $ fst $ B.break (=='+') bc, B.unpack cl, x, y)
    (label, cx, cy) = unzip3 $
        flip map (groupBy ((==) `on` (snd .fst)) $ sortBy (comparing (snd . fst)) input) $ \xs ->
            let cl = B.unpack $ snd $ fst $ head xs
                (x, y) = getCentroid $ map snd xs
            in (cl, x, y)
    getCentroid ps = let (x,y) = unzip ps
                     in ( median medianUnbiased $ V.fromList x
                        , median medianUnbiased $ V.fromList y )
    colors :: [String]
    colors = ["#ddca42","#8945d9","#70df47","#502a95","#b5d853","#d34bbf","#5fc362","#7571d6","#cc923c","#402966","#70e6b5","#d0405f","#5ba178","#d94e2a","#84d2d8","#953872","#668331","#d684c0","#37492b","#80a5dd","#8d4d23","#696999","#d0d6a0","#351b2c","#cdacbf","#6e3232","#53878e","#d68473","#3e475c","#988765"]





--------------------------------------------------------------------------------
-- Workflow
--------------------------------------------------------------------------------
tissueBuilder = do
    node "Tissue_UMAP" [| \() -> do
        dir <- (<> "/Tissue/") <$> lookupConfig "output_dir"
        spFl <- lookupConfig "spectral"
        spRowFl <- lookupConfig "spectral_rows"
        clFl <- lookupConfig "cluster_bin"
        let output = dir <> "tissues.bin"
        liftIO $ do
            shelly $ mkdir_p dir
            rows <- map (head . B.split '\t') . B.lines <$> B.readFile spRowFl
            embed <- runResourceT $ runConduit $ sourceFile spFl .| multiple ungzip .|
                linesUnboundedAsciiC .| sinkList
            let bcMap = M.fromList $ zip rows embed
            tissues <- splitClusterByTissue <$> decodeFile clFl
            res <- forM tissues $ \(t, bcs) -> do
                xy <- getUMAP (map fst bcs) bcMap
                return (t, zip bcs xy)
            encodeFile output res
            return output
        |] $ return ()
    
    node "Plot_Tissues" [| \input -> do
        dir <- lookupConfig "output_dir"
        let output = dir <> "/tissue.pdf"
        liftIO $ do
            input' <- decodeFile input :: IO [(String, [((B.ByteString, B.ByteString), (Double, Double))])]
            R.runRegion $ do
                fig <- plotCells $ snd $ head input'
                [r| ggsave(output_hs, fig_hs, width=180, height=220, unit="mm", useDingbats=F) |]
                return ()
        |] $ return ()
    path ["Tissue_UMAP", "Plot_Tissues"]