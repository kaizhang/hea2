{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE TemplateHaskell   #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE QuasiQuotes       #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE RankNTypes #-}
module Annotation (annoBuilder) where

import HEA
import qualified Data.Vector as V
import qualified Data.Vector.Unboxed as U
import qualified Data.ByteString.Char8 as B
import qualified Data.Text as T
import qualified Data.Text.IO as T
import Language.Javascript.JMacro
import Bio.Data.Bed
import Bio.Utils.Functions (scale, filterFDR, binarySearch)
import Control.Arrow (second)
import Data.Tuple
import Data.Binary (decodeFile)
import Data.Conduit.Internal (zipSources)
import Statistics.Sample
import Data.List.Ordered (nubSort)
import qualified Data.Matrix as Mat
import qualified Data.HashMap.Strict as M
import qualified Data.HashSet as S

import Taiji.Prelude
import qualified Language.R                        as R
import           Language.R.QQ
import Language.R.HExp
import qualified Taiji.Utils.DataFrame as DF
import Taiji.Utils.Plot
import qualified Taiji.Utils.Plot.ECharts as E

markerGenes :: [T.Text]
markerGenes = ["ACAN", "ACTA2", "ACTG2", "ADIPOQ", "AGER", "ALPI", "ANXA9", "APOD", "AQP5", "ATP4B", "BEST1", "BEST2", "BEST4", "BLK", "BRDT", "BTNL3", "C1QA", "C1QB", "C1QC", "CAMK2A", "CAPS", "CARMN", "CASP14", "CBLIF", "CCRL2", "CD22", "CD244", "CD27", "CD300E", "CD34", "CD38", "CD3E", "CD4", "CD72", "CD79A", "CD86", "CD8B", "CD9", "CDH11", "CDH5", "CEBPA", "CHAT", "CHGA", "CIDEA", "CLDN10", "CLDN4", "CLDN5", "CLDN9", "CLEC11A", "CLPS", "COLEC11", "CPA1", "CRACD", "CRP", "CSF2", "CFTR", "CTLA4", "CTRC", "CYP11B1", "CYP11B2", "CYP17A1", "DCD", "DCN", "DEFA11P", "DEFA5", "DEFA6", "DMP1", "DPT", "DSG1", "DSG3", "EGFL7", "EPO", "F2", "FABP2", "FCRL5", "FEV", "FGG", "FLT4", "FOXC1", "FOXE1", "FOXF1", "FOXF2", "FOXG1", "GAD1", "GAL", "GAPT", "GATA1", "GCG", "GDF7", "GHRH", "GHRL", "GJA4", "GKN2", "GP2", "GP9", "GPA33", "GRIK1", "GRM5", "GRP", "GYG2", "GZMB", "GZMK", "HAS1", "HDC", "HEPACAM2", "HEY2", "HIGD1B", "HPR", "HPX", "HSD11B1", "HSD17B2", "HSD3B2", "HTR1A", "IAPP", "IGHA1", "IGHA2", "IGHJ6", "IL2", "IL25", "INS", "INSC", "IRX3", "IRX5", "ISX", "KRT13", "KRT17", "KRT20", "KRT23", "KRT32", "KRT39", "KRT40", "KRT5", "KRT6A", "KRT77", "L1CAM", "LAMA1", "LBX1", "LGALS7", "LHX5", "LIPF", "LIVAR", "LMX1A", "LRRC10", "LRRC38", "LUM", "LYZ", "MAL", "MC2R", "MME", "MNDA", "MPP7", "MRC2", "MRGPRX2", "MS4A2", "MSLN", "MSR1", "MUC12", "MUC15", "MUC2", "MUC4", "MUC5AC", "MUC5B", "MUC6", "MUSK", "MYB", "MYF5", "MYH1", "MYH11", "MYH2", "MYH6", "MYL1", "MYL2", "MYL3", "MYL7", "MYOG", "MYOT", "NAPSA", "NEFH", "NEUROD6", "NF1P7", "NKX2-2", "NKX2-3", "NKX2-5", "NOS3", "NPPA", "NPPB", "NPR3", "OGN", "OMD", "PAX6", "PAX7", "PAX8", "PDGFRA", "PDK4", "PDPN", "PDX1", "PGA3", "PIP", "PKP3", "PLG", "PLIN4", "PLTP", "PODN", "POSTN", "PROX1", "PRSS1", "REG1A", "RELN", "RYR1", "S100B", "SCGB1B2P", "SCGB3A1", "SCGB3A2", "SCIN", "SFTPB", "SFTPC", "SFTPD", "SOST", "SOX10", "SOX17", "SOX2", "SPRY2", "SST", "STAR", "STC2", "SULF1", "TAGLN", "TBX20", "TEKT4", "TG", "THEMIS", "TLR8", "TNNC1", "TNNI2", "TNNT2", "TNXA", "TPO", "TPSD1", "TRPM5", "VIL1", "VIM", "VIP", "VWF", "WNT7A", "WT1"]

computeEnrichment :: M.HashMap T.Text (T.Text, [T.Text])
                  -> DF.DataFrame Double   -- ^ raw accessibility
                  -> DF.DataFrame Double
computeEnrichment markers' df = DF.mkDataFrame (DF.colNames geneExpr)
    (map fst filtered) $ flip map (DF.colNames geneExpr) $ \c ->
        map snd $ enrichment filtered geneExpr c
  where
    geneExpr = toUpperCase $ DF.map (logBase 2 . (+pseudocount)) df
    genes = S.fromList $ DF.rowNames geneExpr
    filtered = filter ((>=at_least) . length . snd) $
        flip map markers $ \(x,y) -> (x, filter (`S.member` genes) y)
    unknown = filter (not . (`S.member` genes)) $ nub $ concatMap snd markers
    markers = map (\(a,b) -> (a, snd b)) $ M.toList markers'
    enrichment marker d c = map (second f) marker
      where
        f = mean . U.fromList . map (\i -> d DF.! (i, c))
    toUpperCase d = DF.fromMatrix (map T.toUpper $ DF.rowNames d)
        (DF.colNames d) $ DF._dataframe_data d
    pseudocount = 0.5
    at_least = 1

scale' xs | U.all (==0) xs = xs
          | otherwise = scale xs

plot :: FilePath -> DF.DataFrame Double -> IO ()
plot output df = savePlots output [] [E.toolbox E.>+> E.heatmap (DF.orderDataFrame id df)]

annotate :: DF.DataFrame Double -> [(T.Text, [T.Text])]
annotate df = numbering $ flip map (zip (DF.rowNames df) (Mat.toRows $ DF._dataframe_data df)) $
    second $ fst . unzip . f . sortBy (flip (comparing snd)) .
    zip (DF.colNames df) . V.toList
  where
    f (a:b:c:_)
        | snd a > 1.5 * snd b = [a]
        | snd a > 1.5 * snd c = [a,b]
        | otherwise = [a,b,c]
    f xs = error $ show xs
    numbering xs = concatMap g $ groupBy ((==) `on` (head . fst)) $ sortBy (comparing (head . fst)) $ map swap xs
      where
        g [x] = [swap x]
        g x = zipWith (\((a:as), b) i ->
            let a' = a <> "." <> T.pack (show i)
            in (b, a':as) ) x [1..]

diagonize :: DF.DataFrame Double -> DF.DataFrame Double
diagonize = DF.reorderRows f
  where
    f xs = map (\x -> (x, M.lookupDefault undefined x xs')) names
      where
        xs' = M.fromList xs
        names = map fst $ sortBy (comparing snd) $ map (\(a,b) -> (a, g b)) xs
        g xs = (i, negate v)
          where
            v = z V.! i
            i = V.maxIndex z
            z = scale xs


plotAnnotation :: DF.DataFrame Double -> R.R s (R.SomeSEXP s)
plotAnnotation df = do
    valMatrix <- toRMatrix df
    [r| library("ggplot2")
        library("ComplexHeatmap")
        library("RColorBrewer")
        library("viridis")
        library("ggplotify")
        library("circlize")
        rwb <- colorRampPalette(colors = rev(brewer.pal(7,"RdBu")))
        #col_fun <- colorRamp2(
        #    c(1, 5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 80),
        #    viridis(12) )

        breaks <- c(10, 100, 1000, 10000)
        h1 <- Heatmap( valMatrix_hs, 
            col=rwb(99),
            cluster_rows=F, cluster_columns=F,
            show_row_names=T, row_names_side="left",
            show_row_dend = F,
            show_column_names=T,
            show_column_dend = F,
            column_names_gp = gpar(fontsize=5),
            row_names_gp = gpar(fontsize=5),
            rect_gp = gpar(col= "white"),
            heatmap_legend_param = list(
                title = "zscore of enrichment",
                title_gp = gpar(fontsize = 6),
                labels_gp = gpar(fontsize = 5),
                direction = "horizontal"
            )
        )
        as.ggplot(h1)
    |]

readMarkers :: FilePath -> IO (M.HashMap T.Text (T.Text, [T.Text]))
readMarkers fl = M.fromListWith undefined . mapMaybe (f . T.splitOn "\t") .
    T.lines <$> T.readFile fl
  where
    f (a:b:c:_) = Just (T.strip b, (b, T.splitOn "," c))
    f (a:b:_) = Just (T.strip a, (a, T.splitOn "," b))
    f _ = Nothing

punchCard :: FilePath -> DF.DataFrame Double -> IO ()
punchCard output df = R.runRegion $ do
    [r| library("ggplot2")
        xs <- factor(xs_hs, levels = rows_hs)
        ys <- factor(ys_hs, levels = cols_hs)
        df <- data.frame(x=xs, y=ys, z=vals_hs)
        fig <- ggplot(df, aes(x=x, y=y)) + 
            geom_point(aes(size=z, fill=y), alpha = 0.75, shape = 21) + 
            #scale_size_continuous(range = c(0, 4)) +
            scale_size_area(max_size=2) +
            labs( x= "", y = "", size = "Relative Abundance (%)", fill = "")  + 
            theme(legend.key=element_blank(), 
                axis.text.x = element_text(colour = "black", size = 5, angle = 90, vjust = 0.3, hjust = 1), 
                axis.text.y = element_text(colour = "black", size = 5), 
                legend.text = element_text(size = 5, colour ="black"), 
                legend.title = element_text(size = 5), 
                panel.background = element_blank(), panel.border = element_rect(colour = "black", fill = NA, size = 0.5), 
                legend.position = "right") +  
            scale_fill_manual(values = colours_hs, guide = FALSE) + 
            scale_y_discrete(limits = rev(levels(df$y))) 
        ggsave(output_hs, fig, width=400, height=300, unit="mm", useDingbats=F)
    |]
    return ()
  where
    colours = take (snd $ DF.dim df) $ cycle cat20
    rows = map T.unpack $ DF.rowNames df
    cols = map T.unpack $ DF.colNames df
    (xs, ys, vals) = unzip3 $ flip map (sequence [rows, cols]) $ \[i, j] ->
        (i, j, df DF.! (T.pack i, T.pack j))

annoBuilder :: Builder ()
annoBuilder = do
    node "Annotate" [| \() -> do
        geneMat <- lookupConfig "geneMatrix"
        markerFl <- lookupConfig "markers"
        dir <- lookupConfig "output_dir"
        let output = dir <> "/annotation.pdf"
            output2 = dir <> "/annotation.tsv"
        liftIO $ do
            gm <- DF.readTable geneMat
            markers <- readMarkers markerFl
            let enrichment = DF.mapRows (V.convert . scale' . V.convert) $
                    DF.mapCols (V.convert . scale' . V.convert) $
                    computeEnrichment markers gm

            anno <- forM (annotate enrichment) $ \(x, ys) -> do
                let c = fst $ M.lookupDefault (error $ show query) query markers
                    query = fst $ T.breakOn "." $ head ys
                return $ T.intercalate "\t" (x:c:ys)
            T.writeFile output2 $ T.unlines anno

            R.runRegion $ do
                fig <- plotAnnotation $ DF.transpose $ diagonize $ DF.orderDataFrame id enrichment
                [r| ggsave(output_hs, fig_hs, width=180, height=100, unit="mm", useDingbats=F)
                |]
                return ()
        |] $ return ()

    node "Plot_Marker_Gene" [| \() -> do
        geneMat <- lookupConfig "geneMatrix"
        annoFl <- lookupConfig "annotation"
        dir <- lookupConfig "output_dir"
        let output = dir <> "/markers.pdf"
        liftIO $ do
            anno <- readAnno annoFl
            gene <- changeName anno . DF.map (logBase 2 . (+1)) . (`DF.rsub` markerGenes) <$>
                DF.readTable geneMat
            punchCard output $ diagonize $ DF.orderDataFrame id $
                DF.mapRows scale gene
            DF.writeTable (dir <> "/markers.tsv") (T.pack . show) $ diagonize $ DF.orderDataFrame id $
                DF.mapRows scale gene
        |] $ return ()