{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell   #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE QuasiQuotes       #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE RankNTypes #-}

module QC (qcBuilder) where

import HEA

import Statistics.Quantile
import qualified Taiji.Utils.DataFrame as DF
import qualified Data.Vector as V
import qualified Data.Text as T
import Bio.Data.Bed.Types
import Bio.Data.Bed
import Bio.HTS.BAM
import Control.Monad.ST (runST)
import Control.Arrow
import Data.Int
import qualified Data.Matrix as Mat
import qualified Data.ByteString.Char8 as B
import qualified Data.HashMap.Strict as M
import Data.List.Ordered (nubSort)
import Bio.Utils.Functions
import Taiji.Utils.Plot
import Data.Binary
import System.Random.MWC.Distributions (uniformShuffle)
import System.Random.MWC (create)
import Data.Conduit.Internal (zipSinks)
import Shelly hiding (FilePath, path)

import Taiji.Prelude
import Taiji.Pipeline.SC.ATACSeq.Functions.QC (readStats)
import Taiji.Pipeline.SC.ATACSeq.Types (Stat(..))
import qualified Language.R                        as R
import           Language.R.QQ
import Language.R.HExp

plotSampleCells :: [(String, [Stat])] -> R.R s (R.SomeSEXP s)
plotSampleCells input = do
    myTheme
    [r| library("ggplot2")
        df <- data.frame(name=names_hs, val=counts_hs, replicate=reps_hs)
        ggplot(df, aes(x=reorder(name, total_hs), y=val)) +
            geom_col(aes(fill=replicate)) +
            coord_flip() +
            myTheme() + myFill() +
            labs(x = "", y = "number of cells")
    |]
  where
    total = concatMap (\x -> map (\_ -> sum $ map snd x) x) $
        groupBy ((==) `on` fst) $ sort $ zip names counts
    (names, counts, reps) = unzip3 $ concatMap (zipWith f [1..]) $
        groupBy ((==) `on` fst) $ sortBy (comparing fst) $
        map (first getName) input
    f i (name, stat) = (name, nCell, show i)
      where
        nCell = fromIntegral $ length $ filter (passedQC 7) stat :: Int32
    getName = T.unpack . T.init . fst . T.breakOnEnd "_" . T.pack 

numCells :: [(String, [Stat])] -> [(String, Int)]
numCells input = concatMap f $ groupBy ((==) `on` fst) $ sortBy (comparing fst) $
    map (first getName) input
  where
    f = M.toList . M.fromListWith (+) . map (second $ length . filter (passedQC 7))
    getName = T.unpack . T.init . fst . T.breakOnEnd "_" . T.pack 

mappability :: FilePath -> IO Double
mappability fl = do
    (n, m) <- runResourceT $ runConduit $
        streamBam fl .| filterC (\x -> isFirstSegment $ flag x) .| sink
    return $ fromIntegral m / fromIntegral n
  where
    sink = zipSinks lengthC $ filterC (\x -> isProperAligned $ flag x) .| lengthC

plotMappability :: [(String, Double)] -> R.R s (R.SomeSEXP s)
plotMappability dat = do
    myTheme
    [r| library("ggplot2")
        df <- data.frame(name=names_hs, val=vals_hs*100)
        ggplot(df, aes(name, val)) + geom_col() +
            coord_flip() +
            myTheme() + myFill() +
            labs(x = "", y = "properly aligned fragment (%)")
    |]
  where
    (names, vals) = unzip dat

--------------------------------------------------------------------------------
-- QC
--------------------------------------------------------------------------------

-- | Whether the given cell passes the QC.
passedQC :: Double -> Stat -> Bool
passedQC cutoff x = _te x >= cutoff && _uniq_reads x >= 1000 && (fromJust $ _doublet_score x) <= 0.5

plotQC :: [(String, [Stat])] -> R.R s (R.SomeSEXP s)
plotQC xs = do
    myTheme
    [r| library("ggplot2")
        nCell <- data.frame(name=names_hs, tissue=tissue_hs,
            val=nCell_hs, cat=rep("number of nuclei passed QC", length(ids_hs))
        )
        doubletRate <- data.frame(name=names_hs, tissue=tissue_hs,
            val=doubletRate_hs, cat=rep("doublet percentage (%)", length(ids_hs))
        )
        df <- rbind(nCell, doubletRate)
        ggplot(df, aes(name, val)) + geom_col() +
            myTheme() + myFill() +
            theme( axis.text.x = element_text(size=5, angle = 90, vjust=0.5, hjust=1),
                strip.text.x=element_text(angle=90),
                plot.margin=unit(c(5,2,5,0),"mm")
            ) +
            labs(x = "", y = "") +
            facet_grid(vars(cat), scales="free", space = "free_x", switch="y")
    |]
  where
    (names, stat) = unzip xs
    (tissue, ids) = unzip $ map breakName names
    nCell = map (fromIntegral . length . filter (passedQC 7)) stat :: [Int32]
    doubletRate = map f stat
    f stat = let n = length $ filter (\x -> _te x >= 7 && _uniq_reads x >= 1000) stat
                 m = length $ filter (passedQC 7) stat
             in (1 - fromIntegral m / fromIntegral n) * 100 :: Double

clusterQC :: FilePath -> FilePath -> IO ()
clusterQC output meta = do
    (cl, reads) <- unzip . map (f . B.split '\t') . tail . B.lines <$> B.readFile meta
    R.runRegion $ do
        myTheme
        [r| library("ggplot2")
            df <- data.frame(cl=cl_hs, reads=reads_hs)
            fig <- ggplot(df, aes(cl, reads)) + geom_violin() +
                #geom_boxplot(lwd=0.3, outlier.size=0.2, outlier.stroke=0) +
                myTheme() + myFill() + myColour() +
                theme( axis.text.x = element_text(size=5, angle = 90, vjust=0.5, hjust=1)) +
                labs(x = "", y = "")
            ggsave(output_hs, fig, width=255, height=45, unit="mm", useDingbats=F)
        |]
        return ()
  where
    f xs = (B.unpack $ xs!!1, logBase 10 $ readDouble $ xs!!5)

plotQC2 :: [(String, [Stat])] -> R.R s (R.SomeSEXP s)
plotQC2 xs = do
    myTheme
    [r| library("ggplot2")
        te <- data.frame(name=names_hs, val=te_hs, cat=rep("TSS enrichment", length(names_hs))
        )
        frag <- data.frame(name=names_hs,
            val=frag_hs, cat=rep("log10(number of fragments)", length(names_hs))
        )
        df <- rbind(te, frag)
        ggplot(df, aes(name, val)) + geom_violin() +
            myTheme() + myFill() +
            theme( axis.text.x = element_text(size=5, angle = 90, vjust=0.5, hjust=1),
                strip.text.x=element_text(angle=90),
                plot.margin=unit(c(2,2,2,0),"mm"),
            ) +
            labs(x = "", y = "") +
            facet_grid(vars(cat), scales="free", space = "free_x", switch="y")
    |]
  where
    (names, te, frag) = unzip3 $ concatMap f xs
    f (name, stat) = map (\x -> (name, _te x, logBase 10 $ fromIntegral $ _uniq_reads x :: Double)) $
        filter (passedQC 7) $ stat

breakName x = let (a, b) = T.breakOnEnd "_" $ T.pack x
              in (T.unpack $ T.init a, T.unpack $ T.drop 3 b)

mkStatTable :: [(String, [Stat])] -> DF.DataFrame Double
mkStatTable stats = DF.mkDataFrame rownames colnames $ transpose
    [nCell, dupRange, teRange, fragRange, doubletRate]
  where
    colnames = ["number of nuclei", "median(percent duplicated)", "median(TSSe)"
        , "median(#fragment)", "percent doublets"]
    rownames = map (T.pack . fst) stats'
    stats' = map (second (filter (passedQC 7))) stats
    fragRange = flip map stats' $
        \(_, stat) -> median medianUnbiased $ V.fromList $
        map (fromIntegral . _uniq_reads) stat
    teRange = flip map stats' $
        \(_, stat) -> median medianUnbiased $ V.fromList $
        map _te stat
    dupRange = flip map stats' $
        \(_, stat) -> (100*) $ median medianUnbiased $ V.fromList $
        map (fromJust . _dup_rate) stat
    nCell = map (fromIntegral . length . snd) stats'
    doubletRate = map (f . snd) stats
    f stat = let n = length $ filter (\x -> _te x >= 7 && _uniq_reads x >= 1000) stat
                 m = length $ filter (passedQC 7) stat
             in (1 - fromIntegral m / fromIntegral n) * 100 :: Double

--------------------------------------------------------------------------------
-- Workflow
--------------------------------------------------------------------------------
qcBuilder = do
    node "Get_QC" [| \() -> do
        dir <- lookupConfig "qc_dir"
        lookupConfig "output_dir" >>= liftIO . shelly . mkdir_p . fromText . T.pack
        liftIO $ do
            fls <- fmap (filter ("_qc.tsv" `T.isSuffixOf`)) $ shelly $ lsT $ fromText $ T.pack dir
            let getName = T.unpack . fst . T.breakOn "_rep" . snd . T.breakOnEnd "/"
            return $ map (\x -> (getName x, T.unpack x)) fls
        |] $ return ()

    node "Make_QC_Table" [| \qc -> do
        dir <- lookupConfig "output_dir"
        let output = dir <> "/QC.tsv"
        liftIO $ do
            stat <- forM qc $ \(x, fl) -> do
                s <- readStats fl
                return (x, s)
            let f x = T.pack $ printf "%.2f" x
            DF.writeTable output id $ DF.map f $ mkStatTable stat
        |] $ return ()
    ["Get_QC"] ~> "Make_QC_Table"

    node "Plot_Cell_Number" [| \qc -> do
        dir <- lookupConfig "output_dir"
        let output = dir <> "/number_of_nuclei.pdf"
        liftIO $ do
            stat <- forM qc $ \(x, fl) -> do
                s <- readStats fl
                return (x, s)
            mapM_ print $ numCells stat
            R.runRegion $ do
                figure <- plotSampleCells stat
                [r| ggsave(output_hs, figure_hs, width=87, height=67, unit="mm", useDingbats=F)
                |]
                return ()
        |] $ return ()
    ["Get_QC"] ~> "Plot_Cell_Number"

    node "Plot_QC" [| \qc -> do
        dir <- lookupConfig "output_dir"
        let output = dir <> "/Sample_QC.pdf"
        liftIO $ do
            stat <- forM qc $ \(x, fl) -> do
                s <- readStats fl
                return (x, s)

            R.runRegion $ do
                [r| library("cowplot") |]
                fig_a <- plotQC stat
                fig_b <- plotQC2 stat
                [r| figure <- plot_grid(fig_a_hs, fig_b_hs, nrow=2,
                        labels = c("a","b"), label_size = 8 )
                    ggsave(output_hs, figure, width=183, height=190, unit="mm", useDingbats=F)
                |]
                return ()
        |] $ return ()
    ["Get_QC"] ~> "Plot_QC"

    node "Cluster_QC" [| \() -> do
        dir <- lookupConfig "output_dir"
        meta <- lookupConfig "cluster_meta"
        let output = dir <> "/cluster_QC.pdf"
        liftIO $ clusterQC output meta
        |] $ return ()