{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell   #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE QuasiQuotes       #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE RankNTypes #-}
import HEA

import Statistics.Quantile
import Data.Conduit.Zlib
import Control.Arrow
import qualified Taiji.Utils.DataFrame as DF
import qualified Data.Vector as V
import qualified Data.Vector.Mutable as VM
import qualified Data.Text as T
import qualified Data.Text.IO as T
import Bio.Data.Bed.Types
import Bio.Data.Bed
import Control.Monad.ST (runST)
import qualified Data.Matrix as Mat
import qualified Data.Map.Strict as Map
import qualified Data.ByteString.Char8 as B
import qualified Data.HashMap.Strict as M
import Data.List.Ordered (nubSort)
import Bio.Utils.Functions
import Taiji.Utils
import Shelly hiding (FilePath, path)
import Data.Binary
import System.Random.MWC.Distributions (uniformShuffle)
import System.Random.MWC (create)
import Statistics.Sample (mean)
import Data.Int

import Taiji.Prelude
import qualified Language.R                        as R
import           Language.R.QQ
import qualified Language.R.HExp as H
import Language.R.HExp
import qualified Data.Vector.SEXP as S

import QC
import Tissue
import Annotation

majorClusters :: [(B.ByteString, B.ByteString)]
majorClusters = 
    [ ("C1", "Mesenchymal / Stromal")
    , ("C2", "Myeloid")
    , ("C3", "Cardiomyocyte")
    , ("C4", "Endothelial")
    , ("C5", "Smooth Muscle / Stromal")
    , ("C6", "Skeletal Myocyte")
    , ("C7", "Vascular Smooth Muscle")
    , ("C8", "Ionic Mesenchymal / Stromal")
    , ("C9", "Gastrointestinal Epithelial")
    , ("C10", "Thyroid Follicular")
    , ("C11", "Pancreatic Acinar")
    , ("C12", "T Lymphocyte")
    , ("C13", "Mural / Pericyte")
    , ("C14", "Epithelial / Squamous Epithelial")
    , ("C15", "Neuroendocrine")
    , ("C16", "Pulmonary Epithelial")
    , ("C17", "Lumenal Epithelial")
    , ("C18", "Gastric Crypt")
    , ("C19", "Peripheral Neural Crest Derived")
    , ("C20", "Neuron")
    , ("C21", "Keratinocyte")
    , ("C22", "Adrenal Cortical")
    , ("C23", "Myoepithelial")
    , ("C24", "Luteal")
    , ("C25", "B Lymphocyte")
    , ("C26", "Hepatocyte")
    , ("C27", "General Nervous Glial")
    , ("C28", "Pancreatic Ductal")
    , ("C29", "Mast")
    , ("C30", "Oligodendrocyte") ]


fetalMajorClusters :: [(B.ByteString, B.ByteString)]
fetalMajorClusters = 
    [ ("C1", "Adrenal Cortical")
    , ("C2", "Erythroblast I")
    , ("C3", "Metanephric / Ureteric")
    , ("C4", "Hepatoblast")
    , ("C5", "Fibroblast I")
    , ("C6", "Cardiomyocyte")
    , ("C7", "Mesenchymal")
    , ("C8", "Pulmonary Epithelial I")
    , ("C9", "Excitatory Neuron / Glia")
    , ("C10", "Erythroblast II")
    , ("C11", "Excitatory Neuron I")
    , ("C12", "Endothelial")
    , ("C13", "Erythroblast III")
    , ("C14", "Thymocytes")
    , ("C15", "Myeloid / Macrophage")
    , ("C16", "Gastrointestinal Epithelial")
    , ("C17", "Peripheral Nervous")
    , ("C18", "Lymphoid I")
    , ("C19", "Skeletal Myocyte")
    , ("C20", "Inhibitory Neuron I")
    , ("C21", "Excitatory Neuron II")
    , ("C22", "CNS Glia I")
    , ("C23", "Placental ")
    , ("C24", "Pancreatic / Gastric / GI / Neuroendocrine")
    , ("C25", "Placental Fibroblast")
    , ("C26", "Erythroblast IV")
    , ("C27", "Erythroblast V")
    , ("C28", "Pancreatic Acinar")
    , ("C29", "Fibroblast II")
    , ("C30", "Lymphoid II")
    , ("C31", "Excitatory Neuron III")
    , ("C32", "Inhibitory Neuron II")
    , ("C33", "Fibroblast III")
    , ("C34", "Excitatory Neuron IV")
    , ("C35", "CNS Glia II")
    , ("C36", "Pulmonary Epithelial II") ]

getClusters :: FilePath   -- ^ Cluster
            -> [T.Text] -- ^ Matrix
            -> IO [[T.Text]]
getClusters clFl names = do
    cls <- map (map readInt . B.split ',') . B.lines <$> B.readFile clFl
    return $ (map . map) (\i -> idx V.! i) cls
  where
    idx = V.fromList names

computeClusterEntropy :: [[T.Text]] -> [Double]
computeClusterEntropy cls = map f cls
  where
    f cl = mean $ V.fromList $ map g $
        filter (any (>0.01)) $ (map . map) snd $
        groupBy ((==) `on` getName) $ sortBy (comparing getName) $
        zip names $ normalize vals
      where
        s = M.fromListWith (+) $ zip cl $ repeat 1
        (names, vals) = unzip $ flip map (M.keys num) $ \x ->
            (x, M.lookupDefault 0 x s / M.lookupDefault undefined x num)
        normalize xs = map (/(foldl1' (+) xs)) xs
    num = M.fromListWith (+) $ zip (concat cls) $ repeat 1
    getName = fst . T.breakOnEnd "_" . fst
    g [x] = 1
    g xs = computeEntropy (V.fromList xs) / logBase 2 (fromIntegral $ length xs)

violin :: [(String, [Double])] -> R.R s (R.SomeSEXP s)
violin input = do
    myTheme
    [r| library("ggplot2")
        df <- data.frame(x=names_hs, y=dat_hs)
        #df$x <- factor(df$x, levels = df$x)
        ggplot(df, aes(factor(x), y)) + geom_violin() +
            geom_jitter() + #height = 0, width = 0.1) +
            myTheme() + myFill() +
            labs(x = "", y = "diversity")
    |]
  where
    (names, dat) = unzip $ flip concatMap input $ \(nm, val) -> zip (repeat nm) val

linePlot :: (String, [[Double]])
         -> (String, [[Double]])
         -> R.R s (R.SomeSEXP s)
linePlot (name1, input1) (name2, input2) = do
    myTheme
    [r| library("ggplot2")
        df <- data.frame(x=xs_hs, y=ys_hs, names=names_hs)
        ggplot(df, aes(x, y, colour = names)) +
            geom_line() +
            myTheme() + myFill() + myColour() +
            labs(x = "number of clusters", y = "median(cluster diversity)")
    |]
  where
    (xs, ys, names) = unzip3 $ map (f name1) input1 ++ map (f name2) input2
    f :: String -> [Double] -> (Double, Double, String)
    f lab vals = (fromIntegral $ length vals, median medianUnbiased $ V.fromList vals, lab)

--------------------------------------------------------------------------------
-- Plot Cluster
--------------------------------------------------------------------------------

-- | Random sample 30,000 cells.
sampleCells :: [CellCluster] -> IO [CellCluster]
sampleCells clusters
    | ratio >= 1 = return clusters
    | otherwise = do
        gen <- create
        forM clusters $ \c -> do
            s <- sampling gen ratio $ V.fromList $ _cluster_member c
            return $ c {_cluster_member = V.toList s}
  where
    n = foldl1' (+) $ map (length . _cluster_member) clusters
    ratio = 1 / (fromIntegral n / 10000) :: Double
    sampling gen frac v = V.take n' <$> uniformShuffle v gen
      where
        n' = max 100 $ truncate $ frac * fromIntegral (V.length v)

plotCells :: [(String, [(Double, Double)])]  -- ^ major clusters
          -> R.R s (R.SomeSEXP s)
plotCells clusters = do
    myTheme
    [r| library("ggplot2")
        library("ggrastr")
        library("gridExtra")
        df <- data.frame(cls=cls_hs, x=xs_hs, y=ys_hs)
        ggplot(df, aes(x,y)) +
            geom_point(size=0.1, alpha=1, stroke=0, shape=20, aes(colour = factor(cls))) +
            #geom_point_rast(size=0.1, alpha=1, stroke=0, shape=20, aes(colour = factor(cls))) +
            geom_text(data=data.frame(cx=cx_hs, cy=cy_hs),
                aes(cx, cy, label = label_hs), size=1.7
            ) +
            myTheme() + myFill() +
            labs(x = "UMAP-1", y = "UMAP-2") +
            theme(
                plot.margin=unit(c(2,0,2,2),"mm"),
                panel.grid.major = element_blank(),
                axis.ticks = element_blank(),
                axis.text = element_blank(),
                axis.line = element_blank(),
                panel.border = element_blank(),
                legend.position = "none"
            ) + scale_color_manual(values=colors_hs) +
            annotate("text", x = min(xs_hs) + 0.1 * (max(xs_hs) - min(xs_hs)),
                y = max(ys_hs), size = 2.1,
                label = paste(as.character(nrow(df)), " nuclei")
            )
    |]
  where
    (cls, xs, ys) = unzip3 $ flip concatMap clusters $ \(cl, ps) ->
        uncurry (zip3 (repeat cl)) $ unzip ps
    (label, cx, cy) = unzip3 $ flip map clusters $ \(cl, ps) ->
        let (x,y) = getCentroid ps in (cl, x, y)
    getCentroid ps = let (x,y) = unzip ps
                     in ( median medianUnbiased $ V.fromList x
                        , median medianUnbiased $ V.fromList y )
    colors :: [String]
    colors = take (length clusters) $ cycle
        ["#ddca42","#8945d9","#70df47","#502a95","#b5d853","#d34bbf","#5fc362","#7571d6","#cc923c","#402966","#70e6b5","#d0405f","#5ba178","#d94e2a","#84d2d8","#953872","#668331","#d684c0","#37492b","#80a5dd","#8d4d23","#696999","#d0d6a0","#351b2c","#cdacbf","#6e3232","#53878e","#d68473","#3e475c","#988765"]

plotSubcluster :: FilePath -> [(String, [(String, [(Double, Double)])])] -> IO ()
plotSubcluster output input = R.runRegion $ do
    myTheme
    [r| library("ggplot2")
        library("ggrastr")
        df <- data.frame(major=major_hs, minor=minor_hs, x=xs_hs, y=ys_hs)
        fig <- ggplot(df, aes(x,y)) +
            geom_point_rast(size=0.2, alpha=0.5, stroke=0, shape=20, aes(colour = factor(minor))) +
            geom_text(data=data.frame(major=tmajor_hs, cx=cx_hs, cy=cy_hs),
                aes(cx, cy, label = label_hs), size=1.7
            ) +
            myTheme() + myFill() +
            labs(x = "UMAP-1", y = "UMAP-2") +
            scale_color_manual(values=colors_hs) +
            theme(
                plot.margin=unit(c(2,0,2,2),"mm"),
                panel.grid.major = element_blank(),
                axis.ticks = element_blank(),
                axis.text = element_blank(),
                #axis.line = element_blank(),
                #panel.border = element_blank(),
                legend.position = "none"
            ) + facet_wrap(~ major, scales="free", ncol=5)
        ggsave(output_hs, fig, width=180, height=220, unit="mm", useDingbats=F)
    |]
    return ()
  where
    (major, minor, xs, ys) = unzip4 $ flip concatMap input $ \(major, clusters) -> flip concatMap (zip [1..] clusters) $ \(i, (_, xys)) ->
        flip map xys $ \(x,y) -> (major, i :: Int32, x, y)
    (tmajor, label, cx, cy) = unzip4 $ flip concatMap input $ \(major, clusters) -> flip map clusters $ \(minor, xys) ->
        let (x,y) = getCentroid xys
        in (major, minor, x, y)
    getCentroid ps = let (x,y) = unzip ps
                     in ( median medianUnbiased $ V.fromList x
                        , median medianUnbiased $ V.fromList y )
    colors :: [String]
    colors = ["#8190c3","#7fce55","#6a46be","#cab450","#c955ba","#82c9ae","#c45630","#4b2e4d","#cc9e98","#505d37","#b84766"]


        
--------------------------------------------------------------------------------
-- Cluster statistics
--------------------------------------------------------------------------------

extractTissueName :: B.ByteString -> B.ByteString
extractTissueName = f . B.init . fst . B.breakEnd (=='_') . updateId . B.init . fst .
    B.breakEnd (=='_')
  where
    updateId x | x == "skin_sun_exposed_SM-IOBHT" = "skin_SM-IOBHT"
               | x == "skin_SM-IOBHU" = "skin_sun_exposed_SM-IOBHU"
               | otherwise = x
    f x | x == "LungMap" = "lung"
        | "CARE" `B.isInfixOf` x = B.intercalate "_" $ take 2 $ B.split '_' x
        | otherwise = x

getTissues :: [CellCluster] -> [B.ByteString]
getTissues cls = nubSort $ flip concatMap cls $ \cl -> map f $ _cluster_member cl
  where
    f cell = extractTissueName $ _cell_barcode cell

getData :: [B.ByteString] -> CellCluster -> (B.ByteString, [Double])
getData tissues cl = (_cluster_name cl, map (\x -> M.lookupDefault 0 x counts) tissues)
  where
    counts = M.fromListWith (+) $ map f $ _cluster_member cl
    f cell = let name = extractTissueName $ _cell_barcode cell
             in (name, 1)

normalize :: DF.DataFrame Double -> DF.DataFrame Double
normalize = DF.mapRows f
  where
    f xs = let s = V.sum xs in V.map (/s) xs

computeEntropy :: V.Vector Double -> Double
computeEntropy = negate . V.sum . V.map f . normalize
  where
    f 0 = 0
    f p = p * logBase 2 p
    normalize xs = V.map (/s) xs
      where
        s = V.sum xs

plotCellComposition :: FilePath -> DF.DataFrame Double -> IO [(String, Int32)]
plotCellComposition output df = R.runRegion $ do
    myTheme
    [r| library("ggplot2")
        library("tidytext")
        library("ggforce")
        df <- data.frame(tissue=tissue_hs, celltype=celltype_hs, val=vals_hs)
            #df$celltype <- factor(df$celltype, levels = df$celltype)
        fig <- ggplot(df, aes(reorder_within(celltype, val, tissue), val)) +
            geom_col() +
            myTheme() + myFill() +
            scale_x_reordered() +
            #coord_flip() +
            theme( axis.text.x = element_text(angle = 45, hjust = 1) ) +
            labs(x = "", y = "fraction") +
            facet_row( ~ tissue, scales="free", space="free")
        ggsave(output_hs, fig, width=1830, height=47, unit="mm", limitsize=F, useDingbats=F)
    |]
    return $ flip map (groupBy ((==) `on` fst) $ sortBy (comparing fst) $ zip tissue celltype) $ \x ->
        (fst $ head x, fromIntegral $ length x)
  where
    (tissue, celltype, vals) = unzip3 $ concat $
        zipWith f (DF.rowNames df) $ Mat.toLists $ DF._dataframe_data $ normalize df
    f nm xs = filter ((>0.002) . (^._3)) $
        zip3 (repeat $ T.unpack nm) (map T.unpack $ DF.colNames df) xs

organDistr :: DF.DataFrame Double -> (forall s. R.R s (R.SomeSEXP s)) 
organDistr df' = do
    myTheme
    [r| library("ggplot2")
        df <- data.frame(cell=cell_hs, val=val_hs, tissue=tissue_hs)
        df$cell <- factor(df$cell, levels = level_hs)
        total <- data.frame(class=level_hs, total=num_cells_hs)
        ggplot(df, aes(x=cell, y=val, width=0.9)) +
            geom_col(aes(fill=tissue), colour="black", size=0.1) +
            geom_text(aes(class, 1, label = total, fill = NULL), data = total) +
            myTheme() + 
            coord_flip() +
            scale_fill_manual(values=colors_hs) +
            labs(x = "", y = "contribution of tissues (%)") +
            theme(
                #axis.text.x = element_text(size=5, angle = 90, vjust=0.5, hjust=1),
                panel.grid.major = element_blank(),
            )
    |]
  where
    num_cells = reverse $ map (show . truncate . V.sum) $ Mat.toColumns $ DF._dataframe_data df'
    df = DF.mapCols (\x -> let s = V.sum x in V.map (/s) x) $ normalize $ consolidate df'
    level = reverse $ map T.unpack $ DF.colNames df
    (tissue, cell, val) = unzip3 $ flip concatMap (DF.rowNames df) $ \i ->
        flip map (DF.colNames df) $ \j ->
            (T.unpack i, T.unpack j, df DF.! (i,j))
    normalize = DF.mapRows $ \xs -> let s = V.sum xs in V.map (\x -> x / s * 10000) xs
    colors :: [String]
    colors = ["#FFFFFF", "#000000", "#D70000", "#8C3CFF", "#028800", "#00ACC7", "#98FF00", "#FF7FD1", "#6C004F", "#FFA530", "#00009D", "#867068", "#004942", "#4F2A00", "#00FDCF", "#BCB7FF", "#95B47A", "#C004B9", "#2566A2", "#280041", "#DCB3AF", "#FEF590", "#50455B", "#A47C00", "#FF7166", "#3F816E", "#82000D", "#A37BB3", "#344E00", "#9BE4FF"]
    consolidate df = DF.mkDataFrame n (DF.colNames df) d
      where
        (n, d) = unzip $ map g $ groupBy ((==) `on` fst) $ sort $
            zip names $ Mat.toLists $ DF._dataframe_data df
        names = map (\x -> T.pack $ fromJust $ lookup (T.unpack x) tissueGroups) $ DF.rowNames df
        g xs = (fst $ head xs, foldl1' (zipWith (+)) $ map snd xs)

tissueGroups :: [(String, String)]
tissueGroups = 
    [ ("Human_brain", "Frontal Cortex")
    , ("adipose_omentum", "Adipose Omentum")
    , ("adrenal_gland", "Adrenal Gland")
    , ("artery_aorta", "Aorta")
    , ("artery_tibial", "Tibial Artery")
    , ("colon_sigmoid", "Sigmoid Colon")
    , ("colon_transverse", "Transverse Colon")
    , ("esophagus_ge_junction", "Esophagus GE Junction")
    , ("esophagus_mucosa", "Esophagus Mucosa")
    , ("esophagus_muscularis", "Esophagus Muscularis")
    , ("heart_atrial_appendage", "RA Appendage")
    , ("heart_la", "Left Atrium")
    , ("heart_lv", "Left Ventricle")
    , ("heart_ra", "Right Atrium")
    , ("heart_rv", "Right Ventricle")
    , ("islet", "Pancreatic Islet")
    , ("liver", "Liver")
    , ("lung", "Lung")
    , ("mammary_tissue", "Breast")
    , ("muscle", "Skeletal Muscle")
    , ("nerve_tibial", "Tibial Nerve")
    , ("ovary", "Ovary")
    , ("pancreas", "Body of Pancreas")
    , ("skin", "Skin Suprapublic")
    , ("skin_sun_exposed", "Skin Lower Leg")
    , ("small_intestine", "Small Intestine")
    , ("stomach", "Stomach")
    , ("thyroid", "Thyroid")
    , ("uterus", "Uterus")
    , ("vagina", "Vagina") ]

--------------------------------------------------------------------------------
-- Workflow
--------------------------------------------------------------------------------
build "wf" [t| SciFlow ENV |] $ do
    qcBuilder
    tissueBuilder
    annoBuilder

    node "Composition" [| \() -> do
        fl <- lookupConfig "cluster_bin"
        annoFl <- lookupConfig "annotation"
        dir <- lookupConfig "output_dir"
        liftIO $ do
            clusters <- decodeFile fl
            anno <- readAnno annoFl
            let tissues = getTissues clusters
                (cls, vals) = unzip $ map (getData tissues) clusters
                df = {-changeName anno $-} DF.mkDataFrame
                    (map (\x -> T.pack $ fromMaybe (error $ show x) $ lookup (B.unpack x) tissueGroups) tissues)
                    (map (T.pack . B.unpack) cls) $ transpose vals
                output = dir <> "/composition.tsv"
                output2 = dir <> "/cell_type_count.pdf"
            (tissueName, counts) <- unzip <$> plotCellComposition (dir <> "/tissue_composition.pdf") df
            R.runRegion $ do
                myTheme
                [r| library("ggplot2")
                    df <- data.frame(tissue=tissueName_hs, count=counts_hs)
                    m <- median(counts_hs)
                    print(m)
                    g <- ggplot(df, aes(reorder(tissue,count), count)) +
                        geom_col() + myTheme() + myFill() +
                        geom_hline(yintercept=m, linetype="dashed", size=0.25) +
                        theme(axis.text.x = element_text(angle = 45, hjust = 1)) +
                        labs(x = "", y = "#Cell types")
                    ggsave(output2_hs, g, width=85, height=40, unit="mm", useDingbats=F)
                |]
                return ()
            DF.writeTable output (T.pack . show) df
            return output
        |] $ return ()
    node "Phylogeny" [| \() -> do
        dir <- lookupConfig "output_dir"
        mat <- lookupConfig "peakMatrix"
        let output = dir ++ "/phylo.rds"
        liftIO $ do
            dat <- DF.readTable mat
            R.runRegion $ do
                mat <- toRMatrix dat
                [r| M <- dist(t( log2(mat_hs + 1) ))
                    saveRDS(M, file=output_hs)
                |]
                return ()
        return output
        |] $ return ()
    node "Plot_Phylogeny2" [| \(compFl, distFl) -> do
        dir <- lookupConfig "output_dir"
        annoFl <- lookupConfig "annotation"
        let output = dir ++ "/phylo2.pdf"
        liftIO $ do
            names <- map ( (\x -> (x!!0, x!!1)) . B.split '\t' ) .
                B.lines <$> B.readFile annoFl
            comp <- DF.readTable compFl
            R.runRegion $ do
                [r| library(ggraph)
                    library(ggplot2)
                    library(ape)
                    data <- readRDS(distFl_hs)
                    hc <- hclust(data, method = "ward.D2")
                    dend <- as.phylo(hc)
                    fig <- ggraph(dend, "dendrogram") + 
                        geom_edge_diagonal(colour="grey") +
                        geom_node_text(aes(filter = leaf, label=name)) +
                        theme_void() +
                        theme(legend.position="none", plot.margin=unit(c(0,0,0,0),"cm"))
                    ggsave(output_hs, fig, width=180, height=120, unit="mm", useDingbats=F)
                |]
                return ()
        |] $ return ()
    node "Plot_Phylogeny" [| \(compFl, distFl) -> do
        dir <- lookupConfig "output_dir"
        annoFl <- lookupConfig "annotation"
        let output = dir ++ "/phylo.pdf"
            output' = dir ++ "/phylo.txt"
        liftIO $ do
            names <- map ( (\x -> (x!!0, x!!1)) . B.split '\t' ) .
                B.lines <$> B.readFile annoFl
            comp <- DF.readTable compFl
            R.runRegion $ do
                rnames <- [r| library("ggplot2")
                    library("cowplot")
                    library("dendextend")
                    data <- readRDS(distFl_hs)
                    hc <- hclust(data, method = "ward.D2")
                    dend <- as.dendrogram(hc)
                    clusters <- cutree(hc, k = 11)
                    clusters <- clusters[order.dendrogram(dend)]
                    clusters_numbers <- unique(clusters) - (0 %in% clusters)
                    n_clusters <- length(clusters_numbers)
                    cols <- c("#c95786", "#91d550", "#9849c8", "#78d5a6", "#4e2d67", "#ccad52", "#8a88ca", "#c2573a", "#a4b4b7", "#523f3c", "#58763e")
                    label_cols <- cols[clusters]
                    dend <- dend %>% 
                        branches_attr_by_clusters(clusters, values = cols[unique(clusters)]) %>%
                        color_labels(col = label_cols) %>%
                        set("leaves_pch", 19) %>%
                        set("leaves_cex", 2) %>%
                        set("leaves_col", label_cols)
                    ggd <- as.ggdend(dend)
                    fig1 <<- ggplot(ggd, horiz = TRUE)
                    names(clusters)
                |]
                let rnames' = R.unSomeSEXP rnames $ \x ->
                        case H.hexp x of
                            H.String x' -> flip map (S.toList x') $ \s ->
                                case H.hexp s of
                                    H.Char s' -> T.pack $ map (toEnum . fromEnum) $ S.toList s'
                liftIO $ T.writeFile output' $ T.unlines rnames'
                fig2 <- organDistr $ changeName names $ comp `DF.csub` (reverse $ rnames' :: [T.Text])
                [r| figure <- plot_grid(fig1, fig2_hs,
                        ncol=2,
                        rel_widths =c(1,1.5),
                        labels = "" )
                    ggsave(output_hs, figure, width=180, height=240, unit="mm", useDingbats=F)
                |]
                return ()
        |] $ return ()
    ["Composition", "Phylogeny"] ~> "Plot_Phylogeny"
    ["Composition", "Phylogeny"] ~> "Plot_Phylogeny2"

    node "Plot_CellCluster" [| \() -> do
        dir <- lookupConfig "output_dir"
        clFl <- lookupConfig "cluster_file"
        let output = dir <> "/umap.pdf"
        liftIO $ do
            let f xs = (B.unpack $ fromJust $ lookup ("C" <> (fst $ B.break (=='.') $ xs!!1)) majorClusters, readDouble $ xs!!2, readDouble $ xs!!3)
            clusters <- map (\x -> (head x ^. _1, map (\a -> (a^._2, a^._3)) x)) .
                groupBy ((==) `on` (^._1)) . sortBy (comparing (^._1)) .
                map (f . B.split '\t') . tail . B.lines <$> B.readFile clFl
            R.runRegion $ do
                fig <- plotCells clusters
                [r| ggsave(output_hs, fig_hs, width=80, height=80, unit="mm", useDingbats=F) |]
                return ()
        |] $ return ()

    node "Plot_SubCluster" [| \() -> do
        dir <- lookupConfig "output_dir"
        subClDir <- lookupConfig "subcluster"
        annoFl <- lookupConfig "annotation"
        let output = dir <> "/subclusters.pdf"
        liftIO $ do
            anno <- readAnno annoFl
            fls <- fmap (filter ("_cluster.bin" `T.isSuffixOf`)) $ shelly $ lsT subClDir
            subClusters <- forM fls $ \fl -> do
                let clId = B.pack $ T.unpack $ fst $ T.breakOn "_cluster.bin" $ snd $ T.breakOnEnd "/" fl
                    clName = B.unpack $ fromMaybe (error $ show clId)$ lookup clId majorClusters
                clusters <- decodeFile $ T.unpack fl
                let subCl = flip mapMaybe clusters $ \CellCluster{..} -> 
                        let nm = B.unpack <$> lookup subId anno
                            subId | length clusters == 1 = clId
                                  | otherwise = clId <> "." <> B.tail _cluster_name
                        in case nm of
                            Nothing -> Nothing
                            Just nm' -> Just (nm', map _cell_2d $ _cluster_member)
                return (clName, subCl)
            plotSubcluster output subClusters
        |] $ return ()

    node "Plot_Fetal_CellCluster" [| \() -> do
        dir <- lookupConfig "output_dir"
        clFl <- lookupConfig "fetal_cluster_file"
        let output = dir <> "/fetal_umap.pdf"
        liftIO $ do
            let f xs = (B.unpack $ fromJust $ lookup ("C" <> (fst $ B.break (=='.') $ xs!!1)) fetalMajorClusters, readDouble $ xs!!2, readDouble $ xs!!3)
            clusters <- map (\x -> (head x ^. _1, map (\a -> (a^._2, a^._3)) x)) .
                groupBy ((==) `on` (^._1)) . sortBy (comparing (^._1)) .
                map (f . B.split '\t') . tail . B.lines <$> B.readFile clFl
            R.runRegion $ do
                fig <- plotCells clusters
                [r| ggsave(output_hs, fig_hs, width=80, height=80, unit="mm", useDingbats=F) |]
                return ()
        |] $ return ()

    node "Plot_Fetal_SubCluster" [| \() -> do
        dir <- lookupConfig "output_dir"
        subClDir <- lookupConfig "fetal_subcluster"
        annoFl <- lookupConfig "fetal_annotation"
        let output = dir <> "/fetal_subclusters.pdf"
        liftIO $ do
            anno <- readAnno annoFl
            fls <- fmap (filter ("_cluster.bin" `T.isSuffixOf`)) $ shelly $ lsT subClDir
            subClusters <- forM fls $ \fl -> do
                let clId = B.pack $ T.unpack $ fst $ T.breakOn "_cluster.bin" $ snd $ T.breakOnEnd "/" fl
                    clName = B.unpack $ fromMaybe (error $ show clId)$ lookup clId fetalMajorClusters
                clusters <- decodeFile $ T.unpack fl
                let subCl = flip mapMaybe clusters $ \CellCluster{..} -> 
                        let nm = B.unpack <$> lookup subId anno
                            subId | length clusters == 1 = clId
                                  | otherwise = clId <> "." <> B.tail _cluster_name
                        in case nm of
                            Nothing -> Nothing
                            Just nm' -> Just (nm', map _cell_2d $ _cluster_member)
                return (clName, subCl)
            plotSubcluster output subClusters
        |] $ return ()


main :: IO ()
main = mainFun wf