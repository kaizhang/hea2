{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell   #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE QuasiQuotes       #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE RankNTypes #-}
import HEA

import Statistics.Quantile
import Data.Conduit.Zlib
import Control.Arrow
import qualified Taiji.Utils.DataFrame as DF
import qualified Data.Vector as V
import qualified Data.Vector.Mutable as VM
import qualified Data.Text as T
import Bio.Data.Bed.Types
import Bio.Data.Bed
import Control.Monad.ST (runST)
import qualified Data.Matrix as Mat
import qualified Data.Map.Strict as Map
import qualified Data.ByteString.Char8 as B
import qualified Data.HashMap.Strict as M
import Data.List.Ordered (nubSort)
import Bio.Utils.Functions
import Taiji.Utils
import Shelly hiding (FilePath, path)
import Data.Binary
import System.Random.MWC.Distributions (uniformShuffle)
import System.Random.MWC (create)
import Statistics.Sample (mean)
import System.Environment
import Data.Int

import Taiji.Prelude
import qualified Language.R                        as R
import           Language.R.QQ
import Language.R.HExp
import qualified Data.Vector.SEXP as S

cellOrder = [ "C45", "C43", "C30", "C13", "C11", "C6", "C48", "C9", "C19", "C21", "C39", "C7", "C4", "C25", "C38", "C17", "C50", "C51", "C54", "C12", "C3", "C20", "C15", "C5", "C53", "C23", "C16", "C33", "C44", "C18", "C8", "C14", "C24", "C34", "C28", "C22", "C47", "C10", "C26", "C41", "C40", "C1", "C37", "C29", "C32", "C52", "C49", "C42", "C2", "C35", "C31", "C27", "C46", "C36"]

renameTissue = 
    [ ("artery_aorta", "artery")
    , ("artery_tibial", "artery")
    , ("colon_sigmoid", "colon")
    , ("colon_transverse", "colon")
    , ("esophagus_ge_junction", "esophagus")
    , ("esophagus_mucosa", "esophagus")
    , ("esophagus_muscularis", "esophagus")
    , ("heart_atrial_appendage", "heart")
    , ("heart_lv", "heart")
    , ("skin_sun_exposed", "skin") ]

-- | normalize to 10000 cell per sample
normalize :: DF.DataFrame Double -> DF.DataFrame Double
normalize df = DF.mapRows f df
  where
    f xs = let s = V.sum xs in V.map (\x -> x / s * 10000) xs

consolidate :: DF.DataFrame Double -> DF.DataFrame Double
consolidate df = DF.mkDataFrame n (DF.colNames df) d
  where
    (n, d) = unzip $ map g $ groupBy ((==) `on` fst) $ sort $
        zip names $ Mat.toLists $ DF._dataframe_data df
    names = map f $ DF.rowNames df
    f x = case lookup x renameTissue of
        Nothing -> T.map (\c -> if c == '_' then ' ' else c) x
        Just x' -> x'
    g xs = (fst $ head xs, foldl1' (zipWith (+)) $ map snd xs)

stackBar :: FilePath -> DF.DataFrame Double -> IO ()
stackBar output df = R.runRegion $ do
    myTheme
    [r| library("ggplot2")
        df <- data.frame(cell=cell_hs, val=val_hs, tissue=tissue_hs)
        df$cell <- factor(df$cell, levels = level_hs)
        fig <- ggplot(df, aes(x=cell, y=val, width=0.9)) +
            geom_col(aes(fill=tissue)) +
            coord_flip() +
            myTheme() + 
            scale_fill_manual(values=cat20_hs) +
            labs(x = "", y = "contribution of tissues (%)") +
            theme(
                panel.grid.major = element_blank(),
            )
        ggsave(output_hs, fig, width=87, height=110, unit="mm", useDingbats=F)
    |]
    return ()
  where
    level = reverse $ map T.unpack $ DF.colNames df
    (tissue, cell, val) = unzip3 $ flip concatMap (DF.rowNames df) $ \i ->
        flip map (DF.colNames df) $ \j ->
            (T.unpack i, T.unpack j, df DF.! (i,j))

main = do
    [fl, anno] <- getArgs
    a <- readAnno anno
    let names = map (\x -> T.pack $ B.unpack $ fromJust $ lookup x a) cellOrder
    df <- (`DF.csub` names) <$> DF.readTable fl
    stackBar "output.pdf" $ DF.mapCols (\x -> let s = V.sum x in V.map (/s) x) $
        consolidate $ normalize df

    mapM_ (print . V.sum) $ Mat.toColumns $ DF._dataframe_data df