{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell   #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE QuasiQuotes       #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE GADTs #-}
import HEA
import qualified Data.Text as T
import Data.Conduit.Zlib (multiple, ungzip, gzip)
import qualified Data.Vector.Unboxed as U
import qualified Data.ByteString.Char8 as B
import Bio.Utils.Functions
import qualified Data.Matrix as Mat
import Control.Concurrent.Async (mapConcurrently)
import Data.Either
import Bio.Data.Bed
import Bio.Data.Bed.Utils
import Bio.Data.Bed.Types
import Bio.Data.Bam
import Shelly (mkdir_p, shelly, lsT, fromText)
import Taiji.Prelude
import qualified Data.HashMap.Strict as M
import qualified Data.Set as S
import qualified Data.Map.Strict as Map
import Control.DeepSeq
import Statistics.Correlation (pearson, spearman)
import Statistics.Distribution.Normal (standard)
import Data.List.Ordered
import Statistics.Sample
import Statistics.Distribution (complCumulative)
import Data.Binary
import qualified Data.Vector as V
import qualified Taiji.Utils.DataFrame as DF

import Taiji.Prelude hiding (groupBy)
import qualified Language.R                        as R
import           Language.R.QQ
import Language.R.HExp

tissueNameMap = 
    [ (("Adipose_Subcutaneous", "Adipose Subcutaneous"), "")
    , (("Adipose_Visceral_Omentum", "Adipose Omentum"), "adipose_omentum")
    , (("Adrenal_Gland", "Adrenal Gland"), "adrenal_gland")
    , (("Artery_Aorta", "Artery Aorta"), "artery_aorta")
    , (("Artery_Coronary", "Artery Coronary"), "")
    , (("Artery_Tibial", "Artery Tibial"), "artery_tibial")
    , (("Brain_Amygdala", "Brain Amygdala"), "")
    , (("Brain_Anterior_cingulate_cortex_BA24", "Brain Anterior cingulate cortex"), "")
    , (("Brain_Caudate_basal_ganglia", "Brain Caudate basal ganglia"), "")
    , (("Brain_Cerebellar_Hemisphere", "Brain Cerebellar Hemisphere"), "")
    , (("Brain_Cerebellum", "Brain Cerebellum"), "")
    , (("Brain_Cortex", "Brain Cortex"), "")
    , (("Brain_Frontal_Cortex_BA9", "Brain Frontal Cortex"), "")
    , (("Brain_Hippocampus", "Brain Hippocampus"), "")
    , (("Brain_Hypothalamus", "Brain Hypothalamus"), "")
    , (("Brain_Nucleus_accumbens_basal_ganglia", "Brain Nucleus accumbens basal ganglia"), "")
    , (("Brain_Putamen_basal_ganglia", "Brain Putamen basal ganglia"), "")
    , (("Brain_Spinal_cord_cervical_c-1", "Brain Spinal cord cervical c"), "")
    , (("Brain_Substantia_nigra", "Brain Substantia nigra"), "")
    , (("Breast_Mammary_Tissue", "Breast Mammary Tissue"), "mammary_tissue")
    , (("Cells_Cultured_fibroblasts", "Cells Cultured fibroblasts"), "")
    , (("Cells_EBV-transformed_lymphocytes", "Cells EBV"), "")
    , (("Colon_Sigmoid", "Colon Sigmoid"), "colon_sigmoid")
    , (("Colon_Transverse", "Colon Transverse"), "colon_transverse")
    , (("Esophagus_Gastroesophageal_Junction", "Esophagus Gastroesophageal Junction"), "esophagus_ge_junction")
    , (("Esophagus_Mucosa", "Esophagus Mucosa"), "esophagus_mucosa")
    , (("Esophagus_Muscularis", "Esophagus Muscularis"), "esophagus_muscularis")
    , (("Heart_Atrial_Appendage", "Heart Atrial Appendage"), "heart_atrial_appendage")
    , (("Heart_Left_Ventricle", "Heart Left Ventricle"), "heart_lv")
    , (("Kidney_Cortex", "Kidney Cortex"), "")
    , (("Liver", "Liver"), "liver")
    , (("Lung", "Lung"), "lung")
    , (("Minor_Salivary_Gland", "Minor Salivary Gland"), "")
    , (("Muscle_Skeletal", "Muscle Skeletal"), "muscle")
    , (("Nerve_Tibial", "Nerve Tibial"), "nerve_tibial")
    , (("Ovary", "Ovary"), "ovary")
    , (("Pancreas", "Pancreas"), "pancreas")
    , (("Pituitary", "Pituitary"), "")
    , (("Prostate", "Prostate"), "")
    , (("Skin_Not_Sun_Exposed_Suprapubic", "Skin Not Sun Exposed Suprapubic"), "skin")
    , (("Skin_Sun_Exposed_Lower_leg", "Skin Sun Exposed Lower leg"), "skin_sun_exposed")
    , (("Small_Intestine_Terminal_Ileum", "Small Intestine Terminal Ileum"), "small_intestine")
    , (("Spleen", "Spleen"), "")
    , (("Stomach", "Stomach"), "stomach")
    , (("Testis", "Testis"), "")
    , (("Thyroid", "Thyroid"), "thyroid")
    , (("Uterus", "Uterus"), "uterus")
    , (("Vagina", "Vagina"), "vagina")
    , (("Whole_Blood", "Whole Blood"), "")
    ]

readFMeQTL :: FilePath -> IO [BED]
readFMeQTL fl = fmap nubSort $ runResourceT $ runConduit $
    sourceFile fl .| multiple ungzip .| linesUnboundedAsciiC .|
        (dropC 1 >> concatMapC f) .| sinkList
  where
    f x = let fs = B.split '\t' x
              e = readInt $ fs!!4
          in case lookup (fs!!0) (map fst tissueNameMap) of
              Nothing -> Nothing
              nm -> Just $ BED ("chr" <> fs!!3) (e-1) e nm Nothing Nothing

readSpecificFMeQTL :: FilePath -> IO [BED]
readSpecificFMeQTL fl = do
    qtls <- readFMeQTL fl
    return $ mapMaybe f $ Map.toList $ Map.fromListWith (<>) $ map (\x -> (convert x :: BED3, [x^.name])) qtls
  where
    f (x, xs) = case nubSort xs of
        [nm] -> Just $ name .~ nm $ convert x 
        _ -> Nothing

readeQTL :: FilePath -> IO [BED]
readeQTL fl = runResourceT $ runConduit $ sourceFile fl .|
    multiple ungzip .| linesUnboundedAsciiC .|
    (dropC 1 >> concatMapC f) .| sinkList
  where
    f x = let xs = B.split '\t' x
              i = readInt $ xs !! 14
              chr = xs !! 13
          in if (readDouble $ xs!!28) < 0.05
              then Just $ name .~ Just (xs!!11) $ asBed chr (i - 1) i
              else Nothing

readieQTL :: FilePath -> IO [BED]
readieQTL fl = runResourceT $ runConduit $ sourceFile fl .|
    multiple ungzip .| linesUnboundedAsciiC .|
    (dropC 1 >> concatMapC f) .| sinkList
  where
    f x = let xs = B.split '\t' x
              (chr:i:_) = B.split '_' $ head xs
              i' = readInt i
          in if (readDouble $ last xs) < 0.05
              then Just $ name .~ Just (head xs) $ asBed chr (i' - 1) i'
              else Nothing

ieQTLOverlap :: [BED] -> [(T.Text, [BED3])] -> DF.DataFrame Bool
ieQTLOverlap ieQTL peaks = DF.mkDataFrame rownames colnames $ transpose val
  where
    rownames = map (T.pack . B.unpack . fromJust . (^.name)) ieQTL
    (colnames, val) = unzip $ flip map peaks $ \(nm, p) ->
        let tree = bedToTree const $ zip p $ repeat ()
        in (nm, map (tree `isIntersected`) ieQTL)

getTissues :: [CellCluster] -> [B.ByteString]
getTissues cls = nubSort $ flip concatMap cls $ \cl -> map f $ _cluster_member cl
  where
    f cell = extractTissueName $ _cell_barcode cell

extractTissueName :: B.ByteString -> B.ByteString
extractTissueName = f . B.init . fst . B.breakEnd (=='_') . B.init . fst .
    B.breakEnd (=='_')
  where
    f x | x == "LungMap" = "lung"
        | "CARE" `B.isInfixOf` x = B.intercalate "_" $ take 2 $ B.split '_' x
        | otherwise = x

getData :: [B.ByteString] -> CellCluster -> (B.ByteString, [Double])
getData tissues cl = (_cluster_name cl, map (\x -> M.lookupDefault 0 x counts) tissues)
  where
    counts = M.fromListWith (+) $ map f $ _cluster_member cl
    f cell = let name = extractTissueName $ _cell_barcode cell
             in (name, 1)

computeEntropy :: V.Vector Double -> Double
computeEntropy = negate . V.sum . V.map f . normalize
  where
    f 0 = 0
    f p = p * logBase 2 p
    normalize xs = V.map (/s) xs
      where
        s = V.sum xs

readClusterPeak :: [BED3]   -- ^ Peak list
                -> FilePath
                -> IO [(T.Text, [BED3])]
readClusterPeak peakList dir = do
    fls <- fmap (filter (".narrowPeak" `T.isSuffixOf`)) $ shelly $
        lsT $ fromText $ T.pack dir
    forM fls $ \fl -> do
        let nm = fst $ T.breakOn "." $ snd $ T.breakOnEnd "/" fl
        peaks <- readBed $ T.unpack fl :: IO [BED3]
        res <- runConduit $ yieldMany peakList .| intersectBed peaks .| sinkList
        return (nm, res)

percentOverlap :: (BEDLike b1, BEDLike b2) => [b1] -> [b2] -> Double
percentOverlap a b = m / n * 100
  where
    n = fromIntegral $ length a
    m = fromIntegral $ runIdentity $ runConduit $ yieldMany a .| intersectBed b .| lengthC

jaccard :: (BEDLike b1, BEDLike b2) => [b1] -> [b2] -> Double
jaccard a b = o / (n1 + n2 - o)
  where
    o = fromIntegral $ runIdentity $ runConduit $ yieldMany a .| intersectBed b .| lengthC
    n1 = fromIntegral $ length a
    n2 = fromIntegral $ length b

plotHeatmap :: FilePath
            -> DF.DataFrame Double
            -> IO()
plotHeatmap output df = R.runRegion $ do
    valMatrix <- toRMatrix df
    [r| library("ggplot2")
        library("ComplexHeatmap")
        library("RColorBrewer")
        library("viridis")
        rwb <- colorRampPalette(colors = rev(brewer.pal(7,"RdBu")))
        h1 <- Heatmap( valMatrix_hs, 
            col=rwb(99), name = "mat",
            cluster_rows=F, cluster_columns=F,
            show_row_names=T, row_names_side="left",
            show_row_dend=F, show_column_dend=F,
            show_column_names=T,
            column_names_gp = gpar(fontsize=5),
            row_names_gp = gpar(fontsize=5),
            rect_gp = gpar(col= "white"),
            heatmap_legend_param = list(
                title = "enrichment",
                title_gp = gpar(fontsize = 6),
                #title_position = "left-top",
                direction = "horizontal",
                labels_gp = gpar(fontsize=5)
            )
        )

        pdf(output_hs, width=5.5, height=4.8)
        draw(h1, heatmap_legend_side="bottom")
        decorate_heatmap_body("mat", {
            grid.text("*", xs1_hs, ys1_hs , default.units = "npc", gp = gpar(fontsize=5))
            grid.text("**", xs2_hs, ys2_hs , default.units = "npc", gp = gpar(fontsize=5))
        })
        dev.off()
    |]
    return ()
  where
    h = fromIntegral $ fst $ DF.dim df :: Double
    w = fromIntegral $ snd $ DF.dim df :: Double
    (s1, s2) = partitionEithers $ catMaybes $ concat $ Mat.toLists $ Mat.imap f $ DF._dataframe_data df
    (xs1, ys1) = unzip s1
    (xs2, ys2) = unzip s2
    f (i,j) v | v > 2.326348 = Just $ Right ((fromIntegral j + 0.5) / w, 1 - (fromIntegral i + 0.65) / h)
              | v > 1.644854 = Just $ Left ((fromIntegral j + 0.5) / w, 1 - (fromIntegral i + 0.65) / h)
              | otherwise = Nothing

build "wf" [t| SciFlow ENV |] $ do
    node "Get_feQTL" [| \() -> do
        dir <- (<> "/feQTL/") <$> lookupConfig "output_dir"
        feQTLfl <- lookupConfig "feQTL"
        liftIO $ do
            shelly $ mkdir_p $ fromText $ T.pack dir
            qtls <- groupBy ((==) `on` (^.name)) . sortBy (comparing (^.name)) .
                map head . filter ((==1) . length) .
                groupBy ((==) `on` (convert :: BED -> BED3)) . sort
                <$> readFMeQTL feQTLfl
            forM qtls $ \xs -> do
                let output = dir <> B.unpack nm <> ".bed"
                    nm = fromJust $ head xs ^. name
                writeBed output xs
                return (nm, output)
        |] $ return ()

    node "Cell_Coverage" [| \() -> do
        accFl <- lookupConfig "accessibility"
        feQTLfl <- lookupConfig "feQTL"
        annoFl <- lookupConfig "annotation"
        creFl <- lookupConfig "cre"
        liftIO $ do
            anno <- readAnno annoFl
            qtls <- readFMeQTL feQTLfl
            cres <- runResourceT $ runConduit $ streamBedGzip creFl .|
                intersectBed qtls .| mapC (T.pack . B.unpack . (showBed :: BED3 -> B.ByteString)) .| sinkList
            acc <- changeName anno <$> DF.readTable accFl
            let n = map V.sum $ Mat.toColumns $ DF._dataframe_data acc 
                m = map V.sum $ Mat.toColumns $ DF._dataframe_data $ acc `DF.rsub` cres
            return (map T.unpack $ DF.colNames acc, zipWith (/) m n)
        |] $ return ()
    node "Plot_Cell_Coverage" [| \(names, vals) -> do
        dir <- lookupConfig "output_dir"
        let output = dir <> "/coverage.pdf"
        liftIO $ R.runRegion $ do
            myTheme
            [r| library("ggplot2")
                library(scales)
                df <- data.frame(name=names_hs, val=vals_hs)
                df$name <- factor(df$name, levels = names_hs)
                fig <- ggplot(df, aes(x=reorder(name, val), y=val)) +
                    geom_point(size=0.3) +
                    myTheme() + myColour() +
                    coord_flip() +
                    #scale_color_manual(breaks = breaks_hs, values=colors_hs) +
                    labs(x = "", y = "max-Z")
                ggsave(output_hs, fig, width=70, height=150, unit="mm", useDingbats=F)
            |]
            return ()
        |] $ return ()
    ["Cell_Coverage"] ~> "Plot_Cell_Coverage"

    node "feQTL_Overlap" [| \input -> do
        creFl <- lookupConfig "cre"
        accFl <- lookupConfig "accessibility"
        annoFl <- lookupConfig "annotation"
        dir <- (<> "/feQTL/") <$> lookupConfig "output_dir"
        let output = dir <> "feQTL_overlap.tsv"
        liftIO $ do
            peaks <- runResourceT $ runConduit $ streamBedGzip creFl .| sinkList :: IO [BED3]
            acc <- DF.map (logBase 2 . (+1)) <$> DF.readTable accFl
            anno <- readAnno annoFl
            (nms, res) <- fmap unzip $ forM input $ \(nm, fl) -> do
                qtl <- readBed fl :: IO [BED3]
                overlapped <- fmap (map (T.pack . B.unpack . showBed)) $ runConduit $ yieldMany peaks .| intersectBed qtl .| sinkList
                let res = map mean $ Mat.toColumns $ DF._dataframe_data $ acc `DF.rsub` overlapped
                return (T.pack $ B.unpack nm, res)
            DF.writeTable output (T.pack . show) $ changeName anno $ DF.mkDataFrame nms (DF.colNames acc) res
            return output
        |] $ return ()
    node "Plot_feQTL_Overlap" [| \input -> do
        fl <- lookupConfig "cluster_file"
        annoFl <- lookupConfig "annotation"
        dir <- (<> "/feQTL/") <$> lookupConfig "output_dir"
        liftIO $ do
            zscore <- DF.orderDataFrame id . DF.mapRows scale <$> DF.readTable input
            let pvalue = DF.map (complCumulative standard) zscore
            DF.writeTable "output/feQTL_zscore.tsv" (T.pack . show) zscore
            DF.writeTable "output/feQTL_pvalue.tsv" (T.pack . show) pvalue

            clusters <- decodeFile fl
            anno <- readAnno annoFl
            let tissues = getTissues clusters
                (cls, vals) = unzip $ map (getData tissues) clusters
                df = changeName anno $ DF.mkDataFrame
                    (map (T.pack . B.unpack . rename) tissues)
                    (map (T.pack . B.unpack) cls) $ transpose vals
                rename x = fromMaybe x $ lookup x $ flip map tissueNameMap $ \((_, a), b) -> (b, a)
                hetero = zip (DF.rowNames df) $
                    map computeEntropy $ Mat.toRows $ DF._dataframe_data df 
                maxz = zip (DF.rowNames zscore) $
                    map V.maximum $ Mat.toRows $ DF._dataframe_data zscore
                (names, dx, dy) = unzip3 $ flip mapMaybe maxz $ \(nm, v) -> case lookup nm hetero of
                    Nothing -> Nothing
                    Just x -> Just (T.unpack nm, x, v)

            print hetero
            print $ spearman $ V.fromList $ zip dx dy
            print names
            R.runRegion $ do
                myTheme
                [r| library("ggplot2")
                    library("ggrepel")
                    df <- data.frame(x=dx_hs, y=dy_hs)
                    fig <- ggplot(df, aes(x=x, y=y)) + 
                        geom_point(size=0.5) +
                        geom_smooth(method=lm, size=0.5) +
                        #geom_text_repel(aes(label = names_hs), size = 1.8) +
                        xlab("heterogeneity") +
                        ylab("maximum enrichment") +
                        myTheme() + myFill()
                    ggsave("output/heter_vs_maxz.pdf", fig, width=60, height=55, unit="mm", useDingbats=F)
                |]
                return ()

            plotHeatmap "output/feQTL.pdf" {-$ DF.filterCols (\_ v -> V.maximum v > 1.644854)-} zscore
        |] $ return ()
    path ["Get_feQTL", "feQTL_Overlap", "Plot_feQTL_Overlap"]

{-
                df = changeName anno $ DF.mkDataFrame nms (map fst peaks) res
            DF.writeTable output (T.pack . show) df
            -}
{-
            let f "Total" _ = False
                f _ x = True
            DF.readTable input >>= plotHeatmap "output/feQTL.pdf"
    path ["feQTL_Overlap", "Plot_feQTL_Overlap"]
    -}

main :: IO ()
main = mainFun wf
