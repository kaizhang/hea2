{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell   #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE QuasiQuotes       #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE GADTs #-}
module Joint (jointBuilder, readTable') where

import HEA

import Statistics.Quantile
import Data.Conduit.Zlib (multiple, ungzip, gzip)
import qualified Taiji.Utils.DataFrame as DF
import Bio.RealWorld.GENCODE
import Control.Monad.State.Strict
import Control.Arrow
import qualified Data.HashMap.Strict as HM
import qualified Data.Vector as V
import qualified Data.Vector.Unboxed as U
import Statistics.Regression (olsRegress)
import Statistics.Correlation (pearson)
import AI.Clustering.Hierarchical
import Data.Either
import Control.Parallel.Strategies (parMap, rdeepseq)
import qualified Data.Text as T
import Bio.Data.Bed.Utils
import Shelly hiding (FilePath, path)
import Data.Int
import Data.Conduit.Internal (zipSinks, zipSources)
import Bio.Data.Bed
import Control.Monad.ST (runST)
import qualified Data.Matrix as Mat
import qualified Data.Matrix.Unboxed as MU
import qualified Data.Matrix.Mutable as MatM
import qualified Data.ByteString.Char8 as B
import qualified Data.ByteString as BS
import qualified Data.HashMap.Strict as M
import qualified Data.Map.Strict as Map
import qualified Data.HashSet as S
import Data.List.Split (chunksOf)
import Data.List.Ordered (nubSort)
import Bio.Utils.Functions
import Taiji.Utils.Plot
import Statistics.Sample (mean, meanVarianceUnb, varianceUnbiased)
import Data.Binary
import System.Random.MWC.Distributions (uniformShuffle)
import System.Random.MWC (create)
import Text.Wrap
import Control.DeepSeq (force)
import Data.Conduit.ConcurrentMap (concurrentMapM_numCaps)
import AI.Clustering.Hierarchical (euclidean)

import Data.Conduit.Async
import Control.Concurrent.Async (forConcurrently)

import Taiji.Prelude
import Taiji.Pipeline.SC.ATACSeq.Functions.Utils (mkCountMat, groupCells)
import qualified Language.R                        as R
import Taiji.Utils.Matrix
import           Language.R.QQ
import Language.R.HExp
import qualified Language.R.HExp as H
import qualified Data.Vector.SEXP as R
import Data.Vector.SEXP (toList)

readTable' :: FilePath -> [Int] -> IO (DF.DataFrame Double)
readTable' input idx = runResourceT $ runConduit $ source .| sink
  where
    source = zipSources (iterateC succ (-1)) $ sourceFile input .|
        linesUnboundedAsciiC .| mapC (B.split '\t')
    sink = do
        cols <- tail . snd . fromJust <$> headC 
        (rows, dat) <- fmap unzip $ filterC ((`S.member` idx') . fst) .|
            mapC ((\x -> (head x, map readDouble $ tail x)) . snd) .| sinkList
        return $ DF.mkDataFrame (map (T.pack . B.unpack) rows)
            (map (T.pack . B.unpack) cols) dat
    idx' = S.fromList idx

getClusterCentroid :: M.HashMap B.ByteString (U.Vector Double) -> [CellCluster] -> [(B.ByteString, U.Vector Double)]
getClusterCentroid m = map f
  where
    f CellCluster{..} = (_cluster_name,
        average $ map (\x -> M.lookupDefault (error $ show $ _cell_barcode x) (_cell_barcode x) m) _cluster_member)
    average xs = U.map (/ (fromIntegral $ length xs)) $ foldl1' (U.zipWith (+)) xs

readCoordinates :: FilePath -> FilePath -> IO (M.HashMap B.ByteString (U.Vector Double))
readCoordinates labelFl datFl = do
    dat <- runResourceT $
        runConduit $ sourceFile datFl .| multiple ungzip .| linesUnboundedAsciiC .|
            mapC (U.fromList . map readDouble . B.split '\t') .| sinkList
    labels <- B.lines <$> B.readFile labelFl
    return $ M.fromListWith undefined $ zip labels dat

removeBatchEffects :: FilePath
                   -> FilePath
                   -> FilePath
                   -> IO ()
removeBatchEffects output input labelFl = do
    dat <- readData input
    label <- U.fromList . map (\x -> if "sample" `B.isInfixOf` x then 1 else 0) .
        B.lines <$> B.readFile labelFl
    let res = map (B.intercalate "\t" . map toShortest) $
            transpose $ flip map dat $ \x -> U.toList $ residue label x
    runResourceT $ runConduit $ yieldMany res .| unlinesAsciiC .| gzip .| sinkFile output
  where
    readData fl = fmap (map U.fromList . transpose) $ runResourceT $
        runConduit $ sourceFile fl .| multiple ungzip .| linesUnboundedAsciiC .|
            mapC (map readDouble . B.split '\t') .| sinkList
    residue x y = U.zipWith f x y
      where
        f x' y' = y' - (a * x' + b)
        [a, b] = U.toList $ fst $ olsRegress [x] y

getCREContext :: FilePath -> FilePath -> IO (S.HashSet B.ByteString, S.HashSet B.ByteString, S.HashSet B.ByteString)
getCREContext annoFl creFl = do
    (within200, within2000) <- getContext <$> readGenes annoFl
    peaks <- runResourceT $ runConduit $ streamBedGzip creFl .|
        mapC centering .| sinkList
    let (rest, o200) = partitionEithers $ runIdentity $
            runConduit $ yieldMany peaks .| intersectBedWith f within200 .| sinkList
        (no, o2000) = partitionEithers $ runIdentity $ runConduit $
            yieldMany rest .| intersectBedWith f within2000 .| sinkList
        f x [] = Left x
        f x _ = Right x
    return ( S.fromList $ map (showBed . (^._data)) o200
           , S.fromList $ map (showBed . (^._data)) o2000
           , S.fromList $ map (showBed . (^._data)) no )
  where
    centering b@(BED3 chr s e) = let c = (s + e) `div` 2 in BEDExt (BED3 chr c (c+1)) b
    getContext :: [Gene] -> ([BED3], [BED3])
    getContext genes = (nubSort . concat) *** (nubSort . concat) $ unzip $
        flip map genes $ \Gene{..} -> 
            let tss | geneStrand = geneLeft : map transLeft geneTranscripts
                    | otherwise = geneRight : map transRight geneTranscripts
                within2000 = map (\x -> BED3 geneChrom (x-2000) (x+2000)) tss
                within200 = map (\x -> BED3 geneChrom (x-200) (x+200)) tss
            in (within200, within2000)

mergePeaks :: [NarrowPeak] -> [NarrowPeak] -> [NarrowPeak]
mergePeaks adult fetal = adult <> p
  where
    p = runIdentity $ runConduit $ yieldMany fetal .| intersectBedWith f adult .| concatC .| sinkList
    f x [] = Just x
    f _ _ = Nothing

mkFeatMatrix :: FilePath -> [BED3] -> [FilePath] -> IO ()
mkFeatMatrix dir regions input = do
    runResourceT $ runConduit $ yieldMany regions .| mapC showBed .|
        unlinesAsciiC .| gzip .| sinkFile outputCol
    let sink1 = mkCountMat regions .|
            sinkRows' (length regions) (fromJust . packDecimal) outputMat
        sink2 = mapC fst .| unlinesAsciiC .| gzip .| sinkFile outputRow
    _ <- runResourceT $ runCConduit $
        mapM_ streamBedGzip input =$=& groupCells =$=& zipSinks sink1 sink2
    return ()
  where
    outputMat = dir <> "/mat.gz"
    outputRow = dir <> "/rownames.tsv.gz"
    outputCol = dir <> "/features.tsv.gz"

mkCountMat' ::[BED3]    -- ^ a list of regions
           -> ConduitT (B.ByteString, [BED]) (Row Int) (ResourceT IO) ()
mkCountMat' regions = mapC $ second (countEachCell bedTree)
  where
    bedTree = bedToTree (++) $ zip regions $ map return [0::Int ..]
    countEachCell :: BEDTree [Int] -> [BED] -> [(Int, Int)]
    countEachCell beds = HM.toList . foldl' f HM.empty
      where
        f m bed = foldl' (\x k -> HM.insertWith (+) k (1::Int) x) m $
            concat $ intersecting beds query
          where
            query = case bed^.strand of
                Just False -> BED3 (bed^.chrom) (bed^.chromEnd - 1) (bed^.chromEnd)
                _ -> BED3 (bed^.chrom) (bed^.chromStart) (bed^.chromStart + 1)

plotTree' :: FilePath -> DF.DataFrame Double -> DF.DataFrame Double -> IO ()
plotTree' output df1 df2 = R.runRegion $ do
    mat <- toRMatrix $ DF.rbind [df1, df2]
    [r| library("ggtree")
        library("ggplot2")
        library("ape")
        tree <- fastme.bal(dist(mat_hs))

        write.tree(tree, file=output_hs)

        #p <- ggtree(tree, layout="rectangular", ladderize=T, branch.length="none")
        #fig <- groupOTU(p, grp_hs, "Stage") + aes(color=Stage) +
        #    geom_text(aes(label=label), size=1)
        #ggsave("dend.pdf", fig, width=150, height=240, unit="mm", useDingbats=F)

        p <- ggtree(tree, layout="daylight")
        fig <- groupOTU(p, grp_hs, "Stage") + aes(color=Stage) +
            geom_text(aes(label=label), size=1) +
            theme(legend.position="none")
        ggsave("dend.pdf", fig, width=220, height=220, unit="mm", useDingbats=F)
    |]
    return ()
  where
    grp = map T.unpack $ DF.rowNames df2

getOrder :: FilePath -> IO [T.Text]
getOrder fl = R.runRegion $ do
    res <-
        [r| library(ape)
            library("ggtree")
            tree <- read.tree(fl_hs)
            p <- ggtree(tree, layout="rectangular", ladderize=T, branch.length="none")
            get_taxa_name(p)
        |]
    R.unSomeSEXP res $ \x -> case H.hexp x of
        H.String x' -> return $ flip map (toList x') $ \a -> case H.hexp a of
            H.Char a' -> T.pack $ B.unpack $ BS.pack $ toList a'


plotTree :: FilePath -> DF.DataFrame Double -> IO ()
plotTree output df = R.runRegion $ do
    mat <- toRMatrix df
    [r| library("ggplot2")
        library("ComplexHeatmap")
        library("dendextend")
        library("pdist")
        library("viridis")
        data <- dist(mat_hs)
        hc <- hclust(data, method = "average")
        dend <- as.dendrogram(hc)
        cols = c("green", "red")
        clusters <- membership_hs
        clusters <- clusters[order.dendrogram(dend)]
        label_cols <- cols[clusters]
        dend <- dend %>% 
            color_labels(col = label_cols) %>%
            set("leaves_pch", 19) %>%
            set("leaves_cex", 0.5) %>%
            set("leaves_col", label_cols)
        ggd <- as.ggdend(dend)
        fig <- ggplot(ggd, horiz = TRUE)
        ggsave("1.pdf", fig, width=150, height=640, unit="mm", useDingbats=F)

        adult <- mat_hs[membership_hs == 1,] 
        fetal <- mat_hs[membership_hs == 2,] 
        h1 <- Heatmap( as.matrix(pdist(adult, fetal)), col=viridis(99),
            cluster_rows=T, cluster_columns=T,
            show_row_names=T, row_names_side="left",
            show_column_names=T,
            clustering_method_rows="ward.D2",
            clustering_method_columns="ward.D2",
            row_names_gp = gpar(fontsize=6),
            column_names_gp = gpar(fontsize=6),
            heatmap_legend_param = list(
                title = "-log10(P-value)",
                title_position = "leftcenter-rot"
                #at = c(-2, 0, 2)
            )
        )
        pdf("2.pdf", width=7.2, height=3.7)
        draw(h1)
        dev.off()
 
    |]
    return ()
  where
    membership = map (\x -> if "adult" `T.isPrefixOf` x then 1 :: Int32 else 2) $
        DF.rowNames df

crossDist :: DF.DataFrame Double -> DF.DataFrame Double -> DF.DataFrame Double
crossDist df1 df2 = DF.mkDataFrame (DF.rowNames df1) (DF.rowNames df2) $
    flip map r1 $ \x -> flip map r2 $ \y -> euclidean x y
  where
    r1 = Mat.toRows $ DF._dataframe_data df1
    r2 = Mat.toRows $ DF._dataframe_data df2

crossCor :: [(B.ByteString, U.Vector Double)]
         -> [(B.ByteString, U.Vector Double)]
         -> DF.DataFrame Double
crossCor xs ys = DF.mkDataFrame (map (T.pack . B.unpack . fst) xs)
    (map (T.pack . B.unpack . fst) ys) $ flip (parMap rdeepseq) xs $ \(_, x) ->
        flip map ys $ \(_, y) -> pearson $ U.filter (\(a,b) -> a > 2 || b > 2) $ U.zip x y

diagonizeDF :: (a -> Double) -> DF.DataFrame a -> DF.DataFrame a
diagonizeDF getV = DF.reorderRows f
  where
    f xs = map (\x -> (x, M.findWithDefault undefined x xs')) names
      where
        xs' = M.fromList xs
        names = map fst $ sortBy (comparing snd) $ map (\(a,b) -> (a, g b)) xs
        g xs = (i, negate v)
          where
            v = z V.! i
            i = V.maxIndex z
            z = scale $ V.map getV xs

plotHeatmap :: FilePath
            -> DF.DataFrame Double
            -> IO()
plotHeatmap output df = R.runRegion $ do
    valMatrix <- toRMatrix {-zeroOutDiagnol-} df
    [r| library("ggplot2")
        library("ComplexHeatmap")
        library("RColorBrewer")
        library("viridis")
        rwb <- colorRampPalette(colors = brewer.pal(7,"RdBu"))
        h1 <- Heatmap( valMatrix_hs, 
            col=rev(rwb(99)), use_raster=T, raster_quality=10,
            cluster_rows=F, cluster_columns=F,
            show_row_names=T, row_names_side="left",
            show_column_names=T, show_column_dend = F,
            row_names_gp = gpar(fontsize=5),
            column_names_gp = gpar(fontsize=5),
            #rect_gp = gpar(type = "none"),
            #cell_fun = function(j, i, x, y, w, h, col) {
            #    if(i > j) {
            #        grid.rect(x, y, w, h, gp = gpar(fill = col, col=NA))
            #    } else if(j == i) {
            #        #grid.text(labels[i], x, y)
            #    } else {
            #    }
            #},
            heatmap_legend_param = list(
                title = "",
                title_position = "leftcenter-rot"
                #at = c(-2, 0, 2)
            )
        )
        pdf(output_hs, width=10, height=10)
        draw(h1)
        dev.off()
    |]
    return ()
  where
    zeroOutDiagnol x = let mat = Mat.imap (\(i,j) x -> if i == j then 0 else x) $ DF._dataframe_data x
                       in x{DF._dataframe_data = mat}

sample :: Int -> [a] -> IO [a]
sample n xs | length xs <= n = return xs
            | otherwise = do
                g <- create
                V.toList . V.take n <$> uniformShuffle (V.fromList xs) g

-- | Random sample 10,000 cells.
sampleCells :: [CellCluster] -> IO [CellCluster]
sampleCells clusters
    | ratio >= 1 = return clusters
    | otherwise = do
        gen <- create
        forM clusters $ \c -> do
            s <- sampling gen ratio $ V.fromList $ _cluster_member c
            return $ c {_cluster_member = V.toList s}
  where
    n = foldl1' (+) $ map (length . _cluster_member) clusters
    ratio = 1 / (fromIntegral n / 10000) :: Double
    sampling gen frac v = V.take n' <$> uniformShuffle v gen
      where
        n' = max 100 $ truncate $ frac * fromIntegral (V.length v)

plotCells :: FilePath
          -> [(String, [(Double, Double)])]  -- ^ major clusters
          -> IO ()
plotCells output clusters = R.runRegion $ do
    myTheme
    [r| library("ggplot2")
        library("ggrastr")
        library("gridExtra")
        df <- data.frame(cls=cls_hs, x=xs_hs, y=ys_hs)
        fig <- ggplot(df, aes(x,y)) +
            geom_point_rast(size=0.2, alpha=0.8, stroke=0, shape=20, aes(colour = factor(cls))) +
            geom_text(data=data.frame(cx=cx_hs, cy=cy_hs),
                aes(cx, cy, label = label_hs), size=1.7
            ) +
            myTheme() + myFill() +
            labs(x = "UMAP-1", y = "UMAP-2") +
            theme(
                plot.margin=unit(c(2,0,2,2),"mm"),
                panel.grid.major = element_blank(),
                axis.ticks = element_blank(),
                axis.text = element_blank(),
                axis.line = element_blank(),
                panel.border = element_blank(),
                legend.position = "none"
            ) +
            annotate("text", x = min(xs_hs) + 0.1 * (max(xs_hs) - min(xs_hs)),
                y = max(ys_hs), size = 2.1,
                label = paste(as.character(nrow(df)), " nuclei")
            )
        ggsave(output_hs, fig, width=100, height=100, unit="mm", useDingbats=F)
    |]
    return ()
  where
    getPrefix x = T.unpack $ fst $ T.breakOn "_" $ T.pack x
    (cls, xs, ys) = unzip3 $ flip concatMap clusters $ \(cl, ps) ->
        uncurry (zip3 (repeat $ getPrefix cl)) $ unzip ps
    (label, cx, cy) = unzip3 $ flip map clusters $ \(cl, ps) ->
        let (x,y) = getCentroid ps in (cl, x, y)
    getCentroid ps = let (x,y) = unzip ps
                     in ( median medianUnbiased $ V.fromList x
                        , median medianUnbiased $ V.fromList y )

jaccardIndex :: FilePath -> [(T.Text, FilePath)] -> IO (DF.DataFrame Double)
jaccardIndex peakList inputs = do
    peaks <- runResourceT $ runConduit $ sourceFile peakList .| linesUnboundedAsciiC .|
        filterC (\x -> B.split '\t' x !! 3 == "Distal") .| mapC fromLine .| sinkList :: IO [BED3]
    let peakList = bedToTree undefined $ zip peaks $ repeat ()
    inputs' <-  forConcurrently inputs $ \(nm, fl) -> do
        ps <- fmap S.fromList $ runResourceT $ runConduit $ streamBedGzip fl .|
            concatMapC (\x -> map (showBed . fst) $ (x :: BED3) `queryIntersect` peakList) .| sinkList
        return (nm, ps)
    return $ DF.mkDataFrame (map fst inputs') (map fst inputs') $
        flip (parMap rdeepseq) inputs' $ \(_, x) -> flip map inputs' $ \(_, y) -> jaccard x y
  where
    jaccard x y = let o = fromIntegral $ S.size $ x `S.intersection` y
                      n = fromIntegral $ S.size $ x `S.union` y
                  in o / n

jointBuilder = do
    node "Merge_Peaks" [| \() -> do
        fetal <- lookupConfig "fetal_peak"
        adult <- lookupConfig "adult_peak"
        dir <- lookupConfig "output_dir"
        let output = dir <> "/merged.bed.gz"
        liftIO $ do
            fp <- runResourceT $ runConduit $ streamBedGzip fetal .| sinkList
            ap <- runResourceT $ runConduit $ streamBedGzip adult .| sinkList
            runResourceT $ runConduit $ yieldMany (mergePeaks ap fp) .| sinkFileBedGzip output
            return output
        |] $ return ()

    node "Jaccard_Similarity" [| \peak -> do
        fetalPeakDir <- lookupConfig "fetal_peak_dir"
        adultPeakDir <- lookupConfig "adult_peak_dir"
        dir <- lookupConfig "output_dir"
        let output = dir <> "/jaccard.tsv"
        liftIO $ do
            peakfiles <- do
                fls1 <- fmap (filter (".bed.gz" `T.isSuffixOf`)) $ shelly $ lsT fetalPeakDir
                fls2 <- fmap (filter (".bed.gz" `T.isSuffixOf`)) $ shelly $ lsT adultPeakDir
                let x = flip map fls1 $ \fl ->
                        (("f_" <>) $ fst $ T.breakOn ".bed.gz" $ snd $ T.breakOnEnd "/" fl, T.unpack fl)
                    y = flip map fls2 $ \fl ->
                        (("a_" <>) $ fst $ T.breakOn ".bed.gz" $ snd $ T.breakOnEnd "/" fl, T.unpack fl)
                return $ x <> y
            df <- jaccardIndex peak peakfiles
            DF.writeTable output (T.pack . show) df
            return output
        |] $ nCore .= 10
    node "Plot_Jaccard" [| \(tree, input) -> do
        dir <- lookupConfig "output_dir"
        adultAnnoFl <- lookupConfig "adult_annotation"
        fetalAnnoFl <- lookupConfig "fetal_annotation"
        liftIO $ do
            names <- getOrder tree
            adultAnno <- map (\(a, b) -> ("a_" <> a, "adult " <> b)) <$> readAnno adultAnnoFl
            fetalAnno <- map (\(a, b) -> ("f_" <> a, "fetal " <> b)) <$> readAnno fetalAnnoFl
            let anno = adultAnno <> fetalAnno
            df <- DF.orderDataFrame id . changeName anno . DF.transpose . changeName anno .
                DF.filterCols (\x _ -> "a_" `T.isPrefixOf` x) .
                DF.filterRows (\x _ -> "f_" `T.isPrefixOf` x)
                <$> DF.readTable input
            {-
            df <- changeName anno . DF.transpose .
                changeName anno .
                (`DF.csub` names) . (`DF.rsub` names) <$> DF.readTable input
                -}
            plotHeatmap (dir <> "/jaccard.pdf") df
        |] $ return ()
    path ["Output_Peaks", "Jaccard_Similarity"]
    ["Plot_Tree", "Jaccard_Similarity"] ~> "Plot_Jaccard"

    node "Output_Peaks" [| \peakFl -> do
        fetal <- lookupConfig "fetal_peak"
        adult <- lookupConfig "adult_peak"
        annoFl <- lookupConfig "gene_annotation"
        dir <- lookupConfig "output_dir"
        let output = dir <> "/peak_metadata.tsv"
        liftIO $ do
            fp <- bedToTree undefined . flip zip (repeat ()) <$> (runResourceT $ runConduit $
                streamBedGzip fetal .| sinkList :: IO [BED3])
            ap <- bedToTree undefined . flip zip (repeat ()) <$> (runResourceT $ runConduit $
                streamBedGzip adult .| sinkList :: IO [BED3])
            (o200, o2000, no) <- getCREContext annoFl peakFl
            let f x = B.intercalate "\t" [toLine (x :: BED3), cat, inFetal, inAdult]
                  where
                    cat | x' `S.member` o200 = "Promoter"
                        | x' `S.member` o2000 = "Promoter Proximal"
                        | x' `S.member` no = "Distal"
                        | otherwise = undefined
                    inAdult | ap `isIntersected` x = "yes"
                            | otherwise = "no"
                    inFetal | fp `isIntersected` x = "yes"
                            | otherwise = "no"
                    x' = showBed x
            runResourceT $ runConduit $ streamBedGzip peakFl .| mapC f .|
                unlinesAsciiC .| sinkFile output
            return output
        |] $ return ()
    path ["Merge_Peaks", "Output_Peaks"]

    uNode "Adult_RPKM_Prep" [| \peak -> do
        dir <- lookupConfig "adult_bed"
        liftIO $ do
            fls <- fmap (filter (".bed.gz" `T.isSuffixOf`)) $ shelly $ lsT dir
            return $ flip map fls $ \fl ->
                let cl = fst $ T.breakOn ".bed.gz" $ snd $ T.breakOnEnd "/" fl
                in (cl, T.unpack fl, peak)
        |]
    nodePar "Adult_RPKM" [| \(cl, fl, peakFl) -> do
        dir <- (<> "/RPKM/Adult/") <$> lookupConfig "output_dir"
        let output = dir <> T.unpack cl <> ".bin"
        liftIO $ do
            shelly $ mkdir_p dir
            peaks <- runResourceT $ runConduit $ streamBedGzip peakFl .| sinkList :: IO [BED3]
            counts <- runResourceT $ runConduit $ streamBedGzip fl .| rpkmBed peaks
            encodeFile output (counts :: U.Vector Double)
            return (cl, output)
        |] $ return ()
    path ["Merge_Peaks", "Adult_RPKM_Prep", "Adult_RPKM"]
    uNode "Fetal_RPKM_Prep" [| \peak -> do
        dir <- lookupConfig "fetal_bed"
        liftIO $ do
            fls <- fmap (filter (".bed.gz" `T.isSuffixOf`)) $ shelly $ lsT dir
            return $ flip map fls $ \fl ->
                let cl = fst $ T.breakOn ".bed.gz" $ snd $ T.breakOnEnd "/" fl
                in (cl, T.unpack fl, peak)
        |]
    nodePar "Fetal_RPKM" [| \(cl, fl, peakFl) -> do
        dir <- (<> "/RPKM/Fetal/") <$> lookupConfig "output_dir"
        let output = dir <> T.unpack cl <> ".bin"
        liftIO $ do
            shelly $ mkdir_p dir
            peaks <- runResourceT $ runConduit $ streamBedGzip peakFl .| sinkList :: IO [BED3]
            counts <- runResourceT $ runConduit $ streamBedGzip fl .| rpkmBed peaks
            encodeFile output (counts :: U.Vector Double)
            return (cl, output)
        |] $ return ()
    path ["Merge_Peaks", "Fetal_RPKM_Prep", "Fetal_RPKM"]

    node "Combine_RPKM" [| \(peakFl, fetal, adult) -> do
        dir <- (<> "/RPKM/") <$> lookupConfig "output_dir"
        let output = dir <> "/combined.tsv"
        liftIO $ do
            let mkSource fl = do
                    vec <- liftIO (decodeFile fl :: IO (U.Vector Double))
                    yieldMany $ map toShortest $ U.toList vec
                peaks = streamBedGzip peakFl .| mapC (showBed :: BED3 -> B.ByteString)
                source = sequenceSources $ peaks : (map (mkSource . snd) $ fetal <> adult)
                header = B.intercalate "\t" $ map (B.pack . T.unpack) $
                    "" : (map (("f_" <>) . fst) fetal <> map (("a_" <>) . fst) adult)
            runResourceT $ runConduit $ source .|
                (yield header >> mapC (B.intercalate "\t")) .| unlinesAsciiC .| sinkFile output
            return output
        |] $ return ()
    ["Merge_Peaks", "Fetal_RPKM", "Adult_RPKM"] ~> "Combine_RPKM"

    node "Normalize_RPKM" [| \input -> do
        dir <- (<> "/RPKM/") <$> lookupConfig "output_dir"
        let output = dir <> "/combined_norm.tsv"
        liftIO $ do
            df <- DF.map (logBase 2 . (+1)) <$> DF.readTable input
            DF.writeTable output (T.pack . show) $
                DF.fromMatrix (DF.rowNames df) (DF.colNames df) $ quantileNormalization $ DF._dataframe_data df
            return output
        |] $ memory .= 150
    path ["Combine_RPKM", "Normalize_RPKM"]

    uNode "Adult_Matrix_Prep" [| \peakFl -> do
        bedFl <- lookupConfig "adult_bed"
        liftIO $ do
            fls <- fmap (map T.unpack) $ shelly $ lsT bedFl
            return $ zip3 (map show [1..]) (repeat peakFl) $ chunksOf 10 fls
        |]
    nodePar "Adult_Matrix" [| \(i, peakFl, fls) -> do
        dir <- (<> "/Adult/" <> i <> "/") <$> lookupConfig "output_dir"
        liftIO $ do
            shelly $ mkdir_p dir
            peaks <- runResourceT $ runConduit $ streamBedGzip peakFl .| sinkList
            mkFeatMatrix dir peaks fls
            return dir
        |] $ nCore .= 4
    node "Merge_Adult_Matrix" [| \inputs -> do
        dir <- (<> "/Adult/") <$> lookupConfig "output_dir"
        let outputMat = dir <> "/adult.mat.gz"
            outputRow = dir <> "/adult_rownames.tsv.gz"
            outputCol = dir <> "/adult_features.tsv.gz"
        liftIO $ do
            mat <- fmap concatMatrix $ forM inputs $ \input -> mkSpMatrix id $ input <> "/mat.gz"
            saveMatrix outputMat id mat 
            mat' <- mkSpMatrix readInt outputMat
            runResourceT $ runConduit $ zipSources
                (mapM_ (\x -> sourceFile (x <> "/rownames.tsv.gz") .| multiple ungzip .| linesUnboundedAsciiC) inputs)
                (streamRows mat' .| mapC (fromJust . packDecimal . foldl1' (+) . map snd . snd)) .|
                mapC (\(a,b) -> a <> "\t" <> b) .| unlinesAsciiC .| gzip .| sinkFile outputRow
            cs <- map (fromJust . packDecimal) . U.toList <$> colSum mat'
            runResourceT $ runConduit $ zipSources
                (sourceFile (head inputs <> "/features.tsv.gz") .| multiple ungzip .| linesUnboundedAsciiC)
                (yieldMany cs) .|
                mapC (\(a,b) -> a <> "\t" <> b) .| unlinesAsciiC .| gzip .| sinkFile outputCol
            return (outputRow, outputCol, outputMat)
        |] $ return ()
    path ["Merge_Peaks", "Adult_Matrix_Prep", "Adult_Matrix", "Merge_Adult_Matrix"]

    uNode "Fetal_Matrix_Prep" [| \peakFl -> do
        bedFl <- lookupConfig "fetal_bed"
        liftIO $ do
            fls <- fmap (map T.unpack) $ shelly $ lsT bedFl
            return $ zip3 (map show [1..]) (repeat peakFl) $ chunksOf 10 fls
        |]
    nodePar "Fetal_Matrix" [| \(i, peakFl, fls) -> do
        dir <- (<> "/Fetal/" <> i <> "/") <$> lookupConfig "output_dir"
        liftIO $ do
            shelly $ mkdir_p dir
            peaks <- runResourceT $ runConduit $ streamBedGzip peakFl .| sinkList
            mkFeatMatrix dir peaks fls
            return dir
        |] $ nCore .= 4
    node "Merge_Fetal_Matrix" [| \inputs -> do
        dir <- (<> "/Fetal/") <$> lookupConfig "output_dir"
        let outputMat = dir <> "/fetal.mat.gz"
            outputRow = dir <> "/fetal_rownames.tsv.gz"
            outputCol = dir <> "/fetal_features.tsv.gz"
        liftIO $ do
            mat <- fmap concatMatrix $ forM inputs $ \input -> mkSpMatrix id $ input <> "/mat.gz"
            saveMatrix outputMat id mat 
            mat' <- mkSpMatrix readInt outputMat
            runResourceT $ runConduit $ zipSources
                (mapM_ (\x -> sourceFile (x <> "/rownames.tsv.gz") .| multiple ungzip .| linesUnboundedAsciiC) inputs)
                (streamRows mat' .| mapC (fromJust . packDecimal . foldl1' (+) . map snd . snd)) .|
                mapC (\(a,b) -> a <> "\t" <> b) .| unlinesAsciiC .| gzip .| sinkFile outputRow
            cs <- map (fromJust . packDecimal) . U.toList <$> colSum mat'
            runResourceT $ runConduit $ zipSources
                (sourceFile (head inputs <> "/features.tsv.gz") .| multiple ungzip .| linesUnboundedAsciiC)
                (yieldMany cs) .|
                mapC (\(a,b) -> a <> "\t" <> b) .| unlinesAsciiC .| gzip .| sinkFile outputCol
            return (outputRow, outputCol, outputMat)
        |] $ return ()
    path ["Merge_Peaks", "Fetal_Matrix_Prep", "Fetal_Matrix", "Merge_Fetal_Matrix"]

    node "Remove_Batch_Effects" [| \() -> do
        dir <- lookupConfig "output_dir"
        dims <- lookupConfig "spectral"
        barcodes <- lookupConfig "barcodes"
        let output = dir <> "/coEnbedding.tsv.gz"
        liftIO $ removeBatchEffects output dims barcodes
        return output
        |] $ return ()

    node "Cluster_Centroid" [| \input -> do
        dir <- lookupConfig "output_dir"
        barcodes <- lookupConfig "barcodes"
        adultClFl <- lookupConfig "adult_cluster"
        fetalClFl <- lookupConfig "cluster"
        let output1 = dir <> "/adult_centroid.tsv"
            output2 = dir <> "/fetal_centroid.tsv"
        liftIO $ do
            m <- readCoordinates barcodes input
            adultCl <- decodeFile adultClFl
            fetalCl <- decodeFile fetalClFl
            let (nm1, dat1) = unzip $ getClusterCentroid m adultCl
                (nm2, dat2) = unzip $ getClusterCentroid m fetalCl
            DF.writeTable output1 (T.pack . show) $ DF.mkDataFrame (map (T.pack . B.unpack) nm1)
                (map (T.pack . show) [1.. U.length (head dat1)]) $ map U.toList dat1
            DF.writeTable output2 (T.pack . show) $ DF.mkDataFrame (map (T.pack . B.unpack) nm2)
                (map (T.pack . show) [1.. U.length (head dat2)]) $ map U.toList dat2
            return (output1, output2)
        |] $ return ()
    node "Plot_Distance" [| \((adultFl, fetalFl), tree) -> do
        dir <- lookupConfig "output_dir"
        adultAnnoFl <- lookupConfig "adult_annotation"
        fetalAnnoFl <- lookupConfig "fetal_annotation"
        let f prefix df = DF.fromMatrix (map (prefix <>) $ DF.rowNames df)
                (DF.colNames df) $ DF._dataframe_data df
        liftIO $ do
            adultAnno <- map (\(a, b) -> ("a_" <> a, "adult " <> b)) <$> readAnno adultAnnoFl
            fetalAnno <- map (\(a, b) -> ("f_" <> a, "fetal " <> b)) <$> readAnno fetalAnnoFl
            names <- getOrder tree
            adult <- f "a_" <$> DF.readTable adultFl
            fetal <- f "f_" <$> DF.readTable fetalFl

            let dist = DF.orderDataFrame id $ changeName anno $ DF.transpose $ changeName anno $ crossDist fetal adult
                anno = adultAnno <> fetalAnno
            plotHeatmap (dir <> "/distance.pdf") dist
            {-
            let df = DF.rbind [adult, fetal]
                dist = changeName anno $ DF.transpose $ changeName anno $
                    (crossDist df df `DF.csub` names) `DF.rsub` names
                anno = adultAnno <> fetalAnno
            plotHeatmap (dir <> "/distance.pdf") dist
            -}
        |] $ return ()
    node "Plot_Tree" [| \(adultFl, fetalFl) -> do
        adultAnnoFl <- lookupConfig "adult_annotation"
        fetalAnnoFl <- lookupConfig "fetal_annotation"
        dir <- lookupConfig "output_dir"
        let f prefix df = DF.fromMatrix (map (prefix <>) $ DF.rowNames df)
                (DF.colNames df) $ DF._dataframe_data df
            output = dir <> "/tree.txt"
        liftIO $ do
            {-
            adultAnno <- readAnno adultAnnoFl
            fetalAnno <- readAnno fetalAnnoFl
            adult <- DF.transpose . changeName adultAnno . DF.transpose <$> DF.readTable adultFl
            fetal <- DF.transpose . changeName fetalAnno . DF.transpose <$> DF.readTable fetalFl
            -}

            adult <- f "a_" <$> DF.readTable adultFl
            fetal <- f "f_" <$> DF.readTable fetalFl

            plotTree' output adult fetal
            return output
        |] $ return ()
    path ["Remove_Batch_Effects", "Cluster_Centroid", "Plot_Tree"]
    ["Cluster_Centroid", "Plot_Tree"] ~> "Plot_Distance"

    node "UMAP" [| \input -> do
        dir <- (<> "/UMAP/") <$> lookupConfig "output_dir"
        barcodes <- lookupConfig "barcodes"
        adultClFl <- lookupConfig "adult_cluster"
        fetalClFl <- lookupConfig "cluster"
        let output1 = dir <> "/umap.txt"
            output2 = dir <> "/label.txt"
        liftIO $ do
            shelly $ mkdir_p dir
            adultCl <- decodeFile adultClFl
            fetalCl <- decodeFile fetalClFl
            cells <- fmap (concatMap (map _cell_barcode . _cluster_member)) $ sampleCells $ adultCl <> fetalCl
            dat <- readCoordinates barcodes input
            let coord = flip map cells $ \x -> U.toList $ M.lookupDefault undefined x dat
            withTemp Nothing $ \tmp -> do
                B.writeFile tmp $ B.unlines $ map (B.intercalate "\t" . map toShortest) coord
                shelly $ run_ "taiji-utils" ["viz", T.pack tmp, T.pack output1]
                B.writeFile output2 $ B.unlines cells
            return (output2, output1)
        |] $ return ()
    node "Plot_UMAP" [| \(label, umap) -> do
        dir <- (<> "/UMAP/") <$> lookupConfig "output_dir"
        adultClFl <- lookupConfig "adult_cluster"
        fetalClFl <- lookupConfig "cluster"
        let output = dir <> "/umap.pdf"
            getMappings f1 f2 = do
                c1 <- decodeFile f1 :: IO [CellCluster]
                c2 <- decodeFile f2
                return $ M.fromList $ concatMap (\x ->
                    zip (map _cell_barcode $ _cluster_member x) $ repeat $ B.unpack $ fst $ B.break (=='.') $ _cluster_name x) $ c1 <> c2
        liftIO $ do
            cls <- getMappings adultClFl fetalClFl
            coord <- map ((\[a, b] -> (a, b)) . map readDouble . B.split '\t') . B.lines <$> B.readFile umap
            let f x = let i = M.lookupDefault undefined x cls
                      in if "sample" `B.isInfixOf` x
                          then "F_" <> i
                          else "A_" <> i
            labels <- map f . B.lines <$> B.readFile label
            plotCells output $ map (\x -> (fst $ head x, map snd x)) $
                groupBy ((==) `on` fst) $ sortBy (comparing fst) $ zip labels coord
        |] $ return ()
    path ["Remove_Batch_Effects", "UMAP", "Plot_UMAP"]

    node "UMAP_All" [| \input -> do
        dir <- (<> "/UMAP_All/") <$> lookupConfig "output_dir"
        barcodes <- lookupConfig "barcodes"
        adultClFl <- lookupConfig "adult_cluster"
        fetalClFl <- lookupConfig "cluster"
        let output1 = dir <> "/umap.txt"
            output2 = dir <> "/label.txt"
        liftIO $ do
            shelly $ mkdir_p dir
            adultCl <- decodeFile adultClFl :: IO [CellCluster]
            fetalCl <- decodeFile fetalClFl
            dat <- readCoordinates barcodes input
            let coord = flip map cells $ \x -> U.toList $ M.lookupDefault undefined x dat
                cells = concatMap (map _cell_barcode . _cluster_member) $ adultCl <> fetalCl
            withTemp Nothing $ \tmp -> do
                B.writeFile tmp $ B.unlines $ map (B.intercalate "\t" . map toShortest) coord
                shelly $ run_ "taiji-utils" ["viz", T.pack tmp, T.pack output1]
                B.writeFile output2 $ B.unlines cells
            return (output2, output1)
        |] $ return ()
    path ["Remove_Batch_Effects", "UMAP_All"]