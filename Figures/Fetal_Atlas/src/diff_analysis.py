import os
os.environ['OPENBLAS_NUM_THREADS'] = '1'
os.environ['MKL_NUM_THREADS'] = '1'
import numpy as np
import itertools
from scipy.stats import chi2
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import log_loss
import math
import multiprocessing as mp
import sys

import gzip
import scipy as sp

nThread = 15

def chunkIt(seq, num):
    n = len(seq)
    step = math.ceil(n / num)
    last = 0
    while last < n:
        i = last + step
        if i <= n:
            yield (last, i)
        else:
            yield (last, n)
        last += step

class InputData:
    def __init__(self, filename):
        self.filename = filename
        with gzip.open(self.filename, mode='rt') as f:
            header = f.readline().strip()
            (m, n) = header.split(": ")[1].split(" x ")
            self.num_doc = int(m)
            self.num_terms = int(n)

    def __iter__(self):
        def convert(x):
            return (int(x[0]), float(x[1]))
        with gzip.open(self.filename, mode='rt') as f:
            f.readline()
            for line in f:
                yield [convert(item.split(",")) for item in line.strip().split("\t")[1:]]

def readMatrix(fl, binary=False):
    data = InputData(fl)
    indptr = [0]
    indices = []
    mat = []
    for row in iter(data):
        for (i,x) in row:
            indices.append(i)
            if (binary):
                mat.append(1)
            else:
                mat.append(x)
        indptr.append(len(indices))
    if (binary):
        mat = sp.sparse.csr_matrix((mat, indices, indptr), shape=(data.num_doc, data.num_terms), dtype=int)
    else:
        mat = sp.sparse.csr_matrix((mat, indices, indptr), shape=(data.num_doc, data.num_terms), dtype=float)
    return mat

def diffAnalysis(input, covariate, label, output):
    Y = readMatrix(input, binary=True)
    X = np.loadtxt(covariate, ndmin=2)
    z = np.loadtxt(label, ndmin=2)
    result = []
    _, n = Y.shape

    result_list = []
    pool = mp.Pool(nThread)
    for r in chunkIt(range(n), 100):
        idx = range(r[0], r[1])
        pool.apply_async(process,
            args=(Y[:, idx], X, z, r),
            callback = lambda x: result_list.append(np.column_stack(
                (range(x[1][0], x[1][1]), x[0])
                )),
            error_callback = error_handler
        )
    pool.close()
    pool.join()
    result = list(itertools.chain.from_iterable(result_list))
    np.savetxt(output, np.array(result),
        header='index\tp-value', fmt='%i\t%1.7e')

def error_handler(e):
    print(dir(e), "\n")
    print("-->{}<--".format(e.__cause__))

def process(Y, X, z, r):
    print(r)
    result = []
    _, n = Y.shape
    for i in range(n):
        y = np.ravel(Y[:, i].todense())
        if np.sum(y) == 0:
            p = 1
        else:
            p = likelihoodTest(X, y, z)
        result.append(p)
    return (result, r)

def likelihoodTest(X, Y, z):
    model = LogisticRegression(penalty="none", random_state=0, n_jobs=1,
        solver="lbfgs", multi_class='ovr', tol=1e-3, warm_start=False
        ).fit(X, Y)
    reduced = -log_loss(Y, model.predict_proba(X), normalize=False)

    X = np.concatenate((X, z), axis=1)
    model = LogisticRegression(penalty="none", random_state=0, n_jobs=1,
        solver="lbfgs", multi_class='ovr', tol=1e-3, warm_start=False
        ).fit(X, Y)
    full = -log_loss(Y, model.predict_proba(X), normalize=False)
    chi = -2 * (reduced - full)
    return chi2.sf(chi, 1)

if __name__ == "__main__":
    diffAnalysis(sys.argv[1], sys.argv[2], sys.argv[3], sys.argv[4])