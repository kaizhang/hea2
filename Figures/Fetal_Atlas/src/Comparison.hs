{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell   #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE QuasiQuotes       #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE GADTs #-}
module Comparison (comBuilder, cellTypeGroup) where

import HEA

import Statistics.Quantile
import Data.Conduit.Zlib (multiple, ungzip, gzip)
import qualified Taiji.Utils.DataFrame as DF
import Bio.RealWorld.GENCODE
import Control.Monad.State.Strict
import Control.Arrow
import qualified Data.HashMap.Strict as HM
import qualified Data.Vector as V
import qualified Data.Vector.Unboxed as U
import Statistics.Regression (olsRegress)
import Statistics.Correlation (pearson)
import AI.Clustering.Hierarchical
import Data.Either
import Control.Parallel.Strategies (parMap, rdeepseq)
import qualified Data.Text as T
import Bio.Data.Bed.Utils
import Shelly hiding (FilePath, path)
import Data.Int
import Data.Conduit.Internal (zipSinks, zipSources)
import Bio.Data.Bed
import Control.Monad.ST (runST)
import qualified Data.Matrix as Mat
import qualified Data.Matrix.Unboxed as MU
import qualified Data.Matrix.Mutable as MatM
import qualified Data.ByteString.Char8 as B
import qualified Data.ByteString as BS
import qualified Data.HashMap.Strict as M
import qualified Data.Map.Strict as Map
import qualified Data.HashSet as S
import Data.List.Split (chunksOf)
import Data.List.Ordered (nubSort)
import Bio.Utils.Functions
import Taiji.Utils.Plot
import Statistics.Sample (mean, meanVarianceUnb, varianceUnbiased)
import Data.Binary
import System.Random.MWC.Distributions (uniformShuffle)
import System.Random.MWC (create)
import Text.Wrap
import Control.DeepSeq (force)
import Data.Conduit.ConcurrentMap (concurrentMapM_numCaps)
import AI.Clustering.Hierarchical (euclidean)

import Data.Conduit.Async
import Control.Concurrent.Async (forConcurrently)

import Taiji.Prelude
import Taiji.Pipeline.SC.ATACSeq.Functions.Utils (mkCountMat, groupCells)
import qualified Language.R                        as R
import Taiji.Utils.Matrix
import           Language.R.QQ
import Language.R.HExp
import qualified Language.R.HExp as H
import qualified Data.Vector.SEXP as R
import Data.Vector.SEXP (toList)

cellTypeGroup :: [(String, [T.Text], [T.Text])]
cellTypeGroup =
    [ ( "Fibroblast"
      , ["C5", "C7.2", "C7.3", "C7.4", "C7.5", "C7.6", "C7.10", "C25.1", "C25.2", "C29", "C33"]
      , ["C1.1", "C1.2", "C1.3", "C1.6", "C1.7", "C8.1"] )
    , ( "Skeletal Myocyte"
      , ["C19.1", "C19.4", "C19.5"]
      , ["C6.1", "C6.2"] )
    , ( "Satellite"
      , ["C19.2", "C19.3"]
      , ["C1.8"] )
    , ("Macrophage"
      , ["C15.1", "C15.2", "C15.3", "C15.4", "C15.5", "C15.6", "C15.7", "C15.8"]
      , ["C2.1", "C2.2", "C2.3"] )
    , ( "Cardiomyocyte"
      , ["C6.1", "C6.2"]
      , ["C3.1", "C3.2"] )
    , ( "Endothelial"
      , ["C12.1", "C12.2", "C12.3", "C12.4", "C12.5", "C12.6", "C12.7", "C12.8", "C12.9"]
      , ["C4.1", "C4.2", "C4.3", "C4.4", "C4.5", "C4.6", "C4.7", "C4.8", "C4.9"] )
    --, ("Smooth Muscle", ["C5.1","C5.2","C5.3","C5.4","C5.5","C5.6","C5.7","C5.8","C5.9","C5.10","C5.11", "C7.1", "C7.2"])
    , ( "Intestinal Epithelial"
      , ["C16.1", "C16.2", "C16.3", "C16.4", "C16.5"]
      , ["C9.1", "C9.2", "C9.3", "C9.4", "C9.5", "C9.6", "C9.7", "C9.8", "C9.9"] )
    , ( "Pancreatic Epithelial"
      , ["C24.2", "C24.3", "C24.5"]
      , ["C28", "C11"] )
    , ( "T Lymphocyte"
      , ["C18.1", "C18.3", "C18.7", "C30", "C14"]
      , ["C12.1", "C12.2", "C12.3", "C12.4"] )
    , ( "Neuroendocrine"
      , ["C24.8", "C24.7"]
      , ["C15.1", "C15.2", "C15.3", "C15.4", "C15.5", "C15.6"] )
    , ( "Pulmonary Epithelial"
      , ["C8.1", "C8.2", "C36"]
      , ["C16.1", "C16.2", "C16.3", "C16.4", "C16.5"] )
    , ( "Gastric Epithelial"
      , ["C24.1", "C24.9"]
      , ["C18.1", "C18.2", "C18.3"] )
    , ( "Neuron"
      , ["C9.1", "C9.2", "C11.1", "C11.2", "C11.3", "C11.4", "C11.5", "C11.6", "C17.3", "C17.4", "C17.7", "C20.1", "C20.2", "C20.3", "C20.4", "C21.1", "C21.2", "C31", "C32", "C34"]
      , ["C20.1", "C20.2", "C20.3", "C20.4", "C20.5"] )
    , ( "Adrenal Cortical"
      , ["C1.1", "C1.2"]
      , ["C22.1", "C22.2", "C22.3", "C22.4"] )
    , ( "B Lymphocyte"
      , ["C18.2", "C18.6", "C18.8"]
      , ["C25.1", "C25.2"] )
    , ( "Hepatocyte"
      , ["C4"]
      , ["C26"] )
    , ( "Glial"
      , ["C17.8", "C17.9", "C22.1", "C22.2", "C22.3", "C22.4", "C22.5", "C35"]
      , ["C27.1", "C27.2", "C27.3", "C30", "C19.1"] )
    ]


organizeDF :: (T.Text -> String) -> DF.DataFrame Double -> DF.DataFrame Double
organizeDF fn d = d `DF.csub` names
  where
    names = reverse $ concat $ map (f . fst) $ flatten $ hclust Ward (V.fromList grp) (euclidean `on` snd)
    f xs = flatten $ hclust Ward (V.fromList xs) (euclidean `on` (d `DF.cindex`))
    grp = map (\x -> (x, averageDF $ d `DF.csub` x)) $ groupBy ((==) `on` fn) $ sortBy (comparing fn) $ DF.colNames d
    averageDF df = V.map (/n) $ foldl1' (V.zipWith (+)) cols
      where
        cols = Mat.toColumns $ DF._dataframe_data df
        n = fromIntegral $ length cols
        
plotCor :: FilePath
        -> Double
        -> Double
        -> (T.Text -> String)
        -> DF.DataFrame Double
        -> IO()
plotCor output width height grpFn df = R.runRegion $ do
    valMatrix <- toRMatrix df
    [r| library("ggplot2")
        library("ComplexHeatmap")
        library("RColorBrewer")
        library("viridis")
        library("circlize")
        library("ggplotify")
        rwb <- colorRampPalette(colors = rev(brewer.pal(7,"RdBu")))(99)
        rsplit <- factor(rsplit_hs, levels = groups_hs)
        csplit <- factor(csplit_hs, levels = groups_hs)
        h1 <- Heatmap( valMatrix_hs, 
            row_split=rsplit, column_split=csplit,
            column_gap = unit(0.01, "mm"), row_gap = unit(0.01, "mm"),
            col=rwb, name="mat",
            show_row_names=F, row_names_side="left", show_column_names=F,
            row_title_rot = 0,
            column_title_rot = 90,
            cluster_rows=F, cluster_columns=F,
            column_title_gp = gpar(fontsize=5), row_title_gp = gpar(fontsize=5),
            column_names_gp = gpar(fontsize=5), row_names_gp = gpar(fontsize=5),
            #rect_gp = gpar(col= "white"),
            layer_fun = function(j, i, x, y, width, height, fill, slice_r, slice_c) {
                if(slice_r == slice_c) {
                    grid.rect(gp = gpar(lwd = 0.5, col = colors_hs[slice_r], fill = "transparent"))
                }
            },
            top_annotation = HeatmapAnnotation(
                bar = anno_block(gp = gpar(lwd=0.3, fill = colors_hs), height = unit(1, "mm"))
            ),
            left_annotation = rowAnnotation(
                bar = anno_block(gp = gpar(lwd=0.3, fill = colors_hs), width = unit(1, "mm"))
            ),
            heatmap_legend_param = list(
                title = "correlation",
                title_gp = gpar(fontsize = 6),
                #title_position = "left-top",
                direction = "horizontal",
                labels_gp = gpar(fontsize = 5)
            )
        )
        pdf(output_hs, width=width_hs, height=height_hs)
        draw(h1, heatmap_legend_side="bottom")
        dev.off()
    |]
    return ()
  where
    groups = map head $ group rsplit
    rsplit = map grpFn $ DF.rowNames df
    csplit = map grpFn $ DF.colNames df
    colors :: [String]
    colors = ["#873e00","#0168dc","#99d934","#001f7b","#00d06c","#ff0964","#008055","#e72000","#01528b","#ffb32a","#e69dff","#93a200","#ff66c1","#afa27e","#500034","#ff558e","#900045"]

plotCor' :: FilePath
        -> Double
        -> Double
        -> (T.Text -> String)
        -> DF.DataFrame Double
        -> IO()
plotCor' output width height grpFn df = R.runRegion $ do
    valMatrix <- toRMatrix df
    [r| library("ggplot2")
        library("ComplexHeatmap")
        library("RColorBrewer")
        library("viridis")
        library("circlize")
        library("ggplotify")
        rwb <- colorRampPalette(colors = rev(brewer.pal(7,"RdBu")))(99)
        rsplit <- factor(rsplit_hs, levels = groups_hs)
        csplit <- factor(csplit_hs, levels = groups_hs)
        h1 <- Heatmap( valMatrix_hs, 
            row_split=rsplit, column_split=csplit,
            column_gap = unit(0.01, "mm"), row_gap = unit(0.01, "mm"),
            col=rwb, name="mat",
            show_row_names=T, row_names_side="left", show_column_names=T,
            row_title_rot = 0,
            column_title_rot = 90,
            cluster_rows=F, cluster_columns=F,
            column_title_gp = gpar(fontsize=5), row_title_gp = gpar(fontsize=5),
            column_names_gp = gpar(fontsize=5), row_names_gp = gpar(fontsize=5),
            #rect_gp = gpar(col= "white"),
            layer_fun = function(j, i, x, y, width, height, fill, slice_r, slice_c) {
                if(slice_r == slice_c) {
                    grid.rect(gp = gpar(lwd = 0.5, col = colors_hs[slice_r], fill = "transparent"))
                }
            },
            top_annotation = HeatmapAnnotation(
                bar = anno_block(gp = gpar(lwd=0.3, fill = colors_hs), height = unit(1, "mm"))
            ),
            left_annotation = rowAnnotation(
                bar = anno_block(gp = gpar(lwd=0.3, fill = colors_hs), width = unit(1, "mm"))
            ),
            heatmap_legend_param = list(
                title = "correlation",
                title_gp = gpar(fontsize = 6),
                #title_position = "left-top",
                direction = "horizontal",
                labels_gp = gpar(fontsize = 5)
            )
        )
        pdf(output_hs, width=width_hs, height=height_hs)
        draw(h1, heatmap_legend_side="bottom")
        dev.off()
    |]
    return ()
  where
    groups = map head $ group rsplit
    rsplit = map grpFn $ DF.rowNames df
    csplit = map grpFn $ DF.colNames df
    colors :: [String]
    colors = ["#873e00","#0168dc","#99d934","#001f7b","#00d06c","#ff0964","#008055","#e72000","#01528b","#ffb32a","#e69dff","#93a200","#ff66c1","#afa27e","#500034","#ff558e","#900045", "black"]


readDataMatrix :: FilePath -> IO ([(B.ByteString, U.Vector Double)], [(B.ByteString, U.Vector Double)])
readDataMatrix fl = do
    ((fidx, fetal), (aidx, adult)) <- runResourceT $ runConduit $
        sourceFile fl .| linesUnboundedAsciiC .| mapC (tail . B.split '\t') .| sink
    return ( zip fidx $ MU.toRows $ MU.fromColumns fetal
           , zip aidx $ MU.toRows $ MU.fromColumns adult )
  where
    sink = do
        Just header <- headC
        let (adult, fetal) = partition (("a_" `B.isPrefixOf`) . fst) $ zip header [0..]
        res <- mapC (f (map snd fetal, map snd adult)) .| sinkList
        let (fetal', adult') = unzip res
        return ((map fst fetal, fetal'), (map fst adult, adult'))
      where
        f (fidx, aidx) x =
            let v = U.fromList $ map readDouble x
            in ( U.fromList $ map (v U.!) fidx
               , U.fromList $ map (v U.!) aidx )

crossCor :: [(B.ByteString, U.Vector Double)]
         -> [(B.ByteString, U.Vector Double)]
         -> DF.DataFrame Double
crossCor xs ys = DF.mkDataFrame (map (T.pack . B.unpack . fst) xs)
    (map (T.pack . B.unpack . fst) ys) $ flip (parMap rdeepseq) xs $ \(_, x) ->
        flip map ys $ \(_, y) -> pearson $ U.filter (\(a,b) -> a > 2 || b > 2) $ U.zip x y

diagonizeDF :: (a -> Double) -> DF.DataFrame a -> DF.DataFrame a
diagonizeDF getV = DF.reorderRows f
  where
    f xs = map (\x -> (x, M.findWithDefault undefined x xs')) names
      where
        xs' = M.fromList xs
        names = map fst $ sortBy (comparing snd) $ map (\(a,b) -> (a, g b)) xs
        g xs = (i, negate v)
          where
            v = z V.! i
            i = V.maxIndex z
            z = scale $ V.map getV xs

comBuilder = do
    node "RPKM_Cor" [| \input -> do
        dir <- (<> "/RPKM/") <$> lookupConfig "output_dir"
        let output = dir <> "/cor.tsv"
            output2 = dir <> "/null_cor.tsv"
        liftIO $ do
            (fetal, adult) <- readDataMatrix input
            DF.writeTable output (T.pack . show) $ crossCor adult fetal
            gen <- create
            fetal' <- forConcurrently fetal $ \(x, y) -> do
                y' <- uniformShuffle y gen
                return (x, y')
            adult' <- forConcurrently adult $ \(x, y) -> do
                y' <- uniformShuffle y gen
                return (x, y')
            DF.writeTable output2 (T.pack . show) $ crossCor adult' fetal'
            return (output, output2)
        |] $ do
            memory .= 100
            nCore .= 10
    node "Plot_Cor" [| \(input, nullInput) -> do
        dir <- lookupConfig "output_dir"
        adultAnnoFl <- lookupConfig "adult_annotation"
        fetalAnnoFl <- lookupConfig "fetal_annotation"
        liftIO $ do
            adultAnno <- map (\(a, b) -> ("a_" <> a, b)) <$> readAnno adultAnnoFl
            fetalAnno <- map (\(a, b) -> ("f_" <> a, b)) <$> readAnno fetalAnnoFl
            df <- DF.readTable input
            let anno = adultAnno <> fetalAnno
                cellTypeGroup' = map (\(a, b, c) -> (a, map ("f_"<>) b, map ("a_"<>) c)) cellTypeGroup
                grpMap = M.fromList $ flip concatMap cellTypeGroup' $ \(a,b,c) ->
                    map (\x -> (T.pack $ B.unpack $ fromMaybe "Other" $ lookup (B.pack $ T.unpack x) anno, a)) b <>
                    map (\x -> (T.pack $ B.unpack $ fromMaybe "Other" $ lookup (B.pack $ T.unpack x) anno, a)) c
                df' = DF.transpose $
                    organizeDF fn $ changeName anno $ DF.transpose $ 
                    organizeDF fn $ changeName anno $
                    (df `DF.rsub` concatMap (^._3) cellTypeGroup') `DF.csub` concatMap (^._2) cellTypeGroup'
                fn x = M.findWithDefault "Other" x grpMap
            print $ DF.dim df'
            plotCor (dir <> "/cor.pdf") 5.5 5 fn df'


            let df'' = DF.transpose $
                    organizeDF fn $ changeName anno $ DF.transpose $ 
                    organizeDF fn $ changeName anno df
                rorder = DF.rowNames df' <> filter (\x -> fn x == "Other") (DF.rowNames df'')
                corder = DF.colNames df' <> filter (\x -> fn x == "Other") (DF.colNames df'')
                df''' = (df'' `DF.csub` corder) `DF.rsub` rorder
            plotCor' (dir <> "/all_cor.pdf") 9 9 fn df'''

            {-
            let nullDistr = U.fromList $ sort $ catMaybes $ Mat.toList $
                    Mat.generate (DF.dim df) $ \(i, j) -> 
                        let j' = V.maxIndex $ DF._dataframe_data df `Mat.takeRow` i
                            i' = V.maxIndex $ DF._dataframe_data df `Mat.takeColumn` j
                        in if i' == i || j == j'
                            then Nothing
                            else Just $ DF._dataframe_data df Mat.! (i, j)
                getP x = let i = fromIntegral $ binarySearch nullDistr x
                             n = fromIntegral $ U.length nullDistr
                         in (n - i) / n
                df' = DF.orderDataFrame fst $ DF.zip df $ DF.map getP df
                df1 = diagonizeDF fst $
                    DF.filterRows (\_ vec -> V.minimum (V.map snd vec) < 0.001) df'
                df2 = diagonizeDF fst $
                    DF.filterRows (\_ vec -> let x = V.minimum (V.map snd vec) in x >= 0.001 && x < 0.01) df'
                df3 = diagonizeDF fst $
                    DF.filterRows (\_ vec -> V.minimum (V.map snd vec) >= 0.01) df'
            plotCor' (dir <> "/cor.pdf") 9 9 [df1, df2, df3]
                    -}
            let vals = map V.maximum $ Mat.toRows $ DF._dataframe_data df'
                labels = map fn $ DF.rowNames df'
                output = dir <> "/violine.pdf"
            R.runRegion $ do
                myTheme
                [r| library("ggplot2")
                    library(dplyr)
                    df <- data.frame(label=labels_hs, vals=vals_hs)
                    summary <- df%>%group_by(label)%>%summarise(Median=median(vals))
                    fig <- ggplot(summary, aes(reorder(label,Median), Median)) +
                        #geom_jitter(size=0.2) +
                        geom_col() +
                        myTheme() + myFill() + myColour() +
                        theme( axis.text.x = element_text(size=5, angle = 90, vjust=0.5, hjust=1)) +
                        labs(x = "", y = "Median(PCC)")
                    ggsave(output_hs, fig, width=85, height=55, unit="mm", useDingbats=F)
                |]
                return ()
        |] $ return ()
    path ["Normalize_RPKM", "RPKM_Cor", "Plot_Cor"]