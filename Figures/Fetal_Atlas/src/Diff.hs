{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell   #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE QuasiQuotes       #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE GADTs #-}
module Diff
    ( -- * Cross clade
      diffSkm
    , diffStl

    , diffBuilder
    ) where

import HEA

import Statistics.Quantile
import qualified Taiji.Utils.DataFrame as DF
import Bio.RealWorld.GENCODE
import Control.Monad.State.Strict
import GHC.Generics (Generic)
import Data.Binary (Binary(..))
import Control.Arrow
import Data.Conduit.Internal (zipSinks)
import qualified Data.Vector as V
import qualified Data.Vector.Unboxed as U
import AI.Clustering.Hierarchical hiding (normalize)
import Data.Either
import Data.Tuple
import Control.Parallel.Strategies (parMap, rdeepseq)
import qualified Data.Text as T
import Shelly hiding (FilePath, path)
import Data.Int
import Bio.Data.Bed
import Control.Monad.ST (runST)
import qualified Data.Matrix as Mat
import AI.Clustering.KMeans
import qualified Data.ByteString.Char8 as B
import qualified Data.HashMap.Strict as M
import qualified Data.HashSet as S
import Data.List.Split (chunksOf)
import Data.List.Ordered (nubSort)
import Bio.Utils.Functions
import Taiji.Utils.Plot
import Taiji.Utils
import Statistics.Sample (mean, meanVarianceUnb)
import System.Random.MWC.Distributions (uniformShuffle)
import System.Random.MWC (create)
import Text.Wrap
import Data.Binary (decodeFile)

import Taiji.Prelude

import Data.Vector.SEXP (toList)
import qualified Language.R                        as R
import qualified Language.R.HExp as H
import           Language.R.QQ

import Joint

readMotifs :: FilePath -> IO (B.ByteString, S.HashSet B.ByteString)
readMotifs fl = do
    s <- decodeFile fl
    return (nm, s)
  where
    nm = B.pack $ T.unpack $ fst $ T.breakOn ".bin" $ snd $ T.breakOnEnd "/" $ T.pack fl

enrichment :: S.HashSet B.ByteString -> [B.ByteString] -> Int -> IO (Double, Double)
enrichment motifs fg nBg' = if e >= 0
    then R.runRegion $ do
        p <- [r| phyper(oFg_hs - 1, oBg_hs, nBg_hs - oBg_hs, nFg_hs, lower.tail=F, log.p=T) / log(10)
            |]
        R.unSomeSEXP p $ \x -> case H.hexp x of
            H.Real x'  -> return (e, head $ toList x')
    else R.runRegion $ do
        p <- [r| phyper(oFg_hs, oBg_hs, nBg_hs - oBg_hs, nFg_hs, lower.tail=T, log.p=T) / log(10)
            |]
        R.unSomeSEXP p $ \x -> case H.hexp x of
            H.Real x'  -> return (e, head $ toList x')
  where
    e = logBase 2 $ (oFg / nFg) / (oBg / nBg)
    oFg = fromIntegral $ length $ filter (`S.member` motifs) fg :: Double
    oBg = fromIntegral $ S.size motifs :: Double
    nFg = fromIntegral $ length fg :: Double
    nBg = fromIntegral nBg' :: Double

toBed :: B.ByteString -> BED3
toBed x = let [chr, x'] = B.split ':' $ x
              [s, e] = B.split '-' x'
           in BED3 chr (readInt s) (readInt e)

mkName :: BED3 -> T.Text
mkName x = chr <> ":" <> s <> "-" <> e
  where
    chr = T.pack $ B.unpack $ x^.chrom 
    s = T.pack $ show $ x^.chromStart
    e = T.pack $ show $ x^.chromEnd

mkPeak :: T.Text -> BED3
mkPeak x = asBed (B.pack $ T.unpack chr) (read $ T.unpack a) (read $ T.unpack b)
  where
    [chr, x'] = T.splitOn ":" x
    [a,b] = T.splitOn "-" x'

plotHeatmap :: FilePath
            -> [DF.DataFrame Double]
            -> IO ()
plotHeatmap output dfs = R.runRegion $ do
    mat <- toRMatrix $ DF.rbind dfs
    [r| library("ggplot2")
        library("ComplexHeatmap")
        library("RColorBrewer")
        library("viridis")
        library(scales)

        pdf(output_hs, width=5.5, height=5)

        split <- factor(split_hs, levels = groups_hs)
        hm <- Heatmap(mat_hs, col=viridis(99), use_raster=T,
            row_split=split, column_title = NULL,
            row_title_rot=0, cluster_row_slices=F,
            row_title_gp = gpar(fontsize = 5),
            show_row_names = F,
            show_column_names = T,
            show_row_dend = F, show_column_dend = F,
            clustering_method_columns="ward.D2",
            cluster_rows=F, cluster_columns=F,
            row_names_gp = gpar(fontsize=4),
            column_names_gp = gpar(fontsize=5),
            heatmap_legend_param = list(
                title = "log2(RPKM+1)",
                title_gp = gpar(fontsize = 6),
                title_position = "leftcenter-rot",
                #direction = "horizontal",
                labels_gp = gpar(fontsize = 5)
            )
        )
        draw(hm)
        dev.off()
    |]
    return ()
  where
    groups = nub split
    split = concat $ flip map dfs $ \x -> 
        let n = fst $ DF.dim x
        in replicate n (printf "N = %d" n :: String)

diffStl= do
    namespace "Stl" $ diffBuilder2 "Stl" ["C19.2", "C19.3"] ["C1.8"]
    ["Merge_Fetal_Matrix", "Merge_Adult_Matrix"] ~> "Stl_Get_Peak_List"
    ["Normalize_RPKM"] ~> "Stl_Get_Table"

diffSkm= do
    namespace "Skm" $ diffBuilder2 "Skm" ["C19.1", "C19.4", "C19.5"] ["C6.1", "C6.2"]
    ["Merge_Fetal_Matrix", "Merge_Adult_Matrix"] ~> "Skm_Get_Peak_List"
    ["Normalize_RPKM"] ~> "Skm_Get_Table"

{-

("12. Peripheral Nervous",["f_C17.4","f_C17.11","f_C17.2"])
("10. Fetal Neuron",["f_C17.7","f_C20.1","f_C32","f_C9.2","f_C9.1","f_C20.4","f_C20.2","f_C20.3","f_C11.2","f_C11.4","f_C11.1","f_C11.6","f_C11.5","f_C11.3","f_C34","f_C25.2","f_C21.1","f_C31","f_C21.2","f_C22.3","f_C35","f_C22.1","f_C22.4","f_C22.2","f_C22.5"])
("14. Neuron/Glia",["a_C19.1","a_C19.2","f_C17.8","f_C17.9","a_C27.1","a_C30","a_C27.2","a_C27.3","a_C20.5","a_C20.3","a_C20.4","a_C20.1","a_C20.2"])
("15. Adult Mesenchymal",["a_C1.5","a_C1.4","a_C1.2","a_C1.1","a_C1.8","a_C1.7","a_C8.1","a_C8.2","a_C1.6","a_C5.5","a_C24","a_C1.3","a_C5.9","a_C5.4","a_C5.8","a_C5.10","a_C5.11","a_C5.6","a_C5.1","a_C5.3","a_C5.2","a_C5.7"])
("7. Mesenchymal",["f_C33","f_C5","f_C7.6","a_C1.9","a_C13.9","a_C13.8","a_C13.7","a_C13.5","a_C13.1","a_C13.6","a_C13.4","a_C13.2","a_C13.3","f_C12.9","f_C7.8","f_C25.1","f_C7.2","f_C7.3","f_C7.5","f_C29","f_C7.4","f_C7.10","f_C7.1","f_C7.7","a_C7.1","a_C7.2"])
-}


diffBuilder2 :: String
            -> [T.Text]    -- ^ fetal
            -> [T.Text]    -- ^ adult
            -> Builder ()
diffBuilder2 prefix fetalGroup adultGroup = do
    uNode "Get_Table" [| return |]
    node "Get_Peak_List" [| \((fridx, fcidx, fetalMat), (aridx, acidx, adultMat)) -> do
        fetalPeakDir <- lookupConfig "fetal_peak_dir"
        adultPeakDir <- lookupConfig "adult_peak_dir"
        dir <- fmap (\x -> x <> "/" <> prefix) $ lookupConfig "output_dir"
        let output = dir <> "/peak_index.txt"
            outputF = dir <> "/fetal.mat.gz"
            outputA = dir <> "/adult.mat.gz"
        liftIO $ do
            shelly $ mkdir_p dir
            let getPeakFiles groups x = do
                    files <- fmap (filter (".bed.gz" `T.isSuffixOf`)) $ shelly $ lsT x
                    return $ map T.unpack $
                        filter ((`elem` groups) . fst . T.breakOn ".bed.gz" . snd . T.breakOnEnd "/") files
            p1 <- getPeakFiles fetalGroup fetalPeakDir 
            p2 <- getPeakFiles adultGroup adultPeakDir
            beds <- fmap concat $ forM (p1 <> p2) $ \fl -> runResourceT $
                runConduit $ streamBedGzip fl .| sinkList :: IO [BED3]
            let f i x = score .~ Just i $ convert x :: BED
            peaks <- runResourceT $ runConduit $
                sourceFile fcidx .| multiple ungzip .| linesUnboundedAsciiC .|
                mapC (toBed . head . B.split '\t') .| sinkList
            idx <- runResourceT $ runConduit $ yieldMany (zipWith f [0..] peaks) .|
                intersectBed beds .| mapC (fromJust . (^.score)) .| sinkList
            (selectCols idx <$> mkSpMatrix id fetalMat) >>= saveMatrix outputF id
            (selectCols idx <$> mkSpMatrix id adultMat) >>= saveMatrix outputA id
            B.writeFile output $ B.unlines $ map (B.pack . show) idx
            return ((fridx, fcidx, outputF), (aridx, acidx, outputA), output)
        |] $ return ()

    node "Covariate" [| \((fridx, _, _), (aridx, _, _), _) -> do
        dir <- fmap (\x -> x <> "/" <> prefix) $ lookupConfig "output_dir"
        fetalClFl <- lookupConfig "cluster"
        adultClFl <- lookupConfig "adult_cluster"
        let output1 = dir <> "/covariate.txt"
            output2 = dir <> "/labels.txt"
        liftIO $ do
            fetalCl <- decodeFile fetalClFl
            adultCl <- decodeFile adultClFl
            let fetalBarcodes = S.fromList $ concatMap (map _cell_barcode . _cluster_member) $
                    filter ((`elem` fetalGroup) . T.pack . B.unpack . _cluster_name) fetalCl
                adultBarcodes = S.fromList $ concatMap (map _cell_barcode . _cluster_member) $
                    filter ((`elem` adultGroup) . T.pack . B.unpack . _cluster_name) adultCl
                f [bc, x] =
                    let rc = toShortest $ logBase 10 $ readDouble x
                    in if bc `S.member` fetalBarcodes
                        then ([rc, "0", "1"], "1")
                        else ([rc, "0", "0"], "0")
                g [bc, x] =
                    let rc = toShortest $ logBase 10 $ readDouble x
                    in if bc `S.member` adultBarcodes
                        then ([rc, "1", "1"], "0")
                        else ([rc, "1", "0"], "0")
                sink1 = mapC (B.unwords . fst) .| unlinesAsciiC .| sinkFile output1
                sink2 = mapC snd .| unlinesAsciiC .| sinkFile output2
            runResourceT $ runConduit $
                ( (sourceFile fridx .| multiple ungzip .| linesUnboundedAsciiC .| mapC (f . B.split '\t')) >>
                  (sourceFile aridx .| multiple ungzip .| linesUnboundedAsciiC .| mapC (g . B.split '\t')) ) .|
                zipSinks sink1 sink2
            return (output1, output2)
        |] $ return ()
    path ["Get_Peak_List", "Covariate"]

    node "Prep_Input" [| \(((fridx,_,fetalMat), (aridx,_,adultMat), _), (cov, lab)) -> do
        fetalClFl <- lookupConfig "cluster"
        adultClFl <- lookupConfig "adult_cluster"
        liftIO $ do
            fetalCl <- decodeFile fetalClFl
            adultCl <- decodeFile adultClFl
            let fetalBarcodes = S.fromList $ concatMap (map _cell_barcode . _cluster_member) $
                    filter ((`elem` fetalGroup) . T.pack . B.unpack . _cluster_name) fetalCl
                adultBarcodes = S.fromList $ concatMap (map _cell_barcode . _cluster_member) $
                    filter ((`elem` adultGroup) . T.pack . B.unpack . _cluster_name) adultCl

            mat1 <- mkSpMatrix readInt fetalMat
            count1 <- fmap (U.map fromIntegral) $ runResourceT $ runConduit $ streamRows mat1 .|
                filterC ((`S.member` fetalBarcodes) . fst) .| colSumC (_num_col mat1)
            s1 <- fmap (/1000000) $ runResourceT $ runConduit $ sourceFile fridx .| multiple ungzip .|
                linesUnboundedAsciiC .| mapC (B.split '\t') .| 
                filterC ((`S.member` fetalBarcodes) . head) .| mapC (readDouble . last) .| sumC
            let rpm1 = U.map (logBase 2 . (+1.1) . (/s1)) count1

            mat2 <- mkSpMatrix readInt adultMat
            count2 <- fmap (U.map fromIntegral) $ runResourceT $ runConduit $ streamRows mat2 .|
                filterC ((`S.member` adultBarcodes) . fst) .| colSumC (_num_col mat2)
            s2 <- fmap (/1000000) $ runResourceT $ runConduit $ sourceFile aridx .| multiple ungzip .|
                linesUnboundedAsciiC .| mapC (B.split '\t') .| 
                filterC ((`S.member` adultBarcodes) . head) .| mapC (readDouble . last) .| sumC
            let rpm2 = U.map (logBase 2 . (+1.1) . (/s2)) count2
                fd = U.zipWith (\x y -> logBase 2 $ x / y) rpm1 rpm2 :: U.Vector Double
            return $ zip (repeat (fetalMat, adultMat, cov, lab)) $ chunksOf 5000 $
                U.toList $ U.filter ((>1) . abs . snd) $ U.imap (\i x -> (i, x)) fd
        |] $ return ()
    nodePar "Diff_Analysis" [| \((fetalMat, adultMat, cov, lab), idx) -> do
        dir <- fmap (\x -> x <> "/" <> prefix) $ lookupConfig "output_dir"
        let output = dir <> "/" <> show (fst $ head idx) <> ".txt"
        liftIO $ withTemp (Just "./") $ \tmp -> do
            mat1 <- mkSpMatrix id fetalMat
            mat2 <- mkSpMatrix id adultMat
            saveMatrix tmp id $ selectCols (map fst idx) $ concatMatrix [mat1, mat2]
            shelly $ run_ "python" ["src/diff_analysis.py", T.pack tmp, T.pack cov, T.pack lab, T.pack output]
            return output
        |] $ nCore .= 15
    ["Get_Peak_List", "Covariate"] ~> "Prep_Input"
    path ["Prep_Input", "Diff_Analysis"]
    node "Get_Diff_Peaks" [| \(((_, fcidx, _), (_, _, _), idx), input2, results, tableFl) -> do
        dir <- fmap (\x -> x <> "/" <> prefix) $ lookupConfig "output_dir"
        let output1 = dir <> "/fetal.bed" 
            output2 = dir <> "/adult.bed" 
            output3 = dir <> "/fetal.tsv" 
            output4 = dir <> "/adult.tsv" 
        liftIO $ do
            idx' <- U.fromList . map readInt . B.lines <$> B.readFile idx
            peaks <- runResourceT $ runConduit $
                sourceFile fcidx .| multiple ungzip .| linesUnboundedAsciiC .|
                mapC (toBed . head . B.split '\t') .| sinkVector
            res <- forM (zip (map snd input2) results) $ \(fd, fl) -> do
                r <- map ((\[a,b] -> (readInt a, readDouble b)) . B.split '\t') . tail . B.lines <$> B.readFile fl
                return $ zipWith (\(_, p) (i, v) -> (((idx' U.! i, peaks V.! (idx' U.! i)), v), p))
                    (sortBy (comparing fst) r) fd
            let (fetal, adult) = partition ((>0) . snd) $ map fst $ V.toList $
                    filterFDR 0.01 $ V.fromList $ concat res
                cells = map ("f_" <>) fetalGroup <> map ("a_" <>) adultGroup
            readTable' tableFl (map (fst . fst) fetal) >>=
                DF.writeTable output3 (T.pack . show) . (`DF.csub` cells)
            readTable' tableFl (map (fst . fst) adult) >>=
                DF.writeTable output4 (T.pack . show) . (`DF.csub` cells)
            writeBed output1 $ map (snd . fst) fetal
            writeBed output2 $ map (snd . fst) adult
            return ((output1, output2), (output3, output4))
        |] $ return ()
    ["Get_Peak_List", "Prep_Input", "Diff_Analysis", "Get_Table"] ~> "Get_Diff_Peaks"
    node "GREAT" [| \((fetal, adult), _) -> liftIO $ do
        go1 <- filterGREAT <$> great fetal
        go2 <- filterGREAT <$> great adult
        return (go1, go2)
        |] $ return ()
    path ["Get_Diff_Peaks", "GREAT"]

    node "Motif_Enrich" [| \(((fetal, adult), _), ((_,pk,_), _, _)) -> do
        dir <- fmap (\x -> x <> "/" <> prefix) $ lookupConfig "output_dir"
        let output1 = dir <> "/fetal_motif.txt"
            output2 = dir <> "/adult_motif.txt"
        motifDir <- lookupConfig "motif_dir"
        liftIO $ do
            nPeak <- runResourceT $ runConduit $ sourceFile pk .| multiple ungzip .| linesUnboundedAsciiC .| lengthC
            fls <- fmap (filter (not . ("mouse" `T.isInfixOf`) . snd . T.breakOnEnd "/")) $ shelly $ lsT motifDir
            fetal' <- map showBed <$> (readBed fetal :: IO [BED3])
            adult' <- map showBed <$> (readBed adult :: IO [BED3])
            (mFetal, mAdult) <- fmap unzip $ forM fls $ \fl -> do
                (motif, set) <- readMotifs $ T.unpack fl
                (e1, p1) <- enrichment set fetal' nPeak
                (e2, p2) <- enrichment set adult' nPeak
                return (((motif, e1), p1), ((motif, e2), p2))
            B.writeFile output1 $ B.unlines $
                map (\((g, f), p) -> B.intercalate "\t" [g, toShortest f, toShortest p]) $
                sortBy (comparing snd) mFetal
            B.writeFile output2 $ B.unlines $
                map (\((g, f), p) -> B.intercalate "\t" [g, toShortest f, toShortest p]) $
                sortBy (comparing snd) mAdult
            return (output1, output2)
        |] $ return ()
    ["Get_Diff_Peaks", "Get_Peak_List"] ~> "Motif_Enrich"

    node "Plot_Diff_Peaks" [| \(_, (fetal, adult)) -> do
        dir <- fmap (\x -> x <> "/" <> prefix) $ lookupConfig "output_dir"
        adultAnnoFl <- lookupConfig "adult_annotation"
        fetalAnnoFl <- lookupConfig "fetal_annotation"
        let output = dir <> "/diff.pdf"
        liftIO $ do
            fanno <- map (first ("f_" <>)) <$> readAnno fetalAnnoFl
            aanno <- map (first ("a_" <>)) <$> readAnno adultAnnoFl
            let anno = fanno <> aanno
            dfFetal <- DF.readTable fetal
            dfAdult <- DF.readTable adult
            [rowF, rowA] <- create >>= sampling' 5000 (map DF.rowNames [dfFetal, dfAdult])
            let df1 = DF.reorderRows (DF.orderByHClust id) $ changeName anno $ dfFetal `DF.rsub` rowF
                df2 = DF.reorderRows (DF.orderByHClust id) $ changeName anno $ dfAdult `DF.rsub` rowA
            plotHeatmap output [df1, df2]
        |] $ return ()
    ["Get_Diff_Peaks"] ~> "Plot_Diff_Peaks"

    node "Summary" [| \(((fpk, apk), _), (fgo, ago), (fmotif, amotif)) -> do
        dir <- fmap (\x -> x <> "/" <> prefix) $ lookupConfig "output_dir"
        let output = dir <> "/summary.tsv"
        liftIO $ do
            nfpk <- length <$> (readBed fpk :: IO [BED3])
            napk <- length <$> (readBed apk :: IO [BED3])
            let fgo' = B.intercalate "," $
                    map (\x -> B.pack $ printf "%s (%.2f)" (B.unpack $ _great_term_name x) $
                    negate $ logBase 10 $ _great_pvalue x) $ take 5 fgo
                ago' = B.intercalate "," $
                    map (\x -> B.pack $ printf "%s (%.2f)" (B.unpack $ _great_term_name x) $
                    negate $ logBase 10 $ _great_pvalue x) $ take 5 ago
                f [a, _, b] = let (motif, cl) = B.breakEnd (=='+') a in (cl, (B.init motif, negate $ readDouble b))
                g a@(_, p1) b@(_, p2) = if p1 > p2 then a else b
            fmotif' <- B.intercalate "," .
                map (\(a, b) -> B.pack $ printf "%s (%.2f)" (B.unpack a) b) .
                take 3 . sortBy (flip (comparing snd)) . M.elems .
                M.fromListWith g . map (f . B.split '\t') . B.lines <$> B.readFile fmotif
            amotif' <- B.intercalate "," .
                map (\(a, b) -> B.pack $ printf "%s (%.2f)" (B.unpack a) b) .
                take 3 . sortBy (flip (comparing snd)) . M.elems .
                M.fromListWith g . map (f . B.split '\t') . B.lines <$> B.readFile amotif
            B.writeFile output $ B.unlines $ map (B.intercalate "\t") $ 
                [ ["Fetal", B.pack $ show nfpk, fgo', fmotif']
                , ["Adult", B.pack $ show napk, ago', amotif'] ]
        |] $ return ()
    ["Get_Diff_Peaks", "GREAT", "Motif_Enrich"] ~> "Summary"

data InputData = InputData
    { _group_name :: String
    , _row_index_fetal :: FilePath 
    , _column_index_fetal :: FilePath 
    , _matrix_fetal :: FilePath 
    , _row_index_adult :: FilePath 
    , _column_index_adult :: FilePath 
    , _matrix_adult :: FilePath 
    , _fetal_cells :: [T.Text]
    , _adult_cells :: [T.Text]
    , _rpkm_file :: FilePath
    , _peak_index :: FilePath
    , _covariate :: FilePath
    , _label :: FilePath
    } deriving (Generic, Show, Eq, Ord)

instance Binary InputData

celltypeGroups :: [(String, [T.Text], [T.Text])]
celltypeGroups = 
    [ ("1_Tlym", ["C30","C18.1","C18.7","C18.3"], ["C12.4","C12.2","C12.1","C12.3"])
    , ("16_Adr", ["C1.2","C1.1"], ["C22.3","C22.1","C22.2","C22.4"])
    , ("2_Blym", ["C18.8","C18.2","C18.6"], ["C25.1","C25.2"])
    , ("6_Cam", ["C6.1","C6.2"], ["C3.1","C3.2"])
    , ("3_Myel", ["C15.6", "C15.4", "C15.5", "C15.1", "C15.2", "C15.3", "C15.8"], ["C29", "C2.1", "C2.2"])
    , ("4_Endo", ["C12.6","C12.4","C12.1","C12.5","C12.2","C12.3","C12.7","C12.8"], ["C4.9","C4.7","C4.4","C4.2","C4.6","C4.8","C4.3","C4.1","C4.5"])
    , ("21_Pulm", ["C8.1","C36","C8.2"], ["C16.3","C16.5","C16.4","C16.1","C16.2"])
    , ("20_Panc", ["C24.2","C24.3","C24.6","C3.3","C24.4","C24.5"], ["C11", "C28"])
    , ("8_Hep", ["C4"], ["C26"])
    , ("19_GI", ["C24.1","C24.9"], ["C18.3","C18.1","C18.2"])
    , ("18_NEC", ["C24.7","C24.8"], ["C15.6","C15.5","C15.3","C15.4","C15.1","C15.2"])
    , ("17_Int", ["C16.4","C16.5","C16.2","C16.3","C16.1"], ["C9.7","C9.4","C9.5","C9.6","C9.1","C9.3","C9.8","C9.9","C9.2"])
    , ("Mesenchymal", ["C33","C5","C7.6", "C12.9","C7.8","C25.1","C7.2","C7.3","C7.5","C29","C7.4","C7.10","C7.1","C7.7"]
        , [ "C1.9","C13.9","C13.8","C13.7","C13.5","C13.1","C13.6","C13.4","C13.2"
          , "C13.3", "C7.1", "C7.2", "C1.5", "C1.4","C1.2","C1.1","C1.8","C1.7","C8.1"
          , "C8.2","C1.6","C5.5","C24","C1.3","C5.9","C5.4","C5.8","C5.10","C5.11"
          , "C5.6","C5.1","C5.3","C5.2","C5.7"])
    , ("Neuronal", ["C9.2","C9.1","C11.2","C11.4","C11.1","C11.6","C11.5","C11.3","C21.1","C21.2","C34","C31", "C20.1", "C20.2", "C20.3", "C20.4", "C32"]
        , ["C20.3", "C20.4", "C20.1", "C20.2"])
    , ("Glial", ["C35", "C22.1", "C22.2", "C22.3", "C22.4", "C22.5"]
        , ["C27.1", "C27.2", "C27.3", "C30"])
    ]


diffBuilder :: [(String, [T.Text], [T.Text])] -> Builder ()
diffBuilder groups = do
    uNode "Diff_Prep" [| \((fridx, fcidx, fmat), (aridx, acidx, amat), rpkmNorm) -> do
        fetalAnnoFl <- lookupConfig "fetal_annotation"
        adultAnnoFl <- lookupConfig "adult_annotation"
        liftIO $ do
            fetalAnno <- readAnno fetalAnnoFl
            adultAnno <- readAnno adultAnnoFl
            forM groups $ \(grpName, fetalGroup, adultGroup) -> do
                let fetalCells = map (\x -> fromMaybe (error $ show x) $ lookup (B.pack $ T.unpack x) fetalAnno) fetalGroup
                    adultCells = map (\x -> fromMaybe (error $ show x) $ lookup (B.pack $ T.unpack x) adultAnno) adultGroup
                print (fetalCells, adultCells)
                return $ InputData grpName fridx fcidx fmat aridx acidx amat fetalGroup adultGroup rpkmNorm
                    "" "" ""
        |]
    nodePar "Get_Peak_List" [| \input@InputData{..} -> do
        fetalPeakDir <- lookupConfig "fetal_peak_dir"
        adultPeakDir <- lookupConfig "adult_peak_dir"
        dir <- fmap (\x -> x <> "/Diff/" <> _group_name) $ lookupConfig "output_dir"
        let output = dir <> "/peak_index.txt"
            outputF = dir <> "/fetal.mat.gz"
            outputA = dir <> "/adult.mat.gz"
        liftIO $ do
            shelly $ mkdir_p dir
            let getPeakFiles groups x = do
                    files <- fmap (filter (".bed.gz" `T.isSuffixOf`)) $ shelly $ lsT x
                    return $ map T.unpack $
                        filter ((`elem` groups) . fst . T.breakOn ".bed.gz" . snd . T.breakOnEnd "/") files
            p1 <- getPeakFiles _fetal_cells fetalPeakDir 
            p2 <- getPeakFiles _adult_cells adultPeakDir
            beds <- fmap concat $ forM (p1 <> p2) $ \fl -> runResourceT $
                runConduit $ streamBedGzip fl .| sinkList :: IO [BED3]
            let f i x = score .~ Just i $ convert x :: BED
            peaks <- runResourceT $ runConduit $
                sourceFile _column_index_fetal .| multiple ungzip .| linesUnboundedAsciiC .|
                mapC (toBed . head . B.split '\t') .| sinkList
            idx <- runResourceT $ runConduit $ yieldMany (zipWith f [0..] peaks) .|
                intersectBed beds .| mapC (fromJust . (^.score)) .| sinkList
            (selectCols idx <$> mkSpMatrix id _matrix_fetal) >>= saveMatrix outputF id
            (selectCols idx <$> mkSpMatrix id _matrix_adult) >>= saveMatrix outputA id
            B.writeFile output $ B.unlines $ map (B.pack . show) idx
            return input{_peak_index = output, _matrix_fetal = outputF, _matrix_adult = outputA}
        |] $ return ()

    nodePar "Covariate" [| \input@InputData{..} -> do
        dir <- fmap (\x -> x <> "/Diff/" <> _group_name) $ lookupConfig "output_dir"
        fetalClFl <- lookupConfig "cluster"
        adultClFl <- lookupConfig "adult_cluster"
        let output1 = dir <> "/covariate.txt"
            output2 = dir <> "/labels.txt"
        liftIO $ do
            fetalCl <- decodeFile fetalClFl
            adultCl <- decodeFile adultClFl
            let fetalBarcodes = S.fromList $ concatMap (map _cell_barcode . _cluster_member) $
                    filter ((`elem` _fetal_cells) . T.pack . B.unpack . _cluster_name) fetalCl
                adultBarcodes = S.fromList $ concatMap (map _cell_barcode . _cluster_member) $
                    filter ((`elem` _adult_cells) . T.pack . B.unpack . _cluster_name) adultCl
                f [bc, x] =
                    let rc = toShortest $ logBase 10 $ readDouble x
                    in if bc `S.member` fetalBarcodes
                        then ([rc, "0", "1"], "1")
                        else ([rc, "0", "0"], "0")
                g [bc, x] =
                    let rc = toShortest $ logBase 10 $ readDouble x
                    in if bc `S.member` adultBarcodes
                        then ([rc, "1", "1"], "0")
                        else ([rc, "1", "0"], "0")
                sink1 = mapC (B.unwords . fst) .| unlinesAsciiC .| sinkFile output1
                sink2 = mapC snd .| unlinesAsciiC .| sinkFile output2
            runResourceT $ runConduit $
                ( (sourceFile _row_index_fetal .| multiple ungzip .| linesUnboundedAsciiC .| mapC (f . B.split '\t')) >>
                  (sourceFile _row_index_adult .| multiple ungzip .| linesUnboundedAsciiC .| mapC (g . B.split '\t')) ) .|
                zipSinks sink1 sink2
            return $ input{_covariate = output1, _label = output2}
        |] $ return ()

    nodePar "Prep_Input" [| \input@InputData{..} -> do
        fetalClFl <- lookupConfig "cluster"
        adultClFl <- lookupConfig "adult_cluster"
        liftIO $ do
            fetalCl <- decodeFile fetalClFl
            adultCl <- decodeFile adultClFl
            let fetalBarcodes = S.fromList $ concatMap (map _cell_barcode . _cluster_member) $
                    filter ((`elem` _fetal_cells) . T.pack . B.unpack . _cluster_name) fetalCl
                adultBarcodes = S.fromList $ concatMap (map _cell_barcode . _cluster_member) $
                    filter ((`elem` _adult_cells) . T.pack . B.unpack . _cluster_name) adultCl
            mat1 <- mkSpMatrix readInt _matrix_fetal
            count1 <- fmap (U.map fromIntegral) $ runResourceT $ runConduit $ streamRows mat1 .|
                filterC ((`S.member` fetalBarcodes) . fst) .| colSumC (_num_col mat1)
            s1 <- fmap (/1000000) $ runResourceT $ runConduit $ sourceFile _row_index_fetal .| multiple ungzip .|
                linesUnboundedAsciiC .| mapC (B.split '\t') .| 
                filterC ((`S.member` fetalBarcodes) . head) .| mapC (readDouble . last) .| sumC
            let rpm1 = U.map (logBase 2 . (+1.1) . (/s1)) count1

            mat2 <- mkSpMatrix readInt _matrix_adult
            count2 <- fmap (U.map fromIntegral) $ runResourceT $ runConduit $ streamRows mat2 .|
                filterC ((`S.member` adultBarcodes) . fst) .| colSumC (_num_col mat2)
            s2 <- fmap (/1000000) $ runResourceT $ runConduit $ sourceFile _row_index_adult .| multiple ungzip .|
                linesUnboundedAsciiC .| mapC (B.split '\t') .| 
                filterC ((`S.member` adultBarcodes) . head) .| mapC (readDouble . last) .| sumC
            let rpm2 = U.map (logBase 2 . (+1.1) . (/s2)) count2
                fd = U.zipWith (\x y -> logBase 2 $ x / y) rpm1 rpm2 :: U.Vector Double
            return $ zip (repeat input) $ chunksOf 5000 $
                U.toList $ U.filter ((>1) . abs . snd) $ U.imap (\i x -> (i, x)) fd
        |] $ return ()
    uNode "Diff_Analysis_Prep" [| return . concat |]
    nodePar "Diff_Analysis" [| \(input@InputData{..}, idx) -> do
        dir <- fmap (\x -> x <> "/Diff/" <> _group_name) $ lookupConfig "output_dir"
        let output = dir <> "/" <> show (fst $ head idx) <> ".txt"
        liftIO $ withTemp (Just "./") $ \tmp -> do
            mat1 <- mkSpMatrix id _matrix_fetal
            mat2 <- mkSpMatrix id _matrix_adult
            saveMatrix tmp id $ selectCols (map fst idx) $ concatMatrix [mat1, mat2]
            shelly $ run_ "python" ["src/diff_analysis.py", T.pack tmp, T.pack _covariate, T.pack _label, T.pack output]
            return (input, output)
        |] $ nCore .= 15
    node "Get_Diff_Peaks" [| \(foldChanges, pvalues) -> do
        dir <- fmap (<> "/Diff/") $ lookupConfig "output_dir"
        let results = map (\x -> (fst $ head x, map snd x)) $
                groupBy ((==) `on` fst) $ zipWith (\(i1, fd) (i2, p) ->
                    if (_group_name i1 /= _group_name i2)
                        then error "mismatch"
                        else (i1, (fd, p)) ) foldChanges pvalues
        forM results $ \(input@InputData{..}, result) -> liftIO $ do
            let output1 = dir <> _group_name <> "/fetal.bed"
                output2 = dir <> _group_name <> "/adult.bed"
                output3 = dir <> _group_name <> "/fetal.tsv"
                output4 = dir <> _group_name <> "/adult.tsv"
            idx' <- U.fromList . map readInt . B.lines <$> B.readFile _peak_index
            peaks <- runResourceT $ runConduit $
                sourceFile _column_index_fetal .| multiple ungzip .| linesUnboundedAsciiC .|
                mapC (toBed . head . B.split '\t') .| sinkVector
            res <- forM result $ \(fd, fl) -> do
                r <- map ((\[a,b] -> (readInt a, readDouble b)) . B.split '\t') . tail . B.lines <$> B.readFile fl
                return $ zipWith (\(_, p) (i, v) -> (((idx' U.! i, peaks V.! (idx' U.! i)), v), p))
                    (sortBy (comparing fst) r) fd
            let (fetal, adult) = partition ((>0) . snd) $ map fst $ V.toList $
                    filterFDR 0.01 $ V.fromList $ concat res
                cells = map ("f_" <>) _fetal_cells <> map ("a_" <>) _adult_cells
            readTable' _rpkm_file (map (fst . fst) fetal) >>=
                DF.writeTable output3 (T.pack . show) . (`DF.csub` cells)
            readTable' _rpkm_file (map (fst . fst) adult) >>=
                DF.writeTable output4 (T.pack . show) . (`DF.csub` cells)
            writeBed output1 $ map (snd . fst) fetal
            writeBed output2 $ map (snd . fst) adult
            return (input, ((output1, output2), (output3, output4)))
        |] $ return ()
    path ["Diff_Prep", "Get_Peak_List", "Covariate", "Prep_Input", "Diff_Analysis_Prep", "Diff_Analysis"]
    ["Diff_Analysis_Prep", "Diff_Analysis"] ~> "Get_Diff_Peaks"

    nodePar "GREAT" [| \(InputData{..}, ((fetal, adult), _)) -> liftIO $ do
        go1 <- filterGREAT <$> great fetal
        go2 <- filterGREAT <$> great adult
        return (_group_name, (go1, go2))
        |] $ return ()
    path ["Get_Diff_Peaks", "GREAT"]

    nodePar "Motif_Enrich" [| \(InputData{..}, ((fetal, adult), _)) -> do
        dir <- fmap (\x -> x <> "/Diff/" <> _group_name) $ lookupConfig "output_dir"
        let output1 = dir <> "/fetal_motif.txt"
            output2 = dir <> "/adult_motif.txt"
        motifDir <- lookupConfig "motif_dir"
        liftIO $ do
            nPeak <- runResourceT $ runConduit $ sourceFile _column_index_fetal .|
                multiple ungzip .| linesUnboundedAsciiC .| lengthC
            fls <- fmap (filter (not . ("mouse" `T.isInfixOf`) . snd . T.breakOnEnd "/")) $ shelly $ lsT motifDir
            fetal' <- map showBed <$> (readBed fetal :: IO [BED3])
            adult' <- map showBed <$> (readBed adult :: IO [BED3])
            (mFetal, mAdult) <- fmap unzip $ forM fls $ \fl -> do
                (motif, set) <- readMotifs $ T.unpack fl
                (e1, p1) <- enrichment set fetal' nPeak
                (e2, p2) <- enrichment set adult' nPeak
                return (((motif, e1), p1), ((motif, e2), p2))
            B.writeFile output1 $ B.unlines $
                map (\((g, f), p) -> B.intercalate "\t" [g, toShortest f, toShortest p]) $
                sortBy (comparing snd) mFetal
            B.writeFile output2 $ B.unlines $
                map (\((g, f), p) -> B.intercalate "\t" [g, toShortest f, toShortest p]) $
                sortBy (comparing snd) mAdult
            return (_group_name, (output1, output2))
        |] $ return ()
    ["Get_Diff_Peaks"] ~> "Motif_Enrich"

    nodePar "Plot_Diff_Peaks" [| \(InputData{..}, (_, (fetal, adult))) -> do
        dir <- fmap (\x -> x <> "/Diff/" <> _group_name) $ lookupConfig "output_dir"
        adultAnnoFl <- lookupConfig "adult_annotation"
        fetalAnnoFl <- lookupConfig "fetal_annotation"
        let output = dir <> "/diff.pdf"
        liftIO $ do
            fanno <- map (first ("f_" <>)) <$> readAnno fetalAnnoFl
            aanno <- map (first ("a_" <>)) <$> readAnno adultAnnoFl
            let anno = fanno <> aanno
            dfFetal <- DF.readTable fetal
            dfAdult <- DF.readTable adult
            [rowF, rowA] <- create >>= sampling' 5000 (map DF.rowNames [dfFetal, dfAdult])
            let df1 = DF.reorderRows (DF.orderByHClust id) $ changeName anno $ dfFetal `DF.rsub` rowF
                df2 = DF.reorderRows (DF.orderByHClust id) $ changeName anno $ dfAdult `DF.rsub` rowA
            plotHeatmap output [df1, df2]
        |] $ return ()
    ["Get_Diff_Peaks"] ~> "Plot_Diff_Peaks"

    node "Summary" [| \(peaks, gos, motifs) -> do
        output <- fmap (\x -> x <> "/Diff/summary.tsv") $ lookupConfig "output_dir"
        liftIO $ do
            res <- forM (zip3 peaks gos motifs) $ \((InputData{..}, ((fpk, apk), _)), (nm, (fgo, ago)), (nm', (fmotif, amotif)))  -> do
                when (_group_name /= nm || nm /= nm') $ error "Mismatch"
                nfpk <- length <$> (readBed fpk :: IO [BED3])
                napk <- length <$> (readBed apk :: IO [BED3])
                let fgo' = B.intercalate "," $
                        map (\x -> B.pack $ printf "%s (%.2f)" (B.unpack $ _great_term_name x) $
                        negate $ logBase 10 $ _great_pvalue x) $ take 5 fgo
                    ago' = B.intercalate "," $
                        map (\x -> B.pack $ printf "%s (%.2f)" (B.unpack $ _great_term_name x) $
                        negate $ logBase 10 $ _great_pvalue x) $ take 5 ago
                    f [a, _, b] = let (motif, cl) = B.breakEnd (=='+') a in (cl, (B.init motif, negate $ readDouble b))
                    g a@(_, p1) b@(_, p2) = if p1 > p2 then a else b
                fmotif' <- B.intercalate "," .
                    map (\(a, b) -> B.pack $ printf "%s (%.2f)" (B.unpack a) b) .
                    take 3 . sortBy (flip (comparing snd)) . M.elems .
                    M.fromListWith g . map (f . B.split '\t') . B.lines <$> B.readFile fmotif
                amotif' <- B.intercalate "," .
                    map (\(a, b) -> B.pack $ printf "%s (%.2f)" (B.unpack a) b) .
                    take 3 . sortBy (flip (comparing snd)) . M.elems .
                    M.fromListWith g . map (f . B.split '\t') . B.lines <$> B.readFile amotif
                return [ B.pack _group_name, B.pack $ show nfpk, B.pack $ show napk
                       , fgo', ago', fmotif', amotif' ]
            let header = [ "Group name"
                         , "# of fetal specific cCREs"
                         , "# of adult specific cCREs"
                         , "Top enriched GO terms (fetal)"
                         , "Top enriched GO terms (adult)"
                         , "Top enriched motifs (fetal)"
                         , "Top enriched motifs (adult)" ]
            B.writeFile output $ B.unlines $ map (B.intercalate "\t") $ header : res
        |] $ return ()
    ["Get_Diff_Peaks", "GREAT", "Motif_Enrich"] ~> "Summary"