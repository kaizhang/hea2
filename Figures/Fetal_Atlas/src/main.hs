{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell   #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE QuasiQuotes       #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE PartialTypeSignatures #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE RankNTypes #-}
import HEA

import Statistics.Quantile
import Data.Conduit.Zlib
import Control.Arrow
import qualified Taiji.Utils.DataFrame as DF
import Data.Conduit.Internal (zipSinks, zipSources)
import qualified Data.Vector as V
import qualified Data.Vector.Unboxed as U
import qualified Data.Vector.Mutable as VM
import qualified Data.Text as T
import qualified Data.Text.IO as T
import Bio.Data.Bed.Types
import Bio.Data.Bed
import Control.Monad.ST (runST)
import qualified Data.Matrix as Mat
import qualified Data.HashSet as S
import qualified Data.Map.Strict as Map
import qualified Data.ByteString.Char8 as B
import qualified Data.HashMap.Strict as M
import Data.List.Ordered (nubSort)
import Bio.Utils.Functions
import Taiji.Utils
import Shelly hiding (FilePath, path)
import Data.Binary
import System.Random.MWC.Distributions (uniformShuffle)
import System.Random.MWC (create)
import Statistics.Sample (mean)
import Data.Int

import Taiji.Prelude
import           Language.R.QQ
import qualified Language.R                        as R
import Language.R.HExp
import qualified Language.R.HExp as H
import qualified Data.Vector.SEXP as R

import Joint
import Comparison
import Phylogeny
import Phylogeny3
import Diff

tissueGroups :: [(String, String)]
tissueGroups = 
    [ ("Human_brain", "Frontal Cortex")
    , ("adipose_omentum", "Adipose Omentum")
    , ("adrenal_gland", "Adrenal Gland")
    , ("artery_aorta", "Aorta")
    , ("artery_tibial", "Tibial Artery")
    , ("colon_sigmoid", "Sigmoid Colon")
    , ("colon_transverse", "Transverse Colon")
    , ("esophagus_ge_junction", "Esophagus GE Junction")
    , ("esophagus_mucosa", "Esophagus Mucosa")
    , ("esophagus_muscularis", "Esophagus Muscularis")
    , ("heart_atrial_appendage", "RA Appendage")
    , ("heart_la", "Left Atrium")
    , ("heart_lv", "Left Ventricle")
    , ("heart_ra", "Right Atrium")
    , ("heart_rv", "Right Ventricle")
    , ("islet", "Pancreatic Islet")
    , ("liver", "Liver")
    , ("lung", "Lung")
    , ("mammary_tissue", "Breast")
    , ("muscle", "Skeletal Muscle")
    , ("nerve_tibial", "Tibial Nerve")
    , ("ovary", "Ovary")
    , ("pancreas", "Body of Pancreas")
    , ("skin", "Skin Suprapublic")
    , ("skin_sun_exposed", "Skin Lower Leg")
    , ("small_intestine", "Small Intestine")
    , ("stomach", "Stomach")
    , ("thyroid", "Thyroid")
    , ("uterus", "Uterus")
    , ("vagina", "Vagina") ]

plotNumCells :: FilePath -> FilePath -> IO ()
plotNumCells input output = do
    tissue <- runResourceT $ runConduit $ sourceFile input .| multiple ungzip .|
        linesUnboundedAsciiC .|
        mapC (lookup' . B.unpack . extractTissueName . fst . B.break (=='+')) .|
        sinkList
    let (names, vals) = unzip $ M.toList $ M.fromListWith (+) $ zip tissue $ repeat (1 :: Double)
    R.runRegion $ do
        myTheme
        [r| library("ggplot2")
            df <- data.frame(name=names_hs, val=log10(vals_hs))
            fig <- ggplot(df, aes(x=reorder(name, val), y=val)) +
                geom_col() +
                coord_flip() +
                myTheme() + myFill() +
                labs(x = "", y = "number of cells")
            ggsave(output_hs, fig, width=55, height=65, unit="mm", useDingbats=F)
        |]
        return ()
  where
    lookup' x = fromMaybe x $ lookup x tissueGroups

extractTissueName :: B.ByteString -> B.ByteString
extractTissueName = f . B.init . fst . B.breakEnd (=='_') . B.init . fst .
    B.breakEnd (=='_')
  where
    f x | x == "LungMap" = "lung"
        | "CARE" `B.isInfixOf` x = B.intercalate "_" $ take 2 $ B.split '_' x
        | otherwise = x

readMetaData :: FilePath -> IO [(B.ByteString, B.ByteString)]
readMetaData fl = runResourceT $ runConduit $ sourceFile fl .| multiple ungzip .|
    linesUnboundedAsciiC .| (dropC 1 >> mapC (f . B.split '\t')) .| sinkList
  where
    f xs = (head xs, last xs)

readATACSamples :: FilePath -> IO [(String, String)]
readATACSamples fl = do
    df <- DF.filterRows (\_ vec -> vec V.! 1 == "ATAC-seq") <$> DF.readTableWith id fl
    return $ zipWith f (DF.rowNames df) (Mat.toRows $ DF._dataframe_data df)
  where
    f sra vec = (B.unpack $ vec V.! 7, T.unpack $ sra)

fasterqDumpPair :: FilePath -> FilePath -> FilePath -> FilePath -> IO (T.Text, T.Text)
fasterqDumpPair outDir temp secret sra = withTempDir (Just temp) $ \tmpDir -> do
        let output1 = T.pack $ outDir ++ "/" ++ sra ++ "_1.fastq"
            output2 = T.pack $ outDir ++ "/" ++ sra ++ "_2.fastq"
        shelly $ run_ "fasterq-dump"
            ["--split-files", "-O", T.pack outDir, T.pack sra, "-t", T.pack tmpDir,
                "--ngc", T.pack secret]
        shelly $ escaping False $ do
            run_ "gzip" [output1]
            run_ "gzip" [output2]
        return (output1 <> ".gz", output2 <> ".gz")

mkGeneMatrix :: FilePath -> FilePath -> FilePath -> [B.ByteString]
             -> IO (FilePath, FilePath, FilePath)
mkGeneMatrix outdir matdir promoter barcodes = do
    shelly $ mkdir_p outdir
    fls <- fmap (filter ("gene.mat.gz" `T.isSuffixOf`)) $ shelly $ lsT matdir
    mat <- fmap concatMatrix $ forM fls $ \fl -> do
        let nm = B.pack $ T.unpack $ fst $ T.breakOn "_rep1" $ snd $ T.breakOnEnd "/" fl
        mapRows (first ((nm <> "_1+") <>)) <$> mkSpMatrix readInt (T.unpack fl)
    let sinkMat = sinkRows (S.size barcodes') (_num_col mat) (fromJust . packDecimal) outputMat
        sinkRow = mapC (\(a, b) ->
            a <> "\t" <> fromJust (packDecimal $ foldl1' (+) $ map snd b)) .|
            unlinesAsciiC .| gzip .| sinkFile outputRow
        sinkCol = colSumC (_num_col mat)
        f (bed, c) = fromJust (bed^.name) <> ":" <> showBed (bed :: BED) <>
            "\t" <> fromJust (packDecimal c)
    (counts, _) <- runResourceT $ runConduit $ streamRows mat .|
        filterC ((`S.member` barcodes') . fst) .|
        zipSinks sinkCol (zipSinks sinkMat sinkRow)
    runResourceT $ runConduit $
        zipSources (streamBed promoter) (yieldMany $ U.toList counts) .|
        mapC f .| unlinesAsciiC .| gzip .| sinkFile outputCol
    return (outputRow, outputCol, outputMat)
  where
    barcodes' = S.fromList barcodes
    outputMat = outdir <> "/mat.gz"
    outputRow = outdir <> "/rownames.tsv.gz"
    outputCol = outdir <> "/features.tsv.gz"

--------------------------------------------------------------------------------
-- Workflow
--------------------------------------------------------------------------------
build "wf" [t| SciFlow ENV |] $ do
    phylogenyBuilder3

    diffSkm
    diffStl

    namespace "Diff" $ diffBuilder cellTypeGroup
    ["Merge_Fetal_Matrix", "Merge_Adult_Matrix", "Normalize_RPKM"] ~> "Diff_Diff_Prep"
 
    jointBuilder
    comBuilder

    node "Print_Lineage" [| \input -> liftIO $ do
        let gr = M.fromListWith undefined $ flip concatMap input $ \(n, ((e1, x1), (e2, x2), (e3, x3))) -> 
                [((n, e1), x1), ((n, e2), x2), ((n, e3), x3)]
            allCells = map (T.pack . B.unpack) $ nubSort $ concat $ M.elems gr
        forM_ majorLineages $ \(nm, (n, e)) -> do
            let cells = map (T.pack . B.unpack) $ M.lookupDefault undefined (B.pack n, B.pack e) gr
            print (nm, cells)
        |] $ return ()
    path ["Phylo3_Output_Tree", "Print_Lineage"]
 
    uNode "Read_ATAC_Sample" [| \() -> do
        fl <- lookupConfig "data_sheet"
        liftIO $ readATACSamples fl
        |]
    nodePar "Download_Data" [| \(_, sra) -> do
        dir <- (<> "/Download/") <$> lookupConfig "output_dir"
        key <- lookupConfig "token"
        tmp <- lookupConfig "tmp_dir"
        liftIO $ do
            shelly $ mkdir_p dir
            fasterqDumpPair dir tmp key sra
        |] $ return ()
    path ["Read_ATAC_Sample", "Download_Data"]

    uNode "Read_File_List" [| \() -> do
        fl <- lookupConfig "fragment_files"
        liftIO $ T.lines <$> T.readFile fl
        |]
    nodePar "Download_Fragment" [| \input -> do
        dir <- (<> "/Fragments/") <$> lookupConfig "output_dir"
        liftIO $ do
            let output = T.pack $ dir <> T.unpack (snd $ T.breakOnEnd "/" input)
            shelly $ do
                mkdir_p dir
                run_ "curl" ["-L", input, "--output", output]
                return output
        |] $ return ()
    nodePar "Lift_Over" [| \input -> do
        dir <- (<> "/Fragments/") <$> lookupConfig "output_dir"
        tmp <- lookupConfig "tmp_dir"
        chain <- lookupConfig "hg19_to_hg38"
        let output = T.unpack $ prefix <> "_hg38.bed.gz"
            prefix = fst (T.breakOn ".txt.gz" input) 
        liftIO $ withTempDir (Just tmp) $ \tmpdir -> do
            let tmp1 = tmpdir <> "/tmp1.bed"
                tmp2 = tmpdir <> "/tmp2.bed"
            shelly $ run_ "liftOver" ["-bedPlus=3", input, T.pack chain, T.pack tmp1, prefix <> "_unmapped.txt"]
            runResourceT $ runConduit $ streamBed tmp1 .|
                filterC ((< 2000) . (size :: BED -> Int)) .| sinkFileBed tmp2
            shelly $ escaping False $ bashPipeFail bash_ "sort" ["-T", T.pack tmp, "-S", "10G", "-k4,4", T.pack tmp2, "|", "gzip", "-c", ">", T.pack output]
            return output
        |] $ return ()
    path ["Read_File_List", "Download_Fragment", "Lift_Over"]

    uNode "Stat_Prep" [| \(x,y) -> return $ zip x y |]
    nodePar "Stat" [| \(input1, input2) -> do
        liftIO $ do
            n1 <- runResourceT $ runConduit $ sourceFile input1 .| multiple ungzip .| linesUnboundedAsciiC .| lengthC
            n2 <- runResourceT $ runConduit $ sourceFile (T.unpack input2) .| multiple ungzip .| linesUnboundedAsciiC .| lengthC
            print (n1 / n2 :: Double)
        |] $ return ()
    ["Lift_Over", "Download_Fragment"] ~> "Stat_Prep"
    path ["Stat_Prep", "Stat"]

    node "Confusion_Table" [| \() -> do
        metaFl <- lookupConfig "metadata"
        clFl <- lookupConfig "cluster"
        dir <- lookupConfig "output_dir"
        let output = dir <> "/confusion.tsv"
        liftIO $ do
            clusters <- decodeFile clFl
            meta <- readMetaData metaFl
            let rows = map (T.pack . B.unpack . head) names
                cols = map (T.pack . B.unpack . _cluster_name) clusters
                (clA, names) = unzip $ map unzip $ groupBy ((==) `on` snd) $ sortBy (comparing snd) meta
            DF.writeTable output (T.pack . show) $ DF.mkDataFrame rows cols $
                confusionTable clA (map (map (snd . B.breakEnd (=='+') . _cell_barcode) . _cluster_member) clusters)
            return output
        |] $ return ()

    node "Make_Fetal_Gene_Matrix" [| \() -> do
        dir <- (<> "/Gene/Fetal/") <$> lookupConfig "output_dir"
        fetalClFl <- lookupConfig "cluster"
        fetalDir <- lookupConfig "fetal_gene_matrix"
        promoter <- lookupConfig "promoter"
        liftIO $ do
            barcodes <- concatMap (map _cell_barcode . _cluster_member) <$>
                (decodeFile fetalClFl :: IO [CellCluster])
            mkGeneMatrix dir fetalDir promoter barcodes
        |] $ return ()
    node "Make_Adult_Gene_Matrix" [| \() -> do
        dir <- (<> "/Gene/Adult/") <$> lookupConfig "output_dir"
        adultClFl <- lookupConfig "adult_cluster"
        adultDir <- lookupConfig "adult_gene_matrix"
        promoter <- lookupConfig "promoter"
        liftIO $ do
            barcodes <- concatMap (map _cell_barcode . _cluster_member) <$>
                (decodeFile adultClFl :: IO [CellCluster])
            mkGeneMatrix dir adultDir promoter barcodes
        |] $ return ()
    node "Merge_Gene_Matrix" [| \((row1, col1, mat1), (row2, _, mat2)) -> do
        dir <- (<> "/Gene/") <$> lookupConfig "output_dir"
        let outputMat = dir <> "/mat.gz"
            outputRow = dir <> "/rownames.tsv.gz"
            outputCol = dir <> "/features.tsv.gz"
            source fl = sourceFile fl .| multiple ungzip .| linesUnboundedAsciiC .|
                mapC (head . B.split '\t')
        liftIO $ do
            runResourceT $ runConduit $ (source row1 >> source row2) .| unlinesAsciiC .|
                gzip .| sinkFile outputRow
            runResourceT $ runConduit $ source col1 .| unlinesAsciiC .|
                gzip .| sinkFile outputCol
            mat1' <- mkSpMatrix id mat1
            mat2' <- mkSpMatrix id mat2
            saveMatrix outputMat id $ concatMatrix [mat1', mat2']
            return (outputRow, outputCol, outputMat)
        |] $ return ()
    ["Make_Fetal_Gene_Matrix", "Make_Adult_Gene_Matrix"] ~> "Merge_Gene_Matrix"

    node "Count_Adult_Cells" [| \(fl, _, _) -> do
        dir <- lookupConfig "output_dir"
        let output = dir <> "/adult_cell.pdf"
        liftIO $ do
            plotNumCells fl output
        |] $ return ()
    path ["Merge_Adult_Matrix", "Count_Adult_Cells"]

    node "Count_Fetal_Cells" [| \(fl, _, _) -> do
        dir <- lookupConfig "output_dir"
        let output = dir <> "/fetal_cell.pdf"
        liftIO $ do
            plotNumCells fl output
        |] $ return ()
    path ["Merge_Fetal_Matrix", "Count_Fetal_Cells"]





main :: IO ()
main = mainFun wf