{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell   #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE QuasiQuotes       #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE DeriveGeneric #-}
module Phylogeny2
    ( phylogenyBuilder2
--    , phylogenyGeneBuilder2
    ) where

import HEA

import IGraph
import GHC.Generics (Generic)
import Statistics.Quantile
import Data.Conduit.Zlib (multiple, ungzip, gzip)
import qualified Taiji.Utils.DataFrame as DF
import Bio.RealWorld.GENCODE
import Control.Monad.State.Strict
import Control.Arrow
import qualified Data.HashMap.Strict as HM
import qualified Data.Vector as V
import qualified Data.Vector.Unboxed as U
import Statistics.Regression (olsRegress)
import AI.Clustering.Hierarchical
import Data.Either
import Data.Tuple (swap)
import Control.Parallel.Strategies (parMap, rdeepseq)
import qualified Data.Text as T
import qualified Data.Text.IO as T
import Bio.Data.Bed.Utils
import Shelly hiding (FilePath, path)
import Data.Int
import Data.Conduit.Internal (zipSinks, zipSources)
import Bio.Data.Bed
import Control.Monad.ST (runST)
import qualified Data.Matrix as Mat
import qualified Data.ByteString.Char8 as B
import qualified Data.ByteString.Lazy.Char8 as BL
import qualified Data.HashMap.Strict as M
import qualified Data.Map.Strict as Map
import qualified Data.HashSet as S
import Data.List.Split (chunksOf)
import Data.List.Ordered (nubSort, isect)
import Bio.Utils.Functions
import Taiji.Utils.Plot
import Statistics.Sample (mean, meanVarianceUnb, varianceUnbiased)
import Data.Binary
import System.Random.MWC.Distributions (uniformShuffle)
import System.Random.MWC (create)
import Text.Wrap
import Control.DeepSeq (force)
import Data.Conduit.ConcurrentMap (concurrentMapM_numCaps)
import AI.Clustering.Hierarchical (euclidean)
import Data.String
import Data.Char (toUpper)
import qualified Data.Tree as Tr

import Data.Conduit.Async
import ELynx.Tree hiding (partition)

import Taiji.Prelude
import Taiji.Pipeline.SC.ATACSeq.Functions.Utils (mkCountMat, groupCells)
import qualified Language.R                        as R
import Taiji.Utils.Matrix

import Data.Vector.SEXP (toList)
import qualified Language.R                        as R
import qualified Language.R.HExp as H
import           Language.R.QQ

majorLineages :: [(String, (String, String))]
majorLineages =
    [ ("Adult Mesenchymal", ("n17", "e12"))
    , ("Adrenal", ("n17", "e35"))
    , ("Fetal Myocyte", ("n42", "e86"))
    , ("T Lymphoid", ("n102", "e206"))
    , ("B Lymphoid", ("n103", "e208"))
    , ("Myeloid", ("n100", "e202"))
    , ("Endothelial", ("n82", "e165"))
    , ("Erythroblast", ("n81", "e164"))
    , ("Cardiomyo", ("n77", "e155"))
    , ("Mesenchymal", ("n129", "e259"))
    , ("Placental Epithelial", ("n163", "e328"))
    , ("Mammary/Skin Epi.", ("n163", "e327"))
    , ("Pulmonary Epithelial", ("n162", "e326"))
    , ("Pancreatic Epithelial", ("n161", "e324"))
    , ("Fetal Neuron", ("n37", "e76"))
    , ("Neuron/Glia", ("n24", "e49"))
    , ("Retinal Nervous", ("n38", "e78"))
    , ("Peripheral Nervous", ("n39", "e79"))
    , ("Adult Myocyte", ("n156", "e314"))
    , ("Hepatic", ("n155", "e312"))
    , ("Gastric Epithelial", ("n190", "e382"))
    , ("Neuroendocrine", ("n191", "e383"))
    , ("Intestinal Epithelial", ("n191", "e384"))
    ]

plotTFs :: FilePath
       -> DF.DataFrame Double
       -> IO ()
plotTFs output df = R.runRegion $ do
    valMatrix <- toRMatrix df
    [r| library("ggplot2")
        library("ComplexHeatmap")
        library("RColorBrewer")
        library("viridis")
        reds <- colorRampPalette(colors = brewer.pal(7,"Reds"))
        h1 <- Heatmap( valMatrix_hs,
            col=c("white", reds(99)),
            #col=c("white", magma(99)),
            cluster_rows=T, cluster_columns=T,
            clustering_method_rows="ward.D2", clustering_method_column="ward.D2",
            show_row_names=T, row_names_side="left",
            show_column_names=T,
            row_names_gp = gpar(fontsize=5),
            column_names_gp = gpar(fontsize=5),
            heatmap_legend_param = list(
                title = "log2(fold change)",
                title_position = "leftcenter-rot"
                #at = c(-2, 0, 2)
            )
        )
        pdf(output_hs, width=w_hs, height=h_hs)
        ht <- draw(h1)
        dev.off()
    |]
    return ()
  where
    (m, n) = DF.dim df
    w = fromIntegral n * 0.1 :: Double
    h = fromIntegral m * 0.3 :: Double

enrichment :: S.HashSet B.ByteString -> [B.ByteString] -> Int -> IO (Double, Double)
enrichment motifs fg nBg' = R.runRegion $ do
    p <- [r| phyper(oFg_hs - 1, oBg_hs, nBg_hs - oBg_hs, nFg_hs, lower.tail=F, log.p=T) / log(10)
        |]
    R.unSomeSEXP p $ \x -> case H.hexp x of
        H.Real x'  -> return (e, head $ toList x')
  where
    e = logBase 2 $ (oFg / nFg) / (oBg / nBg)
    oFg = fromIntegral $ length $ filter (`S.member` motifs) fg :: Double
    oBg = fromIntegral $ S.size motifs :: Double
    nFg = fromIntegral $ length fg :: Double
    nBg = fromIntegral nBg' :: Double

filterMotifResult :: FilePath -> (B.ByteString -> Bool)
                  -> IO [(B.ByteString, Double)]
filterMotifResult fl isExpressed = 
    map (second (negate . snd)) .
        map (\(a,b,c) -> (a, (b,c))) .
        filter (lookupExpr . (^._1)) .
        --map fst . V.toList . filterFDR 0.01 . V.fromList . map (\x -> (x, x^._3)) .
        filter ((>=cutoff) . (^._2)) .
        map (f . B.split '\t') . B.lines <$> B.readFile fl
  where
    f [a,b,c] = ( a
                , readDouble' b  -- fold
                , readDouble c )   -- p-value
    g a@(fd1, p1) b@(fd2, p2)
        | p1 < p2 || (p1 == p2 && abs fd1 > abs fd2) = a
        | otherwise = b
    lookupExpr g = any isExpressed $ B.split '+' $ fst $ B.break (=='_') g
    readDouble' x | x == "-Infinity" = -1 / 0
                  | x == "NaN" = 0
                  | otherwise = readDouble x
    cutoff = logBase 2 1.5

readMotifs :: FilePath -> IO (B.ByteString, S.HashSet B.ByteString)
readMotifs fl = do
    s <- decodeFile fl
    return (nm, s)
  where
    nm = B.pack $ T.unpack $ fst $ T.breakOn ".bin" $ snd $ T.breakOnEnd "/" $ T.pack fl

toBed :: B.ByteString -> BED3
toBed x = let [chr, x'] = B.split ':' $ x
              [s, e] = B.split '-' x'
           in BED3 chr (readInt s) (readInt e)

readPhylo :: FilePath -> IO (Tree Phylo Name)
readPhylo fl = parseOneNewick Standard <$> B.readFile fl

phyloToGraph :: Tree Phylo Name -> Graph 'U B.ByteString B.ByteString
phyloToGraph t = fromLabeledEdges $ map (second (\x -> B.pack $ "e" <> show x)) $ zip (getEdge $ relabel t) [0..]
  where
    getEdge (Node _ _ []) = []
    getEdge t@(Node _ _ x) = map (\a -> (getLabel t, getLabel a)) x <> concatMap getEdge x
    getLabel (Node _ lb _) = B.pack $ BL.unpack $ fromName lb
    relabel = snd . go (0 :: Int)
      where
        go acc (Node a lb []) = (acc, Node a lb [])
        go acc (Node a _ ts) = (acc', Node a (br acc) ts')
          where
            (acc', ts') = loop (acc+1) [] ts
        loop i r (x:xs) = let (i', x') = go i x in loop i' (r <> [x']) xs
        loop i r [] = (i, r)
        br i = fromString $ "n" <> show i

findTriplets :: Graph 'U B.ByteString B.ByteString
             -> [( B.ByteString
                , ((B.ByteString, [B.ByteString]), (B.ByteString, [B.ByteString]), (B.ByteString, [B.ByteString])))]
findTriplets gr = mapMaybe f $ nodes gr
  where
    f i = case neighbors gr i of
        [x] -> Nothing
        [a,b,c] -> Just (lab, ( (edgeLab gr (i, a), map (nodeLab gr) $ findSubGraph i a)
                              , (edgeLab gr (i, b), map (nodeLab gr) $ findSubGraph i b)
                              , (edgeLab gr (i, c), map (nodeLab gr) $ findSubGraph i c) ))
        y -> error $ show $ map (nodeLab gr) y
      where
        lab = nodeLab gr i
    findSubGraph p x = case delete p (neighbors gr x) of
        [] -> [x]
        xs -> concatMap (findSubGraph x) xs

data Group = AdultOnly FilePath FilePath FilePath
           | FetalOnly FilePath FilePath FilePath
           | Both FilePath FilePath FilePath FilePath FilePath FilePath
           deriving (Generic, Show)

instance Binary Group

filterMotifByExpression :: FilePath -> FilePath -> IO [(B.ByteString, Double, Double)]
filterMotifByExpression motif expr = do
    genes <- S.fromList . map (g . B.split '\t') . tail . B.lines <$> B.readFile expr
    sortBy (comparing (^._3)) .
        map snd . M.toList . M.fromListWith h . map (\x -> (x^._1, x)) .
        map snd . M.toList . M.fromListWith h .
        filter (\x -> (x^._2._1) `S.member` genes) .
        map (f . B.split '\t') . B.lines <$> B.readFile motif
  where
    f [a,b,c] = let tf = B.map toUpper $ fst $ B.break (=='_') a 
                    cl = snd $ B.breakEnd (=='+') a
                in (cl, (tf, readDouble b, readDouble c))
    g [a,_,_] = B.map toUpper a
    h x@(a,_,c) y@(a',_,c') | c < c' = x
                            | otherwise = y

                        
sampleList :: Int -> [[a]] -> IO [[a]]
sampleList n xs = do
    gen <- create
    forM xs $ \x -> take sampleSize . V.toList <$> uniformShuffle (V.fromList x) gen
  where
    sampleSize = max n $ minimum $ map length xs
    
phylogenyBuilder2 = do
    path ["Plot_Tree", "Phylo2_Output_Tree"]
    ["Phylo2_Select_Branches", "Merge_Fetal_Matrix", "Merge_Adult_Matrix"] ~> "Phylo2_Get_Peak_List_Prep"
    ["Phylo2_Select_Branches", "Merge_Fetal_Matrix", "Phylo2_Get_Peak_List", "Phylo2_Diff_Analysis"] ~> "Phylo2_Combine_Diff_Peak"
    ["Merge_Peaks", "Phylo2_Output_Diff_Peak"] ~> "Phylo2_Motif_Enrich_Prep"
    namespace "Phylo2" $ do
        node "Output_Tree" [| \input -> do
            dir <- fmap (<> "/Phylo2/") $ lookupConfig "output_dir"
            adultAnnoFl <- lookupConfig "adult_annotation"
            fetalAnnoFl <- lookupConfig "fetal_annotation"
            liftIO $ do
                shelly $ mkdir_p dir
                anno1 <- map (\(a,b) -> ("a_" <> a, "a_" <> b)) <$> readAnno adultAnnoFl
                anno2 <- map (\(a,b) -> ("f_" <> a, "f_" <> b)) <$> readAnno fetalAnnoFl
                let header = "Source\tTarget\tEdge_ID"
                    anno = anno1 <> anno2
                gr <- phyloToGraph <$> readPhylo input
                let f x = fromMaybe (nodeLab gr x) $ lookup (nodeLab gr x) anno
                B.writeFile (dir <> "/edges.tsv") $ B.unlines $ (header:) $
                    map (\((fr, to), e) -> B.intercalate "\t" [f fr, f to, e]) $ labEdges gr
                return $ findTriplets gr
            |] $ return ()
        uNode "Select_Branches" [| \input -> do
            let selection = fmap nubSort $ M.fromListWith (<>) $
                    map ((\(a,b) -> (B.pack a, [B.pack b])) . snd) majorLineages
                f (i, ((a, va), (b, vb), (c, vc))) = case M.lookup i selection of
                    Nothing -> []
                    Just es -> map (\(x,y,z) -> (B.unpack $ B.intercalate "-" $ i : x, y, z)) $
                        filter (\x -> any (`elem` es) $ x^._1) res
                  where
                    res = [ ([a, b], map (T.pack . B.unpack) va, map (T.pack . B.unpack) vb)
                          , ([a, c], map (T.pack . B.unpack) va, map (T.pack . B.unpack) vc)
                          , ([b, c], map (T.pack . B.unpack) vb, map (T.pack . B.unpack) vc) ]
            return $ concatMap f input 
            |]
        path ["Output_Tree", "Select_Branches"]

        uNode "Get_Peak_List_Prep" [| \(a,b,c) -> return $ zip3 a (repeat b) (repeat c) |]
        nodePar "Get_Peak_List" [| \( (i, left, right), (fridx, fcidx, fetalMat), (aridx, acidx, adultMat)) -> do
            fetalPeakDir <- lookupConfig "fetal_peak_dir"
            adultPeakDir <- lookupConfig "adult_peak_dir"
            fetalClFl <- lookupConfig "cluster"
            adultClFl <- lookupConfig "adult_cluster"
            let prefix = "/Phylo2/" <> i
            dir <- fmap (<> prefix) $ lookupConfig "output_dir"
            let output = dir <> "/peak_index.txt"
                outputF = dir <> "/fetal.mat.gz"
                outputA = dir <> "/adult.mat.gz"
                outputLeft = dir <> "/left_barcodes.bin"
                outputRight = dir <> "/right_barcodes.bin"
                outputCov = dir <> "/covariate.txt"
                outputLab = dir <> "/labels.txt"
                groups = nub $ map T.head left <> map T.head right
            liftIO $ do
                shelly $ mkdir_p dir
                peakfiles <- do
                    fls1 <- fmap (filter (".bed.gz" `T.isSuffixOf`)) $ shelly $ lsT fetalPeakDir
                    fls2 <- fmap (filter (".bed.gz" `T.isSuffixOf`)) $ shelly $ lsT adultPeakDir
                    let x = flip map fls1 $ \fl ->
                            (("f_" <>) $ fst $ T.breakOn ".bed.gz" $ snd $ T.breakOnEnd "/" fl, T.unpack fl)
                        y = flip map fls2 $ \fl ->
                            (("a_" <>) $ fst $ T.breakOn ".bed.gz" $ snd $ T.breakOnEnd "/" fl, T.unpack fl)
                    return $ x <> y
                let p1 = flip map left $ \x -> fromJust $ lookup x peakfiles
                    p2 = flip map right $ \x -> fromJust $ lookup x peakfiles
                beds <- fmap concat $ forM (p1 <> p2) $ \fl -> runResourceT $
                    runConduit $ streamBedGzip fl .| sinkList :: IO [BED3]
                let f i x = score .~ Just i $ convert x :: BED
                peaks <- runResourceT $ runConduit $
                    sourceFile fcidx .| multiple ungzip .| linesUnboundedAsciiC .|
                    mapC (toBed . head . B.split '\t') .| sinkList
                pidx <- runResourceT $ runConduit $ yieldMany (zipWith f [0..] peaks) .|
                    intersectBed beds .| mapC (fromJust . (^.score)) .| sinkList
                B.writeFile output $ B.unlines $ map (B.pack . show) pidx

                adultCl <- map (\x -> x{_cluster_name = "a_" <> _cluster_name x}) <$> decodeFile adultClFl
                fetalCl <- map (\x -> x{_cluster_name = "f_" <> _cluster_name x}) <$> decodeFile fetalClFl
                let cls = fetalCl <> adultCl
                    barcode1 = map (map _cell_barcode . _cluster_member) $
                        filter ((`elem` left) . T.pack . B.unpack . _cluster_name) cls
                    barcode2 = map (map _cell_barcode . _cluster_member) $
                        filter ((`elem` right) . T.pack . B.unpack . _cluster_name) cls
                leftBarcodes <- fmap (S.fromList . concat) $ sampleList 500 barcode1
                rightBarcodes <- fmap (S.fromList . concat) $ sampleList 500 barcode2
                encodeFile outputLeft leftBarcodes
                encodeFile outputRight rightBarcodes

                let barcodes = leftBarcodes <> rightBarcodes
                    exclude = S.fromList $ filter (not . (`S.member` barcodes)) $ concat $ barcode1 <> barcode2
                    sink1 = mapC (B.unwords . fst) .| unlinesAsciiC .| sinkFile outputCov
                    sink2 = mapC snd .| unlinesAsciiC .| sinkFile outputLab
                    h [bc, x] =
                        let rc = toShortest $ logBase 10 $ readDouble x
                        in if bc `S.member` leftBarcodes
                            then ([rc], "1")
                            else if bc `S.member` rightBarcodes
                                then ([rc], "0")
                                else undefined
                dat <- case groups of
                    ['a'] -> do
                        ridx <- runResourceT $ runConduit $ zipSources (iterateC succ 0)
                            (sourceFile aridx .| multiple ungzip .| linesUnboundedAsciiC .|
                            mapC (head . B.split '\t')) .| 
                            filterC ((`S.member` barcodes) . snd) .| mapC fst .| sinkList
                        (selectRows ridx . selectCols pidx <$> mkSpMatrix id adultMat) >>= saveMatrix outputA id
                        runResourceT $ runConduit $
                            sourceFile aridx .| multiple ungzip .| linesUnboundedAsciiC .|
                            mapC (B.split '\t') .| filterC ((`S.member` barcodes) . head) .|
                            mapC h .| zipSinks sink1 sink2
                        return $ AdultOnly aridx acidx outputA
                    ['f'] -> do
                        ridx <- runResourceT $ runConduit $ zipSources (iterateC succ 0)
                            (sourceFile fridx .| multiple ungzip .| linesUnboundedAsciiC .| mapC (head . B.split '\t')) .| 
                            filterC ((`S.member` barcodes) . snd) .| mapC fst .| sinkList
                        (selectRows ridx . selectCols pidx <$> mkSpMatrix id fetalMat) >>= saveMatrix outputF id
                        runResourceT $ runConduit $
                            sourceFile fridx .| multiple ungzip .| linesUnboundedAsciiC .|
                            mapC (B.split '\t') .| filterC ((`S.member` barcodes) . head) .|
                            mapC h .| zipSinks sink1 sink2
                        return $ FetalOnly fridx fcidx outputF
                    _ -> do
                        fridx' <- runResourceT $ runConduit $ zipSources (iterateC succ 0)
                            (sourceFile fridx .| multiple ungzip .| linesUnboundedAsciiC .|
                            mapC (head . B.split '\t')) .| 
                            filterC ((`S.member` exclude) . snd) .| mapC fst .| sinkList
                        (deleteRows fridx' . selectCols pidx <$> mkSpMatrix id fetalMat) >>= saveMatrix outputF id
                        aridx' <- runResourceT $ runConduit $ zipSources (iterateC succ 0)
                            (sourceFile aridx .| multiple ungzip .| linesUnboundedAsciiC .|
                            mapC (head . B.split '\t')) .| 
                            filterC ((`S.member` exclude) . snd) .| mapC fst .| sinkList
                        (deleteRows aridx' . selectCols pidx <$> mkSpMatrix id adultMat) >>= saveMatrix outputA id

                        let f [bc, x] =
                                let rc = toShortest $ logBase 10 $ readDouble x
                                in if bc `S.member` leftBarcodes
                                    then ([rc, "0", "1"], "1")
                                    else if bc `S.member` rightBarcodes
                                        then ([rc, "0", "1"], "0")
                                        else ([rc, "0", "0"], "0")
                            g [bc, x] =
                                let rc = toShortest $ logBase 10 $ readDouble x
                                in if bc `S.member` leftBarcodes
                                    then ([rc, "1", "1"], "1")
                                    else if bc `S.member` rightBarcodes
                                        then ([rc, "1", "1"], "0")
                                        else ([rc, "1", "0"], "0")
                        runResourceT $ runConduit $ (
                            (sourceFile fridx .| multiple ungzip .| linesUnboundedAsciiC .|
                                mapC (B.split '\t') .| filterC (not . (`S.member` exclude) . head) .|
                                mapC f) >>
                            (sourceFile aridx .| multiple ungzip .| linesUnboundedAsciiC .|
                                mapC (B.split '\t') .| filterC (not . (`S.member` exclude) . head) .|
                                mapC g )) .| zipSinks sink1 sink2
                        return $ Both fridx fcidx outputF aridx acidx outputA
                return ((i, (outputLeft, outputRight)), dat, outputCov, outputLab, output)
            |] $ return ()
        nodePar "Select_Feature" [| \((nm, (left, right)), dat, cov, lab, _) -> do
            let prefix = "/Phylo2/" <> nm
            dir <- fmap (<> prefix) $ lookupConfig "output_dir"
            liftIO $ do
                leftBarcodes <- decodeFile left
                rightBarcodes <- decodeFile right
                (mat, source) <- case dat of
                    Both fridx _ fetalMat aridx _ adultMat -> do
                        mat1 <- mkSpMatrix readInt fetalMat
                        mat2 <- mkSpMatrix readInt adultMat
                        let mat = concatMatrix [mat1, mat2]
                            s = (sourceFile fridx .| multiple ungzip .| linesUnboundedAsciiC .| mapC (B.split '\t')) >>
                                (sourceFile aridx .| multiple ungzip .| linesUnboundedAsciiC .| mapC (B.split '\t'))
                        return (mat, s)
                    AdultOnly aridx _ adultMat -> do
                        mat <- mkSpMatrix readInt adultMat
                        let s = sourceFile aridx .| multiple ungzip .| linesUnboundedAsciiC .| mapC (B.split '\t')
                        return (mat, s)
                    FetalOnly fridx _ fetalMat -> do
                        mat <- mkSpMatrix readInt fetalMat 
                        let s = sourceFile fridx .| multiple ungzip .| linesUnboundedAsciiC .| mapC (B.split '\t')
                        return (mat, s)
                (count1, count2) <- runResourceT $ runConduit $ streamRows mat .| zipSinks
                    (filterC ((`S.member` leftBarcodes) . fst) .| colSumC (_num_col mat))
                    (filterC ((`S.member` rightBarcodes) . fst) .| colSumC (_num_col mat))
                (s1, s2) <- runResourceT $ runConduit $ source .| zipSinks
                    (filterC ((`S.member` leftBarcodes) . head) .| mapC (readDouble . last) .| sumC)
                    (filterC ((`S.member` rightBarcodes) . head) .| mapC (readDouble . last) .| sumC)
                let rpm1 = U.map (logBase 2 . (+1.1) . (/(s1/1000000)) . fromIntegral) count1
                    rpm2 = U.map (logBase 2 . (+1.1) . (/(s2/1000000)) . fromIntegral) count2
                    fd = U.zipWith (\x y -> logBase 2 $ x / y) rpm1 rpm2 :: U.Vector Double
                    batch = chunksOf 5000 $ U.toList $ U.filter ((>1) . abs . snd) $
                        U.imap (\i x -> (i, x)) fd
                forM (zip [1 :: Int ..] batch) $ \(i, b) -> do
                    let output = dir <> "/batch_" <> show i <> ".bin"
                    encodeFile output b
                    return (nm, dat, cov, lab, output)
            |] $ return ()
        uNode "Diff_Analysis_Prep" [| return . concat |]
        nodePar "Diff_Analysis" [| \(nm, dat, cov, lab, idx) -> do
            let output = idx <> ".txt"
            liftIO $ withTemp (Just "./") $ \tmp -> do
                mat <- case dat of
                    Both _ _ fetalMat _ _ adultMat -> do
                        mat1 <- mkSpMatrix id fetalMat
                        mat2 <- mkSpMatrix id adultMat
                        return $ concatMatrix [mat1, mat2]
                    AdultOnly _ _ adultMat -> mkSpMatrix id adultMat
                    FetalOnly _ _ fetalMat -> mkSpMatrix id fetalMat
                i <- map fst <$> (decodeFile idx :: IO [(Int, Double)])
                saveMatrix tmp id $ selectCols i mat
                shelly $ run_ "python" ["src/diff_analysis.py", T.pack tmp, T.pack cov, T.pack lab, T.pack output]
                return (nm, (output, idx))
            |] $ nCore .= 15
        path ["Get_Peak_List_Prep", "Get_Peak_List", "Select_Feature", "Diff_Analysis_Prep", "Diff_Analysis"]

        node "Combine_Diff_Peak" [| \(branches, (_, fcidx, _), peakIndex, res) -> do
            let prefix = "/Phylo2/Diff_Peaks/"
            dir <- fmap (<> prefix) $ lookupConfig "output_dir"
            liftIO $ do
                shelly $ mkdir_p dir
                peaks <- runResourceT $ runConduit $ sourceFile fcidx .|
                    multiple ungzip .| linesUnboundedAsciiC .| mapC (head . B.split '\t') .| 
                    sinkVector
                peakIdxMap <- fmap (M.fromListWith undefined) $ forM peakIndex $ \((nm, _), _, _, _, idxFl) -> do
                    vec <- V.fromList . map readInt . B.lines <$> B.readFile idxFl
                    return (nm, vec)
                forM (groupBy ((==) `on` fst) $ sortBy (comparing fst) res) $ \xs -> do
                    let nm = fst $ head xs
                        peakIdx = M.lookupDefault undefined nm peakIdxMap
                    ps <- fmap concat $ forM xs $ \(_, (fl, idxFl)) -> do
                        idx <- U.fromList <$> (decodeFile idxFl :: IO [(Int, Double)])
                        let f [a, b] = let (i, fd) = idx U.! readInt a
                                           p = peaks V.! (peakIdx V.! i)
                                        in ((p, fd), readDouble b)
                        map (f . B.split '\t') . tail . B.lines <$> B.readFile fl
                    let (left, right) = partition ((>0) . fst . snd) $ M.toList $
                            M.fromListWith (\a@(_, p1) b@(_, p2) -> if p1 < p2 then a else b) $
                            map (\((g, fd), p) -> (g, (fd, p))) $ V.toList $ filterFDR 0.01 $ V.fromList ps
                        output1 = dir <> nm <> "_left.txt"
                        output2 = dir <> nm <> "_right.txt"
                        leftLabel = B.pack $ T.unpack $ T.intercalate "," $ fst $ fromJust $
                            lookup nm $ map (\(a,b,c) -> (a, (b,c))) branches
                        rightLabel = B.pack $ T.unpack $ T.intercalate "," $ snd $ fromJust $
                            lookup nm $ map (\(a,b,c) -> (a, (b,c))) branches
                    B.writeFile output1 $ B.unlines $ (leftLabel:) $
                        map (\(g, (f, p)) -> B.intercalate "\t" [g, toShortest f, toShortest p]) $
                        sortBy (comparing (snd . snd)) left
                    B.writeFile output2 $ B.unlines $ (rightLabel:) $
                        map (\(g, (f, p)) -> B.intercalate "\t" [g, toShortest f, toShortest p]) $
                        sortBy (comparing (snd . snd)) right
                    return (nm, output1, output2)
            |] $ return ()
        node "Output_Diff_Peak" [| \input -> do
            let prefix = "/Phylo2/Diff_Peaks/"
            dir <- fmap (<> prefix) $ lookupConfig "output_dir"
            liftIO $ do
                res <- fmap (M.fromListWith (<>)) $ forM input $ \(nm, fl1, fl2) -> do
                    let [n, e1, e2] = map B.unpack $ B.split '-' $ B.pack nm
                    b1 <- map (toBed . head . B.split '\t') . tail . B.lines <$> B.readFile fl1
                    b2 <- map (toBed . head . B.split '\t') . tail . B.lines <$> B.readFile fl2
                    return (n, [((e1, b1), (e2, b2))])
                forM majorLineages $ \(_, (n, e)) -> do
                    let [d1, d2] = mapMaybe f $ M.lookupDefault undefined n res
                        f ((a, va), (b, vb)) | a == e = Just va
                                             | b == e = Just vb
                                             | otherwise = Nothing
                        output = dir <> "/" <> n <> "-" <> e <> ".bed"
                    writeBed output $ isect d1 d2
                    return ((n, e), output)
            |] $ return ()
        nodePar "Diff_GO" [| \(i, bed) -> liftIO $ do
            go <- great bed
            return (i, go)
            |] $ return ()
        node "Output_Diff_GO" [| \go -> do
            let prefix = "/Phylo2/"
            dir <- fmap (<> prefix) $ lookupConfig "output_dir"
            let output = dir <> "/CRE_module_go.tsv"
                f terms = fst $ unzip $ sortBy (comparing snd) $ V.toList $
                    filterFDR 0.01 $ V.fromList $ map (\x -> (x, _great_pvalue x)) $
                    filter ((>=2) . _great_enrichment) terms
                majorLineages' = map swap majorLineages
                go' = map (\(i,x) -> (B.pack $ fromJust $ lookup i majorLineages', f x)) go
            liftIO $ do
                B.writeFile output $ B.unlines $ flip map go' $ \(i, xs) ->
                    B.intercalate "\t" $ i :
                        map (\x -> _great_term_name x <> "," <> B.pack (show $ negate $ logBase 10 $ _great_pvalue x)) (take 20 xs)
            |] $ return ()
        path ["Combine_Diff_Peak", "Output_Diff_Peak", "Diff_GO", "Output_Diff_GO"]

        uNode "Motif_Enrich_Prep" [| \(pk, x) -> return $ zip x $ repeat pk |]
        nodePar "Motif_Enrich" [| \(((n, e), bedFl), pk) -> do
            let prefix = "/Phylo2/Diff_Motifs/"
            dir <- fmap (<> prefix) $ lookupConfig "output_dir"
            let output = dir <> n <> "-" <> e <> ".txt"
            motifDir <- lookupConfig "motif_dir"
            liftIO $ do
                shelly $ mkdir_p dir
                nPeak <- runResourceT $ runConduit $ (streamBedGzip pk :: ConduitT _ BED3 _ _) .| lengthC
                beds <- map showBed <$> (readBed bedFl :: IO [BED3])
                fls <- fmap (filter (not . ("mouse" `T.isInfixOf`) . snd . T.breakOnEnd "/")) $ shelly $ lsT motifDir
                res <- forM fls $ \fl -> do
                    (motif, set) <- readMotifs $ T.unpack fl
                    (e1, p1) <- enrichment set beds nPeak
                    return ((motif, e1), p1)
                B.writeFile output $ B.unlines $
                    map (\((g, f), p) -> B.intercalate "\t" [g, toShortest f, toShortest p]) $
                    sortBy (comparing snd) res
                return ((n, e), output)
            |] $ return ()
        node "Output_Motif_Enrich" [| \input -> do
            dir <- fmap (<> "/Phylo2/") $ lookupConfig "output_dir"
            let output = dir <> "motifs_pvalue.tsv"
                output2 = dir <> "motifs_log_fold.tsv"
                majorLineages' = map swap majorLineages
            liftIO $ do
                res <- fmap (filter (not . null . snd)) $ forM input $ \(nm, fl) -> do
                    ms <- filterMotifResult fl $ const True
                    return (fromJust $ lookup nm majorLineages',  ms)
                let tfs = nubSort $ concatMap (map fst . snd) res 
                    (rows, dat) = unzip $ flip map res $ \(nm, xs) ->
                        (nm, map (\x -> fromMaybe 0 $ lookup x xs) tfs)
                    df = DF.mkDataFrame (map T.pack rows) (map (T.pack . B.unpack) tfs) dat
                DF.writeTable output (T.pack . show) df
                --DF.writeTable output2 (T.pack . show) $ DF.map fst df
                return output
            |] $ return ()
        path ["Motif_Enrich_Prep", "Motif_Enrich", "Output_Motif_Enrich"]

{-
    ["Phylo_Get_Branches", "Merge_Fetal_Matrix", "Merge_Adult_Matrix"] ~> "Phylo_Get_Peak_List_Prep"
    ["Phylo_Gene_Compute_Expr", "Phylo_Motif_Enrich"] ~> "Phylo_Output_Motif_Enrich"
    path ["Plot_Tree", "Phylo_Get_Branches"]
    ["Plot_Tree", "Phylo_Motif_Enrich"] ~> "Phylo_Branch_Clustering"
    namespace "Phylo" $ do
        uNode "Get_Branches" [| \input -> liftIO $ do
            readPhylo input >>= return . zip [1 :: Int ..] . branchLeaves
            |]
        uNode "Get_Peak_List_Prep" [| \(a,b,c) -> return $ zip3 a (repeat b) (repeat c) |]



        node "Branch_Clustering" [| \(treeFl, input) -> do
            dir <- fmap (<> "/Phylo/") $ lookupConfig "output_dir"
            adultAnnoFl <- lookupConfig "adult_annotation"
            fetalAnnoFl <- lookupConfig "fetal_annotation"
            liftIO $ do
                let readMotifs fl = do
                        let f [a,b,c] = (B.map toUpper $ fst $ B.break (=='_') a, (readDouble b, readDouble c))
                            h a@(fd1, p1) b@(fd2, p2)
                                | p1 < p2 || (p1 == p2 && fd1 > fd2) = a
                                | otherwise = b
                        sortBy (comparing (snd . snd)) . M.toList . M.fromListWith h .
                            map (f . B.split '\t') . B.lines <$> B.readFile fl
                res <- fmap (filter (not . null . snd) . concat) $ forM input $ \(nm, left, right) -> do
                    m1 <- readMotifs left
                    m2 <- readMotifs right
                    return [(show nm <> ":L", m1), (show nm <> ":R", m2)]
                let tfs = nubSort $ concatMap (map fst . snd) res 
                    (rows, dat) = unzip $ flip map res $ \(nm, xs) -> (nm, map (\x -> fromMaybe (0, 1) $ lookup x xs) tfs)
                    df = DF.mkDataFrame (map T.pack rows) (map (T.pack . B.unpack) tfs) dat
                    log10 x | x == 0 = 300
                            | otherwise = negate $ logBase 10 x
                    colors = map T.pack cat20
                (dists, cls) <- plotHeatmap (dir <> "motifs.pdf") $
                    DF.orderDataFrame id $ DF.map (min 3 . fst) df
                DF.writeTable "dists.tsv" (T.pack . show) dists
                T.writeFile "clusters.tsv" $ T.unlines $ flip concatMap cls $
                    \(c, xs) -> zipWith (\i x -> T.pack i <> "\t" <> x) (repeat c) xs
                {-
                (tree, gr) <- phyloToGraph <$> readPhylo treeFl
                anno1 <- map (\(a,b) -> ("a_" <> a, "a_" <> b)) <$> readAnno adultAnnoFl
                anno2 <- map (\(a,b) -> ("f_" <> a, "f_" <> b)) <$> readAnno fetalAnnoFl
                let branches = findContinuousBranches (M.fromList $ concat $ zipWith (\i cl -> zip (map T.unpack cl) (repeat $ show i)) [0..] cls) tree
                    header = "Source\tTarget\tEdge_ID"
                    anno = anno1 <> anno2
                    annotate (a,b,c) = B.intercalate "\t" [fromMaybe a $ lookup a anno, fromMaybe b $ lookup b anno, c]
                B.writeFile "edges.tsv" $ B.unlines $ (header:) $ map annotate $ simplifyGraph branches gr
                -}
            |] $ return ()

        node "Plot_Select_Motif" [| \(fl, _) -> do
            dir <- fmap (<> "/Phylo/") $ lookupConfig "output_dir"
            let output = dir <> "1.pdf"
            liftIO $ do
                let branches = ["77:L", "78:L", "78:R", "80:L", "80:R", "79:L", "79:R"] :: [T.Text]
                df <- DF.map (min 100) . (`DF.rsub` branches) <$> DF.readTable fl
                let motifIdx = nubSort $ flip concatMap
                        (Mat.toRows $ DF._dataframe_data df) $ \v -> 
                            map fst $
                            sortBy (flip (comparing snd)) $
                            map (maximumBy (comparing snd)) $ 
                            groupBy ((==) `on` (tfCluster . fst)) $
                            sortBy (comparing (tfCluster . fst)) $
                            filter ((>5) . abs . snd) $
                            zip (DF.colNames df) $ V.toList v
                    tfCluster = snd . T.breakOnEnd "_"
                print $ DF.dim $ df `DF.csub` motifIdx
                plotTFs output $ df `DF.csub` motifIdx
            |] $ return ()
        path ["Output_Motif_Enrich", "Plot_Select_Motif"]
        -}