{-# LANGUAGE OverloadedStrings #-}
import Shelly
import Control.Monad
import qualified Data.Text as T
import System.Environment

main = do
  [dir] <- getArgs
  dirs <- shelly $ lsT dir
  s <- fmap sum $ forM dirs $ \d -> do
    fls <- fmap (map (snd . T.breakOnEnd "/")) $ shelly $ lsT $ T.unpack d
    let n = length $ filter (\x -> ".bin" `T.isSuffixOf` x && "batch" `T.isPrefixOf` x) fls
        m = length $ filter (".bin.txt" `T.isSuffixOf`) fls
    return $ n - m
  print s
