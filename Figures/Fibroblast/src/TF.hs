{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell   #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE QuasiQuotes       #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE GADTs #-}
module TF (tfBuilder) where

import HEA

import qualified Taiji.Utils.DataFrame as DF
import qualified Data.Vector as V
import qualified Data.Text as T
import Bio.Data.Bed.Types
import Bio.Data.Bed
import Control.Monad.ST (runST)
import qualified Data.Matrix as Mat
import qualified Data.ByteString.Char8 as B
import qualified Data.HashMap.Strict as M
import Data.List.Ordered (nubSort)
import Bio.Utils.Functions
import Taiji.Utils.Plot
import Statistics.Sample
import Data.Binary
import System.Random.MWC.Distributions (uniformShuffle)
import System.Random.MWC (create)
import Shelly hiding (FilePath)

import Taiji.Prelude hiding (groupBy)
import qualified Language.R                        as R
import           Language.R.QQ
import Language.R.HExp

diagonize :: (a -> V.Vector Double) -> [a] -> [a]
diagonize f xs = map fst $ sortBy (comparing snd) $ map (\x -> (x, g $ f x)) xs
  where
    g vec = let ave = mean vec
                max' = V.maximum vec
                i = if (max' - ave) / max' < 0.5 then -1 else V.maxIndex vec
            in (i, negate ave)

diagonizeDF :: DF.DataFrame Double -> DF.DataFrame Double
diagonizeDF df = DF.mkDataFrame names (DF.colNames df) $ map V.toList rows
  where
    (names, rows) = unzip $ diagonize snd $ zip (DF.rowNames df) $ Mat.toRows $ DF._dataframe_data df

readRanks :: FilePath   -- ^ Ranks
          -> FilePath   -- ^ P-values
          -> IO (DF.DataFrame Double)
readRanks rankFl pvalFl = do
    pvals <- DF.readTable pvalFl
    let tfs = nubSort $ flip concatMap (Mat.toColumns $ DF._dataframe_data pvals) $ \pval ->
            V.toList $ fst $ V.unzip $ filterFDR 0.001 $
            V.zip (V.fromList $ DF.rowNames pvals) pval
    ranks <- DF.readTable rankFl
    let df = ranks `DF.rsub` tfs
    return df

plotRanks :: FilePath
          -> DF.DataFrame Double
          -> IO()
plotRanks output df = R.runRegion $ do
    valMatrix <- toRMatrix df
    [r| library("ggplot2")
        library("ComplexHeatmap")
        library("RColorBrewer")
        library("viridis")
        library("circlize")
        #rwb <- colorRampPalette(colors = rev(brewer.pal(7,"RdBu")))(99)
        rwb <- colorRamp2(c(-4.5, -3, -1.5, 0, 1.5, 3, 4.5), rev(brewer.pal(7,"RdBu")))
        h1 <- Heatmap( valMatrix_hs, 
            col=rwb,
            cluster_rows=F, cluster_columns=F,
            show_row_names=T, row_names_side="left",
            show_column_names=T,
            column_names_gp = gpar(fontsize=5),
            row_names_gp = gpar(fontsize=5),
            rect_gp = gpar(col= "white"),
            heatmap_legend_param = list(
                title = "zscore of ranking score",
                title_gp = gpar(fontsize = 6),
                #title_position = "left-top",
                direction = "horizontal",
                labels_gp = gpar(fontsize = 5)
            )
        )

        pdf(output_hs, width=1.5, height=5.5)
        draw(h1, heatmap_legend_side = "bottom")
        dev.off()
    |]
    return ()
  where
    names = map (T.unpack . head . T.splitOn ".") $ DF.rowNames df

-- Take the top TFs in each cluster
topTFs :: Int -> DF.DataFrame Double -> DF.DataFrame Double
topTFs n df = df `DF.rsub` tfs
  where
    tfs = nubSort $ flip concatMap (Mat.toColumns $ DF._dataframe_data $
        DF.mapRows scale df) $ \v -> take n $ reverse $ fst $ unzip $
            sortBy (comparing snd) $ zip (DF.rowNames df) $ V.toList v

commonTF :: DF.DataFrame Double -> [T.Text]
commonTF = DF.rowNames . DF.filterRows (\_ x -> V.minimum x >= 1.6) . DF.mapRows scale

tfBuilder :: String -> [T.Text] -> Builder ()
tfBuilder prefix inputs = do
    node "Plot_Ranks" [| \() -> do
        rankFl <- lookupConfig "ranks"
        pvalFl <- lookupConfig "pvalues"
        annoFl <- lookupConfig "annotation"
        dir <- fmap (\x -> x <> "/" <> prefix) $ lookupConfig "output_dir"
        let output = dir <> "/PageRank.pdf"
        liftIO $ do
            anno <- readAnno annoFl
            df <- changeName anno . (`DF.csub` inputs) . DF.mapRows scale . DF.map (logBase 2) <$>
                readRanks rankFl pvalFl
            plotRanks output $ diagonizeDF $
                DF.reorderRows (DF.orderByHClust id) $
                DF.map (\x -> max (-4) $ min 4 x) $
                DF.filterRows (\_ x -> V.maximum x >= 1.65) df
        |] $ return ()