{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell   #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE QuasiQuotes       #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE GADTs #-}
module FB
    ( fbBuilder
    ) where

import HEA

import Bio.Pipeline.CallPeaks
import Statistics.Quantile
import qualified Taiji.Utils.DataFrame as DF
import Bio.RealWorld.GENCODE
import Control.Monad.State.Strict
import Control.Arrow
import qualified Data.Vector as V
import qualified Data.Vector.Mutable as VM
import AI.Clustering.Hierarchical hiding (normalize, size)
import Bio.Data.Bed.Utils
import Bio.Data.Bam
import qualified Data.Text as T
import Bio.Data.Bed.Types hiding (convert)
import Data.List.Split
import Shelly hiding (FilePath, path)
import Data.Int
import qualified Data.IntSet as IS
import Bio.Data.Bed hiding (convert)
import Control.Monad.ST (runST)
import qualified Data.Matrix as Mat
import Data.Matrix.Generic (convert)
import AI.Clustering.KMeans
import qualified Data.ByteString.Char8 as B
import qualified Data.HashMap.Strict as M
import qualified Data.Set as S
import Data.List.Ordered (nubSort)
import Bio.Utils.Functions
import Statistics.Sample (mean)
import Statistics.Correlation (pearson, spearman)

import Taiji.Prelude
import qualified Language.R                        as R
import           Language.R.QQ
import Language.R.HExp

callpeaks :: FilePath -> FilePath -> IO ()
callpeaks input output = withTemp Nothing $ \subsample -> do
    let fl = location .~ subsample $ emptyFile :: File '[] 'Bed
    shelly $ escaping False $
        run_ "zcat" [T.pack $ show input, "|", "shuf", "-n", "20000000", ">", T.pack subsample]
    _ <- callPeaks output fl Nothing opt
    return ()
  where
    opt = def & mode .~ NoModel (-100) 200
        & cutoff .~ QValue 0.01
        & callSummits .~ True
        & gSize .~ Just "hs"

rename :: DF.DataFrame a -> DF.DataFrame a
rename df = DF.fromMatrix (map f $ DF.rowNames df) (DF.colNames df) $ DF._dataframe_data df
  where
    f x | x == "ATAC - sigmoid colon" = "Tissue, ATAC-seq"
        | x == "DNase - hepatocyte" = "control"
        | x == "DNase - myocyte" = "control"
        | "C" `T.isPrefixOf` x = "HEA"
        | "ATAC" `T.isPrefixOf` x = "Fibroblast, in vitro, ATAC-seq"
        | "snATAC" `T.isPrefixOf` x = "Fibroblast, in vivo, snATAC-seq"
        | "DNase" `T.isPrefixOf` x = "Fibroblast, in vitro, DNase-seq"

mkName :: BED3 -> T.Text
mkName x = chr <> ":" <> s <> "-" <> e
  where
    chr = T.pack $ B.unpack $ x^.chrom 
    s = T.pack $ show $ x^.chromStart
    e = T.pack $ show $ x^.chromEnd

fbData :: [(T.Text, String)]
fbData =
    --, ("ATAC - cardiac early", "data/cardiac_fibroblasts_1_rep1.bed.gz")
    --, ("ATAC - cardiac late", "data/cardiac_fibroblasts_4_rep1.bed.gz")
    --, ("ATAC - coronary", "data/coronary_fibroblasts_3_rep1.bed.gz")
    [ ("DNase - dermis", "data/fibroblast of dermis_ENCSR000EPO_rep0_merged.bed.gz")
    , ("DNase - gingiva", "data/fibroblast of gingiva_ENCSR000ENS_rep0_merged.bed.gz")
    , ("DNase - lung", "data/fibroblast of lung_ENCSR000EOJ_rep0_merged.bed.gz")
    --, ("DNase - lung", "data/fibroblast of lung_ENCSR000EPR_rep0_merged.bed.gz")
    , ("DNase - mammary gland", "data/fibroblast of mammary gland_ENCSR000ENW_rep0_merged.bed.gz")
    , ("DNase - peridontal ligament", "data/fibroblast of peridontal ligament_ENCSR000EOI_rep0_merged.bed.gz")
    , ("DNase - pulmonary artery", "data/fibroblast of pulmonary artery_ENCSR000EOH_rep0_merged.bed.gz")
    , ("DNase - aortic adventitia", "data/fibroblast of the aortic adventitia_ENCSR000EMC_rep0_merged.bed.gz")
    , ("DNase - conjunctiva", "data/fibroblast of the conjunctiva_ENCSR000ENK_rep0_merged.bed.gz")
    , ("DNase - villous mesenchyme", "data/fibroblast of villous mesenchyme_ENCSR000EOR_rep0_merged.bed.gz")
    , ("DNase - foreskin", "data/foreskin fibroblast_ENCSR000ENQ_rep0_merged.bed.gz")
    --, ("DNase - foreskin", "data/foreskin fibroblast_ENCSR000EPP_rep0_merged.bed.gz")
    , ("DNase - cardiac", "data/cardiac fibroblast_ENCSR000ENH_rep0_merged.bed.gz")
    --, ("DNase - cardiac", "data/cardiac fibroblast_ENCSR000ENI_rep0_merged.bed.gz")
    , ("DNase - hepatocyte", "data/hepatocyte_ENCSR364MFN_rep0_merged.bed.gz")
    , ("DNase - B cell", "data/B cell_ENCSR000EMJ_rep0_merged.bed.gz")
    ]

fetal_fibroblasts :: [(T.Text, FilePath)]
fetal_fibroblasts = map (\(x, y) -> (x, dir <> y <> suffix)) $
    [ ("sciATAC - adrenal", "adrenal_stromal_cells")
    , ("sciATAC - heart", "heart_stromal_cells")
    , ("sciATAC - intestine", "intestine_stromal_cells")
    , ("sciATAC - lung", "lung_stromal_cells")
    , ("sciATAC - muscle", "muscle_stromal_cells")
    , ("sciATAC - pancreas", "pancreas_stromal_cells")
    , ("sciATAC - placenta", "placenta_stromal_cells")
    , ("sciATAC - spleen", "spleen_stromal_cells")
    , ("sciATAC - stomach", "stomach_stromal_cells")
    , ("sciATAC - liver", "liver_stellate_cells") ]
  where
    dir = "/projects/ps-renlab/kai/project/HEA/Analyses/Figures/Fetal_atlas/output/bigwig/"
    suffix = ".sorted.bgr"

colorSet :: [String]
colorSet = cat20

count :: FilePath -> [BED3] -> IO (V.Vector Double)
count fl regions
    | isBam = do
        header <- getBamHeader fl
        runResourceT $ runConduit $ streamBam fl .| bamToBedC header .| rpkmBed regions
    | isBedGraph = do
        let regions' = zipWith BEDExt regions [0..]
        vec <- VM.replicate (length regions) 0
        let f acc (bdg, xs) = do
                forM_ xs $ \x -> do
                    let v = fromIntegral (sizeOverlapped bdg x) * (bdg^.bdgValue)
                    VM.modify vec (+v) (x^._data)
                return $ acc + fromIntegral (size bdg) * (bdg^.bdgValue)
        s <- runResourceT $ runConduit $ streamBed fl .|
            intersectBedWith (\a b -> (a, b)) regions' .|
            foldMC f 0
        V.map (/(s/1000000)) <$> V.unsafeFreeze vec
    | otherwise = runResourceT $ runConduit $
        streamBedGzip fl .| rpkmBed regions
  where
    isBam = ".bam" `T.isSuffixOf` T.pack fl
    isBedGraph = ".bgr" `T.isSuffixOf` T.pack fl

cor :: DF.DataFrame Double -> DF.DataFrame Double -> DF.DataFrame Double
cor df1 df2 = DF.mkDataFrame (DF.colNames df1) (DF.colNames df2) $
    flip map vals1 $ \v1 -> flip map vals2 $ \v2 -> f v1 v2
  where
    f a b = mean $ V.zipWith (\x y -> (logBase 2 (x+1) - logBase 2 (y+1))**2) a b
    vals1 = Mat.toColumns $ DF._dataframe_data df1
    vals2 = Mat.toColumns $ DF._dataframe_data df2

plotCor :: FilePath
        -> DF.DataFrame Double
        -> DF.DataFrame Double
        -> IO()
plotCor output df1 df2 = R.runRegion $ do
    valMatrix <- toRMatrix df
    [r| library("ggplot2")
        library("ComplexHeatmap")
        library("RColorBrewer")
        library("viridis")
        library("circlize")
        #rwb <- colorRampPalette(colors = rev(brewer.pal(7,"RdBu")))(99)
        rwb <- colorRamp2(c(0, 0.2, 0.4, 0.6, 0.8,1), viridis(6))
        h1 <- Heatmap( valMatrix_hs, 
            row_split = c( rep("Core signature", n_hs)
                , rep("subtype specific signature", n_hs) ),
            col=rwb,
            cluster_rows=F, cluster_columns=T,
            show_row_names=T, row_names_side="left",
            show_column_dend=F,show_row_dend=F,
            show_column_names=T,
            column_names_gp = gpar(fontsize=5),
            row_names_gp = gpar(fontsize=5),
            row_title_gp = gpar(fontsize=6),
            heatmap_legend_param = list(
                title = "Jaccard index",
                title_gp = gpar(fontsize = 6),
                #title_position = "left-top",
                direction = "horizontal",
                labels_gp = gpar(fontsize = 5)
            )
        )

        pdf(output_hs, width=3.0, height=3.2)
        draw(h1, heatmap_legend_side = "bottom")
        dev.off()
    |]
    return ()
  where
    names = map (T.unpack . head . T.splitOn ".") $ DF.rowNames df
    n = fromIntegral $ Mat.rows $ DF._dataframe_data df1 :: Double
    df = DF.rbind [df1,df2]

plotCor' :: FilePath
         -> DF.DataFrame Double
         -> IO()
plotCor' output df = R.runRegion $ do
    valMatrix <- toRMatrix df
    [r| library("ggplot2")
        library("ComplexHeatmap")
        library("RColorBrewer")
        library("viridis")
        library("circlize")
        h1 <- Heatmap( valMatrix_hs, 
            col=viridis(99),
            cluster_rows=T, cluster_columns=T,
            show_row_names=T, row_names_side="left",
            show_column_dend=F,show_row_dend=F,
            show_column_names=T,
            column_names_gp = gpar(fontsize=5),
            row_names_gp = gpar(fontsize=5)
           # heatmap_legend_param = list(
           #     title = "correlatioin",
           #     title_gp = gpar(fontsize = 6),
           #     #title_position = "left-top",
           #     direction = "horizontal",
           #     labels_gp = gpar(fontsize = 5)
           # )
        )

        pdf(output_hs, width=3.0, height=3.2)
        draw(h1, heatmap_legend_side = "bottom")
        dev.off()
    |]
    return ()

fbBuilder = do
    node "Get_Promoters" [| \() -> do
        dir <- lookupConfig "output_dir"
        annoFl <- lookupConfig "gencode"
        let output = dir <> "/promoters.bed"
        liftIO $ do
            genes <- readGenes annoFl
            let pros = flip concatMap genes $ \Gene{..} -> flip map geneTranscripts $ \Transcript{..} ->
                    case () of
                        _ | transStrand -> BED geneChrom (max 0 $ transLeft - 1000)
                                (transLeft + 1000) Nothing Nothing (Just transStrand)
                          | otherwise -> BED geneChrom (max 0 $ transRight - 1000)
                                (transRight + 1000) Nothing Nothing (Just transStrand)
            runResourceT $ runConduit $ mergeBed pros .| sinkFileBed output
            return output
      |] $ return ()
 
    node "Get_Specific_Peak" [| \pkFls -> do
        dir <- fmap (<>"/FB") $ lookupConfig "output_dir"
        let output = dir <> "/specific_peaks.bed"
        liftIO $ do
            peaks <- concat <$> mapM (readBed . snd) pkFls :: IO [BED3]
            writeBed output $ nubSort peaks
            return output
        |] $ return ()
    ["Specific_Peaks"] ~> "Get_Specific_Peak"

    node "List_Files" [| \() -> do
        dir <- lookupConfig "bed_dir"
        liftIO $ do
            files <- fmap (filter (".bed.gz" `T.isSuffixOf`)) $ shelly $
                lsT $ fromText $ T.pack dir
            let beds = filter ((`elem` fibroblast) . fst) $ flip map files $ \fl ->
                    let nm = fst $ T.breakOn ".bed.gz" $ snd $ T.breakOnEnd "/" fl
                    in (nm, T.unpack fl)
            return $ beds ++ fbData
        |] $ return ()
    nodePar "Call_Peaks" [| \(nm, fl) -> do
        dir <- fmap (<>"/FB/Peaks/") $ lookupConfig "output_dir"
        let output = dir <> T.unpack nm <> ".narrowPeak.gz"
        liftIO $ do
            shelly $ mkdir_p $ fromText $ T.pack dir
            callpeaks fl output
        return (nm, output)
        |] $ return ()
    path ["List_Files", "Call_Peaks"]

    uNode "Share_Intersection_Prep" [| \(a,b) -> return $ zip (repeat a) b |]
    nodePar "Share_Intersection" [| \(pkFl, (nm, fl)) -> liftIO $ do
        ref <- readBed pkFl :: IO [BED3]
        query <- runResourceT $ runConduit $ streamBedGzip fl .| sinkList :: IO [BED3]
        p <- fmap (map (^._data)) $ runConduit $
            yieldMany (zipWith BEDExt ref [0::Int ..]) .|
            intersectBed query .| sinkList
        return (nm, p)
        |] $ return ()
    ["Core_Signature", "Call_Peaks"] ~> "Share_Intersection_Prep"
    node "Share_Cor" [| \input -> do
        dir <- fmap (<>"/FB") $ lookupConfig "output_dir"
        let output = dir <> "/share_cor.tsv"
            (d1, d2) = partition (("C" `T.isPrefixOf`) . fst) $
                map (second IS.fromList) input
            val = map (\[x,y] -> f (snd x) $ snd y) $ sequence [d1,d2]
            f x y = let m = fromIntegral $ IS.size $ x `IS.intersection` y
                        n = fromIntegral $ IS.size $ x `IS.union` y
                    in m / n :: Double
            df = DF.mkDataFrame (map fst d1) (map fst d2) $ chunksOf (length d2) val
        liftIO $ do
            DF.writeTable output (T.pack . show) df
            return output
        |] $ return ()
    path ["Share_Intersection_Prep", "Share_Intersection", "Share_Cor"]

    uNode "Specific_Intersection_Prep" [| \(a,b) -> return $ zip (repeat a) b |]
    nodePar "Specific_Intersection" [| \(pkFl, (nm, fl)) -> liftIO $ do
        ref <- readBed pkFl :: IO [BED3]
        query <- runResourceT $ runConduit $ streamBedGzip fl .| sinkList :: IO [BED3]
        p <- fmap (map (^._data)) $ runConduit $
            yieldMany (zipWith BEDExt ref [0::Int ..]) .|
            intersectBed query .| sinkList
        return (nm, p)
        |] $ return ()
    ["Get_Specific_Peak", "Call_Peaks"] ~> "Specific_Intersection_Prep"
    node "Specific_Cor" [| \input -> do
        dir <- fmap (<>"/FB") $ lookupConfig "output_dir"
        let output = dir <> "/specific_cor.tsv"
            (d1, d2) = partition (("C" `T.isPrefixOf`) . fst) $
                map (second IS.fromList) input
            val = map (\[x,y] -> f (snd x) $ snd y) $ sequence [d1,d2]
            f x y = let m = fromIntegral $ IS.size $ x `IS.intersection` y
                        n = fromIntegral $ IS.size $ x `IS.union` y
                    in m / n :: Double
            df = DF.mkDataFrame (map fst d1) (map fst d2) $ chunksOf (length d2) val
        liftIO $ do
            DF.writeTable output (T.pack . show) df
            return output
        |] $ return ()
    path ["Specific_Intersection_Prep", "Specific_Intersection", "Specific_Cor"]
    node "Plot_Cor" [| \(a,b) -> do
        dir <- fmap (<>"/FB") $ lookupConfig "output_dir"
        annoFl <- lookupConfig "annotation"
        let output = dir <> "/cor.pdf"
        liftIO $ do
            anno <- readAnno annoFl
            df1 <- DF.transpose . changeName anno . DF.transpose <$> DF.readTable a
            df2 <- DF.transpose . changeName anno . DF.transpose <$> DF.readTable b
            plotCor output df1 df2
        |] $ return ()
    ["Share_Cor", "Specific_Cor"] ~> "Plot_Cor"

    uNode "List_Files_2" [| \(pro, core, diff) -> return $ zip (repeat (("promoter", pro) : ("core", core) : diff)) $ fbData ++ fetal_fibroblasts
        |]
    nodePar "Sig_Enrich" [| \(sigs, (nm, fl)) -> do
        dir <- fmap (<>"/FB/") $ lookupConfig "output_dir"
        let output = dir <> T.unpack nm <> ".tsv"
        liftIO $ do
            (names, peaks) <- fmap (unzip . concat) $ forM sigs $ \(a, b) -> do
                ps <- readBed b
                return $ zip (repeat a) ps
            rc <- count fl peaks
            B.writeFile output $ B.unlines $ map (\(a, b) -> (B.pack $ T.unpack a <> "\t" <> show b)) $ zip names $ V.toList rc
            {-
            let r = M.toList $ M.fromListWith (++) $ zip names $ map return $ V.toList rc
                scaling = foldl' (+) 0 (fromJust $ lookup "promoter" r) / 1000000
                f x = mean $ V.fromList $ map (log . (+1) . (/scaling)) x
                -}
            return (nm, output)
        |] $ return ()
    node "Plot_Enrich" [| \input -> do
        dir <- fmap (<>"/FB") $ lookupConfig "output_dir"
        let output = dir <> "/enrich.pdf"
        liftIO $ do
            let g x = mean $ V.fromList $ map (log . (+1)) x
            results <- forM input $ \(nm, fl) -> do
                let f [a,b] = (T.pack $ B.unpack a, [readDouble b])
                r <- M.toList . M.fromListWith (++) . map (f . B.split '\t') . B.lines <$> B.readFile fl
                return (nm, map (\(a, b) -> (a, g b))  r)
            let df = DF.rbind $ flip map results $ \(nm, xs) -> 
                    DF.mkDataFrame [nm] (map fst xs) $ [map snd xs]
            plotCor' output df
        |] $ return ()
    ["Get_Promoters", "Core_Signature", "Specific_Peaks"] ~> "List_Files_2"
    path ["List_Files_2", "Sig_Enrich", "Plot_Enrich"]
{-
    nodePar "Specific_Readcount" [| \(pkFl, (nm, fl)) -> liftIO $ do
        peaks <- readBed pkFl
        rc <- count fl peaks
        return (nm, rc)
        |] $ return ()
    node "Make_Specific_Count_Table" [| \inputs -> do
        dir <- fmap (<>"/FB") $ lookupConfig "output_dir"
        let (names, cols) = unzip inputs
            output = dir <> "/fb_specific_count_table.tsv"
            n = V.length $ head cols
        liftIO $ DF.writeTable output (T.pack . show) $
            DF.fromMatrix (map (T.pack . show) [1..n]) names $
            Mat.fromColumns cols
        return output
        |] $ return ()
    path ["Get_Specific_Peak", "List_Files", "Specific_Readcount", "Make_Specific_Count_Table"]

    node "Specific_Cor" [| \(pkFl, t1, t2) -> do
        dir <- fmap (<>"/FB") $ lookupConfig "output_dir"
        let output = dir <> "/specific_cor.tsv"
        liftIO $ do
            let mkName x = chr <> ":" <> s <> "-" <> e
                  where
                    chr = T.pack $ B.unpack $ x^.chrom 
                    s = T.pack $ show $ x^.chromStart
                    e = T.pack $ show $ x^.chromEnd
            peaks <- map mkName <$> (readBed pkFl :: IO [BED3])
            df1 <- DF.readTable t1
            df2 <- DF.readTable t2
            DF.writeTable output (T.pack . show) $ cor (df1 `DF.rsub` peaks) df2
            return output
        |] $ return ()
    ["Get_Specific_Peak", "Extract_Data", "Make_Specific_Count_Table"] ~> "Specific_Cor"

    uNode "List_Files_Share" [| \peakFl -> zip (repeat peakFl) fbData |]
    nodePar "Share_Readcount" [| \(pkFl, (nm, fl)) -> liftIO $ do
        peaks <- readBed pkFl
        rc <- count fl peaks
        return (nm, rc)
        |] $ return ()
    node "Make_Share_Count_Table" [| \(pkFl, inputs) -> do
        dir <- fmap (<>"/FB") $ lookupConfig "output_dir"
        let (names, cols) = unzip inputs
            output = dir <> "/fb_share_count_table.tsv"
            n = V.length $ head cols
        liftIO $ do
            peaks <- map mkName <$> readBed pkFl
            DF.writeTable output (T.pack . show) $
                DF.fromMatrix peaks names $ Mat.fromColumns cols
        return output
        |] $ return ()
    path ["Core_Signature", "List_Files_Share", "Share_Readcount"]
    ["Core_Signature", "Share_Readcount"] ~> "Make_Share_Count_Table"
    -}

    node "ACE2" [| \() -> do
        dir <- fmap (<>"/FB") $ lookupConfig "output_dir"
        geneFl <- lookupConfig "gene"
        annoFl <- lookupConfig "annotation"
        let output = dir <> "/ACE2.pdf"
        liftIO $ do
            anno <- readAnno annoFl
            df <- changeName anno <$> DF.readTable geneFl
            R.runRegion $ do
                [r| library("ggplot2") |]
                fig <- barPlot $ zip (map T.unpack $ DF.colNames df) $
                    V.toList $ df `DF.rindex` ("ACE2" :: T.Text)
                [r| ggsave(output_hs, fig_hs, width=89, height=55, unit="mm", useDingbats=F) |]
                return ()
        |] $ return ()

barPlot :: [(String, Double)] -> R.R s (R.SomeSEXP s)
barPlot input = do
    myTheme
    [r| library("ggplot2")
        df <- data.frame(name=names_hs, val=dat_hs)
        ggplot(df, aes(x=reorder(name, val), y=val)) +
            geom_col() +
            myTheme() + myFill() +
            theme( axis.text.x = element_text(angle = 90, hjust = 1) ) +
            labs(x = "", y = "promoter accessibility of ACE2")
    |]
  where
    (names, dat) = unzip input