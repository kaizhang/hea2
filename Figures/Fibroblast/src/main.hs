{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell   #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE QuasiQuotes       #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE GADTs #-}
import HEA

import qualified Taiji.Utils.DataFrame as DF
import qualified Data.Vector as V
import qualified Data.Text as T
import Bio.Data.Bed.Types
import Bio.Data.Bed
import Control.Monad.ST (runST)
import qualified Data.Matrix as Mat
import qualified Data.ByteString.Char8 as B
import qualified Data.HashMap.Strict as M
import Data.List.Ordered (nubSort)
import Bio.Utils.Functions
import Taiji.Utils.Plot
import Data.Binary
import System.Random.MWC.Distributions (uniformShuffle)
import System.Random.MWC (create)
import Shelly (shelly, run_)

import Taiji.Prelude hiding (groupBy)
import qualified Language.R                        as R
import           Language.R.QQ
import Language.R.HExp

import Diff
import FB
import TF

goblet :: [T.Text]
goblet = ["C40", "C41"]

enc :: [T.Text]
enc = ["C10", "C26", "C47"]

readRanks :: FilePath   -- ^ Ranks
          -> FilePath   -- ^ P-values
          -> IO (DF.DataFrame Double)
readRanks rankFl pvalFl = do
    pvals <- DF.readTable pvalFl
    let tfs = nubSort $ flip concatMap (Mat.toColumns $ DF._dataframe_data pvals) $ \pval ->
            V.toList $ fst $ V.unzip $ filterFDR 0.001 $
            V.zip (V.fromList $ DF.rowNames pvals) pval
    ranks <- DF.readTable rankFl
    let df = ranks `DF.rsub` tfs
    return df

plotRanks :: FilePath
          -> DF.DataFrame Double
          -> IO()
plotRanks output df = R.runRegion $ do
    valMatrix <- toRMatrix df
    [r| library("ggplot2")
        library("ComplexHeatmap")
        library("RColorBrewer")
        library("viridis")
        rwb <- colorRampPalette(colors = rev(brewer.pal(7,"RdBu")))
        h1 <- Heatmap( valMatrix_hs, 
            col=rwb(99),
            cluster_rows=F, cluster_columns=F,
            show_row_names=T, row_names_side="left",
            show_column_names=T,
            column_names_gp = gpar(fontsize=5),
            row_names_gp = gpar(fontsize=5),
            heatmap_legend_param = list(
                title = "zscore of ranking score",
                title_gp = gpar(fontsize = 6),
                #title_position = "left-top",
                direction = "horizontal",
                labels_gp = gpar(fontsize = 5)
            )
        )

        pdf(output_hs, width=1.5, height=4.5)
        draw(h1, heatmap_legend_side = "bottom")
        dev.off()
    |]
    return ()
  where
    names = map (T.unpack . head . T.splitOn ".") $ DF.rowNames df

-- Take the top TFs in each cluster
topTFs :: Int -> DF.DataFrame Double -> DF.DataFrame Double
topTFs n df = df `DF.rsub` tfs
  where
    tfs = nubSort $ flip concatMap (Mat.toColumns $ DF._dataframe_data $
        DF.mapRows scale df) $ \v -> take n $ reverse $ fst $ unzip $
            sortBy (comparing snd) $ zip (DF.rowNames df) $ V.toList v

build "wf" [t| SciFlow ENV |] $ do
    namespace "FB" $ diffBuilder "FB" fibroblast 10
    namespace "FB" $ tfBuilder "FB" fibroblast
    namespace "FB" $ fbBuilder

main :: IO ()
main = mainFun wf