{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell   #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE QuasiQuotes       #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE GADTs #-}
module Diff
    ( diffBuilder
    , fibroblast
    ) where

import HEA

import Statistics.Quantile
import qualified Taiji.Utils.DataFrame as DF
import Bio.RealWorld.GENCODE
import Control.Monad.State.Strict
import Control.Arrow
import qualified Data.Vector as V
import AI.Clustering.Hierarchical hiding (normalize)
import Data.Either
import Data.Tuple
import Control.Parallel.Strategies (parMap, rdeepseq)
import qualified Data.Text as T
import Bio.Data.Bed.Types hiding (convert)
import Shelly hiding (FilePath, path)
import Data.Int
import Bio.Data.Bed hiding (convert)
import Control.Monad.ST (runST)
import qualified Data.Matrix as Mat
import Data.Matrix.Generic (convert)
import AI.Clustering.KMeans
import qualified Data.ByteString.Char8 as B
import qualified Data.HashMap.Strict as M
import qualified Data.Set as S
import Data.List.Ordered (nubSort)
import Bio.Utils.Functions
import Taiji.Utils.Plot
import Taiji.Utils
import Statistics.Sample (mean, meanVarianceUnb)
import System.Random.MWC.Distributions (uniformShuffle)
import System.Random.MWC (create)
import Text.Wrap

import Taiji.Prelude
import qualified Language.R                        as R
import           Language.R.QQ
import Language.R.HExp

fdrCutoff :: Double
fdrCutoff = 0.01

fibroblast :: [T.Text]
fibroblast = ["C1.1", "C1.2", "C1.3", "C1.6", "C1.7", "C1.9", "C8.1"]

{-
Thus, we adopted the following criteria: (i)
expression observed in all tissues; (ii) low variance over
tissues: standard-deviation [log2(RPKM)]<1; and (iii) no
exceptional expression in any single tissue; that is, no logexpression value differed from the averaged log2(RPKM)
by two (fourfold) or more. These criteria resulted in a list of
37 363 unique exons (20% of studied exons), belonging to
11 648 RefSeq transcripts and 6289 genes.
-}
housekeepingCRE :: DF.DataFrame Double -> [T.Text]
housekeepingCRE df = DF.rowNames $ DF.filterRows f df
  where
    f _ vec = V.all (>1) vec && cv < 0.5 && V.all (\x -> abs (logBase 2 x - m) / std < 2) vec 
      where
        (m, var) = meanVarianceUnb $ V.map (logBase 2) vec
        std = sqrt var
        cv = std / m
        
mkName :: BED3 -> T.Text
mkName x = chr <> ":" <> s <> "-" <> e
  where
    chr = T.pack $ B.unpack $ x^.chrom 
    s = T.pack $ show $ x^.chromStart
    e = T.pack $ show $ x^.chromEnd

mkPeak :: T.Text -> BED3
mkPeak x = asBed (B.pack $ T.unpack chr) (read $ T.unpack a) (read $ T.unpack b)
  where
    [chr, x'] = T.splitOn ":" x
    [a,b] = T.splitOn "-" x'
 

-- | Compute the cluster x tissue composition table.
cellCount :: [CellCluster] -> DF.DataFrame Int
cellCount clusters = DF.mkDataFrame rownames colnames $
    flip map rows $ \x -> map (\i -> M.lookupDefault 0 i x) colnames
  where
    (rownames, rows) = unzip $ flip map clusters $ \CellCluster{..} ->
        ( T.pack $ B.unpack _cluster_name
        , M.fromListWith (+) $ map (\x -> (getName x, 1)) _cluster_member )
    colnames = nubSort $ concatMap M.keys rows
    getName Cell{..} = head $ T.splitOn "_SM" $
        T.pack $ B.unpack _cell_barcode

getIndex :: [BED3]  -- ^ Master peak list
         -> [BED3]  -- ^ Peaks
         -> [Int]
getIndex peakList peaks = runIdentity $ runConduit $
    yieldMany p .| intersectBed peaks .| mapC (^._data) .| sinkList
  where
    p = zipWith (\x i -> BEDExt x i) peakList [0..]

type Peak = (Int, Double, Double, Double)

getDiffPeaks' :: FilePath   -- ^ peak list
              -> FilePath   -- ^ peak 1
              -> FilePath   -- ^ peak 2
              -> FilePath   -- ^ input 1
              -> FilePath   -- ^ input 2
              -> IO [Peak]
getDiffPeaks' peaklist pk1 pk2 i1 i2 = do
    peaks <- runResourceT $ runConduit $ streamBedGzip peaklist .| sinkList
    p1 <- runResourceT $ runConduit $ streamBedGzip pk1 .| sinkList
    p2 <- runResourceT $ runConduit $ streamBedGzip pk2 .| sinkList
    let idx2 = getIndex peaks p2
        idx1 = getIndex peaks p1
        idx = nubSort $ idx1 ++ idx2
    withTemp Nothing $ \tmp -> do
        diffPeak tmp idx i1 i2
        map (f . B.split '\t') . tail . B.lines <$> B.readFile tmp
  where
    f xs = (readInt $ head xs, readDouble $ xs!!3, readDouble $ xs!!4, readDouble $ xs!!5)
    diffPeak :: FilePath
            -> [Int]
            -> FilePath
            -> FilePath
            -> IO ()
    diffPeak output idx fl1 fl2 = withTemp Nothing $ \tmp -> do
        writeFile tmp $ unlines $ map show idx
        shelly $ run_ "taiji-utils" ["diff", T.pack fl1, T.pack fl2
            , "--index", T.pack tmp
            , "--fold", "2"
            , "--thread", "20"
            , "--output", T.pack output]

getDiffPeaks :: T.Text -> T.Text
             -> ReaderT ENV IO 
                 ( (T.Text, [Peak])
                 , (T.Text, [Peak]) )
getDiffPeaks a b = do
    peak <- lookupConfig "master_peak"
    dir <- lookupConfig "cluster_dir"
    liftIO $ do
        res <- liftIO $ getDiffPeaks' peak
            (dir <> "/" <> T.unpack a <> ".narrowPeak.gz")
            (dir <> "/" <> T.unpack b <> ".narrowPeak.gz")
            (dir <> "/" <> T.unpack a <> ".mat.gz")
            (dir <> "/" <> T.unpack b <> ".mat.gz")
        let (da, db) = partition (\x -> x^._2 > 0) res
        return ((a, da), (b, db))

comb (x:xs) = zip (repeat x) xs ++ comb xs
comb [] = []

-- | Compute the cluster x tissue composition table.
composition :: [CellCluster] -> DF.DataFrame Double
composition clusters = 
    DF.mapRows normalize $
    DF.mapCols normalize $
    DF.map fromIntegral $
    DF.mkDataFrame rownames colnames $
    flip map rows $ \x -> map (\i -> M.lookupDefault 0 i x) colnames
  where
    (rownames, rows) = unzip $ flip map clusters $ \CellCluster{..} ->
        ( T.pack $ B.unpack _cluster_name
        , M.fromListWith (+) $ map (\x -> (getName x, 1)) _cluster_member )
    colnames = nubSort $ concatMap M.keys rows
    getName Cell{..} = fst $ T.breakOn "_atrial_appendage" $
        fst $ T.breakOn "_lv" $
        fst $ T.breakOn "_ge_junction" $
        fst $ T.breakOn "_mucosa" $
        fst $ T.breakOn "_muscularis" $
        fst $ T.breakOn "_sun_exposed" $
        fst $ T.breakOn "_tibial" $
        fst $ T.breakOn "_aorta" $
        T.init $ fst $ T.breakOnEnd "_" $ fst $ T.breakOnEnd "+" $
        T.pack $ B.unpack _cell_barcode
    normalize :: V.Vector Double -> V.Vector Double
    normalize xs | V.all (==0) xs = xs
                 | otherwise = V.map (/V.sum xs) xs

plotHeatmap :: FilePath
            -> DF.DataFrame Double
            -> [(T.Text, FilePath)]
            -> [[GREAT]]
            -> IO ()
plotHeatmap output input peaks gos = do
    (dfs, title, sizes) <- fmap (unzip3 . diagonize (average . (^._1))) $ forM (snd $ unzip peaks) $ \fl -> do
        beds <- readBed fl :: IO [BED3]
        let names = map (\x -> T.pack $ B.unpack (x^.chrom) <> ":" <> show (x^.chromStart) <> "-" <> show (x^.chromEnd)) beds
            df = DF.reorderRows (DF.orderByHClust id) $ input `DF.rsub` sample n' names
            n = length names
            n' = truncate $ fromIntegral n / 10
        return (df, "n = " <> show n, n')
    let groups = flip concatMap (zip [1..] sizes) $ \(i, n) -> replicate n $ show i

    R.runRegion $ do
        let dat = concat $ Mat.toLists $ DF._dataframe_data df
            names = map T.unpack $ DF.colNames df
            row = fromIntegral $ Mat.rows $ DF._dataframe_data df :: Double
            df = DF.rbind dfs

        [r| go_terms <<- list() |]
        forM_ gos $ \x -> do
                let res = map (B.unpack . _great_term_name) $ take 3 x
                [r| go_terms[[length(go_terms)+1]] <<- data.frame(go=res_hs) |]

        [r| library("ggplot2")
            library("ComplexHeatmap")
            library("RColorBrewer")
            library("viridis")
            library("ggplotify")
            library("gridExtra")

            mat <- matrix(dat_hs, row_hs, byrow=T)
            colnames(mat) <- names_hs

            rg <- range(mat)

            # GO terms
            panel_fun <- function(index, nm) {
                pushViewport(viewport(xscale = rg, yscale = c(0, 2)))
                grid.rect()
                #grid.xaxis(gp = gpar(fontsize = 8))
                d <- sapply(lapply(go_terms[[as.integer(nm)]]$go, strwrap, width=60), paste, collapse="\n")
                grid.table(d,
                    theme=ttheme_minimal(base_size=5,
                        padding=unit(c(1, 1), "mm"),
                    )
                )
                popViewport()
            }

            color <- c(cat20_hs, "black")

            hm <- Heatmap(mat, col=viridis(99), use_raster=F,
                show_row_names = F,
                cluster_rows=F, cluster_columns=F,
                row_split=groups_hs,
                row_title=title_hs,
                column_names_gp = gpar(fontsize=6),
                row_title_gp = gpar(fontsize=6),
                heatmap_legend_param = list(
                    title = "log2(accessibility)",
                    title_gp = gpar(fontsize = 6),
                    title_position = "leftcenter-rot",
                    labels_gp = gpar(fontsize = 5)
                ),
                right_annotation = rowAnnotation(go=anno_zoom(
                    align_to = groups_hs, which = "row", panel_fun = panel_fun, 
                    size = unit(10, "mm"), gap = unit(5, "mm"),
                    width = unit(60, "mm")
                ))
            )
            pdf(output_hs, width=5, height=4.2)
            draw(hm, heatmap_legend_side = "right")
            dev.off()
        |]
        return ()
  where
    diagonize :: (a -> V.Vector Double) -> [a] -> [a]
    diagonize f xs = map fst $ sortBy (comparing snd) $ map (\x -> (x, g $ f x)) xs
      where
        g vec = let ave = mean vec
                    max' = V.maximum vec
                    i = if (max' - ave) / max' < 0.5 then -1 else V.maxIndex vec
                in (i, negate ave)
    average :: DF.DataFrame Double -> V.Vector Double
    average df = V.map (/n) $ foldl1' (V.zipWith (+)) $ Mat.toRows $ DF._dataframe_data df
      where
        n = fromIntegral $ fst $ DF.dim df

sample :: Int -> [a] -> [a]
sample n xs = runST $ fmap (V.toList . V.take n) $ create >>= uniformShuffle (V.fromList xs)

getHomerResult :: FilePath -> IO [String]
getHomerResult dir = mapM (readMotif . getMotif) [1,2,3]
  where
    getMotif i = printf "%s/homerResults/motif%d.motif" dir (i :: Int)
    readMotif fl = do
        xs <- B.split '\t' . head . B.lines <$> B.readFile fl
        return $ B.unpack $ fst $ B.break (=='/') $ fst $
            B.break (=='(') $ B.tail $ snd $ B.break (==':') $ xs!!1

clustering :: Int -> DF.DataFrame Double -> [[BED3]]
clustering k df = map (map toPeak) $ decode (membership res) $ DF.rowNames df
  where
    toPeak x = let [chr,y] = T.splitOn ":" x
                   [a,b] = T.splitOn "-" y
               in BED3 (B.pack $ T.unpack chr) (read $ T.unpack a) (read $ T.unpack b)
    res = kmeans k (convert $ DF._dataframe_data df) defaultKMeansOpts

plotDis :: FilePath -> DF.DataFrame Double -> IO()
plotDis output df = R.runRegion $ do
    valMatrix <- toRMatrix df
    [r| library("ggplot2")
        library("ComplexHeatmap")
        library("RColorBrewer")
        library("viridis")
        library("circlize")
        h1 <- Heatmap( valMatrix_hs, 
            col=viridis(99),
            cluster_rows=F, cluster_columns=F,
            show_row_names=T, row_names_side="left",
            show_column_names=T,
            column_names_gp = gpar(fontsize=5),
            row_names_gp = gpar(fontsize=5),
            heatmap_legend_param = list(
                title = "fraction",
                title_gp = gpar(fontsize = 6),
                #title_position = "left-top",
                direction = "horizontal",
                labels_gp = gpar(fontsize = 5)
            )
        )

        pdf(output_hs, width=1.5, height=2.5)
        draw(h1, heatmap_legend_side = "bottom")
        dev.off()
    |]
    return ()
  where
    names = map (T.unpack . head . T.splitOn ".") $ DF.rowNames df

diffBuilder :: String -> [T.Text] -> Int -> Builder ()
diffBuilder prefix inputs nCl = do
    node "Get_Peak_List" [| \() -> do
        peakDir <- lookupConfig "cluster_dir"
        peakFl <- lookupConfig "master_peak"
        dir <- fmap (\x -> x <> "/" <> prefix) $ lookupConfig "output_dir"
        shelly $ mkdir_p $ fromText $ T.pack dir
        let output = dir <> "/all_peaks.bed"
        liftIO $ do
            files <- fmap (filter (".narrowPeak.gz" `T.isSuffixOf`)) $ shelly $
                lsT $ fromText $ T.pack peakDir
            let peakFiles = map T.unpack $ filter ((`elem` inputs) . fst . T.breakOn ".narrowPeak.gz" . snd . T.breakOnEnd "/") files
            beds <- fmap concat $ forM peakFiles $ \fl -> runResourceT $
                runConduit $ streamBedGzip fl .| sinkList :: IO [BED3]
            runResourceT $ runConduit $
                (streamBedGzip peakFl :: ConduitT () BED3 (ResourceT IO) ()) .| intersectBed beds .| sinkFileBed output
            return output
        |] $ return ()

    node "Extract_Data" [| \fl -> do
        input <- lookupConfig "input"
        dir <- fmap (\x -> x <> "/" <> prefix) $ lookupConfig "output_dir"
        shelly $ mkdir_p $ fromText $ T.pack dir
        let output = dir <> "/table.tsv"
            clusters = inputs
        liftIO $ do
            beds <- readBed fl :: IO [BED3]
            df <- DF.readTable input
            DF.writeTable output (T.pack . show) $
                (df `DF.csub` clusters) `DF.rsub` map mkName beds 
            return output
        |] $ return ()

    node "Get_Shared_Peaks" [| \table -> do
        peakDir <- lookupConfig "cluster_dir"
        dir <- fmap (\x -> x <> "/" <> prefix) $ lookupConfig "output_dir"
        shelly $ mkdir_p $ fromText $ T.pack dir
        let output = dir <> "/shared_peaks.bed.gz"
        liftIO $ do
            df <- DF.readTable table
            files <- fmap (filter (".narrowPeak" `T.isSuffixOf`)) $ shelly $
                lsT $ fromText $ T.pack peakDir
            let peakFiles = map T.unpack $ filter ((`elem` inputs) . fst . T.breakOn "." . snd . T.breakOnEnd "/") files
                refPeak = map mkPeak $ DF.rowNames df
                f acc x = do
                    p <- readBed x :: IO [BED3]
                    runConduit $ yieldMany acc .| intersectBed p .| sinkList
            shared <- foldM f refPeak peakFiles
            let result = map mkPeak $ housekeepingCRE $ df `DF.rsub` map mkName shared
            runResourceT $ runConduit $ yieldMany result .| sinkFileBedGzip output
            return output
        |] $ return ()
    path ["Get_Peak_List", "Extract_Data", "Get_Shared_Peaks"]
    node "Merge_Matrix" [| \() -> do
        peakDir <- lookupConfig "cluster_dir"
        dir <- fmap (\x -> x <> "/" <> prefix) $ lookupConfig "output_dir"
        shelly $ mkdir_p $ fromText $ T.pack dir
        let output1 = dir <> "/merged.mat.gz"
            output2 = dir <> "/control.mat.gz"
        liftIO $ do
            files <- fmap (filter (".mat.gz" `T.isSuffixOf`)) $ shelly $
                lsT $ fromText $ T.pack peakDir
            let (target, control) = partition
                    ((`elem` inputs) . fst . T.breakOn ".mat.gz" . snd . T.breakOnEnd "/") files
            gen <- create
            mat <- fmap concatMatrix $ forM target $ \x -> do
                m <- mkSpMatrix id $ T.unpack x
                sampleRows 1000 m gen
            saveMatrix output1 id mat
            mat' <- fmap concatMatrix <$> forM control $ \x -> do
                m <- mkSpMatrix id $ T.unpack x
                sampleRows 100 m gen
            saveMatrix output2 id mat'
            return (output1, output2)
        |] $ return ()
    node "Core_Signature" [| \(shared, (target, control)) -> do
        dir <- fmap (\x -> x <> "/" <> prefix) $ lookupConfig "output_dir"
        peakFl <- lookupConfig "master_peak"
        let output = dir <> "/core_signature.bed"
        liftIO $ do
            res <- getDiffPeaks' peakFl shared shared target control
            let idx = map (^._1) $ filter (\x -> x^._4 < fdrCutoff) $ fst $
                    partition (\x -> x^._2 > 0) res
            peaks <- fmap V.fromList $ runResourceT $
                runConduit $ streamBedGzip peakFl .| sinkList :: IO (V.Vector BED3)
            writeBed output $ map (peaks V.!) idx
            return output
        |] $ return ()
    ["Get_Shared_Peaks", "Merge_Matrix"] ~> "Core_Signature"

    ---------------------------------------------------------------------------
    -- Diff analysis
    ---------------------------------------------------------------------------
    uNode "Diff_Analysis_Prep" [| \() -> return $ comb inputs |]
    nodePar "Diff_Analysis" [| uncurry getDiffPeaks |] $ nCore .= 10

    node "Specific_Peaks" [| \(peaklist, input) -> do
        peakFl <- lookupConfig "master_peak"
        dir <- fmap (\x -> x <> "/" <> prefix) $ lookupConfig "output_dir"
        liftIO $ do
            shelly $ mkdir_p $ fromText $ T.pack dir
            allPeaks <- readBed peaklist :: IO [BED3]

            peaks <- fmap V.fromList $ runResourceT $
                runConduit $ streamBedGzip peakFl .| sinkList

            let f fdr = second $ map (^._1) . filter (\x -> x^._4 < fdr)
                intersectDiff = M.toList $ M.fromListWith (\a b -> a `intersect` b) $
                    concatMap (\(a,b) -> [f fdrCutoff a, f fdrCutoff b]) input
            --    unionDiff = S.fromList $ map (peaks V.!) $ S.toList $
            --        S.fromList $ concat $ map snd $ 
            --       concatMap (\(a,b) -> [f 0.1 a, f 0.1 b]) input
            --writeBed (dir <> "/common.bed") $ filter (not . (`S.member` unionDiff)) allPeaks 
            forM intersectDiff $ \(nm, idx) -> do
                let output = dir <> "/" <> T.unpack nm <> ".bed"
                    beds = map (peaks V.!) idx :: [BED3]
                runResourceT $ runConduit $ yieldMany beds .| sinkFileBed output
                return (nm, output)
        |] $ return ()
    uNode "Diff_GREAT_Prep" [| \(a, xs) -> return $ ("Common", a) : xs |]
    nodePar "Diff_GREAT" [| \(nm, bed) -> liftIO $ do
        go <- filterGREAT <$> great bed
        return (nm, go)
        |] $ return ()
    path ["Diff_Analysis_Prep", "Diff_Analysis"]
    ["Get_Peak_List", "Diff_Analysis"] ~> "Specific_Peaks"
    ["Core_Signature", "Specific_Peaks"] ~> "Diff_GREAT_Prep"
    path ["Diff_GREAT_Prep", "Diff_GREAT"]

    node "Diff_Make_Heatmap" [| \(tableFl, peaks, terms) -> do
        dir <- fmap (\x -> x <> "/" <> prefix) $ lookupConfig "output_dir"
        annoFl <- lookupConfig "annotation"
        let output = dir <> "/diff.pdf"
        liftIO $ do
            forM_ terms $ \(nm, go) ->
                writeGREAT (dir <> "/" <> T.unpack nm <> "_go.tsv") go
            anno <- readAnno annoFl
            table <- DF.reorderColumns (DF.orderByHClust id) . changeName anno . DF.map (min 6 . logBase 2 . (+1)) <$> DF.readTable tableFl
            plotHeatmap output table peaks (map snd terms)
        |] $ return ()
    ["Extract_Data", "Diff_GREAT_Prep",
        "Diff_GREAT"] ~> "Diff_Make_Heatmap"

    node "HOMER_Background" [| \() -> do
        dir <- fmap (\x -> x <> "/" <> prefix) $ lookupConfig "output_dir"
        peakFl <- lookupConfig "master_peak"
        let output = dir <> "/background_peak.bed"
        liftIO $ do
            gen <- create
            peaks <- runResourceT $ runConduit $
                streamBedGzip peakFl .| sinkVector :: IO (V.Vector BED3)
            bg <- V.toList . V.take 10000 <$> uniformShuffle peaks gen
            writeBed output bg
            return output
        |] $ return ()
    uNode "HOMER_Prep" [| \(bg, xs) -> return $ zip (repeat bg) xs |]
    nodePar "HOMER" [| \(bg, (nm, bed)) -> do
        dir <- fmap (\x -> x <> "/" <> prefix) $ lookupConfig "output_dir"
        let output = dir <> "/" <> T.unpack nm <> "_HOMER"
        liftIO $ do
            shelly $ run_ "findMotifsGenome.pl"
                [ T.pack bed
                , "hg38"
                , T.pack output 
                , "-size"
                , "200"
                , "-mask" 
                , "-bg"
                , T.pack bg
                ]
            return (nm, output)
        |] $ return ()
    ["HOMER_Background", "Diff_GREAT_Prep"] ~> "HOMER_Prep"
    path ["HOMER_Prep", "HOMER"]