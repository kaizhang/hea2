{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell   #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE QuasiQuotes       #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE PartialTypeSignatures #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE GADTs #-}
import HEA

import Statistics.Quantile
import qualified Taiji.Utils.DataFrame as DF
import Bio.RealWorld.GENCODE
import Control.Monad.State.Strict
import Control.Arrow
import qualified Data.Vector as V
import AI.Clustering.Hierarchical hiding (normalize)
import Data.Either
import Control.Parallel.Strategies (parMap, rdeepseq)
import qualified Data.Text as T
import Bio.Data.Bed.Types
import Shelly hiding (FilePath, path)
import Data.Int
import Bio.Data.Bed
import Control.Monad.ST (runST)
import qualified Data.Matrix as Mat
import qualified Data.ByteString.Char8 as B
import qualified Data.HashMap.Strict as M
import Data.List.Ordered (nubSort)
import Bio.Utils.Functions
import Taiji.Utils.Plot
import Statistics.Sample (mean, meanVarianceUnb)
import Data.Binary
import System.Random.MWC.Distributions (uniformShuffle)
import System.Random.MWC (create)
import Text.Wrap

import Taiji.Prelude
import qualified Language.R                        as R
import           Language.R.QQ
import Language.R.HExp

import CREModule
import Motif
import ML

abbr :: T.Text -> T.Text
abbr txt' | T.length txt > 30 = T.unwords $ map f $ T.words txt
          | otherwise = txt
  where
    txt = T.replace "independent" "indep." $
        T.replace "dependent" "dep." $
        T.replace "platelet-derived growth factor receptor-beta" "PDGFR-beta" $
        T.replace "transforming growth factor beta" "TGF-beta" txt'
    f x = fromMaybe x $ lookup x table
    table = [ ("regulation", "reg.")
            , ("inactivation", "inact.")
            , ("adhesion", "adh.")
            , ("proliferation", "prolifer.")
            , ("signaling", "sig.")
            , ("development", "dev.")
            , ("negative", "neg.")
            , ("postive", "pos.")
            , ("process", "proc.")
            ]

--------------------------------------------------------------------------------
-- Restricted peaks
--------------------------------------------------------------------------------

average :: DF.DataFrame Double -> V.Vector Double
average df = f $ Mat.toRows $ DF._dataframe_data df
  where
    f xs = V.map (/n) $ foldl1' (V.zipWith (+)) xs
      where n = fromIntegral $ length xs

getSignal :: DF.DataFrame Double -> (T.Text, [BED3]) -> (T.Text, V.Vector Double)
getSignal df (name, peaks) = 
    let rows = map mkName peaks 
        cols = DF.colNames df
        df' = DF.mkDataFrame (map (\x -> name <> "_" <> x) rows) cols $
            Mat.toLists $ DF._dataframe_data $ df `DF.rsub` rows
    in (name, average df')
    --return $ DF.mkDataFrame [name] (DF.colNames df) [V.toList $ average $ df `DF.rsub` rows]

mkName :: BED3 -> T.Text
mkName p = T.pack $ B.unpack (p^.chrom) <> ":" <> show (p^.chromStart) <>
    "-" <> show (p^.chromEnd)

mkGOMatrix :: [(T.Text, [GREAT])] -> DF.DataFrame Double
mkGOMatrix input = DF.mkDataFrame (map fst input) (map (abbr . T.pack . B.unpack) terms) $
    flip map input $ \(_, go) -> 
        let m = M.fromList $ map (\x -> (_great_term_name x, negate $ logBase 10 $ max 1e-100 $ _great_pvalue x)) go
        --let m = M.fromList $ map (\x -> (_great_term_name x, _great_enrichment x)) go
        in map (\x -> M.lookupDefault 0 x m) terms
  where
    terms = nubSort $ concatMap (take 3 . map _great_term_name . snd) $
        flip map input $ second $ \terms -> fst $ unzip $ sortBy (comparing snd) $
            V.toList $ filterFDR 0.01 $ V.fromList $ map (\x -> (x, _great_pvalue x)) $
            filter ((>=2) . _great_enrichment) terms

plotGO :: FilePath
       -> DF.DataFrame Double
       -> IO()
plotGO output df = R.runRegion $ do
    valMatrix <- toRMatrix df
    [r| library("ggplot2")
        library("ComplexHeatmap")
        library("RColorBrewer")
        library("viridis")
        h1 <- Heatmap( valMatrix_hs, 
            col=magma(9),
            cluster_rows=F, cluster_columns=F,
            show_row_names=T, row_names_side="left",
            show_column_names=T,
            row_names_gp = gpar(fontsize=5),
            column_names_gp = gpar(fontsize=5),
            heatmap_legend_param = list(
                title = "-log10(P-value)",
                title_position = "leftcenter-rot"
                #at = c(-2, 0, 2)
            )
        )
        pdf(output_hs, width=6.8, height=8.0)
        draw(h1)
        dev.off()
    |]
    return ()
  where
    names = map (T.unpack . head . T.splitOn ".") $ DF.rowNames df

plotGREAT :: FilePath -> [(T.Text, [GREAT])] -> IO [(T.Text, String)]
plotGREAT output input = R.runRegion $ do
    myTheme
    [r| library("ggplot2")
        library("gridExtra")
        plt <<- list()
    |]
    forM_ input $ \(title, terms) -> unless (null terms) $ do
        let (go, pval) = unzip $ map (\x ->
                ( T.unpack $ wrapText defaultWrapSettings{breakLongWords=True} 45 $
                    T.pack $ B.unpack $ _great_term_name x
                , negate $ logBase 10 $ _great_pvalue x) ) $ take 5 terms
            t = T.unpack title
        [r| df <- data.frame(go=go_hs, pval=pval_hs)
            df$go <- factor(df$go, levels = df$go)
            g <- ggplot(df, aes(go,pval)) +
                geom_col() +
                coord_flip() +
                labs(title=t_hs, x = "", y = "-log10(p-value)") +
                myTheme() + myFill() +
                theme( plot.margin=unit(c(1,0.5,1,0.5),"mm") )
            plt <<- append(plt, list(g))
        |]
        return ()

    [r| fig <- marrangeGrob(grobs=plt, ncol=5, nrow=10, top=NULL)
        ggsave(output_hs, fig, width=183, height=247, unit="mm", useDingbats=F)
    |]
    return topTerms
  where
    topTerms = flip map (filter (not . null . snd) input) $
        \(nm, x) -> (nm, B.unpack $ _great_term_name $ head x)

--plotGREAT' :: FilePath -> [(T.Text, [GREAT])] -> IO ()
--plotGREAT' output terms = do


plotRestrictedPeaks :: [(T.Text, Int)]   -- ^ Number of peaks
                    -> [(T.Text, String)]   -- ^ Enriched GO terms
                    -> DF.DataFrame Double
                    -> (forall s. R.R s (R.SomeSEXP s)) 
plotRestrictedPeaks num_peaks terms df = do
    mat <- toRMatrix df
    [r| library("ggplot2")
        library("ComplexHeatmap")
        library("ggplotify")
        library("viridis")
        as.ggplot(Heatmap( mat_hs,
            col=viridis(99),
            cluster_rows=F, cluster_columns=F,
            show_row_names=F,
            show_column_names=T, column_names_side="top",
            column_names_gp = gpar(fontsize=5),
            heatmap_legend_param = list(
                title = "mean(log(1+acc.))",
                title_gp = gpar(fontsize = 6),
                title_position = "leftcenter-rot",
                labels_gp = gpar(fontsize = 5)
            ),
            right_annotation = rowAnnotation( 
                num_elems=anno_barplot(num_hs,
                    gp = gpar(col = "white", fill = "#4DBBD5"), 
                    border=F,
                    axis_param = list(
                        #at=c(0, 5000, 10000, 20000),
                        #labels=c("0", "5k", "10k", "20k"),
                        gp=gpar(fontsize=5)
                    )
                ),
                foo = anno_text(text_hs, gp = gpar(fontsize=5, lineheight=0.65)),
                show_annotation_name = F
            )
        ))
    |]
  where
    num = map (\x -> fromIntegral $ fromJust $ lookup x num_peaks :: Double) $ DF.rowNames df
    text = map (\x -> fromMaybe "NA" $ lookup x terms) $ DF.colNames df

--------------------------------------------------------------------------------
-- Workflow
--------------------------------------------------------------------------------

build "wf" [t| SciFlow ENV |] $ do
    moduleBuilder
    mlBuilder
    motifBuilder

main :: IO ()
main = mainFun wf