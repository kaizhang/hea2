{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell   #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE QuasiQuotes       #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE GADTs #-}
module Motif (motifBuilder) where

import HEA

import Statistics.Quantile
import Data.Char (toUpper, isLower)
import qualified Taiji.Utils.DataFrame as DF
import Bio.RealWorld.GENCODE
import Control.Monad.State.Strict
import Control.Arrow
import qualified Data.Vector as V
import AI.Clustering.Hierarchical
import Data.Either
import Control.Parallel.Strategies (parMap, rdeepseq)
import qualified Data.Text as T
import Bio.Motif
import Bio.Data.Bed.Utils
import Shelly hiding (FilePath, path)
import Data.Int
import Bio.Data.Bed
import Control.Monad.ST (runST)
import qualified Data.Matrix as Mat
import qualified Data.ByteString.Char8 as B
import qualified Data.ByteString.Lazy.Char8 as BL
import qualified Data.HashMap.Strict as M
import qualified Data.Map.Strict as Map
import qualified Data.HashSet as S
import Data.List.Ordered (nubSort)
import Bio.Utils.Functions
import Taiji.Utils.Plot
import Statistics.Sample (mean, meanVarianceUnb, varianceUnbiased)
import Data.Binary
import Data.List.Split (chunksOf)
import Bio.Seq.IO
import System.Random.MWC.Distributions (uniformShuffle)
import System.Random.MWC (create)
import Text.Wrap
import ELynx.Tree hiding (partition)

import Taiji.Prelude
import Data.Vector.SEXP (toList)
import qualified Language.R                        as R
import qualified Language.R.HExp as H
import           Language.R.QQ

enrichment :: S.HashSet B.ByteString -> [B.ByteString] -> Int -> IO (Double, Double)
enrichment motifs fg nBg' = if e >= 0
    then R.runRegion $ do
        p <- [r| phyper(oFg_hs - 1, oBg_hs, nBg_hs - oBg_hs, nFg_hs, lower.tail=F, log.p=T) / log(10)
            |]
        R.unSomeSEXP p $ \x -> case H.hexp x of
            H.Real x'  -> return (e, head $ toList x')
    else R.runRegion $ do
        p <- [r| phyper(oFg_hs, oBg_hs, nBg_hs - oBg_hs, nFg_hs, lower.tail=T, log.p=T) / log(10)
            |]
        R.unSomeSEXP p $ \x -> case H.hexp x of
            H.Real x'  -> return (e, head $ toList x')
  where
    e = logBase 2 $ (oFg / nFg) / (oBg / nBg)
    oFg = fromIntegral $ length $ filter (`S.member` motifs) fg :: Double
    oBg = fromIntegral $ S.size motifs :: Double
    nFg = fromIntegral $ length fg :: Double
    nBg = fromIntegral nBg' :: Double

filterMotifResult :: FilePath
                  -> IO [(B.ByteString, Double)]
filterMotifResult fl = 
    map (second h) .
        map (\(a,b,c) -> (a, (b,c))) .
        filter (not . isMouse . (^._1)) .
        map (f . B.split '\t') . B.lines <$> B.readFile fl
  where
    f [a,b,c] = ( a
                , readDouble' b  -- fold
                , readDouble c )   -- p-value
    h (a, b) | a > 0 = negate b
             | otherwise = b
    readDouble' x | x == "-Infinity" = -1 / 0
                  | otherwise = readDouble x
    isMouse x = any (B.any isLower) $ B.split '+' $ fst $ B.break (=='_') x

mkMotifTable :: [(Int, FilePath)] -> IO (DF.DataFrame Double)
mkMotifTable input = do
    (rows, res) <- fmap unzip $ forM input $ \(i, fl) -> do
        r <- map (\(a,b) -> (T.pack $ B.unpack $ a, b)) <$> filterMotifResult fl
        return (T.pack $ show i, r)
    let cols = nubSort $ concatMap (map fst) res
        dat = flip map res $ \x -> map (\y -> fromMaybe 0 $ lookup y x) cols
    return $ DF.mkDataFrame rows cols dat

plotMotif :: FilePath
       -> DF.DataFrame Double
       -> IO()
plotMotif output df = R.runRegion $ do
    valMatrix <- toRMatrix df
    [r| library("ggplot2")
        library("ComplexHeatmap")
        library("RColorBrewer")
        library("viridis")
        rwb <- colorRampPalette(colors = rev(brewer.pal(7,"RdBu")))
        h1 <- Heatmap( valMatrix_hs, 
            col=rwb(99),
            cluster_rows=F, cluster_columns=F,
            show_row_names=T, row_names_side="left",
            show_column_names=T,
            row_names_gp = gpar(fontsize=5),
            column_names_gp = gpar(fontsize=5),
            heatmap_legend_param = list(
                title = "-log10(P-value)",
                title_position = "leftcenter-rot"
                #at = c(-2, 0, 2)
            )
        )
        pdf(output_hs, width=20.0, height=9.0)
        draw(h1)
        dev.off()
    |]
    return ()

diagonizeDF :: DF.DataFrame Double -> DF.DataFrame Double
diagonizeDF = DF.reorderColumns f
  where
    f xs = map (\x -> (x, M.lookupDefault undefined x xs')) names
      where
        xs' = M.fromList xs
        names = map fst $ sortBy (comparing snd) $ map (\(a,b) -> (a, g b)) xs
        g xs = (i, negate v)
          where
            v = z V.! i
            i = V.maxIndex z
            z = scale xs



motifBuilder = do
    uNode "Read_Motifs" [| \() -> do
        motifFl <- lookupConfig "motif"
        liftIO $ do
            motifs <- readMEME motifFl
            return $ chunksOf 100 motifs
        |]
    nodePar "Scan_Motifs" [| \motifs -> do
        dir <- (<> "/Motif/") <$> lookupConfig "output_dir"
        peakFl <- lookupConfig "peak"
        genome <- lookupConfig "genome"
        liftIO $ withGenome genome $ \g -> do
            shelly $ mkdir_p dir
            let sink = sequenceSinks $ flip map outputs $ \(nm, out) ->
                    filterC ((==nm) . fromJust . (^.name)) .| sinkFileBedGzip out
                outputs = flip map motifs $ \m -> (_name m, dir <> B.unpack (_name m) <> ".bed.gz")
            runResourceT $ runConduit $ (streamBedGzip peakFl :: ConduitT _ BED3 _ _) .|
                scanMotif g (map (mkCutoffMotif def 1e-5) motifs) .| sink
            return outputs
        |] $ return ()
    nodePar "Motif_Overlap" [| \inputs -> do
        dir <- (<> "/Motif_Overlap/") <$> lookupConfig "output_dir"
        peakFl <- lookupConfig "peak"
        liftIO $ do
            shelly $ mkdir_p dir
            forM inputs $ \(nm, fl) -> do
                motifs <- runResourceT $ runConduit $ streamBedGzip fl .| sinkList :: IO [BED3]
                let tree = bedToTree const $ zip motifs $ repeat ()
                    output = dir <> B.unpack nm <> ".bin"
                s <- fmap S.fromList $ runResourceT $ runConduit $ streamBedGzip peakFl .|
                    filterC (isIntersected tree) .|
                    mapC (showBed :: BED3 -> B.ByteString) .| sinkList
                encodeFile output s
                return (nm, output)
        |] $ return ()
    path ["Read_Motifs", "Scan_Motifs", "Motif_Overlap"]

    uNode "CRE_Module_Motif_Prep" [| \(motifs, modules) ->
        return $ zip (repeat $ filter (not . ("mouse" `B.isInfixOf`) . fst) $ concat motifs) $
            map (\(a,b,_) -> (a,b)) modules
        |]
    ["Motif_Overlap", "Output_Modules"] ~> "CRE_Module_Motif_Prep"
    nodePar "CRE_Module_Motif" [| \(motifs, (i, fl)) -> do
        dir <- (<> "/modules/motif/") <$> lookupConfig "output_dir"
        peakFl <- lookupConfig "peak"
        let output = dir <> show i <> ".tsv"
        liftIO $ do
            shelly $ mkdir_p dir
            n <- fmap length (runResourceT $ runConduit $ streamBedGzip peakFl .| sinkList :: IO [BED3])
            regions <- map showBed <$> (readBed fl :: IO [BED3])
            r <- fmap B.unlines $ forM motifs $ \(nm, motif) -> do
                motif' <- decodeFile motif
                (e, p) <- enrichment motif' regions n
                return $ B.intercalate "\t" [nm, toShortest e, toShortest p]
            B.writeFile output r
            return (i, output)
        |] $ return ()
    node "Plot_Motif" [| \input -> do
        dir <- lookupConfig "output_dir"
        let output = dir <> "/motif.tsv"
        liftIO $ do
            df <- mkMotifTable input
            DF.writeTable output (T.pack . show) df
            plotMotif (dir <> "/motif.pdf") $ diagonizeDF $
                DF.reorderColumns (DF.orderByHClust id) $ DF.map (max (-200) . min 200) df
            return output
        |] $ return ()
    path ["CRE_Module_Motif_Prep", "CRE_Module_Motif", "Plot_Motif"]

    