{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell   #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE QuasiQuotes       #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE LambdaCase #-}
module ML (mlBuilder) where

import Bio.Seq.IO
import Bio.Seq hiding (length)
import Bio.Data.Bed
import Bio.Data.Bed.Utils
import Bio.Utils.Misc
import System.Random.MWC
import System.Random.MWC.Distributions
import Data.Int
import qualified Data.ByteString.Char8 as B
import qualified Data.Matrix as M
import qualified Data.Matrix.Mutable as MM
import qualified Data.Vector as V
import qualified Data.Text as T
import Lens.Micro
import Conduit
import Data.Conduit.Internal (zipSinks)
import Shelly hiding (path)

import HEA

import qualified Taiji.Utils.DataFrame as DF
import Taiji.Prelude
import qualified Language.R                        as R
import           Language.R.QQ
import Language.R.HExp
import qualified Data.Vector.SEXP as R

barPlot :: FilePath -> [(String, Double)] -> IO ()
barPlot output input = R.runRegion $ do
    myTheme
    [r| library("ggplot2")
        df <- data.frame(x=factor(x_hs, levels=x_hs),y=y_hs)
        fig <- ggplot(df, aes(x,y)) +
            geom_col() + coord_flip() +
            myTheme() + myFill() +
            labs(x = "", y = "")
        ggsave(output_hs, fig, width=40, height=155, unit="mm", useDingbats=F)
    |]
    return ()
  where
    (x, y) = unzip input 

-- | This can perform oversample
sample :: Int -> [a] -> IO [a]
sample n xs = create >>= uniformShuffle (V.fromList xs) >>=
    return . take n . cycle . V.toList

-- | Perform inplace shuffling
shuffle :: [a] -> IO [a]
shuffle xs = create >>= uniformShuffle (V.fromList xs) >>= return . V.toList

readSeq :: (BEDLike b, MonadIO m)
        => Maybe Int -> Genome -> [(b, a)] -> ConduitT () (B.ByteString, a) m ()
readSeq resize genome xs = yieldMany xs .| mapMC f .| concatC
  where
    f (x, v) = liftIO $ fetchSeq genome (convert $ getCenter x) >>= \case
        Left _ -> return Nothing
        Right x' -> return $ Just (toBS (x' :: DNA IUPAC), v)
    getCenter x = case resize of
        Nothing -> x
        Just n -> let c = (x^.chromStart + x^.chromEnd) `div` 2
                  in chromStart .~ c - n $ chromEnd .~ c + n $ x

readBedData :: Int -> FilePath -> IO [(BED3, Double)]
readBedData i fl = map (f . B.split '\t') . B.lines <$> B.readFile fl
  where
    f xs = (asBed (xs!!0) (readInt $ xs!!1) (readInt $ xs!!2), readDouble $ xs!!i)

prepData :: FilePath
         -> FilePath
         -> FilePath
         -> [(Int, FilePath)]
         -> IO ()
prepData trainFl testFl genomeFl input = do
    (test, train) <- fmap unzip $ forM input $ \(i, fl) -> do
        beds <- readBed fl >>= shuffle :: IO [BED3]
        let t = zip (take 100 beds) $ repeat i
        beds' <- sample 20000 $ drop 100 beds
        return (t, zip beds' $ repeat i)
    test' <- shuffle $ concat test
    train' <- shuffle $ concat train
    withGenome genomeFl $ \g -> do
        runResourceT $ runConduit $
            readSeq (Just 200) g test' .| mapC f .| unlinesAsciiC .| sinkFile testFl
        runResourceT $ runConduit $
            readSeq (Just 200) g train' .| mapC f .| unlinesAsciiC .| sinkFile trainFl
  where
    f (x, i) = x <> "\t" <> B.pack (show $ i - 1)
    n = 20000

train :: FilePath -> (FilePath, FilePath) -> IO FilePath
train outdir (trainData, testData) = shelly $ do
    run_ "deep-grammar" ["bpnet", T.pack trainData, T.pack testData,
        "--nClass", "150", "--layers", "5", "--gpu", "1", "--model-dir", T.pack outdir]
    T.unpack . getBest . filter ("val_loss" `T.isInfixOf`) <$> lsT outdir
  where
    getBest xs = fst $ minimumBy (comparing snd) $ flip map xs $ \x ->
        (x, read $ T.unpack $ snd $ T.breakOnEnd "=" $ T.init $ fst $ T.breakOnEnd "." x :: Double)

--------------------------------------------------------------------------------
-- Workflow
--------------------------------------------------------------------------------

mlBuilder = do
    node "Prep_Train_Data" [| \input -> do
        dir <- fmap (<> "/ML/data/") $ lookupConfig "output_dir"
        genome <- lookupConfig "genome"
        let outputTrain = dir <> "train.txt"
            outputTest = dir <> "test.txt"
        liftIO $ do
            shelly $ mkdir_p dir
            prepData outputTrain outputTest genome $ map (\(a,b,_) -> (a,b)) input
            return (outputTrain, outputTest)
        |] $ return ()
    node "Train" [| \input -> do
        dir <- fmap (<> "/ML/model/") $ lookupConfig "output_dir"
        liftIO $ do
            shelly $ mkdir_p dir
            train dir input
        |] $ return ()
    path ["Output_Modules", "Prep_Train_Data", "Train"]

    node "Confusion_Matrix" [| \((_, testData), modelFl) -> do
        dir <- fmap (<> "/ML/") $ lookupConfig "output_dir"
        let output = dir <> "evaluation.tsv"
        liftIO $ do
            shelly $ run_ "deep-grammar" ["eval", "--model", "BPNet",
                "--checkpoint", T.pack modelFl, T.pack testData, T.pack output]
            return output
      |] $ return ()
    ["Prep_Train_Data", "Train"] ~> "Confusion_Matrix"
    node "Compute_Accuracy" [| \input -> do
        dir <- fmap (<> "/ML/") $ lookupConfig "output_dir"
        liftIO $ do
            dat <- readData input
            let mat = confusionMatrix dat
            plotConfusion (dir <> "/confusion.pdf") mat
            let f xs = (fst $ head xs, 100 * accuracy 1 xs, 100 * accuracy 5 xs) 
                res = sortBy (comparing (^._2)) $ map f $ groupBy ((==) `on` fst) $ sortBy (comparing fst) dat
            return res
      |] $ return ()
    path ["Confusion_Matrix", "Compute_Accuracy"]

    node "Compute_AUC" [| \input -> do
        dir <- fmap (<> "/ML/") $ lookupConfig "output_dir"
        liftIO $ do
            dat <- readData input
            let cls = nubSort $ map fst dat
            res <- forM cls $ \i -> do
                r <- auc $ flip map dat $ \(i', xs) -> (xs !! i, i == i')
                return (i, r)
            mapM_ (\(a,(b,c)) -> putStrLn $ show a <> "\t" <> show b <> "\t" <> show c) res
            let res' = sortBy (comparing (snd . snd)) $ map (\(i, (a,b)) -> (show $ i + 1, (a, b))) res
            --plotAUC (dir <> "AUC.pdf") $ DF.mkDataFrame (map (T.pack . show) a) ["1"] $ map (return . fst) b
            barPlot (dir <> "AUROC.pdf") $ map (second fst) res'
            barPlot (dir <> "AUPRC.pdf") $ map (second snd) res'
            return res
      |] $ return ()
    path ["Confusion_Matrix", "Compute_AUC"]




plotConfusion :: FilePath
              -> DF.DataFrame Int
              -> IO ()
plotConfusion output df = R.runRegion $ do
    mat <- toRMatrix $ DF.map fromIntegral df
    [r| library("ggplot2")
        library("ComplexHeatmap")
        library("viridis")
        library("RColorBrewer")

        pdf(output_hs, width=5.4, height=5)

        hm <- Heatmap(mat_hs, col=viridis(99),
            row_title = "Predicted class",
            column_title = "Actual class",
            row_title_gp = gpar(fontsize = 6),
            column_title_gp = gpar(fontsize = 6),
            show_row_names = T, show_column_names = T,
            row_names_side = "left",
            column_names_side = "top",
            cluster_rows=F, cluster_columns=F,
            row_names_gp = gpar(fontsize=5),
            column_names_gp = gpar(fontsize=5),
            heatmap_legend_param = list(
                title = "Count",
                title_gp = gpar(fontsize = 6),
                title_position = "leftcenter-rot",
                #direction = "horizontal",
                grid_height = unit(2, "mm"),
                grid_width = unit(3, "mm"),
                labels_gp = gpar(fontsize = 5)
            )
        )
        draw(hm)
        dev.off()
    |]
    return ()

accuracy :: Int -> [(Int, [Double])] -> Double
accuracy k input = (fromIntegral $ length $ filter id r) / (fromIntegral $ length r)
  where
    r = map (isCorrect k) input
    isCorrect :: Int -> (Int, [Double]) -> Bool
    isCorrect k (label, prob) = label `elem` pred
      where
        pred = take k $ map fst $ sortBy (flip (comparing snd)) $ zip [0..] prob

normalize :: DF.DataFrame Int -> DF.DataFrame Double
normalize = DF.mapRows f
  where
    f xs = V.map ((/ (fromIntegral $ V.sum xs)) . fromIntegral) xs

confusionMatrix :: [(Int, [Double])] -> DF.DataFrame Int
confusionMatrix xs = DF.fromMatrix labels labels mat
  where
    n = length $ snd $ head xs
    labels = map (T.pack . show) [1..n]
    mat = M.create $ do
        mat <- MM.replicate (n,n) 0
        forM_ xs $ \(i, vs) -> do
            let j = fst $ maximumBy (comparing snd) $ zip [0..] vs
            MM.read mat (i,j) >>= MM.write mat (i,j) . (+1)
        return mat

auc :: [(Double, Bool)] -> IO (Double, Double)
auc input = R.runRegion $ do
    roc <- [r| library("ROCR")
        pred1 <- prediction(predictions_hs, labels_hs)
        perf1 <- performance(pred1, measure="auc")
        perf1@y.values[[1]]
        |]
    pr <- [r| library("ROCR")
        pred2 <- prediction(predictions_hs, labels_hs)
        perf2 <- performance(pred2, measure="aucpr")
        perf2@y.values[[1]]
        |]
    return ( R.unSomeSEXP roc $ \x -> case hexp x of Real r -> R.head r
           , R.unSomeSEXP pr $ \x -> case hexp x of Real r -> R.head r )
  where
    predictions = map fst input
    labels = map ((\x -> if x then 1 :: Int32 else 0) . snd) input

readData :: FilePath -> IO [(Int, [Double])]
readData fl = map (f . B.words) . B.lines <$> B.readFile fl
  where
    f (x:xs) = (readInt x, map readDouble xs)

plotAUC :: FilePath
        -> DF.DataFrame Double
        -> IO ()
plotAUC output df = R.runRegion $ do
    mat <- toRMatrix df
    [r| library("ggplot2")
        library("ComplexHeatmap")
        library("RColorBrewer")
        library("viridis")
        library("circlize")
        names <- c("< 0.7", "0.7 - 0.8", "0.8 - 0.9", "> 0.9")
        fun <- function(x) {
            if (x < 0.7) {
                return(names[1])
            }
            else if (x < 0.8) {
                return(names[2])
            }
            else if (x < 0.9) {
                return(names[3])
            }
            else {
                return(names[4])
            }
        }
        mat <- apply(mat_hs, 1:2, fun)
        colors <- structure(c("#fef0d9", "#fdcc8a", "#fc8d59", "#d7301f"), names = names)

        pdf(output_hs, width=4.5, height=6)
        rwb <- colorRampPalette(colors = rev(brewer.pal(7,"RdBu")))
        hm <- Heatmap(mat, col=colors,
            row_title_gp = gpar(fontsize = 5),
            show_row_names = F,
            row_names_side = c("left"),
            show_column_names = F,
            show_row_dend = F, show_column_dend = F,
            cluster_rows=F, cluster_columns=F,
            heatmap_legend_param = list(
                title = "z-score",
                title_gp = gpar(fontsize = 6),
                title_position = "leftcenter-rot",
                #direction = "horizontal",
                labels_gp = gpar(fontsize = 5)
            )
        )
        draw(hm)
        dev.off()
    |]
    return ()

