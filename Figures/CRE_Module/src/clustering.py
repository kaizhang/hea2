from sklearn.cluster import KMeans
from sklearn.metrics import silhouette_score
from pandas import read_csv
from joblib import dump, load
import sys
import numpy as np
import numpy.random as npr
sys.path.insert(0, '/projects/ren-transposon/home/kai/Atlas/Figures/CRE_Module/src/eakmeans/lib') 
import kmeans as km

def kmeans_(X, k, n_init = 5):
    npr.seed(394)
    result = None
    for i in list(npr.randint(10000, size=n_init)):
        clustering = km.get_clustering(X = X, n_clusters = k, algorithm = "auto",
            verbose = 1, n_threads = 1, seed=i)
        if result == None or clustering['mse'] < result['mse']:
            result = clustering
    return(result)

'''
def kmeans(X, k):
    res = KMeans(n_clusters=k, random_state=0, n_init=20, init="k-means++", n_jobs=1).fit(X)
    score = silhouette_score(X, res.labels_, sample_size=50000, random_state=123)
    return (res, score)
'''

data = read_csv(sys.argv[1], sep="\t", header=0, index_col=0).to_numpy()
#data = np.log1p(data)
res = kmeans_(data, int(sys.argv[2]))

with open(sys.argv[3], 'w') as fl:
    txt = '\n'.join( [str(res['mse'])
        , '\t'.join(map(str, list(res['L'])))
        , '\n'.join(['\t'.join(map(str, x)) for x in res['C'].tolist()])] )
    fl.write(txt)