{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell   #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE QuasiQuotes       #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE GADTs #-}
module CREModule (moduleBuilder) where

import HEA

import Statistics.Quantile
import qualified Taiji.Utils.DataFrame as DF
import Bio.RealWorld.GENCODE
import Control.Monad.State.Strict
import Control.Arrow
import qualified Data.Vector as V
import AI.Clustering.Hierarchical
import Data.Either
import Control.Parallel.Strategies (parMap, rdeepseq)
import qualified Data.Text as T
import Bio.Data.Bed.Types
import Shelly hiding (FilePath, path)
import Data.Int
import Bio.Data.Bed
import Control.Monad.ST (runST)
import qualified Data.Matrix as Mat
import qualified Data.ByteString as BS
import qualified Data.ByteString.Char8 as B
import qualified Data.ByteString.Lazy.Char8 as BL
import qualified Data.HashMap.Strict as M
import qualified Data.Map.Strict as Map
import qualified Data.HashSet as S
import Data.List.Ordered (nubSort, nubSortOn)
import Bio.Utils.Functions
import Taiji.Utils.Plot
import Statistics.Sample (mean, meanVarianceUnb, varianceUnbiased)
import Data.Binary
import System.Random.MWC.Distributions (uniformShuffle)
import System.Random.MWC (create)
import Text.Wrap
import ELynx.Tree

import Taiji.Prelude

import Data.Vector.SEXP (toList)
import qualified Language.R                        as R
import qualified Language.R.HExp as H
import           Language.R.QQ

readPhylo :: FilePath -> IO (Tree Phylo Name)
readPhylo fl = parseOneNewick Standard <$> B.readFile fl

getOrder :: FilePath -> IO [T.Text]
getOrder fl = R.runRegion $ do
    res <-
        [r| library(ape)
            library("ggtree")
            tree <- read.tree(fl_hs)
            p <- ggtree(tree, layout="rectangular", ladderize=T, branch.length="none")
            get_taxa_name(p)
        |]
    R.unSomeSEXP res $ \x -> case H.hexp x of
        H.String x' -> return $ flip map (toList x') $ \a -> case H.hexp a of
            H.Char a' -> T.pack $ B.unpack $ BS.pack $ toList a'

superModule :: Double -> (a -> V.Vector Double) -> [a] -> [[a]]
superModule cutoff f input = map flatten $ tree `cutAt` cutoff
  where
    tree = normalize $ hclust Ward (V.fromList input) (euclidean `on` (scale . f))

normScale :: V.Vector Double -> V.Vector Double
normScale vec = V.map (/s) vec 
  where
    s = sqrt $ V.sum $ V.map (\x -> x * x) vec

euclideanNorm :: V.Vector Double -> V.Vector Double -> Double
euclideanNorm x y = 0.5 * v / (vx + vy)
  where
    v = varianceUnbiased $ V.zipWith (-) x y 
    vx = varianceUnbiased x
    vy = varianceUnbiased y

sample :: Int -> [a] -> IO [a]
sample n xs = create >>= uniformShuffle (V.fromList xs) >>=
    return . take n . V.toList

plotHeatmap :: FilePath
            -> [DF.DataFrame Double]
            -> IO ()
plotHeatmap output dfs = R.runRegion $ do
    mat <- toRMatrix $ rename $ DF.rbind dfs
    [r| library("ComplexHeatmap")
        library("RColorBrewer")
        library("viridis")
        library(scales)

        pdf(output_hs, width=12, height=7)

        r <- unique(split_hs)
        colors <- c(colors_hs)[1:length(r)]
        names(colors) <- r

        split <- factor(split_hs, levels = groups_hs)
        hm <- Heatmap(mat_hs, col=viridis(99), use_raster=T,
            row_split=split, column_title = NULL, row_gap = unit(0.0, "mm"),
            row_title_rot=0, cluster_row_slices=F,
            row_title_gp = gpar(fontsize = 4),
            show_row_names = F,
            show_column_names = T,
            show_row_dend = F, show_column_dend = F,
            clustering_method_rows="ward.D2",
            cluster_rows=F, cluster_columns=F,
            row_names_gp = gpar(fontsize=4),
            column_names_gp = gpar(fontsize=5, col = colcolors_hs),
            left_annotation=rowAnnotation(
                cl=anno_block(gp = gpar(fill = colors_hs)),
                show_legend=F
                
                #cl=split_hs,
                #show_annotation_name=F, col=list(cl=colors),
            ),
            heatmap_legend_param = list(
                title = "log(1+accessibility)",
                title_gp = gpar(fontsize = 6),
                title_position = "leftcenter-rot",
                #direction = "horizontal",
                labels_gp = gpar(fontsize = 5)
            )
        )
        draw(hm)
        dev.off()
    |]
    return ()
  where
    colcolors :: [String]
    colcolors = flip map (DF.colNames $ head dfs) $ \x -> if "Adult" `T.isPrefixOf` x
        then "#F8766D" else "#00BFC4"
    colors = take 500 $ cycle ["black", "gray"] :: [String]
    groups = nub split
    split = concat $
        zipWith (\i x -> replicate (fst $ DF.dim x) $ show i) [1..] dfs :: [String]
    stage = map (T.unpack . head . T.words) $ DF.colNames $ head dfs
    rename x = DF.fromMatrix (DF.rowNames x)
        (map (\n -> T.unwords $ tail $ T.words n) $ DF.colNames x) $ DF._dataframe_data x

mkGOMatrix :: [(T.Text, [GREAT])] -> DF.DataFrame Double
mkGOMatrix input = DF.mkDataFrame (map fst input) (map (T.pack . B.unpack) terms) $
    flip map input $ \(_, go) -> 
        let m = M.fromList $ map (\x -> (_great_term_name x, negate $ logBase 10 $ max 1e-100 $ _great_pvalue x)) go
        in map (\x -> M.lookupDefault 0 x m) terms
  where
    terms = nubSort $ concatMap (take 10 . map _great_term_name . snd) $
        flip map input $ second $ \terms -> fst $ unzip $ sortBy (comparing snd) $
            V.toList $ filterFDR 0.01 $ V.fromList $ map (\x -> (x, _great_pvalue x)) $
            filter ((>=2) . _great_enrichment) terms

diagonize :: (a -> V.Vector Double) -> [a] -> [a]
diagonize f xs = map fst $ sortBy (comparing snd) $ map (\x -> (x, g $ f x)) xs
  where
    g vec = let m = mean vec
                ratio = V.maximum vec / m
                i = if ratio < 2 then -1 else V.maxIndex vec
            in (i, negate m)
    {-
    g vec = let (m, var) = meanVarianceUnb vec
                cv = sqrt var / m
                i = if cv < 0.3 then -1 else V.maxIndex vec
            in (i, negate m)
    -}

average :: [V.Vector Double] -> V.Vector Double
average xs = V.map (/n) $ foldl1' (V.zipWith (+)) xs
  where
    n = fromIntegral $ length xs

readAnno' f1 f2 = do
    a1 <- map (\(a, b) -> ("f_" <> a, "Fetal " <> b)) <$> readAnno f1
    a2 <- map (\(a, b) -> ("a_" <> a, "Adult " <> b)) <$> readAnno f2
    return $ a1 <> a2

plotGO :: FilePath
       -> DF.DataFrame Double
       -> IO()
plotGO output df = R.runRegion $ do
    valMatrix <- toRMatrix df
    [r| library("ggplot2")
        library("ComplexHeatmap")
        library("RColorBrewer")
        library("viridis")
        h1 <- Heatmap( valMatrix_hs, 
            col=magma(9),
            cluster_rows=F, cluster_columns=F,
            show_row_names=T, row_names_side="left",
            show_column_names=T,
            row_names_gp = gpar(fontsize=5),
            column_names_gp = gpar(fontsize=5),
            heatmap_legend_param = list(
                title = "-log10(P-value)",
                title_position = "leftcenter-rot"
                #at = c(-2, 0, 2)
            )
        )
        pdf(output_hs, width=20.0, height=9.0)
        draw(h1)
        dev.off()
    |]
    return ()
  where
    names = map (T.unpack . head . T.splitOn ".") $ DF.rowNames df

diagonizeDF :: DF.DataFrame Double -> DF.DataFrame Double
diagonizeDF = DF.reorderColumns f
  where
    f xs = map (\x -> (x, M.lookupDefault undefined x xs')) names
      where
        xs' = M.fromList xs
        names = map fst $ sortBy (comparing snd) $ map (\(a,b) -> (a, g b)) xs
        g xs = (i, negate v)
          where
            v = z V.! i
            i = V.maxIndex z
            z = scale xs

moduleGroup :: [(String, [Int], [T.Text])]
moduleGroup = 
    [ ( "Endothelial cells"
      , [27..35]
      , ["a_C4.1", "a_C4.2", "a_C4.3", "a_C4.4",
            "a_C4.5", "a_C4.6", "a_C4.7", "a_C4.8", "a_C4.9", "f_C12.1", "f_C12.2",
            "f_C12.3", "f_C12.4", "f_C12.5", "f_C12.6", "f_C12.7", "f_C12.8"]
      )
    , ("White Blood Cells", [8..25], ["f_C14", "f_C18.2", "f_C18.6",
        "a_C25.1", "a_C25.2", "a_C2.3", "f_C18.8", "f_C18.7", "f_C18.3",
        "a_C12.1", "a_C12.3", "a_C12.2", "a_C12.4", "f_C18.1", "f_C30",
        "f_C18.5", "f_C15.6", "a_C2.1", "a_C2.2", "f_C15.5", "f_C15.1",
        "f_C15.2", "f_C15.3", "f_C15.8", "f_C15.4", "a_C29"])
    , ("Gastrointestinal Epithelial Cells", [73..84], ["a_C9.1", "a_C9.3",
        "a_C9.6", "a_C9.5", "a_C9.4", "a_C9.8", "a_C9.9", "a_C9.7", "f_C16.4",
        "f_C16.2", "f_C16.1", "a_C9.2", "f_C16.3", "f_C16.5"])
    ]

plotHeatmapGroup :: FilePath -> DF.DataFrame Double -> DF.DataFrame Double -> DF.DataFrame Double -> IO ()
plotHeatmapGroup output df1 df2 df3 = R.runRegion $ do
    mat1 <- toRMatrix $ rename df1
    mat2 <- toRMatrix df2
    mat3 <- toRMatrix df3
    [r| library("ggplot2")
        library("ComplexHeatmap")
        library("RColorBrewer")
        library("viridis")
        library("circlize")
        rwb <- colorRamp2(seq(-100, 100, length.out=9), rev(brewer.pal(9,"RdBu")))
        color2 <- colorRamp2(seq(0, 50, length.out=9), magma(9))
        h1 <- Heatmap( mat1_hs, 
            col=viridis(99),
            cluster_rows=F, cluster_columns=F,
            show_row_names=T, row_names_side="left",
            show_column_names=T,
            row_names_gp = gpar(fontsize=5),
            column_names_gp = gpar(fontsize=5, col = colors_hs),
            heatmap_legend_param = list(
                title = "Average accessibility",
                title_position = "leftcenter-rot"
            )
        )
        h2 <- Heatmap( mat2_hs, 
            col=color2,
            cluster_rows=F, cluster_columns=F,
            show_row_names=T, row_names_side="left",
            show_column_names=T,
            row_names_gp = gpar(fontsize=5),
            column_names_gp = gpar(fontsize=5),
            heatmap_legend_param = list(
                title = "-log10(P-value)",
                title_position = "leftcenter-rot"
            )
        )
        h3 <- Heatmap( mat3_hs, 
            col=rwb,
            cluster_rows=F, cluster_columns=F,
            show_row_names=T, row_names_side="left",
            show_column_names=T,
            row_names_gp = gpar(fontsize=5),
            column_names_gp = gpar(fontsize=5),
            heatmap_legend_param = list(
                title = "-log10(P-value)",
                title_position = "leftcenter-rot"
            )
        )
        pdf(output_hs, width=8.0, height=5.0)
        draw(h1+h2+h3)
        dev.off()
    |]
    return ()
  where
    colors :: [String]
    colors = flip map (DF.colNames df1) $ \x -> if "Adult" `T.isPrefixOf` x
        then "#F8766D" else "#00BFC4"
    rename x = DF.fromMatrix (DF.rowNames x)
        (map (\n -> T.unwords $ tail $ T.words n) $ DF.colNames x) $ DF._dataframe_data x


--------------------------------------------------------------------------------
-- Workflow
--------------------------------------------------------------------------------

moduleBuilder = do
-------------------------------------------------------------------------------
-- Identify module
-------------------------------------------------------------------------------
    node "CRE_Module_KMeans_Prep" [| \() -> do
        dir <- fmap (<> "/modules/") $ lookupConfig "output_dir"
        let output = dir <> "/acc_unit_len.tsv"
        input <- lookupConfig "accessibility"
        liftIO $ do
            shelly $ mkdir_p $ fromText $ T.pack dir
            let norm v = let s = sqrt $ V.sum $ V.map (\x -> x * x) v
                          in V.map (/s) v
            (DF.mapRows norm <$> DF.readTable input) >>= DF.writeTable output (T.pack . show)
        return $ zip [10 :: Int, 20..400] $ repeat output
        |] $ return ()
    nodePar "CRE_Module_KMeans" [| \(k, input) -> do 
        dir <- fmap (<> "/modules/") $ lookupConfig "output_dir"
        let output = dir <> "/kmeans_k=" <> show k <> ".txt" 
        liftIO $ shelly $ do
            mkdir_p $ fromText $ T.pack dir
            run_ "python" ["src/clustering.py", T.pack input, T.pack $ show k, T.pack output]
            return (k, output)
        |] $ return ()
    node "CRE_Module_KMeans_Select" [| \xs -> do
        dir <- lookupConfig "output_dir"
        let output = dir <> "/kmeans.pdf"
            (k', fls) = unzip xs
            k = map fromIntegral k' :: [Int32]
            selection = 150 :: Int32
        res <- liftIO $ forM fls $ \fl -> do
            readDouble . head . B.lines <$> B.readFile fl
        liftIO $ R.runRegion $ do
            myTheme
            [r| library("ggplot2")
                df <- data.frame(x=k_hs, y=res_hs)
                fig <- ggplot(df, aes(x, y)) +
                    geom_line() +
                    geom_vline(xintercept=selection_hs, linetype="dashed", color = "red", size=0.5) +
                    myTheme() + myFill() +
                    labs(x = "k", y = "MSE")
                ggsave(output_hs, fig, width=89, height=50, unit="mm", useDingbats=F)
            |]
            return ()
            let [(_, input)] = filter (\x -> x^._1 == fromIntegral selection) xs
            return input
        |] $ return ()
    node "Get_Modules" [| \input -> do
        dir <- fmap (<> "/modules") $ lookupConfig "output_dir"
        creFl <- lookupConfig "accessibility"
        treeFl <- lookupConfig "tree"
        let output = dir <> "/module_average.tsv"
        liftIO $ do
            shelly $ mkdir_p dir
            orders <- getOrder treeFl
            let reorder x = flip map orders $ \i -> (i, fromJust $ lookup i x)
            table <- DF.reorderColumns reorder <$> DF.readTable creFl
            idx <- map readInt . B.split '\t' . (!!1) . B.lines <$> B.readFile input
            let dfs = diagonize (average . Mat.toRows . DF._dataframe_data) $
                    map (table `DF.rsub`) $
                    (map . map) snd $ groupBy ((==) `on` fst) $
                    sortBy (comparing fst) $ zip idx $ DF.rowNames table
                toBed x = let [chr, x'] = B.split ':' $ B.pack $ T.unpack x
                              [s, e] = B.split '-' x'
                           in BED3 chr (readInt s) (readInt e)
                nSample = let ws = map (fromIntegral . fst . DF.dim) dfs
                          in map (\x -> max 10 $ truncate $ x / sum ws * 10000) ws
            let (rows, vals) = unzip $ zipWith (\i x -> (T.pack $ show i, V.toList $ average $ Mat.toRows $ DF._dataframe_data x))
                    [1 :: Int ..] dfs
            DF.writeTable output (T.pack . show) $ DF.mkDataFrame rows
                (DF.colNames $ head dfs) vals
            res <- forM (zip3 [1 :: Int ..] dfs nSample) $ \(i, df, n) -> do
                let out = printf "%s/CRM_%d.bed" dir i
                    out2 = printf "%s/CRM_%d.tsv" dir i
                df' <- fmap (DF.reorderRows (DF.orderByHClust id) . (df `DF.rsub`)) $
                    sample n $ DF.rowNames df
                DF.writeTable out2 (T.pack . show) df'
                writeBed out $ map toBed $ DF.rowNames df
                return (i, out, out2)
            return (res, output)
        |] $ return ()

    uNode "Output_Modules" [| return . fst |]
    node "Module_Meta" [| \input -> do
        dir <- lookupConfig "output_dir"
        let output = dir <> "/module_meta.tsv"
        liftIO $ do
            let source = forM_ input $ \(i, fl, _) -> do
                    let f x = toLine (x :: BED3) <> "\t" <> B.pack (show i)
                    streamBed fl .| mapC f
            runResourceT $ runConduit $ source .| unlinesAsciiC .| sinkFile output
        |] $ return ()
    path ["Output_Modules", "Module_Meta"]

    nodePar "Module_GO" [| \(i, bed, _) -> liftIO $ do
        l <- length <$> (readBed bed :: IO [BED3])
        go <- great bed
        return (i, l, go)
        |] $ return ()
    node "Plot_Module_GO" [| \go -> do
        dir <- lookupConfig "output_dir"
        let output1 = dir <> "/CRE_module_go.tsv"
            output2 = dir <> "/CRE_module_go.pdf"
            output3 = dir <> "/CRE_module_go_matrix.tsv"
            f terms = fst $ unzip $ sortBy (comparing snd) $ V.toList $
                filterFDR 0.01 $ V.fromList $ map (\x -> (x, _great_pvalue x)) $
                filter ((>=2) . _great_enrichment) terms
            go' = map (\(i,n,x) -> (i,n,f x)) go
        liftIO $ do
            B.writeFile output1 $ B.unlines $ flip map go' $ \(i, n, xs) ->
                B.intercalate "\t" $ map (B.pack . show) [i, n] <>
                    map (\x -> _great_term_name x <> "," <> B.pack (show $ negate $ logBase 10 $ _great_pvalue x)) (take 20 xs)
            let df = mkGOMatrix $ map (\(i,_,x) -> (T.pack $ show i, x)) go
            plotGO output2 $ diagonizeDF $ DF.reorderColumns (DF.orderByHClust id) $
                    DF.map (min 50) df
            DF.writeTable output3 (T.pack . show) df
            return output3
        |] $ return ()
    path ["CRE_Module_KMeans_Prep", "CRE_Module_KMeans", "CRE_Module_KMeans_Select", "Get_Modules", "Output_Modules", "Module_GO", "Plot_Module_GO"]

    node "Plot_Modules" [| \input -> do
        dir <- lookupConfig "output_dir"
        annoFl1 <- lookupConfig "fetal_annotation"
        annoFl2 <- lookupConfig "cluster_annotation"
        let output = dir <> "/modules.pdf"
        liftIO $ do
            anno <- readAnno' annoFl1 annoFl2
            dfs <- forM input $ \(_,_,fl) -> DF.map (min 6) . changeName anno <$> DF.readTable fl
            --plotHeatmap output $ diagonize (average . Mat.toRows . DF._dataframe_data) dfs
            plotHeatmap output dfs
        |] $ return ()
    path ["Output_Modules", "Plot_Modules"]

    node "Plot_Selected_Modules" [| \((_, moduleFl), motifFl, goFl) -> do 
        dir <- (<> "/Selected_Modules/") <$> lookupConfig "output_dir"
        annoFl1 <- lookupConfig "fetal_annotation"
        annoFl2 <- lookupConfig "cluster_annotation"
        liftIO $ do
            anno <- readAnno' annoFl1 annoFl2
            shelly $ mkdir_p dir
            modules <- DF.readTable moduleFl
            motif <- DF.readTable motifFl
            go <- DF.readTable goFl
            let goMax = DF.mapCols (\v -> let m = V.maximum v in V.map (== m) v) go
                motifMax = DF.mapCols (\v -> let m = V.maximum v in V.map (== m) v) motif
                getTops v = map fst $ take 3 $ sortBy (flip (comparing snd)) $
                    zip [0 :: Int ..] $ V.toList v
            forM moduleGroup $ \(nm, ms, cells) -> do
                let output = dir <> nm <> ".pdf"
                    ms' = map (T.pack . show) ms
                    acc = changeName anno $ DF.orderDataFrame id $ (modules `DF.rsub` ms') `DF.csub` cells
                    motifIdx = nubSortOn f $ flip concatMap
                        (Mat.toRows $ DF._dataframe_data $ motif `DF.rsub` ms') $ \v -> 
                            map fst $ take 5 $
                            sortBy (flip (comparing snd)) $
                            map (maximumBy (comparing snd)) $ 
                            groupBy ((==) `on` (tfCluster . fst)) $
                            sortBy (comparing (tfCluster . fst)) $
                            zip (DF.colNames motif) $ V.toList v
                    tfCluster = snd . T.breakOnEnd "+"
                    f x = fst $ T.breakOn "_" x
                    motif' = DF.reorderColumns (DF.orderByHClust id) $
                        (motif `DF.rsub` (DF.rowNames acc)) `DF.csub` motifIdx
                    goIdx = nubSort $ concatMap getTops $ Mat.toRows $
                        DF._dataframe_data $ go `DF.rsub` ms'
                    go' = DF.reorderColumns (DF.orderByHClust id) $
                        (go `DF.rsub` (DF.rowNames acc)) `DF.csub` goIdx
                plotHeatmapGroup output acc go' $ DF.fromMatrix (DF.rowNames motif')
                    (map (fst . T.breakOn "_") $ DF.colNames motif') $ DF._dataframe_data motif'
        |] $ return ()
    ["Get_Modules", "Plot_Motif", "Plot_Module_GO"] ~> "Plot_Selected_Modules"