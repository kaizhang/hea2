{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell   #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE QuasiQuotes       #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE GADTs #-}

module CRE (creBuilder) where

import HEA
import Data.BBI.BigWig

import Data.Default.Class
import Statistics.Quantile
import qualified Taiji.Utils.DataFrame as DF
import Data.Int
import Bio.RealWorld.GENCODE
import Control.Monad.State.Strict
import Control.Arrow
import qualified Data.Vector as V
import qualified Data.Vector.Unboxed as U
import qualified Data.Vector.Unboxed.Mutable as UM
import AI.Clustering.Hierarchical hiding (normalize)
import Data.Either
import Control.Parallel.Strategies (parMap, rdeepseq)
import qualified Data.Text as T
import Bio.Data.Bed.Types
import Shelly hiding (FilePath, path)
import Data.Int
import Bio.Data.Bed
import Bio.Seq.IO
import Control.Monad.ST (runST)
import qualified Data.Matrix as Mat
import qualified Data.ByteString.Char8 as B
import qualified Data.HashMap.Strict as M
import Data.List.Ordered (nubSort)
import Bio.Utils.Functions
import Taiji.Utils.Plot
import Statistics.Sample (mean, meanVarianceUnb)
import Data.Binary
import System.Random.MWC.Distributions (uniformShuffle)
import System.Random.MWC (create, uniformR)
import Text.Wrap
import Data.Colour.SRGB (sRGB24read)
import Data.Colour (opaque)

import Taiji.Prelude
import qualified Language.R                        as R
import           Language.R.QQ
import Language.R.HExp

creColor :: [String]
creColor = ["#FF410D", "#F79D1E", "#F7C530"]
--creColor = ["red", "orange", "yellow"]

variability :: DF.DataFrame Double -> DF.DataFrame Double
variability df = DF.mkDataFrame (DF.rowNames df) ["variability", "average"] $
    map f $ Mat.toRows $ DF._dataframe_data df
  where
    --f vec = let (m, v) = meanVarianceUnb vec
    --in [v, m]
    f vec = [V.maximum vec - V.minimum vec, median medianUnbiased vec]

plotCRE :: FilePath
        -> [(String, [(Double, Double)])]
        -> IO ()
plotCRE output cres = R.runRegion $ do
    myTheme
    [r| library("ggplot2")
        library("ggrastr")
        df <- data.frame(cls=factor(cls_hs, levels=levels_hs), x=xs_hs, y=ys_hs)
        fig <- ggplot(df, aes(x,y)) +
            #geom_point_rast(size=0.1, alpha=1, stroke = 0, shape=20, aes(colour = factor(cls))) +
            #geom_density_2d(size=0.2) +
            geom_density_2d_filled(contour_var = "ndensity") +
            myTheme() +
            theme(legend.position='none') +
            xlab(expression(paste(Delta, "(log(accessibility))"))) +
            ylab("median(log(accessibility))") +
            #scale_color_manual(values=creColor_hs) +
            facet_wrap(vars(cls))
            
        ggsave(output_hs, fig, width=90, height=45, unit="mm", useDingbats=F)
    |]
    return ()
  where
    levels = map fst cres
    (cls, xs, ys) = unzip3 $ flip concatMap cres $ \(label, xs) ->
        zipWith (\l (x,y) -> (l,x,y)) (repeat label) xs

barPlot :: [Double] -> (forall s. R.R s (R.SomeSEXP s))
barPlot vals = do
    myTheme
    [r| library("ggplot2")
        names <- c("< 200", "200 - 2000", "> 2000")
        df <- data.frame(name=factor(names, levels=names), val=vals_hs)
        ggplot(df, aes(x=name, y=val, fill=name)) +
            geom_col() + myTheme() +
            theme(legend.position='none') +
            scale_fill_manual(values=creColor_hs)
    |]

randomRegion :: Int
             -> BEDTree ()  -- exclude
             -> [BED3] -- genome
             -> IO [BED3]
randomRegion num tree genome = do
    g <- create
    replicateM num $ sampleRegion g
  where
    genome' = V.fromList genome
    n = V.length genome'
    sampleRegion g = do
        i <- uniformR (0, n-1) g
        let BED3 chr s e = genome' V.! i
            go = do
                x <- uniformR (s+10000, e-10000) g
                let region = BED3 chr x (x + 400)
                if tree `isIntersected` region
                    then go 
                    else return region
        go
 
readCRE :: FilePath    -- ^ Master peak list
        -> [(T.Text, FilePath)]  -- ^ cluster peaks
        -> IO [BED]
readCRE fl clusters = do
    peaks <- fmap concat $ forM clusters $ \(nm, x) ->
        let f x = name .~ Just (B.pack $ T.unpack nm) $ x
        in runResourceT $ runConduit $ streamBedGzip x .| mapC (f . resize) .| sinkList 
    runResourceT $ runConduit $ streamBedGzip fl .| intersectBedWith g peaks .| sinkList
  where
    g :: BED3 -> [NarrowPeak] -> BED
    g x [] = undefined
    g x pk = 
        let names = nubSort $ map getSummit pk
        in name .~ Just (B.intercalate "+" names) $
            score .~ Just (length names) $ convert x
      where
        getSummit p = fromJust (p^.name) <> ":" <> B.pack (show $ p^.chromStart + fromJust (p^.npPeak) - x^.chromStart)
    resize pk = chromStart .~ max 0 (summit - halfWindowSize) $
        chromEnd .~ summit + halfWindowSize$
        npPeak .~ Just halfWindowSize $ pk
      where
        summit = pk^.chromStart + fromJust (pk^.npPeak)
    halfWindowSize = 200

getContext :: [Gene] -> ([BED3], [BED3])
getContext genes = (nubSort . concat) *** (nubSort . concat) $ unzip $
    flip map genes $ \Gene{..} -> 
        let tss | geneStrand = geneLeft : map transLeft geneTranscripts
                | otherwise = geneRight : map transRight geneTranscripts
            within2000 = map (\x -> BED3 geneChrom (x-2000) (x+2000)) tss
            within200 = map (\x -> BED3 geneChrom (x-200) (x+200)) tss
        in (within200, within2000)

computeIntersect :: [BED3] -> [BED3] -> (Int, (Int, Int), Int)
computeIntersect a b = (length a - oa, (oa, ob), length b - ob)
  where
    oa = runIdentity $ runConduit $ yieldMany a .| intersectBed b .| lengthC
    ob = runIdentity $ runConduit $ yieldMany b .| intersectBed a .| lengthC

sample :: Int -> [a] -> IO [a]
sample n xs | length xs <= n = return xs
            | otherwise = do
                g <- create
                V.toList . V.take n <$> uniformShuffle (V.fromList xs) g

bwCount :: BWFile -> [BED3] -> IO (U.Vector Double)
bwCount fl beds = do
    vec <- UM.replicate n 0
    mapM_ (f vec) beds
    U.map (/ (fromIntegral $ length beds)) <$> U.unsafeFreeze vec
  where
    n = Bio.Data.Bed.size $ head beds
    f vec (BED3 chr s e) = runConduit $ queryBWFile fl (chr, s, e) .| mapM_C g
      where
        g (_, s', e', x) = do
            let i = max 0 $ s' - s
                j = min n $ e' - s
            forM_ [i .. j - 1] $ UM.unsafeModify vec (+x)

creBuilder = do
    node "CRE_Acc" [| \() -> do
        creFl <- lookupConfig "accessibility"
        dir <- lookupConfig "output_dir"
        annoFl <- lookupConfig "cluster_annotation"
        let output = dir <> "/cre_accessibility.tsv"
        liftIO $ do
            anno <- readAnno annoFl
            (changeName anno <$> DF.readTable creFl) >>= DF.writeTable output (T.pack . show)
        |] $ return ()

    node "CRE_Intersect_ENCODE" [| \() -> do
        screenFl <- lookupConfig "screen"
        creFl <- lookupConfig "cre"
        dir <- lookupConfig "output_dir"
        liftIO $ do
            screen <- runResourceT $ runConduit $ streamBedGzip screenFl .| sinkList :: IO [BED3]
            let f x o | null o = Left x
                      | otherwise = Right x
            (no, o) <- fmap partitionEithers $ runResourceT $ runConduit $
                streamBedGzip creFl .| intersectBedWith f screen .| sinkList
            let output1 = dir <> "/overlap_with_SCREEN.bed"
                output2 = dir <> "/no_overlap_with_SCREEN.bed"
            writeBed output1 (o :: [BED3])
            writeBed output2 (no :: [BED3])
            return (output1, output2)
        |] $ return ()

    node "CRE_Distr" [| \() -> do
        creFl <- lookupConfig "cre"
        dir <- lookupConfig "output_dir"
        annoFl <- lookupConfig "annotation"
        let output = dir <> "/cre_distr.pdf"
        liftIO $ do
            let centering b@(BED3 chr s e) =
                    let c = (s + e) `div` 2
                    in BEDExt (BED3 chr c (c+1)) b
            (within200, within2000) <- getContext <$> readGenes annoFl
            peaks <- runResourceT $ runConduit $ streamBedGzip creFl .|
              mapC centering .| sinkList
            let (rest, o200) = partitionEithers $ runIdentity $
                    runConduit $ yieldMany peaks .| intersectBedWith f within200 .| sinkList
                (no, o2000) = partitionEithers $ runIdentity $ runConduit $
                    yieldMany rest .| intersectBedWith f within2000 .| sinkList
                f x [] = Left x
                f x _ = Right x
                vals :: [Double]
                vals = map ((/1000) . fromIntegral . length) [o200, o2000, no]
                percent = let s = sum vals / 100 in map (/s) vals
                labels = map (printf "%.2f%%") percent :: [String]
                output1 = dir <> "/promoter.bed"
                output2 = dir <> "/proximal.bed"
                output3 = dir <> "/distal.bed"
                color = creColor
            liftIO $ do
                writeBed output1 $ map (^._data) o200
                writeBed output2 $ map (^._data) o2000
                writeBed output3 $ map (^._data) no
                R.runRegion $ do
                    [r| library("ggplot2")
                        library("scales")
                        df <- data.frame(group=c("promoter", "proximal", "distal"), value = percent_hs)
                        fig <- ggplot(df, aes(x="", y=value, fill=group)) +
                            geom_bar(width = 1, stat = "identity", color = "white") +
                            coord_polar("y", start = 0)+
                            geom_text(aes(y = c(20, 50, 90), label = labels_hs))+
                            scale_fill_manual(values = color_hs) +
                            theme_void()
                        ggsave(output_hs, fig, width=80, height=80, unit="mm", useDingbats=F)
                    |]
                    return ()
            return (output1, output2, output3)
        |] $ return ()

    node "CRE_Usage" [| \() -> do
        dir <- lookupConfig "output_dir"
        creFl <- lookupConfig "cre"
        cluster <- lookupConfig "cluster_peak"
        let output = dir <> "/cre_annotated.bed"
        liftIO $ do
            peaks <- fmap (filter (".narrowPeak.gz" `T.isSuffixOf`)) $ shelly $
                lsT $ fromText $ T.pack cluster
            beds <- readCRE creFl $ map
                (\x -> (fst $ T.breakOn ".narrowPeak.gz" $ snd $ T.breakOnEnd "/" x, T.unpack x)) peaks
            writeBed output beds
            return output
        |] $ return ()

    node "Plot_CRE_Usage" [| \(input, (cat1,cat2,_)) -> do
        dir <- lookupConfig "output_dir"
        let output = dir <> "/cre_annotated.pdf"
            output2 = dir <> "/cre_annotated2.pdf"
        liftIO $ do
            let mkTree fl = do
                    peaks <- readBed fl :: IO [BED3]
                    return $ bedToTree undefined $ zip peaks $ repeat ()
            beds <- readBed input :: IO [BED]
            promoter <- mkTree cat1
            proximal <- mkTree cat2
            let (cat, xs, ys) = unzip3 $ map (\((a,b), c) -> (a,b,c)) $ M.toList $
                    M.fromListWith (+) $ map f beds
                f x = ((label, fromIntegral $ fromJust $ x^.score :: Int32), 1 :: Int32)
                  where
                    label | isIntersected promoter x = "promoter" :: String
                          | isIntersected proximal x = "proximal"
                          | otherwise = "distal"
                color = creColor
            R.runRegion $ do
                myTheme
                [r| library("ggplot2")
                    df <- data.frame(x=xs_hs, y=ys_hs, cat=factor(cat_hs, levels=c("promoter", "proximal", "distal")))
                    fig <- ggplot(df, aes(x=x, y=y, fill=cat)) +
                        geom_col() + myTheme() +
                        #coord_cartesian(ylim=c(1e3,NA), xlim=c(1,NA)) +
                        theme(legend.position='none') +
                        scale_fill_manual(values =color_hs) +
                        xlab("Number of cell types") +
                        ylab("Number of cCREs")
                        #facet_zoom(xlim = c(10, 54), ylim = c(0, 15000), horizontal = FALSE)
                    ggsave(output_hs, fig, width=70, height=45, unit="mm", useDingbats=F)
                    fig <- ggplot(df[df$x>=12,], aes(x=x, y=y, fill=cat)) +
                        geom_col() + myTheme() +
                        theme(legend.position='none',
                            panel.border = element_rect(colour = "black"),
                            panel.grid.major = element_blank()
                        ) +
                        scale_fill_manual(values =color_hs) +
                        xlab("") + ylab("")
                    ggsave(output2_hs, fig, width=50, height=30, unit="mm", useDingbats=F)
                |]
                return ()
        |] $ return ()
    ["CRE_Usage", "CRE_Distr"] ~> "Plot_CRE_Usage"

    node "Compute_Variability" [| \() -> do
        creFl <- lookupConfig "accessibility"
        dir <- lookupConfig "output_dir"
        let output = dir <> "/CRE_variability.tsv"
        liftIO $ do
            df <- DF.map (logBase 2 . (+1)) <$> DF.readTable creFl
            DF.writeTable output (T.pack . show) $ variability df
            return output
        |] $ return ()
    node "Plot_Variability" [| \(var, (fl1,fl2,fl3)) -> do
        dir <- lookupConfig "output_dir"
        let output = dir <> "/CRE_variability_scatter.pdf"
            output2 = dir <> "/CRE_variability_bar.pdf"
        liftIO $ do
            df <- DF.readTable var
            dat <- forM [("promoter", fl1), ("proximal", fl2), ("distal", fl3)] $ \(nm, fl) -> do
                idx <- map (T.pack . B.unpack . showBed) <$> (readBed fl :: IO [BED3])
                dat <- sample 20000 $ map (\[a,b] -> (a,b)) $ Mat.toLists $
                        DF._dataframe_data $ df `DF.rsub` idx
                return (nm, dat, mean $ V.fromList $ map fst dat)
            plotCRE output $ map (\x -> (x^._1, x^._2)) dat
            R.runRegion $ do
                fig <- barPlot $ map (^._3) dat
                [r| ggsave(output2_hs, fig_hs, width=45, height=38, unit="mm", useDingbats=F) |]
                return ()
        |] $ return ()
    ["Compute_Variability", "CRE_Distr"] ~> "Plot_Variability"

    node "Random_Region" [| \(fl1,fl2,fl3) -> do
        dir <- lookupConfig "output_dir"
        genomeFl <- lookupConfig "hg38"
        let output = dir <> "/random_regions.bed"
        liftIO $ do
            chr <- fmap (filter (("chr" `B.isPrefixOf`) . fst)) $
                withGenome genomeFl $ \g -> return $ getChrSizes g
            let genome = map (\(a,b) -> BED3 a 0 b) chr
            b1 <- readBed fl1 :: IO [BED3]
            b2 <- readBed fl2
            b3 <- readBed fl3
            let tree = bedToTree undefined $ zip (b1 ++ b2 ++ b3) $ repeat ()
            randomRegion 200000 tree genome >>= writeBed output
            return output
        |] $ return ()
    ["CRE_Distr"] ~> "Random_Region"

    uNode "Compute_Conservation_Prep" [| \(x, (fl1,fl2,fl3), (fl4, fl5)) -> return [x,fl1,fl2,fl3,fl4,fl5] |]
    nodePar "Compute_Conservation" [| \fl -> do
        phylo <- lookupConfig "phyloP"
        liftIO $ do
            bw <- openBWFile phylo
            let extend (BED3 chr s e) = let c = (s + e) `div` 2 in BED3 chr (c-1000) (c+1001)
            beds <- readBed fl
            res <- bwCount bw beds
            closeBWFile bw
            return res
        |] $ return ()
    ["Random_Region", "CRE_Distr", "CRE_Intersect_ENCODE"] ~> "Compute_Conservation_Prep"
    node "Plot_Conservation" [| \scores -> do
        dir <- lookupConfig "output_dir"
        let output = dir <> "/conservation.pdf"
            names = ["random", "promoter", "proximal", "distal"] :: [String]
            (label, xs, ys) = unzip3 $ flip concatMap (zip names scores) $ \(nm, vec) ->
                let dat = --filter ((\x -> x >= -200 && x <= 200) . fst) $
                        zip [-200 :: Int32, -199 .. 199] $ U.toList vec
                in flip map dat $ \(a,b) -> (nm, a, b)
            color = creColor
        liftIO $ R.runRegion $ do
            myTheme
            [r| library("ggplot2")
                df <- data.frame(name=factor(label_hs, levels=names_hs), x=xs_hs, y=ys_hs)
                fig <- ggplot(df, aes(x=x, y=y, color=name, group=name)) +
                    geom_line() +
                    myTheme() +
                    #theme(legend.position='none') +
                    scale_colour_manual(values =c("gray", color_hs)) +
                    xlab("Distance from region center") +
                    ylab("Average phyloP score")
                ggsave(output_hs, fig, width=42, height=45, unit="mm", useDingbats=F)
            |]
            return ()
        |] $ return ()
    path ["Compute_Conservation_Prep", "Compute_Conservation", "Plot_Conservation"]

    node "Plot_Conservation2" [| \[a,_,_,_,b,c] -> do
        dir <- lookupConfig "output_dir"
        let output = dir <> "/conservation.pdf"
            names = ["random", "SCREEN", "Novel"] :: [String]
            (label, xs, ys) = unzip3 $ flip concatMap (zip names [a,b,c]) $ \(nm, vec) ->
                let dat = --filter ((\x -> x >= -200 && x <= 200) . fst) $
                        zip [-200 :: Int32, -199 .. 199] $ U.toList vec
                in flip map dat $ \(a,b) -> (nm, a, b)
            color = creColor
        liftIO $ R.runRegion $ do
            myTheme
            [r| library("ggplot2")
                df <- data.frame(name=factor(label_hs, levels=names_hs), x=xs_hs, y=ys_hs)
                fig <- ggplot(df, aes(x=x, y=y, color=name, group=name)) +
                    geom_line() +
                    myTheme() +
                    #theme(legend.position='none') +
                    scale_colour_manual(values =c("gray", color_hs)) +
                    xlab("Distance from region center") +
                    ylab("Average phyloP score")
                ggsave(output_hs, fig, width=42, height=45, unit="mm", useDingbats=F)
            |]
            return ()
        |] $ return ()
    path ["Compute_Conservation", "Plot_Conservation2"]