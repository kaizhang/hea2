{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell   #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE QuasiQuotes       #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE GADTs #-}

module Intersect (intersectBuilder) where

import HEA

import Statistics.Quantile
import qualified Taiji.Utils.DataFrame as DF
import Bio.RealWorld.GENCODE
import Control.Monad.State.Strict
import Control.Arrow
import qualified Data.Vector as V
import AI.Clustering.Hierarchical hiding (normalize)
import Data.Either
import Control.Parallel.Strategies (parMap, rdeepseq)
import qualified Data.Text as T
import Bio.Data.Bed.Types
import Shelly hiding (FilePath, path)
import Data.Int
import Bio.Data.Bed
import Bio.Seq.IO
import Control.Monad.ST (runST)
import qualified Data.Matrix as Mat
import qualified Data.ByteString.Char8 as B
import qualified Data.HashMap.Strict as M
import Data.List.Ordered (nubSort)
import Bio.Utils.Functions
import Taiji.Utils.Plot
import Statistics.Sample (mean)
import Data.Binary
import System.Random.MWC.Distributions (uniformShuffle)
import System.Random.MWC (create)
import Text.Wrap

import Taiji.Prelude
import qualified Language.R                        as R
import           Language.R.QQ
import Language.R.HExp

showBed :: BED3 -> T.Text
showBed (BED3 chr s e) = T.pack $ B.unpack chr <> ":" <> show s <> "-" <> show e

--------------------------------------------------------------------------------
-- Intersecting with previous cCREs
--------------------------------------------------------------------------------
readCRE :: FilePath    -- ^ Master peak list
        -> [(T.Text, FilePath)]  -- ^ Diff peaks
        -> IO [BED]
readCRE fl diff = do
    diffs <- fmap concat $ forM diff $ \(nm, x) ->
        let f x = name .~ Just (B.pack $ T.unpack nm) $ x
        in runResourceT $ runConduit $ streamBedGzip x .| mapC f .| sinkList 
    runResourceT $ runConduit $ streamBedGzip fl .| intersectBedWith g diffs .| sinkList
  where
    g :: BED3 -> [NarrowPeak] -> BED
    g x [] = convert x
    g x pk = name .~
        Just (B.intercalate "+" $ nubSort $ map (fromJust . (^.name)) pk) $ convert x

binGenome :: Int -> Genome -> [BED3]
binGenome n genome = concatMap (splitBedBySizeLeft n) $
    map (\(chr, s) -> BED3 chr 0 s) $ getChrSizes genome

annotatePeak :: (BEDLike a, BEDLike b)
             => [a]   -- ^ Peaks to be annotated
             -> Sorted (V.Vector b)  -- ^ Annotations
             -> [a]
annotatePeak a b = runIdentity $ runConduit $
    yieldMany a .| intersectSortedBedWith f b .| sinkList
  where
    f x [] = name .~ Nothing $ x
    f x xs = name .~
        Just (B.intercalate "+" $ nubSort $ map (fromJust . (^.name)) xs) $ x

visualizeIntersection :: BEDLike b
                      => DF.DataFrame Double
                      -> ([b], [b])
                      -> IO ()
visualizeIntersection df (no, o) = do
    no' <- sample no
    o' <- sample o
    let df' = df `DF.rsub` (no' ++ o')
    R.runRegion $ do
        mat <- toRMatrix df'
        [r| library("ggplot2")
            library("ComplexHeatmap")
            library("RColorBrewer")
            library("viridis")

            h1 <- Heatmap( mat_hs, row_split = c(rep("NO", 4000), rep("O", 4000)),
                col=viridis(99),
                cluster_rows=T, show_row_dend=F,
                cluster_columns=T,
                show_row_names=F, row_names_side="left",
                show_column_names=T,
                column_names_gp = gpar(fontsize=4),
                heatmap_legend_param = list(
                    title = "log(1+acc.)",
                    title_gp = gpar(fontsize = 6),
                    title_position = "leftcenter-rot",
                    labels_gp = gpar(fontsize = 5)
                )
            )
            pdf("heatmap.pdf", width=7.2, height=7.2)
            draw(h1)
            dev.off()
        |]
        return ()
  where
    sample xs = fmap (map f . V.toList . V.take 4000) $
        create >>= uniformShuffle (V.fromList xs)
      where
        f x = T.pack $ printf "%s:%d-%d" (B.unpack $ x^.chrom)
            (x^.chromStart) $ x^.chromEnd


barPlot :: String -> [(String, Double)] -> R.R s (R.SomeSEXP s)
barPlot col input = do
    myTheme
    [r| library("ggplot2")
        df <- data.frame(name=names_hs, val=dat_hs)
        ggplot(df, aes(x=reorder(name, val), y=val)) +
            geom_col(fill=col_hs) +
            coord_flip() +
            myTheme() +
            labs(x = "", y = "enrichment")
    |]
  where
    (names, dat) = unzip input
    
barPlot2 :: String -> [(String, Double)] -> R.R s (R.SomeSEXP s)
barPlot2 col input = do
    myTheme
    [r| library("ggplot2")
        df <- data.frame(name=names_hs, val=dat_hs)
        ggplot(df, aes(x=reorder(name, val), y=val)) +
            geom_col(fill=col_hs) +
            myTheme() +
            labs(x = "", y = "enrichment")
    |]
  where
    (names, dat) = unzip input

venn3 :: FilePath -> ([Int32], [Int32], [Int32]) -> IO ()
venn3 output (a,b,c) = R.runRegion $ do
    [r| library("ggVennDiagram")
        library("ggplot2")
        x <- list(A=a_hs, B=b_hs, C=c_hs)
        fig <- ggVennDiagram(x)
        ggsave(output_hs, fig, width=150, height=150, unit="mm", useDingbats=F)
    |]
    return ()

upset :: FilePath -> ([Int32], [Int32], [Int32]) -> IO ()
upset output (a,b,c) = R.runRegion $ do
    [r| library("ComplexHeatmap")
        m <- make_comb_mat(list(A=a_hs, B=b_hs, C=c_hs))

        ss <- set_size(m)
        cs <- comb_size(m)
        ht <- UpSet(m, lwd=1, set_order = order(ss), comb_order = order(comb_degree(m), -cs),
            top_annotation = HeatmapAnnotation(
                "Intersection size" = anno_barplot(cs, 
                    ylim = c(0, max(cs)*1.1),
                    border = FALSE, 
                    gp = gpar(fontsize=5, fill = "black"), 
                    axis_param = list(gp=gpar(fontsize=5)),
                    height = unit(1.3, "cm")
                    ), 
                annotation_name_side = "left", 
                annotation_name_rot = 90,
                annotation_name_gp = gpar(fontsize=6)
            ),
            left_annotation = rowAnnotation(
                "Set size" = anno_barplot(-ss, 
                    baseline = 0,
                    border = FALSE, 
                    gp = gpar(fontsize=5, fill = "black"), 
                    axis_param = list(gp=gpar(fontsize=5)),
                    width = unit(1.2, "cm")
                ),
                set_name = anno_text(set_name(m), 
                    location = 0.5, 
                    just = "center",
                    width = max_text_width(set_name(m)) + unit(4, "mm")
                ),
                annotation_name_gp = gpar(fontsize=6)
            ), 
            right_annotation = NULL,
            show_row_names = FALSE
        )
        pdf(output_hs, width=2.5, height=1.5)
        ht <- draw(ht)
        od <- column_order(ht)
        decorate_annotation("Intersection size", {
            grid.text(cs[od], x = seq_along(cs), y = unit(cs[od], "native") + unit(2, "pt"), 
                default.units = "native", just = c("left", "bottom"), 
                gp = gpar(fontsize = 5, col = "#404040"), rot = 45)
        })
        dev.off()
    |]
    return ()

intersectBuilder = do
    node "Compute_Intersection" [| \creFl -> do
        dhsFl <- lookupConfig "dhs"
        screenFl <- lookupConfig "screen"
        liftIO $ do
            let f x = let summits = map (readInt . last . B.split ':') $ B.split '+' $ fromJust $ (x :: BED) ^.name
                      in BED3 (x^.chrom) (x^.chromStart + minimum summits - 200)
                            (x^.chromStart + maximum summits + 200)
            dhs <- runResourceT $ runConduit $
                streamBedGzip dhsFl .| sinkList :: IO [BED3]
            cre <- runResourceT $ runConduit $
                streamBed creFl .| mapC f .| sinkList :: IO [BED3]
            screen <- runResourceT $ runConduit $
                streamBedGzip screenFl .| sinkList :: IO [BED3]
            regions <- fmap (\x -> zipWith BEDExt x [1 :: Int32 ..]) $
                runConduit $ mergeBed (dhs <> cre <> screen) .| sinkList
            [dhs', cre', screen'] <- forM [dhs, cre, screen] $
                \xs -> fmap (map (^._data)) $ runConduit $ yieldMany regions .| intersectBed xs .| sinkList
            return (cre', dhs', screen')
        |] $ return ()

    node "Plot_Intersection" [| \(a,b,c) -> do
        liftIO $ upset "output/intersection.pdf" (a,b,c)
        |] $ return ()
    path ["CRE_Usage", "Compute_Intersection", "Plot_Intersection"]

