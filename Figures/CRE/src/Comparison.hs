{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell   #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE QuasiQuotes       #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE DataKinds #-}

module Comparison (compareBuilder) where

import HEA
import Data.BBI.BigWig

import Statistics.Quantile
import qualified Data.IntSet as IS
import qualified Taiji.Utils.DataFrame as DF
import Data.Int
import Bio.RealWorld.GENCODE
import Control.Monad.State.Strict
import Control.Arrow
import qualified Data.Vector as V
import qualified Data.Vector.Unboxed as U
import qualified Data.Vector.Unboxed.Mutable as UM
import AI.Clustering.Hierarchical hiding (normalize)
import Data.Either
import Control.DeepSeq
import Control.Parallel.Strategies (parMap, rdeepseq)
import qualified Data.Text as T
import qualified Data.Text.IO as T
import Bio.Data.Bed.Types
import Shelly hiding (FilePath, path)
import Data.Int
import Data.Conduit.Internal (zipSources)
import Bio.Data.Bed
import Bio.Data.Bed.Utils
import qualified Data.Matrix as Mat
import qualified Data.ByteString.Char8 as B
import qualified Data.HashMap.Strict as M
import qualified Data.HashSet as S
import Data.List.Ordered (nubSort)
import Bio.Utils.Functions
import Taiji.Utils.Plot
import Statistics.Sample (mean, meanVarianceUnb)
import Statistics.Correlation (pearson)
import Statistics.Distribution.Normal (standard)
import Statistics.Distribution (complCumulative)
import Data.Binary
import System.Random.MWC.Distributions (uniformShuffle)
import System.Random.MWC (create, uniformR)
import Bio.RealWorld.ENCODE
import Bio.RealWorld.ID
import           Data.Conduit.Zlib           (gzip, ungzip, multiple)
import Control.Concurrent.Async (forConcurrently)

import Taiji.Prelude
import qualified Language.R                        as R
import           Language.R.QQ
import Language.R.HExp

iummuneCells :: [T.Text]
iummuneCells = ["CD8 Effector T", "CD4 Effector T", "Natural Killer T", "Naive T", "Plasma B", "Memory B", "Macrophage 1", "Mast"]

sample :: Int -> [a] -> IO [a]
sample n xs = create >>= uniformShuffle (V.fromList xs) >>=
    return . take n . V.toList

groupDF :: DF.DataFrame Double   -- ^ Correlation matrix
        -> DF.DataFrame Double
groupDF input = DF.mkDataFrame rownames (DF.colNames input) dat
  where
    (rownames, dat) = unzip $
        map (\x -> (fst $ head x, average $ map snd x)) $
        groupBy ((==) `on` fst) $ sortBy (comparing fst) $
        zip (map getName $ DF.rowNames input) $
        Mat.toRows $ DF._dataframe_data input
    getName x = if y `elem` ["embryonic", "adult", "unknown", "child", "newborn"]
        then T.unwords (reverse $ drop 1 x') -- <> " (" <> y <> ")"
        else T.unwords (reverse $ drop 2 x') <> " (ATAC)"
      where
        x' = reverse $ T.splitOn "_" x
        y = head x'
    average xs = V.toList $ V.map (/ (fromIntegral $ length xs)) $
        foldl1' (V.zipWith (+)) xs

barPlot :: FilePath -> DF.DataFrame Double -> IO ()
barPlot output df = R.runRegion $ do
    myTheme
    [r| library("ggplot2")
        library(scales)
        df <- data.frame(name=names_hs, val=vals_hs, cell=cats_hs)
        df$name <- factor(df$name, levels = groups_hs)
        fig <- ggplot(df, aes(name, val, fill=cell)) +
            geom_bar(position = "dodge", stat = "summary", fun.y = "mean") +
            #geom_errorbar(stat = "summary", position = 'dodge', width = 0.9) +
            geom_point(aes(x=name), size=0.5, shape = 21, position = 
                position_jitterdodge(jitter.width = 0.5, dodge.width=0.9)) +
            myTheme() + myColour() + myFill() +
            theme(axis.text.x=element_text(angle = 45, hjust=1)) +
            scale_x_discrete(labels = wrap_format(10)) +
            labs(x = "", y = "Z-score")
            #scale_color_manual(values=c("black","black"))
        ggsave(output_hs, fig, width=w_hs, height=50, unit="mm", useDingbats=F)
    |]
    return ()
  where
    w = fromIntegral (length $ nubSort rownames) * 10 + 8 :: Double
    rownames = map f $ DF.rowNames df
    groups = nub names
    f = T.unpack . T.unwords . reverse . drop 3 . reverse . T.splitOn "_"
    (names, vals, cats) = unzip3 $ flip concatMap (zip (DF.colNames df) $ Mat.toColumns $ DF._dataframe_data df) $ \(c, vec) -> 
        zipWith (\a b -> (a, b, T.unpack c)) rownames $ V.toList vec

violin :: FilePath -> [((String, String), Double)] -> IO ()
violin output input = R.runRegion $ do
    myTheme
    [r| library("ggplot2")
        library(scales)
        df <- data.frame(name=names_hs, val=vals_hs, group=grp_hs)
        fig <- ggplot(df, aes(x=name, y=val, fill=name)) +
            geom_violin(trim=F, lwd=0.2) +
            scale_x_discrete(labels = wrap_format(10)) +
            geom_jitter(size=0.2, shape=16, position=position_jitter(0.35)) +
            myTheme() + myColour() +
            theme(legend.position="none") +
            labs(x = "", y = "max-Z") +
            facet_grid(. ~ group, scales="free_x", space="free_x")
        ggsave(output_hs, fig, width=180, height=50, unit="mm", useDingbats=F)
    |]
    return ()
  where
    (names, grp, vals) = unzip3 $ map (\((x,y),z) -> (x,y,z)) input

violin' :: FilePath -> [(String, Double)] -> IO ()
violin' output input = R.runRegion $ do
    myTheme
    [r| library("ggplot2")
        library("ggstatsplot")
        library("gginnards")
        df <- data.frame(name=names_hs, val=vals_hs)
        fig <- ggbetweenstats(data=df, x=name, y=val,
            violin.args = list(trim=F, lwd=0.2, width = 0.5, alpha = 0.2),
            point.args = list(alpha=0, size=0, stroke=0),
            mean.plotting = T,
            mean.point.args = list(size = 1, color = "darkred"),
            mean.label.args = list(size = 2),
            plot.type = "violin",
            type="np", pairwise.comparisons=F
            ) +
            stat_summary(fun.data=mean_sdl, mult=1, 
                 geom="pointrange", color="red") +
            myTheme() + myColour() +
            theme(legend.position="none") +
            labs(x = "", y = "max-Z")
        #fig <- delete_layers(x = fig, match_type = "GeomPoint")
        ggsave(output_hs, fig, width=45, height=50, unit="mm", useDingbats=F)
    |]
    return ()
  where
    (names, vals) = unzip input

boxPlot :: FilePath -> [(String, [Double])] -> IO ()
boxPlot output input = R.runRegion $ do
    myTheme
    [r| library("ggplot2")
        library("ggstatsplot")
        library("gginnards")
        df <- data.frame(name=names_hs, val=vals_hs)
        df$name <- factor(df$name, levels = groups_hs)
        fig <- ggbetweenstats(data=df, x=name, y=val,
            plot.type = "box",
            #point.args=list(position="none"),
            type="np", pairwise.comparisons=T, mean.plotting=F) +
            myTheme() + myColour() +
            theme(legend.position="none") +
            #theme(axis.text.x = element_text(angle = 90, vjust = 0.5, hjust=1)) +
            labs(x = "VISTA enhancers", y = "z-score in cardiomyocyte")
        fig <- delete_layers(x = fig, match_type = "GeomPoint")
        ggsave(output_hs, fig, width=40, height=40, unit="mm", useDingbats=F)
    |]
    return ()
  where
    (names, vals) = unzip $ flip concatMap input $ \(nm, val) -> zip (repeat nm) val
    groups = map fst input

linePlot :: FilePath -> DF.DataFrame Double -> IO ()
linePlot output df = R.runRegion $ do
    myTheme
    [r| library("ggplot2")
        df <- data.frame(tissue=tissue_hs, cl=cl_hs, val=val_hs)
        fig <- ggplot(df, aes(x = cl, y=val, color=tissue, group=tissue)) +
            geom_line() +
            myTheme() + myFill() + myColour() +
            theme(axis.text.x = element_text(angle = 90, vjust = 0.5, hjust=1)) +
            labs(x = "cell type", y = "Average log2(1+acc.)")
        ggsave(output_hs, fig, width=120, height=80, unit="mm", useDingbats=F)
    |]
    return ()
  where
    (tissue, cl, val) = unzip3 $ map f $ sequence [DF.rowNames df, DF.colNames df]
    f [i,j] = let x = df DF.! (i,j)
                  i' = T.unpack i
                  j' = T.unpack j
               in (i', j', x)

linePlot' :: FilePath -> [Matching] -> IO ()
linePlot' output input = R.runRegion $ do
    myTheme
    [r| library("ggplot2")
        library(scales)
        df <- data.frame(name=names_hs, val=val_hs, cl=cl_hs, label=labels_hs)
        df$name <- factor(df$name, levels = names_hs)
        fig <- ggplot(df, aes(x=name, y=val)) +
            geom_point(aes(colour=cl), size=0.3) +
            geom_hline(yintercept=2.326348, linetype="dashed", color = "black", size=0.25) +
            myTheme() + myColour() +
            theme(axis.title.x=element_blank(),
                axis.text.x=element_blank(),
                axis.ticks.x=element_blank())
            #scale_color_manual(breaks = breaks_hs, values=colors_hs) +
            labs(x = "", y = "max-Z")
        ggsave(output_hs, fig, width=70, height=50, unit="mm", useDingbats=F)
    |]
    return ()
  where
    names = map _sampleName input
    cl = map _classification input
    val = map _maxZ input
    labels = map _annotation input

linePlot'' :: FilePath -> [Matching] -> IO ()
linePlot'' output input = R.runRegion $ do
    myTheme
    [r| library("ggplot2")
        library(scales)
        df <- data.frame(name=names_hs, val=val_hs, cl=cl_hs, label=labels_hs)
        df$name <- factor(df$name, levels = names_hs)
        fig <- ggplot(df, aes(x=name, y=val)) +
            geom_point(aes(colour=cl), size=0.6) +
            geom_hline(yintercept=2.326348, linetype="dashed", color = "black", size=0.25) +
            geom_text(aes(label=label), angle=90, size=1.6, hjust=1) +
            #scale_x_discrete(labels=wrap_format(40)(cl_hs)) +
            myTheme() + myColour() +
            theme(axis.text.x = element_text(size=5, angle = 45, hjust=1)) +
            #scale_color_manual(breaks = breaks_hs, values=colors_hs) +
            labs(x = "", y = "max-Z")
        ggsave(output_hs, fig, width=165, height=80, unit="mm", useDingbats=F)
    |]
    return ()
  where
    names = map _sampleName input
    cl = map _classification input
    val = map _maxZ input
    labels = map _annotation input

plotMultiHeatmap :: FilePath -> String
            -> [[DF.DataFrame Double]]
            -> IO ()
plotMultiHeatmap output rowTitle dfs = R.runRegion $ do
    mat <- toRMatrix df
    [r| library("ggplot2")
        library("ComplexHeatmap")
        library("RColorBrewer")
        library("viridis")
        library("circlize")

        pdf(output_hs, width=30, height=10)
        hm <- Heatmap(mat_hs, col=viridis(99),
            use_raster=T,
            column_split = cSplit_hs, column_title = NULL,
            #row_split = rSplit_hs, row_title = NULL,
            row_title = rowTitle_hs,
            row_title_gp = gpar(fontsize = 5),
            show_row_names = F,
            show_column_names = T,
            show_row_dend = F, show_column_dend = F,
            #clustering_method_rows="ward.D2",
            cluster_rows=F, cluster_columns=F,
            column_names_gp = gpar(fontsize=5),
            row_names_gp = gpar(fontsize=5),
            heatmap_legend_param = list(
                title = "log(1+RPM)",
                title_gp = gpar(fontsize = 6),
                title_position = "leftcenter-rot",
                #direction = "horizontal",
                labels_gp = gpar(fontsize = 5)
            )
        )
        draw(hm)
        dev.off()
    |]
    return ()
  where
    df = DF.rbind $ map DF.cbind dfs
    cSplit = concat $ zipWith (\i x -> replicate (snd $ DF.dim x) $ show i) [1..] $ head dfs :: [String]
    --rSplit = concat $ zipWith (\i x -> replicate (fst $ DF.dim x) $ show i) [1..] $ map head dfs :: [String]


plotHeatmap :: FilePath
            -> DF.DataFrame Double
            -> IO ()
plotHeatmap output df = R.runRegion $ do
    mat <- toRMatrix df
    [r| library("ggplot2")
        library("ComplexHeatmap")
        library("RColorBrewer")
        library("viridis")
        library("circlize")
        mat <- mat_hs
        pdf(output_hs, width=5.0, height=5)
        hm <- Heatmap(mat, col=viridis(99),
            show_row_names = T,
            show_column_names = T,
            show_row_dend = T, show_column_dend = T,
            clustering_method_rows="ward.D2",
            clustering_method_column="ward.D2",
            cluster_rows=T, cluster_columns=T,
            row_names_gp = gpar(fontsize=5),
            column_names_gp = gpar(fontsize=5),
            heatmap_legend_param = list(
                title = "correlation",
                title_gp = gpar(fontsize = 6),
                title_position = "leftcenter-rot",
                #direction = "horizontal",
                labels_gp = gpar(fontsize = 5)
            )
        )
        draw(hm)
        dev.off()
    |]
    return ()

plotDFs :: FilePath
        -> DF.DataFrame Double
        -> IO ()
plotDFs output df = R.runRegion $ do
    mat <- toRMatrix df
    [r| library("ggplot2")
        library("ComplexHeatmap")
        library("RColorBrewer")
        library("viridis")
        library("circlize")
        names <- c(cat1_hs, cat2_hs, cat3_hs, cat4_hs)
        fun <- function(x) {
            if (x < 1.65) {
                return(names[1])
            }
            else if (x < 2.35) {
                return(names[2])
            }
            else if (x < 3.1) {
                return(names[3])
            }
            else {
                return(names[4])
            }
        }
        mat <- apply(mat_hs, 1:2, fun)
        colors <- structure(c("#fef0d9", "#fdcc8a", "#fc8d59", "#d7301f"), names = names)

        pdf(output_hs, width=width_hs, height=height_hs)
        rwb <- colorRampPalette(colors = rev(brewer.pal(7,"RdBu")))
        hm <- Heatmap(mat, col=colors,
            row_title_gp = gpar(fontsize = 5),
            show_row_names = T,
            row_names_side = c("left"),
            show_column_names = T,
            show_row_dend = F, show_column_dend = F,
            clustering_method_rows="ward.D2",
            cluster_rows=F, cluster_columns=F,
            column_names_gp = gpar(fontsize=5),
            row_names_gp = gpar(fontsize=5),
            heatmap_legend_param = list(
                title = "P-value",
                title_gp = gpar(fontsize = 6),
                title_position = "leftcenter-rot",
                #direction = "horizontal",
                labels_gp = gpar(fontsize = 5)
            )
        )
        draw(hm)
        dev.off()
    |]
    return ()
  where
    vals = map V.maximum $ Mat.toRows $ DF._dataframe_data df
    cat1 = printf "NS (N = %d)" $ length $ filter (<1.65) vals :: String
    cat2 = printf "< 0.05 (N = %d)" $ length $ filter (\x -> x >= 1.65 && x < 2.35) vals :: String
    cat3 = printf "< 0.01 (N = %d)" $ length $ filter (\x -> x >= 2.35 && x < 3.1) vals :: String
    cat4 = printf "< 0.001 (N = %d)" $ length $ filter (\x -> x >= 3.1) vals :: String
    (row, col) = DF.dim df
    height = fromIntegral row * 0.15 :: Double
    width = fromIntegral col * 0.2 :: Double

readInput :: FilePath -> IO [(T.Text, FilePath)]
readInput fl = map (f . T.splitOn "\t") . T.lines <$> T.readFile fl
  where
    f [a,b,c] = (a <> "_" <> b, base <> T.unpack c)
    base = "/projects/ren-transposon/home/kai/ENCODE_DNase/"

readATACInput :: FilePath -> IO [(T.Text, FilePath)]
readATACInput fl = map (f . T.splitOn "\t") . T.lines <$> T.readFile fl
  where
    f xs = ((xs!!1) <> "_" <> (xs!!3), base <> T.unpack ((xs!!1) <> "_rep" <> (xs!!3) <> ".bed.gz"))
    base = "/projects/ren-transposon/home/kai/ATAC_collection/output/ATACSeq/Bed/"

count :: FilePath -> [BED3] -> IO (V.Vector Double)
count fl regions = runResourceT $ runConduit $ streamBedGzip fl .| rpkmBed regions

lookupMeta :: String -> IO (String, (String, String, [String], [String]))
lookupMeta x = queryById (EncodeAcc $ B.pack x) >>= \case
    Left e -> error e
    Right r -> do
        let life = as $ r |@ "replicates" |! 0 |@ "library" |@ "biosample" |@ "life_stage"
            cl = as $ r |@ "biosample_ontology" |@ "classification"
            organ = as $ r |@ "biosample_ontology" |@ "organ_slims"
            cells = as $ r |@ "biosample_ontology" |@ "cell_slims"
        return (x, (life, cl, organ, cells))

lookupLifeStage :: String -> IO String
lookupLifeStage x = queryById (EncodeAcc $ B.pack x) >>= \case
    Left e -> error e
    Right r -> return $ as $ r |@ "replicates" |! 0
        |@ "library" |@ "biosample" |@ "life_stage"

seqDepth :: FilePath -> IO Int
seqDepth fl = runResourceT $ runConduit $ sourceFile fl .|
    multiple ungzip .| linesUnboundedAsciiC .| lengthC

readVista :: FilePath -> IO [BED]
readVista fl = map (f . tail . map T.strip . T.splitOn "|") .
    filter ((=='>') . T.head) . filter (not . T.null) . T.lines <$> T.readFile fl
  where
    f (a:_:b:xs) = BED (B.pack $ T.unpack chr) (read $ T.unpack s) (read $ T.unpack e)
        nm Nothing Nothing
      where
        nm = case b of
            "positive" -> Just $ B.pack $ T.unpack $ T.intercalate "+" $
                map (T.map (\x -> if x == ' ' then '_' else x)) xs
            "negative" -> Nothing
            _ -> undefined
        [chr, a'] = T.splitOn ":" a
        [s,e] = T.splitOn "-" a'

intersectTable :: [BED] -> DF.DataFrame a -> DF.DataFrame a
intersectTable beds df = df `DF.rsub` idx
  where
    idx = runIdentity $ runConduit $
        yieldMany (zipWith BEDExt (map mkBed $ DF.rowNames df) [0 :: Int ..]) .|
        intersectBed beds .| mapC (^._data) .| sinkList
    mkBed x = let [chr,x'] = T.splitOn ":" x
                  [s,e] = T.splitOn "-" x'
              in BED3 (B.pack $ T.unpack chr) (read $ T.unpack s) (read $ T.unpack e)

callpeaks :: Int -> FilePath -> FilePath -> IO ()
callpeaks nReads input output = withTemp Nothing $ \subsample -> do
    let fl = location .~ subsample $ emptyFile :: File '[] 'Bed
    shelly $ escaping False $
        run_ "zcat" [T.pack $ show input, "|", "shuf", "-n", T.pack $ show nReads, ">", T.pack subsample]
    _ <- callPeaks output fl Nothing opt
    return ()
  where
    opt = mode .~ NoModel (-100) 200 $
        cutoff .~ QValue 0.01 $
        callSummits .~ True $
        gSize .~ Just "hs" $ def

similarity :: [BED3] -> [BED3] -> Double
similarity a b =
    let m = fromIntegral $ IS.size $ a' `IS.intersection` b'
        n = fromIntegral $ IS.size $ a' `IS.union` b'
    in m / n
  where
    a' = IS.fromList $ concatMap (\x -> map snd $ queryIntersect x set) a
    b' = IS.fromList $ concatMap (\x -> map snd $ queryIntersect x set) b
    set = bedToTree undefined $ zip (runIdentity $ runConduit $ mergeBed (a ++ b) .| sinkList) [0..]

diagonize :: DF.DataFrame Double -> DF.DataFrame Double
diagonize = DF.reorderRows f
  where
    f xs = map (\x -> (x, M.lookupDefault undefined x xs')) names
      where
        xs' = M.fromList xs
        names = map fst $ sortBy (comparing snd) $ map (\(a,b) -> (a, g b)) xs
        g z = (base + i, negate v)
          where
            base | v >= 3.1 = 0
                 | v >= 2.35 = 0
                 | v >= 1.65 = 0
                 | otherwise = 300
            v = z V.! i
            i = V.maxIndex z

plot :: FilePath   -- ^ output dir
     -> [(T.Text, Double)]
     -> DF.DataFrame Double
     -> IO [(T.Text, Double)]
plot dir abundance df = do
    let cor = concat $ Mat.toLists $ DF._dataframe_data df
        output = dir <> "/cor_qq_plot.pdf"
    R.runRegion $ do
        myTheme
        [r| library("ggplot2")
            df <- data.frame(y=cor_hs)
            fig <- ggplot(df, aes(sample = y)) + stat_qq() + stat_qq_line() + myTheme()
            ggsave(output_hs, fig, width=45, height=45, unit="mm", useDingbats=F)
        |]
        return ()
    let dfMatch = groupDF df
        df1 = DF.filterCols (\_ x -> V.maximum x >= 3.1) dfMatch
        df2 = DF.filterCols (\_ x -> V.maximum x >= 2.35 && V.maximum x < 3.1) dfMatch
        df3 = DF.filterCols (\_ x -> V.maximum x >= 1.65 && V.maximum x < 2.35) dfMatch
        df4 = DF.filterCols (\_ x -> V.maximum x < 1.65) dfMatch
        (cellName, cellAb) = unzip $ flip map (DF.colNames df4) $ \x -> (T.unpack x, fromJust $ lookup x abundance)
        number = map (fromIntegral . snd . DF.dim) [df4, df3, df2, df1] :: [Int32]
        pie = dir <> "/groups_pie.pdf"
        bar = dir <> "/group4.pdf"
    R.runRegion $ do
        myTheme
        [r| library("ggplot2")
            df <- data.frame(group=c("1", "2", "3", "4"), value = number_hs)
            fig <- ggplot(df, aes(x="", y=value, fill=group)) +
                geom_bar(width = 1, stat = "identity", color = "white") +
                coord_polar("y", start = 0) +
                geom_text(aes(y = c(20, 40, 60, 80), label = value)) +
                scale_fill_manual(values = c("#fef0d9", "#fdcc8a", "#fc8d59", "#d7301f")) +
                theme_void()
            ggsave(pie_hs, fig, width=80, height=80, unit="mm", useDingbats=F)

            df <- data.frame(x=cellName_hs, y=cellAb_hs)
            print(median(cellAb_hs))
            fig <- ggplot(df, aes(x=reorder(x, y), y=y)) +
                geom_hline(yintercept=median(cellAb_hs), linetype="dashed", color = "red") +
                geom_col() + coord_flip() + myTheme()
            ggsave(bar_hs, fig, width=60, height=100, unit="mm", useDingbats=F)
        |]
        return ()
    plotDFs (dir <> "/group1_similarity.pdf") $ diagonize $ DF.orderDataFrame id $ DF.transpose $ getTops df1
    plotDFs (dir <> "/group2_similarity.pdf") $ diagonize $ DF.orderDataFrame id $ DF.transpose $ getTops df2
    plotDFs (dir <> "/group3_similarity.pdf") $ diagonize $ DF.orderDataFrame id $ DF.transpose $ getTops df3
    plotDFs (dir <> "/group4_similarity.pdf") $ diagonize $ DF.orderDataFrame id $ DF.transpose $ getTops df4
    return $ zip (DF.colNames dfMatch) $ map V.maximum $ Mat.toColumns $ DF._dataframe_data dfMatch
  where
    getMaxZ d = zip (map T.unpack $ DF.rowNames d) $ map V.maximum $
        Mat.toRows $ DF._dataframe_data d
    output1 = dir <> "/similarity.tsv"
    output2 = dir <> "/similarity.pdf"
    output3 = dir <> "/selected_similarity.pdf"
    tissues :: [([T.Text], [T.Text])]
    tissues =
        --[ (["thyroid", "adrenal gland_adult", "nerve", "lower leg skin"], ["PDC", "Acn"])
        [ (["gastrocnemius medialis", "psoas muscle", "myocyte", "skeletal muscle", "LHCN", "SJCRH30", "A673"], ["Sk1", "Sk2"])
        , (["T cell", "B cell", "E61", "GM12878"], ["BLy", "TLy.1", "TLy.2"])
        , (["lobe of liver", "hepatocyte", "HepG2"], ["Hpc"])
        , (["pancreas", "Panc1", "acinar", "ductal", "JD13D", "HPNE"], ["PDC", "Acn"])
        ]
    computeRatio d (cellA, cellB) = --DF.imapCols
        --(\nm vec -> let m = mean $ bk `DF.cindex` nm in V.map (subtract m) vec) $
        (d `DF.rsub` selected) `DF.csub` cellB
      where
        selected = selectNames d cellA
        others = filter (not . (`elem` selected)) $ DF.rowNames d
        bk = (d `DF.rsub` others) `DF.csub` cellB
        selectNames d names = filter (\x -> any (\nm -> nm `T.isInfixOf` x) names) $
            DF.rowNames d
    getTops d = d `DF.rsub` tops
      where
        tops = nubSort $ flip concatMap (Mat.toColumns (DF._dataframe_data d)) $ \x ->
            map fst $ take 1 $ sortBy (flip (comparing snd)) $ zip (DF.rowNames d) $ V.toList x

computeRange :: Maybe [B.ByteString] -> FilePath -> FilePath
             -> IO [(B.ByteString, (Double, Double))]
computeRange names f1 f2 = runResourceT $ runConduit $ 
        source .| mapC (\((a,x),(_,y)) -> (a, (x,y))) .| filt .| sinkList
  where
    filt = case names of
        Nothing -> mapC id
        Just n -> let names' = S.fromList n in filterC ((`S.member` names') . fst)
    source = zipSources (sourceFile f1 .| linesUnboundedAsciiC .| (dropC 1 >> mapC (f . B.split '\t')))
        (sourceFile f2 .| linesUnboundedAsciiC .| (dropC 1 >> mapC (f . B.split '\t')))
    f (x:xs) = let xs' = U.fromList $ map (logBase 2 . (+1) . readDouble) xs
                --in (x, (U.maximum xs' - U.minimum xs') / mean xs')
                in (x, U.maximum xs' - mean xs')

plotRange :: [(B.ByteString, (Double, Double))] -> (forall s. R.R s (R.SomeSEXP s)) 
plotRange input = do
    let n = "N = " <> show (length xs)
    myTheme
    [r| library("ggplot2")
        library("cowplot")
        df <- data.frame(x=xs_hs, y=ys_hs)
        fig1 <- ggplot(df, aes(x=x,y=y)) +
            geom_hex(bins = 70) +
            scale_fill_continuous(type = "viridis") +
            xlim(0, 8) + ylim(0, 8) +
            coord_fixed() +
            labs(title = n_hs, x = "DNase-seq", y = "scATAC-seq") +
            geom_abline(slope = 1, colour="red", size = 0.2, linetype = "dashed") +
            myTheme()
        df2 <- data.frame(x=cats_hs, y=dat_hs)
        fig2 <- ggplot(df2, aes(x=x, y=y)) +
            geom_boxplot(lwd=0.3, outlier.size=0.2, outlier.stroke=0) +
            ylab("log(max / min)") + 
            myTheme() + myFill() + myColour()
        fig3 <- plot_grid(fig2, NULL, nrow=2,labels = "", rel_heights=c(2,1))
        plot_grid(fig1, fig3,
            ncol=2,
            rel_widths =c(2,1),
            labels = "" )
    |]
  where
    (xs, ys) = unzip $ map snd input
    (cats, dat) = unzip $ zip (repeat ("DNase" :: String)) xs <> zip (repeat "scATAC") ys
 
data Matching = Matching
    { _id :: String
    , _sampleName :: String
    , _classification :: String
    , _lifeStage :: String
    , _annotation :: String
    , _maxZ :: Double
    }

compareBuilder = do
    node "Com_Read_Input" [| \() -> liftIO $ readInput "data/dnase.txt" |] $ return ()
    node "Com_Get_Meta" [| \input -> liftIO $ mapM lookupMeta $ nubSort $
        map (T.unpack . (!!1) . reverse . T.splitOn "_" . fst) input
        |] $ return ()
    path ["Com_Read_Input", "Com_Get_Meta"]

    nodePar "Com_Seq_Depth" [| \(nm, fl) -> liftIO $ do 
        n <- seqDepth fl
        return (nm, n)
        |] $ return ()
    node "Com_Depth_Cor" [| \(depth, cor) -> liftIO $ do 
            df <- DF.rbind <$> mapM DF.readTable cor
            let f i v = let d = fromJust $ lookup i depth
                        in V.map (\x -> (logBase 10 $ fromIntegral d :: Double, x)) v
                (x,y) = unzip $ V.toList $ (!!3) $ Mat.toColumns $ DF._dataframe_data $ DF.imapRows f df
            R.runRegion $ do
                myTheme
                [r| library("ggplot2")
                    df <- data.frame(x=x_hs, y=y_hs)
                    fig <- ggplot(df, aes(x=x,y=y)) +
                        geom_point(size=0.2) +
                        xlab("log10(reads)") +
                        ylab("correlation") +
                        myTheme()
                    ggsave("1.pdf", fig, width=45, height=45, unit="mm", useDingbats=F)
                |]
                return ()
        |] $ return ()
    path ["Com_Read_Input", "Com_Seq_Depth"]
    ["Com_Seq_Depth", "Com_Cor"] ~> "Com_Depth_Cor"

    nodePar "Com_ReadCount" [| \(nm, fl) -> do
        dir <- (<> "/Comparison/Counts") <$> lookupConfig "output_dir"
        creFl <- lookupConfig "cre"
        let output = dir <> "/" <> T.unpack nm <> "_readcount.tsv"
        liftIO $ do
            shelly $ mkdir_p $ fromText $ T.pack dir
            peaks <- runResourceT $ runConduit $
                streamBedGzip creFl .| sinkList :: IO [BED3]
            rc <- count fl peaks
            B.writeFile output $ B.unlines $ map toShortest $ V.toList rc
            return (nm, output)
        |] $ return ()

    node "Com_Combine_ReadCount" [| \input -> do
        dir <- (<> "/Comparison/Counts") <$> lookupConfig "output_dir"
        accFl <- lookupConfig "accessibility"
        let output = dir <> "/merged_readcount.tsv"
        liftIO $ do
            rownames <- force . DF.rowNames <$> DF.readTable accFl
            dat <- forM input $ \(_, fl) -> force . V.fromList . map readDouble . B.lines <$> B.readFile fl
            let f xs = (fst $ head xs, average $ map snd xs)
                average xs = V.map (/ (fromIntegral $ length xs)) $
                    foldl1' (V.zipWith (+)) xs
                (nms, dat') = unzip $ map f $ groupBy ((==) `on` fst) $ sortBy (comparing fst) $ zip (map (T.init . fst . T.breakOnEnd "_" . fst) input) dat
                df = force $ DF.fromMatrix rownames nms $ Mat.fromColumns dat'
            DF.writeTable output (T.pack . show) df
            return output
        |] $ return ()
    path ["Com_ReadCount", "Com_Combine_ReadCount"]

    node "Com_Restricted_Peaks" [| \(input, peakFls) -> do
        dir <- (<> "/Comparison/") <$> lookupConfig "output_dir"
        accFl <- lookupConfig "accessibility"
        let output1 = dir <> "/restricted_peak_readcount1.tsv"
            output2 = dir <> "/restricted_peak_readcount2.tsv"
        liftIO $ do
            beds <- fmap (S.fromList . map showBed . nubSort . concat) $
                mapM (\(nm, fl) -> readBed fl :: IO [BED3]) peakFls
            runResourceT $ runConduit $ sourceFile input .| linesUnboundedAsciiC .|
                (takeC 1 >> filterC ((`S.member` beds) . fst . B.break (=='\t'))) .|
                unlinesAsciiC .| sinkFile output1
            runResourceT $ runConduit $ sourceFile accFl .| linesUnboundedAsciiC .|
                (takeC 1 >> filterC ((`S.member` beds) . fst . B.break (=='\t'))) .|
                unlinesAsciiC .| sinkFile output2
            return (output1, output2)
        |] $ return ()
    ["Com_Combine_ReadCount", "Restricted_Peaks"] ~> "Com_Restricted_Peaks"

    node "Com_Restricted_Peaks_Average" [| \((input1, input2), peakFls) -> do
        dir <- (<> "/Comparison/") <$> lookupConfig "output_dir"
        let output = dir <> "/restricted_peak_average.tsv"
            average xs = V.toList $ V.map (/ (fromIntegral $ length xs)) $
                foldl1' (V.zipWith (+)) xs
        liftIO $ do
            df1 <- DF.readTable input1
            df2 <- DF.readTable input2
            let df = force $ DF.map (logBase 2 . (+1)) $ DF.cbind [df1, df2]

            names <- sample 5000 $ DF.rowNames df
            DF.writeTable output (T.pack . show) $ df `DF.rsub` names
            {-
            (nms, dat) <- fmap (unzip . catMaybes) $ forM peakFls $ \(nm, fl) -> do
                beds <- map (T.pack . B.unpack . showBed) <$> (readBed fl :: IO [BED3])
                return $ force $ if null beds
                    then Nothing
                    else Just (nm, average $ Mat.toRows $ DF._dataframe_data $ df `DF.rsub` beds)
            DF.writeTable output (T.pack . show) $ DF.mkDataFrame nms (DF.colNames df) dat
            -}

            return output
        |] $ return ()
    ["Com_Restricted_Peaks", "Restricted_Peaks"] ~> "Com_Restricted_Peaks_Average"

    node "Com_Plot_Restricted_Peaks_Average" [| \input -> do
        annoFl <- lookupConfig "cluster_annotation"
        liftIO $ do
            names <- map (T.pack . B.unpack . fst) <$> readAnno annoFl
            df <- DF.readTable input
            let (idx1, idx2) = partition (`elem` names) $ DF.colNames df
                df1 = diagonize $ DF.orderDataFrame id $ df `DF.csub` idx1
                df2 = DF.reorderColumns (DF.orderByHClust id) $ (df `DF.csub` idx2) `DF.rsub` DF.rowNames df1
            print $ DF.dim df2
            plotMultiHeatmap "1.pdf" "" [map (DF.map (min 6)) [df1, df2]]
        |] $ return ()
    path ["Com_Restricted_Peaks_Average", "Com_Plot_Restricted_Peaks_Average"]


    nodePar "Com_Cor" [| \(nm, fl) -> do
        accFl <- lookupConfig "accessibility"
        dir <- (<> "/Comparison/Correlation/") <$> lookupConfig "output_dir"
        let output = dir <> "/" <> T.unpack nm <> "_cor.tsv"
        liftIO $ do
            shelly $ mkdir_p $ fromText $ T.pack dir
            df <- DF.map (logBase 2 . (+1)) <$> DF.readTable accFl
            input <- V.fromList . map (logBase 2 . (+1) . readDouble) . B.lines <$> B.readFile fl
            let res = flip map (Mat.toColumns $ DF._dataframe_data df) $ \x ->
                    pearson $ V.filter (\(x,y) -> x /= 0 || y /= 0) $ V.zip x input
            DF.writeTable output (T.pack . show) $
                DF.mkDataFrame [nm] (DF.colNames df) $ [res]
            return output
        |] $ return ()
    path ["Com_Read_Input", "Com_ReadCount", "Com_Cor"]

    node "Com_Stat_Cor" [| \(input, meta) -> do
        dir <- (<> "/Comparison/") <$> lookupConfig "output_dir"
        annoFl <- lookupConfig "cluster_annotation"
        compFl <- lookupConfig "composition"
        let output1 = dir <> "zscores.tsv"
            getID = T.unpack . head . reverse . T.splitOn "_" 
            average xs = V.toList $ V.map (/ (fromIntegral $ length xs)) $
                foldl1' (V.zipWith (+)) xs
            mergeReplicates df = DF.mkDataFrame nm (DF.colNames df) rows 
              where
                (nm, rows) = unzip $ map (\x -> let (a, b) = unzip x in (head a, average b)) $
                    groupBy ((==) `on` fst) $ sortBy (comparing fst) $
                    zip (map (T.init . fst . T.breakOnEnd "_") $ DF.rowNames df) $ Mat.toRows $ DF._dataframe_data df
        liftIO $ do
            anno <- readAnno annoFl
            inputData <- changeName anno . mergeReplicates . DF.rbind <$>
                mapM DF.readTable input
            let info = map lookupMeta $ DF.rowNames inputData
                lookupMeta x = let (stage,cl,organ,_) = fromJust $ lookup (getID x) meta
                               in ([getID x, getName x, cl, stage], inputData `DF.rindex` x)
                getName = T.unpack . T.init . fst . T.breakOnEnd "_" 
            DF.writeTable output1 T.pack $ DF.mkDataFrame (map (T.pack . head . fst) info)
                (["Sample name", "Type", "Life stage"] <> DF.colNames inputData)
                $ map (\(a, b) -> tail a <> map show (V.toList b)) info
            let info' = map (\x -> let (a,b) = unzip x in (tail $ head a, average b)) $
                    groupBy ((==) `on` (tail . fst)) $ sortBy (comparing (tail . fst)) info
                zscores = DF.mapRows scale $ DF.mkDataFrame
                    (map ((\x -> head x <> "_" <> last x) . map T.pack . fst) info')
                    (DF.colNames inputData) $ map snd info'
            comp <- DF.mapRows (\x -> let s = V.sum x in V.map (/s) x) . changeName anno <$> DF.readTable compFl
            let comp' = zip (DF.colNames comp) $ map ((100*) . V.maximum) $ Mat.toColumns $ DF._dataframe_data comp
            matchingScores <- plot dir comp' zscores
            let (datLabel, datX, datY) = unzip3 $
                    flip map (filter (not . (`elem` iummuneCells) . fst) matchingScores) $
                    \(nm, x) -> (T.unpack nm, x, fromJust $ lookup nm comp')
            R.runRegion $ do
                let out = dir <> "/abundance_vs_maxz.pdf"
                myTheme
                [r| library("ggplot2")
                    library("ggrepel")
                    df <- data.frame(names = datLabel_hs, y = datX_hs, x = datY_hs)
                    fig <- ggplot(df, aes(x=x,y=y)) +
                        geom_point(size=0.2) +
                        geom_smooth() +
                        scale_x_log10() +
                        #geom_text_repel(aes(label = names), label.size = 0.01,) +
                        ylab("MaxZ") +
                        xlab("Max abundance") +
                        myTheme()
                    ggsave(out_hs, fig, width=55, height=55, unit="mm", useDingbats=F)
                |]
                return ()
        |] $ return ()
    ["Com_Cor", "Com_Get_Meta"] ~> "Com_Stat_Cor"

    node "Com_BioSample_Stat" [| \(input, meta) -> do
        dir <- (<> "/Comparison/") <$> lookupConfig "output_dir"
        annoFl <- lookupConfig "cluster_annotation"
        liftIO $ do
            anno <- readAnno annoFl
            zscores <- changeName anno . DF.mapRows scale . DF.rbind <$>
                mapM DF.readTable input
            let info = mapMaybe lookupMeta $ DF.rowNames zscores
                lookupMeta x = let (stage,cl,organ,_) = fromJust $ lookup (getID x) meta
                               in if any (`elem` organ) ["kidney", "urinary bladder"]
                                    then Nothing
                                    else Just ([getID x, getName x, cl, stage], zscores `DF.rindex` x)
                getID = T.unpack . (!!1) . reverse . T.splitOn "_" 
                getName = T.unpack . (!!2) . reverse . T.splitOn "_" 
            let dat = map f $ groupBy ((==) `on` ((init . tail) . fst)) $
                    sortBy (comparing ((init . tail) . fst)) $
                    filter (\x -> not $ (fst x !! 3) `elem` ["embryonic", "unknown"]) $ info
                f xs =
                    let (a,b) = maximumBy (comparing snd) $
                            zip (map T.unpack $ DF.colNames zscores) $ average $ map snd xs
                        [_, nm, ty, stage] = fst $ head xs
                    in Matching { _id = nm <> ty
                                , _sampleName = nm
                                , _classification = ty
                                , _lifeStage = stage
                                , _annotation = a
                                , _maxZ = b }
                average xs = V.toList $ V.map (/ (fromIntegral $ length xs)) $
                    foldl1' (V.zipWith (+)) xs

            let mkName ((a,_,b), (c,v)) = (a, b, c , v)
                dat = catMaybes $ zipWith f (DF.rowNames zscores) $ Mat.toRows $ DF._dataframe_data zscores
                f x vec = if any (`elem` val^._3) ["kidney", "urinary bladder"]
                    then Nothing
                    else Just ((getName x, val^._2, val^._1), vec)
                  where
                    val = fromJust $ lookup (getID x) meta
                average xs = V.toList $ V.map (/ (fromIntegral $ length xs)) $
                    foldl1' (V.zipWith (+)) xs

            violin (dir <> "/maxZ_violin.pdf") $
                map (\((_,a,b), c) -> ((a,b), V.maximum c)) dat
            violin' (dir <> "/maxZ_violin2.pdf") $
                filter ((`elem` ["primary cell", "cell line", "tissue"]) . fst) $
                map (\((_,a,_), b) -> (a, V.maximum b)) $
                filter (\x -> all (/=x^._1._3) ["embryonic", "unknown"]) dat
        |] $ return ()
    ["Com_Cor", "Com_Get_Meta"] ~> "Com_BioSample_Stat"


    node "Read_Vista" [| \() -> do
        dir <- (<> "/Comparison/") <$> lookupConfig "output_dir"
        chain <- lookupConfig "hg19_to_hg38"
        let output = dir <> "/vista.bed"
        liftIO $ do
            readVista "data/vista.txt" >>= liftOver chain >>= writeBed output
            return output
        |] $ return ()
    node "Plot_Vista" [| \input -> do
        accFl <- lookupConfig "accessibility"
        dir <- lookupConfig "output_dir"
        annoFl <- lookupConfig "cluster_annotation"
        liftIO $ do
            anno <- readAnno annoFl
            df <- changeName anno . DF.map (logBase 2 . (+1)) <$> DF.readTable accFl
            vista <- readBed input

            let getNames (Just x) = map (Just . fst . B.break (=='[')) $ B.split '+' x
                getNames Nothing = [Just "negative"]
                tissues = ["forebrain", "heart", "negative", "blood_vessels"]
                enhancerGroup = filter ((`elem` tissues) . fromJust . (^.name) . head) $
                    filter ((>10) . length) $
                    groupBy ((==) `on` (^.name)) $
                    sortBy (comparing (^.name)) $
                    flip concatMap vista $ \v -> map (\x -> name .~ x $ v) $ getNames $ v^.name
                (rows, dat) = unzip $ flip map enhancerGroup $ \enh ->
                    let nm = T.pack $ B.unpack $ fromMaybe "negative" $ head enh^.name
                    in (nm, average $ zscore $ intersectTable enh df)
                --zscore x = Mat.toRows $ DF._dataframe_data x
                zscore x = map scale $ Mat.toRows $ DF._dataframe_data x
                average xs = V.toList $ V.map (/ (fromIntegral $ length xs)) $
                    foldl1' (V.zipWith (+)) xs
                matrix = DF.mkDataFrame rows (DF.colNames df) dat
                cols = nubSort $ flip concatMap dat $ \r -> map snd $ take 10 $ reverse $ sortBy (comparing fst) $ zip r (DF.colNames df)
            plotHeatmap (dir <> "/vista.pdf") $ (`DF.csub` cols) $
                DF.mkDataFrame rows (DF.colNames df) dat
            DF.writeTable (dir <> "/vista.tsv") (T.pack . show) $ (`DF.csub` cols) $
                DF.mkDataFrame rows (DF.colNames df) dat
        |] $ return ()
    path ["Read_Vista", "Plot_Vista"]