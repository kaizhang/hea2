{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell   #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE QuasiQuotes       #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE GADTs #-}
import HEA

import Statistics.Quantile
import qualified Taiji.Utils.DataFrame as DF
import Bio.RealWorld.GENCODE
import Control.Monad.State.Strict
import Control.Arrow
import qualified Data.Vector as V
import AI.Clustering.Hierarchical hiding (normalize)
import Data.Either
import Control.Parallel.Strategies (parMap, rdeepseq)
import qualified Data.Text as T
import qualified Data.Text.IO as T
import Shelly hiding (FilePath, path)
import Data.Int
import Bio.Data.Bed
import Control.Monad.ST (runST)
import qualified Data.Matrix as Mat
import qualified Data.ByteString.Char8 as B
import qualified Data.HashMap.Strict as M
import qualified Data.HashSet as S
import Data.List.Ordered (nubSort, nubSortOn)
import Bio.Utils.Functions
import Taiji.Utils.Plot
import Statistics.Sample (mean, meanVarianceUnb)
import Data.Binary
import System.Random.MWC.Distributions (uniformShuffle)
import System.Random.MWC (create)
import Text.Wrap
import Control.Concurrent.Async (forConcurrently)
import Data.Char (toUpper)

import Taiji.Prelude
import Data.Vector.SEXP (toList)
import qualified Language.R                        as R
import qualified Language.R.HExp as H
import           Language.R.QQ

import Intersect
import CRE
import Comparison

iummuneCells = ["CD8 Effector T", "CD4 Effector T", "Natural Killer T", "Naive T", "Plasma B", "Memory B", "Macrophage 1", "Mast"]

--------------------------------------------------------------------------------
-- Restricted peaks
--------------------------------------------------------------------------------

enrichment :: S.HashSet B.ByteString -> [B.ByteString] -> Int -> IO (Double, Double)
enrichment motifs fg nBg' = if e >= 0
    then R.runRegion $ do
        p <- [r| phyper(oFg_hs - 1, oBg_hs, nBg_hs - oBg_hs, nFg_hs, lower.tail=F, log.p=T) / log(10)
            |]
        R.unSomeSEXP p $ \x -> case H.hexp x of
            H.Real x'  -> return (e, head $ toList x')
    else R.runRegion $ do
        p <- [r| phyper(oFg_hs, oBg_hs, nBg_hs - oBg_hs, nFg_hs, lower.tail=T, log.p=T) / log(10)
            |]
        R.unSomeSEXP p $ \x -> case H.hexp x of
            H.Real x'  -> return (e, head $ toList x')
  where
    e = logBase 2 $ (oFg / nFg) / (oBg / nBg)
    oFg = fromIntegral $ length $ filter (`S.member` motifs) fg :: Double
    oBg = fromIntegral $ S.size motifs :: Double
    nFg = fromIntegral $ length fg :: Double
    nBg = fromIntegral nBg' :: Double

average :: DF.DataFrame Double -> V.Vector Double
average df = f $ Mat.toRows $ DF._dataframe_data df
  where
    f xs = V.map (/n) $ foldl1' (V.zipWith (+)) xs
      where n = fromIntegral $ length xs

getSignal :: DF.DataFrame Double -> (T.Text, [BED3]) -> (T.Text, V.Vector Double)
getSignal df (name, peaks) = 
    let rows = map mkName peaks 
        cols = DF.colNames df
        df' = DF.mkDataFrame (map (\x -> name <> "_" <> x) rows) cols $
            Mat.toLists $ DF._dataframe_data $ df `DF.rsub` rows
    in (name, average df')
    --return $ DF.mkDataFrame [name] (DF.colNames df) [V.toList $ average $ df `DF.rsub` rows]

mkName :: BED3 -> T.Text
mkName p = T.pack $ B.unpack (p^.chrom) <> ":" <> show (p^.chromStart) <>
    "-" <> show (p^.chromEnd)

mkGOMatrix :: Int -> [(T.Text, [GREAT])] -> DF.DataFrame Double
mkGOMatrix n input = DF.mkDataFrame (map fst input) (map (abbrGo . T.pack . B.unpack) terms) $
    flip map input $ \(_, go) -> 
        let m = M.fromList $ map (\x -> (_great_term_name x, negate $ logBase 10 $ max 1e-100 $ _great_pvalue x)) go
        --let m = M.fromList $ map (\x -> (_great_term_name x, _great_enrichment x)) go
        in map (\x -> M.lookupDefault 0 x m) terms
  where
    terms = nubSort $ concatMap (take n . map _great_term_name . snd) $
        flip map input $ second $ \terms -> fst $ unzip $ sortBy (comparing snd) $
            V.toList $ filterFDR 0.01 $ V.fromList $ map (\x -> (x, _great_pvalue x)) $
            filter ((>=2) . _great_enrichment) terms

diagonize :: DF.DataFrame Double -> DF.DataFrame Double
diagonize = DF.reorderRows f
  where
    f xs = map (\x -> (x, M.lookupDefault undefined x xs')) names
      where
        xs' = M.fromList xs
        names = map fst $ sortBy (comparing snd) $ map (\(a,b) -> (a, g b)) xs
        g xs = (i, negate v)
          where
            v = z V.! i
            i = V.maxIndex z
            z = scale xs

plotGO :: FilePath
       -> DF.DataFrame Double
       -> IO()
plotGO output df = R.runRegion $ do
    valMatrix <- toRMatrix df
    [r| library("ggplot2")
        library("ComplexHeatmap")
        library("RColorBrewer")
        library("viridis")
        h1 <- Heatmap( valMatrix_hs, 
            col=magma(9),
            cluster_rows=F, cluster_columns=F,
            show_row_names=T, row_names_side="left",
            show_column_names=T,
            row_names_gp = gpar(fontsize=5),
            column_names_gp = gpar(fontsize=5),
            heatmap_legend_param = list(
                title = "-log10(P-value)",
                title_position = "leftcenter-rot"
                #at = c(-2, 0, 2)
            )
        )
        pdf(output_hs, width=10.0, height=11.0)
        draw(h1)
        dev.off()
    |]
    return ()
  where
    names = map (T.unpack . head . T.splitOn ".") $ DF.rowNames df

plotRestrictedPeaks :: FilePath
                    -> DF.DataFrame Double
                    -> IO ()
plotRestrictedPeaks output df = R.runRegion $ do
    mat <- toRMatrix df
    [r| library("ComplexHeatmap")
        library("viridis")
        pdf(output_hs, width=8.0, height=7)
        hm <- Heatmap( mat_hs,
            col=viridis(99),
            cluster_rows=F, cluster_columns=F,
            show_row_names=F,
            show_column_names=T, column_names_side="top",
            column_names_gp = gpar(fontsize=5),
            heatmap_legend_param = list(
                title = "mean(log(1+acc.))",
                title_gp = gpar(fontsize = 6),
                title_position = "leftcenter-rot",
                labels_gp = gpar(fontsize = 5)
            )
        )
        draw(hm)
        dev.off()
    |]
    return ()

diagonize' :: (a -> V.Vector Double) -> [a] -> [a]
diagonize' f xs = map fst $ sortBy (comparing snd) $ map (\x -> (x, g $ f x)) xs
  where
    g vec = let m = mean vec
                ratio = V.maximum vec / m
                i = if ratio < 2 then -1 else V.maxIndex vec
            in (i, negate m)

plotMultiHeatmap :: FilePath
            -> [DF.DataFrame Double]
            -> IO ()
plotMultiHeatmap output dfs = R.runRegion $ do
    mat <- toRMatrix df
    [r| library("ggplot2")
        library("ComplexHeatmap")
        library("RColorBrewer")
        library("viridis")
        library("circlize")

        pdf(output_hs, width=30, height=10)
        hm <- Heatmap(mat_hs, col=viridis(99),
            use_raster=T,
            row_split = rSplit_hs, row_title = NULL,
            row_title_gp = gpar(fontsize = 5),
            show_row_names = F,
            show_column_names = T,
            show_row_dend = F, show_column_dend = F,
            #clustering_method_rows="ward.D2",
            cluster_rows=F, cluster_columns=F,
            column_names_gp = gpar(fontsize=5),
            row_names_gp = gpar(fontsize=5),
            heatmap_legend_param = list(
                title = "log(1+RPM)",
                title_gp = gpar(fontsize = 6),
                title_position = "leftcenter-rot",
                #direction = "horizontal",
                labels_gp = gpar(fontsize = 5)
            )
        )
        draw(hm)
        dev.off()
    |]
    return ()
  where
    df = DF.rbind dfs
    rSplit = concat $ zipWith (\i x -> replicate (fst $ DF.dim x) $ show i) [1..] dfs :: [String]

readMotifs :: FilePath -> IO (B.ByteString, S.HashSet B.ByteString)
readMotifs fl = do
    s <- decodeFile fl
    return (nm, s)
  where
    nm = B.pack $ T.unpack $ fst $ T.breakOn ".bin" $ snd $ T.breakOnEnd "/" $ T.pack fl

plotHeatmapGroup :: FilePath
                 -> DF.DataFrame Double
                 -> DF.DataFrame Double -> IO ()
plotHeatmapGroup output df1 df2 = R.runRegion $ do
    mat1 <- toRMatrix df1
    mat2 <- toRMatrix df2
    [r| library("ggplot2")
        library("ComplexHeatmap")
        library("RColorBrewer")
        library("viridis")
        library("circlize")
        rwb <- colorRamp2(seq(-100, 100, length.out=9), rev(brewer.pal(9,"RdBu")))
        color2 <- colorRamp2(seq(0, 50, length.out=9), magma(9))
        h1 <- Heatmap( mat1_hs, 
            col=color2,
            cluster_rows=F, cluster_columns=F,
            show_row_names=T, row_names_side="left",
            show_column_names=T,
            row_names_gp = gpar(fontsize=5),
            column_names_gp = gpar(fontsize=5),
            heatmap_legend_param = list(
                title = "-log10(P-value)",
                title_position = "leftcenter-rot"
            )
        )
        h2 <- Heatmap( mat2_hs, 
            col=rwb,
            cluster_rows=F, cluster_columns=F,
            show_row_names=T, row_names_side="left",
            show_column_names=T,
            row_names_gp = gpar(fontsize=5),
            column_names_gp = gpar(fontsize=5),
            heatmap_legend_param = list(
                title = "-log10(P-value)",
                title_position = "leftcenter-rot"
            )
        )
        pdf(output_hs, width=8.0, height=5.0)
        draw(h1+h2)
        dev.off()
    |]
    return ()

filterMotifResult :: FilePath -> (B.ByteString -> Bool)
                  -> IO [(B.ByteString, Double)]
filterMotifResult fl isExpressed = 
    map (second h) .
        map (\(a,b,c) -> (a, (b,c))) .
        filter (lookupExpr . (^._1)) .
        map (f . B.split '\t') . B.lines <$> B.readFile fl
  where
    f [a,b,c] = ( a
                , readDouble' b  -- fold
                , readDouble c )   -- p-value
    g a@(fd1, p1) b@(fd2, p2)
        | p1 < p2 || (p1 == p2 && abs fd1 > abs fd2) = a
        | otherwise = b
    lookupExpr g = any isExpressed $ B.split '+' $ fst $ B.break (=='_') g
    h (a, b) | a > 0 = negate b
             | otherwise = b
    readDouble' x | x == "-Infinity" = -1 / 0
                  | otherwise = readDouble x

plotMotif :: FilePath
       -> DF.DataFrame Double
       -> IO()
plotMotif output df = R.runRegion $ do
    valMatrix <- toRMatrix df
    [r| library("ggplot2")
        library("ComplexHeatmap")
        library("RColorBrewer")
        library("viridis")
        rwb <- colorRampPalette(colors = rev(brewer.pal(7,"RdBu")))
        h1 <- Heatmap( valMatrix_hs, 
            col=rwb(99),
            cluster_rows=F, cluster_columns=F,
            show_row_names=T, row_names_side="left",
            show_column_names=T,
            row_names_gp = gpar(fontsize=5),
            column_names_gp = gpar(fontsize=5),
            heatmap_legend_param = list(
                title = "-log10(P-value)",
                title_position = "leftcenter-rot"
                #at = c(-2, 0, 2)
            )
        )
        pdf(output_hs, width=20.0, height=9.0)
        draw(h1)
        dev.off()
    |]
    return ()



--------------------------------------------------------------------------------
-- Workflow
--------------------------------------------------------------------------------

build "wf" [t| SciFlow ENV |] $ do
    creBuilder
    compareBuilder
    intersectBuilder

    ----------------------------------------------------------------------------
    -- Restricted peaks
    ----------------------------------------------------------------------------
    node "Restricted_Peaks" [| \() -> do
        dir <- lookupConfig "output_dir"
        pvalueFl <- lookupConfig "pvalues"
        liftIO $ do
            pvalues <- DF.readTable pvalueFl
            let cols = zip (DF.colNames pvalues) $ Mat.toColumns $ DF._dataframe_data pvalues
                mkPeak x = let [chr, x'] = T.splitOn ":" x
                               [s,e] = T.splitOn "-" x'
                           in BED3 (B.pack $ T.unpack chr) (read $ T.unpack s) (read $ T.unpack e)
            shelly $ mkdir_p $ fromText $ T.pack $ dir <> "/restricted_peaks"
            forConcurrently cols $ \(nm, vec) -> do
                let output = dir <> "/restricted_peaks/" <> T.unpack nm <> ".bed"
                writeBed output $ map mkPeak $ fst $ unzip $ V.toList $
                    --filterFDR 0.05 $ V.zip (V.fromList $ DF.rowNames pvalues) vec
                    V.filter ((<0.025) . snd) $ V.zip (V.fromList $ DF.rowNames pvalues) vec
                return (nm, output)
        |] $ return ()

    node "Plot_Restricted_Peaks" [| \input -> do
        tableFl <- lookupConfig "accessibility"
        dir <- lookupConfig "output_dir"
        orderFl <- lookupConfig "cluster_order"
        annoFl <- lookupConfig "cluster_annotation"
        let output = dir <> "/restricted_peaks.pdf"
        liftIO $ do
            orders <- reverse . T.lines <$> T.readFile orderFl
            anno <- readAnno annoFl
            peaks <- forM input $ \(_, peakFl) ->
                map (T.pack . B.unpack . showBed) <$> (readBed peakFl :: IO [BED3])
            print $ length $ nubSort $ concat peaks
            peaks' <- create >>= sampling' 5000 peaks
            df <- DF.map (logBase 2 . (+1)) . (`DF.csub` orders) <$> DF.readTable tableFl
            plotRestrictedPeaks output $ changeName anno $
                DF.rbind $ diagonize' average $ 
                map (DF.map (min 6) . (df `DF.rsub`)) peaks'
            {-
            let tables = V.fromList $ parMap rdeepseq (getSignal df) peaks
                (names, vecs) = unzip $ flatten $ hclust Ward tables (euclidean `on` snd)
                df' = DF.mkDataFrame names (DF.colNames df) $ map V.toList vecs
                num_peak = map (\(x,y) -> (x, length y)) peaks

            plotRestrictedPeaks (dir <> "/restricted_peaks.pdf") num_peak $ df' `DF.csub` names

            DF.writeTable output (T.pack . show) $ df' `DF.csub` names
            return (output, num_peak)
            -}
        |] $ return ()
    path ["Restricted_Peaks", "Plot_Restricted_Peaks"]

    nodePar "Restricted_Peaks_GO" [| \(nm, fl) -> liftIO $ do
            go <- great fl
            return (nm, go)
        |] $ return ()
    node "Plot_Restricted_Peaks_GO" [| \go -> do
        dir <- lookupConfig "output_dir"
        annoFl <- lookupConfig "cluster_annotation"
        let output2 = dir <> "/restricted_peaks_go.pdf"
            output3 = dir <> "/restricted_peaks_go.tsv"
            f terms = fst $ unzip $ sortBy (comparing snd) $ V.toList $
                filterFDR 0.01 $ V.fromList $ map (\x -> (x, _great_pvalue x)) $
                filter ((>=2) . _great_enrichment) terms
        liftIO $ do
            anno <- readAnno annoFl
            let go' = map (first (\x -> maybe x (T.pack . B.unpack) $ lookup (B.pack $ T.unpack x) anno)) $
                    map (second f) go
            plotGO output2 $ changeName anno $ diagonize $ DF.orderDataFrame id $ DF.map (min 50) $ DF.transpose $ mkGOMatrix 3 go
            DF.writeTable output3 (T.pack . show)$ changeName anno $ DF.transpose $ mkGOMatrix 50 go
        |] $ return ()
    path ["Restricted_Peaks", "Restricted_Peaks_GO", "Plot_Restricted_Peaks_GO"]

    nodePar "Restricted_Peaks_Motif" [| \(nm, fl) -> do
        dir <- (<> "/restricted_peaks/motif/") <$> lookupConfig "output_dir"
        peakFl <- lookupConfig "combined_peak"
        motifDir <- lookupConfig "motif_dir"
        let output = dir <> T.unpack nm <> ".tsv"
        liftIO $ do
            motifFls <- fmap (filter (not . ("mouse" `T.isInfixOf`) . snd . T.breakOnEnd "/")) $ shelly $ lsT motifDir
            shelly $ mkdir_p dir
            n <- fmap length (runResourceT $ runConduit $ streamBedGzip peakFl .| sinkList :: IO [BED3])
            regions <- map showBed <$> (readBed fl :: IO [BED3])
            r <- fmap B.unlines $ forM motifFls $ \m -> do
                (nm, motif') <- readMotifs $ T.unpack m
                (p1, p2) <- enrichment motif' regions n
                return $ B.intercalate "\t" [nm, toShortest p1, toShortest p2]
            B.writeFile output r
            return (nm, output)
        |] $ return ()

    node "Plot_Restricted_Peaks_Motif" [| \input -> do
        dir <- lookupConfig "output_dir"
        geneFl <- lookupConfig "gene_expr"
        annoFl <- lookupConfig "cluster_annotation"
        let output = dir <> "/restricted_peaks_motif.pdf"
            output' = dir <> "/restricted_peaks_motif.tsv"
        liftIO $ do
            anno <- readAnno annoFl
            genes <- DF.readTable geneFl
            (rows, res) <- fmap unzip $ forM input $ \(nm, fl) -> do
                res <- filterMotifResult fl $ \x -> case genes `DF.indexMaybe` (T.pack (B.unpack x), nm) of
                    Nothing -> False
                    Just e -> e > 1
                return (nm, map (\(a, b) -> (T.pack $ B.unpack a, b)) res)
            let cols = nubSort $ concatMap (map fst) res
                dat = flip map res $ \x -> map (\y -> fromMaybe 0 $ lookup y x) cols
                df = DF.transpose $ DF.mkDataFrame rows cols dat
            --plotMotif output $ DF.orderDataFrame id $ DF.map (max (-100) . min 100) $ changeName anno df
            DF.writeTable output' (T.pack . show) $ changeName anno df
            return output'
        |] $ return ()
    path ["Restricted_Peaks", "Restricted_Peaks_Motif", "Plot_Restricted_Peaks_Motif"]

    node "Plot_Selected_Restricted_Peaks" [| \(go, motifFl) -> do
        dir <- lookupConfig "output_dir"
        annoFl <- lookupConfig "cluster_annotation"
        let output =dir <> "/selected_restricted_peaks.pdf"
            f terms = fst $ unzip $ sortBy (comparing snd) $ V.toList $
                filterFDR 0.01 $ V.fromList $ map (\x -> (x, _great_pvalue x)) $
                filter ((>=2) . _great_enrichment) terms
            selection :: [T.Text]
            --selection = ["C20.1", "C20.2", "C20.3", "C20.4", "C27.3", "C20.5", "C27.2", "C27.1", "C30"]
            --selection = ["C4.1", "C4.2", "C4.3", "C4.4", "C4.5", "C4.6", "C4.7", "C4.8", "C4.9"]
            --selection = ["C9.1", "C9.2", "C9.3", "C9.4", "C9.5", "C9.6", "C9.7", "C9.8", "C9.9"]
            selection = ["C18.1", "C18.2", "C18.3", "C15.6", "C26", "C11", "C28"]
                --["C1.8", "C8.2", "C1.7", "C1.2", "C1.1", "C8.1", "C1.3", "C5.8", "C19.2", "C13.3", "C7.1", "C7.2", "C5.5", "C5.4", "C1.6", "C13.2", "C19.1", "C24", "C1.4", "C1.5"]
        liftIO $ do
            anno <- readAnno annoFl
            motif <- changeName anno . DF.filterRows (\_ v -> V.maximum v > 3) . (`DF.csub` selection) <$> DF.readTable motifFl
            let tfs = nubSortOn f $ flip concatMap (Mat.toColumns $ DF._dataframe_data motif) $ \v -> 
                    map fst $ take 10 $
                        sortBy (flip (comparing snd)) $
                        map (maximumBy (comparing snd)) $ 
                        groupBy ((==) `on` (tfCluster . fst)) $
                        sortBy (comparing (tfCluster . fst)) $
                        zip (DF.rowNames motif) $ V.toList v
                f x = fst $ T.breakOn "_" x
                tfCluster = snd . T.breakOnEnd "+"
                goDF = DF.transpose $ changeName anno $ DF.transpose $ DF.orderDataFrame id $
                    mkGOMatrix 5 $ filter ((`elem` selection) . fst) go
                motifDF = (DF.orderDataFrame id $ DF.transpose $ motif `DF.rsub` tfs) `DF.rsub` (DF.rowNames goDF)
            plotHeatmapGroup output goDF $ DF.fromMatrix (DF.rowNames motifDF)
                (map (fst . T.breakOn "_") $ DF.colNames motifDF) $ DF._dataframe_data motifDF
        |] $ return ()
    ["Restricted_Peaks_GO", "Plot_Restricted_Peaks_Motif"] ~> "Plot_Selected_Restricted_Peaks"

    node "Restricted_Peaks_Intersect" [| \input -> do
        dhsFl <- lookupConfig "dhs"
        compFl <- lookupConfig "composition"
        annoFl <- lookupConfig "cluster_annotation"
        liftIO $ do
            anno <- readAnno annoFl
            comp <- DF.mapRows (\x -> let s = V.sum x in V.map (/s) x) <$> DF.readTable compFl
            let comp' = zip (DF.colNames comp) $ map V.maximum $ Mat.toColumns $ DF._dataframe_data comp
            dhs <- fmap sortBed (runResourceT $ runConduit $ streamBedGzip dhsFl .| sinkList :: IO [BED3])
            (datLabel, datY, datX) <- fmap (unzip3 . filter (\x -> not $ x^._1 `elem` iummuneCells)) $ forM input $ \(nm, fl) -> do
                peaks <- readBed fl :: IO [BED3]
                m <- runConduit $ yieldMany peaks .| intersectSortedBed dhs .| lengthC
                return ( B.unpack $ fromJust $ lookup (B.pack $ T.unpack nm) anno
                       , 100 * (1 - fromIntegral m / fromIntegral (length peaks)) :: Double
                       , 100 * (fromJust $ lookup nm comp'))
            R.runRegion $ do
                let out = "1.pdf" :: String
                myTheme
                [r| library("ggplot2")
                    library("ggrepel")
                    df <- data.frame(names = datLabel_hs, x = datX_hs, y = datY_hs)
                    fig <- ggplot(df, aes(x=x,y=y)) +
                        geom_point(size=0.2) +
                        geom_smooth() +
                        scale_x_log10() +
                        #geom_text_repel(aes(label = names), label.size = 0.01,) +
                        xlab("Max abundance") +
                        ylab("%novel") +
                        myTheme()
                    ggsave(out_hs, fig, width=45, height=45, unit="mm", useDingbats=F)
                |]
                return ()
        |] $ return ()
    path ["Restricted_Peaks", "Restricted_Peaks_Intersect"]

main :: IO ()
main = mainFun wf