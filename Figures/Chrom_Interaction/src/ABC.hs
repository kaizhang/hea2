{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE QuasiQuotes       #-}
{-# LANGUAGE TemplateHaskell   #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE KindSignatures #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE ExplicitNamespaces #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE InstanceSigs #-}
{-# LANGUAGE RecordWildCards #-}

module ABC (abcBuilder) where

import HEA

import qualified Data.Text as T
import qualified Data.Vector.Unboxed as U
import qualified Data.ByteString.Char8 as B
import Bio.Utils.Functions
import qualified Data.Matrix as Mat
import Control.Concurrent.Async (forConcurrently_, forConcurrently)
import Bio.Data.Bed
import Bio.Data.Bed.Utils
import Bio.Data.Bed.Types
import Bio.Data.Bam
import Data.List.Ordered
import Data.Either
import Bio.RealWorld.GENCODE
import Data.Int (Int32)
import Data.Conduit.List (chunksOf)
import Shelly (mkdir_p, shelly, lsT, fromText)
import Taiji.Prelude
import Statistics.Quantile
import qualified Data.Map.Strict as M
import Data.Binary
import qualified Data.Vector as V
import qualified Taiji.Utils.DataFrame as DF
import Data.Singletons.Prelude hiding ((@@))
import Data.Matrix.Static.IO (toMM)
import qualified Data.Set as Set

import qualified Data.Matrix.Dynamic as Dyn
import qualified Data.Matrix.Static.Dense as D
import Data.Matrix.Static.LinearAlgebra
import qualified Data.Matrix.Static.Sparse as S
import qualified Data.Matrix.Static.Generic as G

import Taiji.Prelude hiding (groupBy)
import qualified Language.R                        as R
import           Language.R.QQ
import Language.R.HExp

pcHiC :: [(String, FilePath, FilePath)]
pcHiC = flip map dat $ \(nm, abb) ->
    (nm, printf "data/pcHiC/%s.po.txt" (abb :: String), printf "data/pcHiC/%s.pp.txt" abb)
  where
    dat = [ ("Adrenal Gland", "AD2")
          , ("Aorta", "AO")
          , ("Cardiomyocytes", "CM")
          , ("Esophagus", "EG2")
          , ("FAT", "FT2")
          , ("Gastric tissue", "GA")
          , ("Lung", "LG")
          , ("Liver", "LI11")
          , ("Left Ventricle", "LV")
          , ("Ovary", "OV2")
          , ("Pancreas", "PA")
          , ("Right heart atrium", "RA3")
          , ("Right Ventricle", "RV")
          , ("Sigmoid colon", "SG1")
          , ("Dorsorateral prefrontal cortex", "X5628FC") ]



data Interaction = Interaction
    { _inter_locus1 :: BED3
    , _inter_locus2 :: BED3
    , _inter_pvalue :: Double
    , _inter_normalized_freq :: Double
    }

readInteraction :: FilePath -> IO [Interaction]
readInteraction fl = map (f . B.split '\t') . tail . B.lines <$> B.readFile fl
  where
    f [ _, frag1, frag2, frag1_cov, frag2_cov, freq
      , dist, all_exp, all_capture_res, exp_value_dist
      , dist_res, pvalue] = Interaction (mkBed frag1) (mkBed frag2) (readDouble pvalue) (readDouble dist_res)
    mkBed x = let [chr,x'] = B.split ':' x
                  [s,e] = B.split '-' x'
              in BED3 chr (readInt s) $ readInt e

liftInteraction :: FilePath -> [Interaction] -> IO [Interaction]
liftInteraction chain inter = do
    regions <- M.fromList . map (\x -> (x^._data, x^._bed)) <$> liftOver chain uniqRegions
    return $ mapMaybe (change regions) inter
  where
    change table x = case (M.lookup (showBed $ _inter_locus1 x) table, M.lookup (showBed $ _inter_locus2 x) table) of
        (Just a, Just b) -> Just x{_inter_locus1=a, _inter_locus2=b}
        _ -> Nothing
      where
    uniqRegions = map (\x -> BEDExt x $ showBed x) $ nubSort $ map _inter_locus1 inter <> map _inter_locus2 inter
    showBed (BED3 chr s e) = B.unpack chr <> ":" <> show s <> "-" <> show e

assignInteractons :: BEDLike b
                  => [BED]   -- ^ promoter
                  -> [b]   -- ^ CRE
                  -> [Interaction]
                  -> [((BED, b), Double)]
assignInteractons promoters cre interactions = flip concatMap interactions $ \Interaction{..} ->
    let links = getLinks _inter_locus1 _inter_locus2 ++ getLinks _inter_locus2 _inter_locus1
    in zip links $ repeat _inter_normalized_freq
  where
    getLinks l1 l2 = flip concatMap ps $ \p -> zip (repeat p) $
        map snd (queryIntersect p creTree) ++ es
      where
        ps = map snd $ queryIntersect l1 proTree
        es = map snd $ queryIntersect l2 creTree
    creTree = bedToTree undefined $ map (\x -> (x,x)) cre
    proTree = bedToTree const $ map (\x -> (x,x)) promoters

abcScore' :: FilePath   -- ^ output dir
          -> FilePath   -- ^ matrix
          -> FilePath   -- ^ CRE activity
          -> IO ()
abcScore' dir matFl creFl = do
    contacts <- map (f . B.split '\t') . B.lines <$> B.readFile matFl
    let genes = nubSort $ map (^._1) contacts
        enhs = nubSort $ map (^._2) contacts
    B.writeFile rowIdxFl $ B.unlines genes
    B.writeFile colIdxFl $ B.unlines enhs
    let rowIdx = M.fromList $ zip genes [0..]
        colIdx = M.fromList $ zip enhs [0..]
        mat = Dyn.fromTriplet (M.size rowIdx, M.size colIdx) $ U.fromList $ flip map contacts $ \(x,y,z) ->
            ( M.findWithDefault undefined x rowIdx
            , M.findWithDefault undefined y colIdx
            , z )
    cre <- DF.map (\x -> log $ (+1) $ if x < 1 then 0 else x) . (`DF.rsub` map (T.pack . B.unpack) enhs) <$> DF.readTable creFl
    forConcurrently_ (DF.colNames cre) $ \cell -> do
        let output = dir <> "/" <> T.unpack cell <> ".mtx"
        Dyn.withDyn mat $ \a@(S.SparseMatrix _ _ _) -> runResourceT $ runConduit $
            toMM (abcScore a $ D.fromColumns [V.convert $ cre `DF.cindex` cell]) .| sinkFile output
    return ()
  where
    f [x,y,z] = (x,y,readDouble z) 
    rowIdxFl = dir <> "/genes.tsv"
    colIdxFl = dir <> "/features.tsv"

abcScore :: (SingI r, SingI c)
         => SparseMatrix r c Double   -- ^ Promoter x enhancer contact matrix
         -> Matrix c 1 Double         -- ^ Enhancer activity
         -> SparseMatrix r c Double
abcScore contact act = S.diag s @@ contact @@ S.diag act
  where
    s = G.map recip $ contact @@ act

abcShareCount :: [FilePath] -> IO (M.Map (B.ByteString, B.ByteString) [String])
abcShareCount fls = do
    m <- runResourceT $ runConduit $ mapM_ readInter fls .| foldlC f M.empty
    return $ fmap nubSort m
  where
    readInter fl = sourceFile fl .|
        linesUnboundedAsciiC .| (dropC 1 >> mapC B.words) .|
        mapC (\x -> ((x!!0, x!!1), fl))
    f m (i, x) = M.insertWith (<>) i [x] m

abcCRECount :: [FilePath] -> IO [Int32]
abcCRECount fls = do
    fmap concat $ forM fls $ \fl -> do
        m <- runResourceT $ runConduit $ readInter fl .| foldlC f M.empty
        return $ M.elems m
  where
    readInter fl = sourceFile fl .|
        linesUnboundedAsciiC .| (dropC 1 >> mapC B.words) .| mapC (\x -> x!!3)
    f m x = M.insertWith (+) x 1 m
 
abcGeneCount :: [FilePath] -> IO [Int32]
abcGeneCount fls = do
    fmap concat $ forM fls $ \fl -> do
        m <- runResourceT $ runConduit $ readInter fl .| foldlC f M.empty
        return $ map (fromIntegral . length . nubSort) $ M.elems m
  where
    readInter fl = sourceFile fl .|
        linesUnboundedAsciiC .| (dropC 1 >> mapC B.words) .|
        mapC (\x -> (x!!0, [head $ B.split ':' $ x!!3]))
    f m (a,b) = M.insertWith (<>) a b m

abcBuilder = do
    uNode "CRE_Interaction_Prep" [| \z -> return $ zip pcHiC $ repeat z |]
    nodePar "CRE_Interaction" [| \((nm,po,pp), proFl) -> do
        dir <- (<> "/Capture/") <$> lookupConfig "output_dir"
        chain <- lookupConfig "hg19_to_hg38"
        creFl <- lookupConfig "peak"
        let output = dir <> nm <> "_cre.tsv"
        liftIO $ do
            shelly $ mkdir_p dir
            let rename x = name .~ Just (showBed x) $ x
                showBed x = B.pack $ B.unpack (x^.chrom) <> ":" <> show (x^.chromStart) <> "-" <> show (x^.chromEnd)
            pro <- readBed proFl
            cre <- fmap (map rename) $ runResourceT $ runConduit $
                streamBedGzip creFl .| sinkList :: IO [NarrowPeak]
            interPO <- (filter ((>=1) . _inter_pvalue) <$> readInteraction po) >>= liftInteraction chain
            interPP <- (filter ((>=1) . _inter_pvalue) <$> readInteraction pp) >>= liftInteraction chain
            let inter = interPP <> interPO
            B.writeFile output $ B.unlines $ nubSort $
                flip map (assignInteractons pro cre inter) $ \((a,b),v) -> B.intercalate "\t"
                    [fromJust $ a^.name, fromJust $ b^.name, B.pack $ show v]
            return output
      |] $ return ()
    node "CRE_Average_Interaction" [| \inputs -> do
        dir <- (<> "/Capture/") <$> lookupConfig "output_dir"
        let output = dir <> "average.tsv"
        liftIO $ do
            let f [a,b,c] = ((a,b), [readDouble c])
                average xs = foldl1' (+) xs / fromIntegral (length xs)
            res <- fmap (M.toList . fmap average . M.unionsWith (++)) $ forM inputs $ \input ->
                M.fromListWith max . map (f . B.split '\t') . B.lines <$>
                B.readFile input
            B.writeFile output $ B.unlines $ flip map res $ \((a,b), v) ->
                B.intercalate "\t" [a,b,B.pack $ show v]
            return output
      |] $ return ()
    ["Get_Promoters"] ~> "CRE_Interaction_Prep"

    node "ABC" [| \matrix -> do
        dir <- (<> "/ABC/") <$> lookupConfig "output_dir"
        acc <- lookupConfig "accessibility"
        liftIO $ do
            shelly $ mkdir_p $ fromText $ T.pack dir
            abcScore' dir matrix acc
        return dir
        |] $ return ()
    path ["CRE_Interaction_Prep", "CRE_Interaction", "CRE_Average_Interaction", "ABC"]

    node "ABC_Hist" [| \dir -> liftIO $ do
        fls <- fmap (filter (".mtx" `T.isSuffixOf`)) $ shelly $ lsT $ fromText $ T.pack dir
        let source fl = sourceFile (T.unpack fl) .| linesUnboundedAsciiC .|
                (dropC 3 >> mapC B.words) .| chunksOf 10 .| mapC head .| mapC (\[i,j,v] -> readDouble v)

        val <- runResourceT $ runConduit $ mapM_ source fls .| sinkList
        R.runRegion $ do
            myTheme
            [r| library("ggplot2")
                df <- data.frame(val=val_hs)
                fig <- ggplot(df, aes(x=val)) +
                    geom_density() +
                    xlim(0,0.05) +
                    #geom_histogram(color="#e9ecef", alpha=0.6, position = 'identity') +
                    myTheme()
                ggsave("ABC.pdf", fig, width=52, height=40, unit="mm", useDingbats=F)
            |]
            return ()
        |] $ return ()
    path ["ABC", "ABC_Hist"]

    node "Output_ABC" [| \(dir, proFl) -> do
        outdir <- (<> "/ABC_plain/") <$> lookupConfig "output_dir"
        geneFl <- lookupConfig "gene_expr"
        liftIO $ do
            shelly $ mkdir_p $ fromText $ T.pack outdir
            col <- V.fromList . B.lines <$> B.readFile (dir <> "/features.tsv")
            let f x = ( fromJust $ x^.name
                      , (x^.chrom) <> ":" <> (B.pack $ show $ x^.chromStart) <> "-" <> (B.pack $ show $ x^.chromEnd) )
            promoters <- M.fromListWith undefined . map f <$> (readBed proFl :: IO [BED])
            row <- V.fromList . map (\x -> (x, M.findWithDefault undefined x promoters)) . B.lines <$>
                B.readFile (dir <> "/genes.tsv")

            fls <- fmap (filter (".mtx" `T.isSuffixOf`)) $ shelly $ lsT $ fromText $ T.pack dir
            let header = "cCRE\tPromoter\tABC_score\tGene Name\tDistance"
                toBed x = let [chr, x'] = B.split ':' x
                              [s, e] = B.split '-' x'
                          in BED3 chr (readInt s) $ readInt e
                dist a b = let a' = toBed a
                               b' = toBed b
                               mid x = (x^.chromStart + x^.chromEnd) `div` 2
                            in if a'^.chrom /= b'^.chrom
                                then -1
                                else abs $ mid a' - mid b'
                show' [i,j,v] = 
                    let a = col V.! (readInt j - 1)
                        (d, b) = row V.! (readInt i - 1)
                    in B.intercalate "\t" [a, b, v, d, B.pack $ show $ dist a b]
            geneExpr <- DF.readTable geneFl
            let allGenes = Set.fromList $ DF.rowNames geneExpr
            forConcurrently fls $ \fl -> do
                let output = outdir <> T.unpack clName <> ".tsv"
                    clName = fst $ T.breakOn ".mtx" $ snd $ T.breakOnEnd "/" fl
                    filterFn [i,_,v] =
                        let gene = T.pack $ B.unpack $ fst $ B.break (==':') $ fst $ row V.! (readInt i - 1)
                            expr | gene `Set.member` allGenes = geneExpr DF.! (gene, clName)
                                 | otherwise = 0
                        in readDouble v > 0.015 && expr > 1
                runResourceT $ runConduit $ sourceFile (T.unpack fl) .|
                    linesUnboundedAsciiC .| (dropC 3 >> mapC B.words) .|
                    filterC filterFn .|
                    (yield header >> mapC show') .|
                    unlinesAsciiC .| sinkFile output
                return output
        |] $ return ()
    ["ABC", "Get_Promoters"] ~> "Output_ABC"

    node "ABC_Stat" [| \input -> do
        dir <- (<> "/ABC/") <$> lookupConfig "output_dir"
        let output1 = dir <> "dist.pdf"
            output2 = dir <> "share.pdf"
            output3 = dir <> "CRE.pdf"
        liftIO $ do
            let getDist x = map (readInt . last . B.split '\t') . tail . B.lines <$> B.readFile x
            dist <- map (fromIntegral . min 2000000) . concat <$> mapM getDist input :: IO [Int32]
            R.runRegion $ do
                myTheme
                [r| library("ggplot2")
                    df <- data.frame(val=dist_hs)
                    m <- median(dist_hs)
                    fig <- ggplot(df, aes(x=val)) +
                        geom_histogram() +
                        geom_vline(xintercept=m) +
                        ggtitle(m) +
                        myTheme()
                    ggsave(output1_hs, fig, width=60, height=55, unit="mm", useDingbats=F)
                |]
                return ()

            shared <- abcShareCount input
            let count = map (fromIntegral . length) $ M.elems shared :: [Int32]
            printf "Unique interaction: %d\n" $ M.size shared
            printf "Median unique interaction: %f\n" $ median medianUnbiased $
                V.fromList $ M.elems $ M.fromListWith (+) $
                concatMap (\xs -> zip xs $ repeat 1) $ M.elems shared
            printf "Median specific interaction: %f\n" $ median medianUnbiased $
                V.fromList $ M.elems $
                M.fromListWith (+) $ map (\[x] -> (x,1)) $ M.elems $ M.filter ((==1) . length) shared
            R.runRegion $ do
                myTheme
                [r| library("ggplot2")
                    df <- data.frame(val=count_hs)
                    fig <- ggplot(df, aes(x=val)) +
                        geom_histogram() +
                        myTheme()
                    ggsave(output2_hs, fig, width=60, height=55, unit="mm", useDingbats=F)
                |]
                return ()

            countCRE <- abcCRECount input
            countGene <- abcGeneCount input
            let (label, counts) = unzip $ zip (repeat ("CRE" :: String)) countCRE ++
                    zip (repeat "gene") countCRE
            R.runRegion $ do
                myTheme
                [r| library("ggplot2")
                    df <- data.frame(label=label_hs, val=counts_hs)
                    fig <- ggplot(df, aes(x=label, y=val)) +
                        geom_violin(trim=FALSE) +
                        myTheme()
                    ggsave(output3_hs, fig, width=60, height=55, unit="mm", useDingbats=F)
                |]
                return ()
        |] $ return ()
    path ["Output_ABC", "ABC_Stat"]

    node "ABC_GO_Prep" [| \input -> do
        dir <- lookupConfig "restricted_peaks"
        liftIO $ do
            let getName = fst . T.breakOnEnd "." . snd . T.breakOnEnd "/"
            fls <- fmap (map (\x -> (getName x, T.unpack x))) $ shelly $ lsT $
                fromText $ T.pack dir
            return $ flip map input $ \x ->
                let nm = getName $ T.pack x
                in (nm, x, fromJust $ lookup nm fls)
        |] $ return ()
    nodePar "ABC_GO" [| \(nm, interFl, creFl) -> do
        dir <- (<> "/GO/") <$> lookupConfig "output_dir"
        gencode <- lookupConfig "gencode"
        let output = dir <> T.unpack nm <> "tsv"
        liftIO $ do
            shelly $ mkdir_p $ fromText $ T.pack dir
            let showBed (BED3 chr s e) = chr <> ":" <> B.pack (show s) <> "-" <> B.pack (show e)
            peaks <- Set.fromList . map showBed <$> readBed creFl
            gs <- fmap (M.fromListWith (+)) $ runResourceT $ runConduit $ sourceFile interFl .|
                linesUnboundedAsciiC .| (dropC 1 >> mapC B.words) .|
                filterC ((`Set.member` peaks) . head) .|
                mapC (\x -> (x!!3, readDouble $ x!!2)) .| sinkList
            let gs' = M.fromListWith max $ map (\(a,b) -> (head $ B.split ':' a, b)) $ M.toList gs
            B.writeFile output $ B.unlines $ map (\(a,b) -> a <> "\t" <> B.pack (show b)) $ sortBy (flip (comparing snd)) $ M.toList gs'
            --genes <- map ((\x -> (B.unpack x, if x `Set.member` gs then 1 else 0)) . original . geneName) <$> readGenes gencode
            --goEnrich output genes
            return (T.init nm, output)
        |] $ return ()
    path ["Output_ABC", "ABC_GO_Prep", "ABC_GO"]