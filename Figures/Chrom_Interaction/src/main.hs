{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE PartialTypeSignatures #-}
{-# LANGUAGE TemplateHaskell   #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE QuasiQuotes       #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE GADTs #-}
import HEA

import Bio.Data.Bed
import Bio.Seq.IO
import Bio.Utils.Misc (readInt)
import Control.Concurrent.Async (forConcurrently_, forConcurrently)
import qualified Data.ByteString.Char8 as B
import           Data.CaseInsensitive  (original)
import Bio.RealWorld.GENCODE
import Data.ByteString.Lex.Integral (packDecimal)
import qualified Data.Vector as V
import qualified Data.Vector.Unboxed as U
import qualified Data.Text as T
import Data.List.Ordered (nubSort)
import System.Random.MWC (create)
import Shelly hiding (FilePath, path)
import qualified Taiji.Utils.DataFrame as DF
import Control.DeepSeq (force)

import qualified Language.R                        as R
import           Language.R.QQ
import Language.R.HExp

import Taiji.Prelude
import Taiji.Utils
import qualified Data.Map.Strict as M
import qualified Data.Set as Set

import Data.Matrix.Static.IO (fromMM')
import qualified Data.Matrix.Dynamic as Dyn
import qualified Data.Matrix.Static.Dense as D
import qualified Data.Matrix.Static.Sparse as S

import Cicero
import ABC

data CRE = CRE
    { _cre_location :: BED3
    , _cre_id :: B.ByteString
    , _cre_cell_type :: M.Map B.ByteString (Double, [(B.ByteString, Double)])
    }

showCRE :: CRE -> B.ByteString
showCRE CRE{..} = B.intercalate "\t" $
    [toLine _cre_location, _cre_id] ++ map f (M.toList _cre_cell_type)
  where
    f (nm, (acc, inter)) = nm <> "=" <> B.pack (show acc) <> genes
      where
        genes = case map (\(a,b) -> a <> "," <> B.pack (show b)) inter of
            [] -> ""
            xs -> "|" <> B.intercalate ";" xs

readCRE' :: [FilePath]   -- ^ Directory containing Cicero scores
        -> FilePath   -- ^ CRE file
        -> FilePath   -- ^ CRE accessibility
        -> FilePath   -- ^ gene accessibility
        -> IO [CRE]
readCRE' fls creFl accFl geneFl = do
    cre <- readBed creFl :: IO [BED]
    acc <- DF.readTable accFl
    geneExpr <- DF.readTable geneFl
    mats <- readCicero fls
    let lookupGene cell region input = case M.lookup region m of
            Nothing -> []
            Just j -> j
          where
            m = M.findWithDefault (error $ show cell) cell input
    return $ flip map cre $ \x -> 
        let cells = map (head . B.split ':') $ B.split '+' $ fromJust $ x^.name
            nm = (x^.chrom) <> ":" <> B.pack (show $ x^.chromStart) <>
                "-" <> B.pack (show $ x^.chromEnd)
        in CRE (convert x) nm $ M.fromList $ flip map cells $ \c -> 
            let inter = lookupGene c nm mats
                v = acc DF.! (T.pack $ B.unpack nm, T.pack $ B.unpack c)
            in (c, (v, inter))
  where
    readCicero fls = do
        fmap M.fromList $ forConcurrently fls $ \fl -> do
            let nm = fst $ T.breakOn ".tsv" $ snd $ T.breakOnEnd "/" $ T.pack fl
                f xs = (head xs, [(xs!!3, readDouble $ xs!!2)])
            m <- fmap nubSort . M.fromListWith (<>) . map (f . B.split '\t') . tail . B.lines <$> B.readFile fl
            return $ force (B.pack $ T.unpack nm, m)

build "wf" [t| SciFlow ENV |] $ do
    ciceroBuilder
    abcBuilder
    node "Get_Promoters" [| \() -> do
        dir <- lookupConfig "output_dir"
        annoFl <- lookupConfig "gencode"
        let output = dir <> "/promoters.bed"
        liftIO $ do
            genes <- readGenes annoFl
            writeBed output $ flip concatMap genes $ \Gene{..} -> flip map geneTranscripts $ \Transcript{..} ->
                let region = case () of
                        _ | transStrand -> BED geneChrom (max 0 $ transLeft - 1000)
                                (transLeft + 1000) Nothing Nothing (Just transStrand)
                          | otherwise -> BED geneChrom (max 0 $ transRight - 1000)
                                (transRight + 1000) Nothing Nothing (Just transStrand)
                    nm = Just $ original geneName <> ":" <> transId
                    --nm = Just $ original geneName <> ":" <> geneChrom <> ":" <>
                    --        B.pack (show $ region^.chromStart) <> "-" <>
                    --        B.pack (show $ region^.chromEnd)
                in name .~ nm $ region
            return output
      |] $ return ()

    node "Save_CRE" [| \matDir -> do
        dir <- lookupConfig "output_dir"
        acc <- lookupConfig "accessibility"
        geneFl <- lookupConfig "gene_expr"
        cre_annotated <- lookupConfig "cre_annotated"
        let output = dir <> "/cre.tsv"
        liftIO $ do
            cre <- readCRE' matDir cre_annotated acc geneFl
            B.writeFile output $ B.unlines $ map showCRE cre
            return output
        |] $ return ()
    ["Output_ABC"] ~> "Save_CRE"

    node "Save_CRE_Cicero" [| \matDir -> do
        dir <- lookupConfig "output_dir"
        acc <- lookupConfig "accessibility"
        geneFl <- lookupConfig "gene_expr"
        cre_annotated <- lookupConfig "cre_annotated"
        let output = dir <> "/cre_cicero.tsv"
        liftIO $ do
            cre <- readCRE' matDir cre_annotated acc geneFl
            B.writeFile output $ B.unlines $ map showCRE cre
            return output
        |] $ return ()
    ["Output_Cicero_PE"] ~> "Save_CRE_Cicero"

    node "Save_CRE_Cicero_Merged" [| \matDir -> do
        dir <- lookupConfig "output_dir"
        acc <- lookupConfig "accessibility"
        geneFl <- lookupConfig "gene_expr"
        cre_annotated <- lookupConfig "cre_annotated"
        let output = dir <> "/cre_cicero_merged.tsv"
        liftIO $ do
            cre <- readCRE' matDir cre_annotated acc geneFl
            B.writeFile output $ B.unlines $ map showCRE cre
            return output
        |] $ return ()
    ["Output_Cicero_Merged_PE"] ~> "Save_CRE_Cicero_Merged"


main :: IO ()
main = mainFun wf