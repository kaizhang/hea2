{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell   #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE QuasiQuotes       #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE GADTs #-}

module Cicero (ciceroBuilder) where

import HEA

import Bio.Data.Bed
import Bio.Seq.IO
import Data.Int (Int32)
import Bio.Utils.Misc (readInt)
import qualified Data.ByteString.Char8 as B
import Data.ByteString.Lex.Integral (packDecimal)
import qualified Data.Vector as V
import qualified Data.Vector.Unboxed.Mutable as UM
import qualified Data.Vector.Unboxed as U
import qualified Data.Text as T
import Data.List.Ordered (nubSort)
import Data.Char (toUpper)
import System.Random.MWC (create)
import Shelly hiding (FilePath, path)
import qualified Taiji.Utils.DataFrame as DF
import qualified Data.Set as Set

import qualified Language.R                        as R
import           Language.R.QQ
import Language.R.HExp

import Taiji.Prelude
import Taiji.Utils

cicero :: FilePath  -- ^ output
       -> FilePath  -- ^ genom_index
       -> FilePath  -- ^ Peak file
       -> FilePath  -- ^ matrix file
       -> IO (FilePath, FilePath)
cicero output genome peakFl matFl = do
    chrSizes <- withGenome genome $ return . getChrSizes
    peaks <- if isGzip peakFl
        then runResourceT $ runConduit $ streamBedGzip peakFl .| sinkList
        else runResourceT $ runConduit $ streamBed peakFl .| sinkList
    mat <- mkSpMatrix readInt $ matFl
    withTempDir Nothing $ \tmpdir -> do
        --let count = tmpdir <> "/count"
        let count = "count.txt"
            chr = tmpdir <> "/chr"
        B.writeFile chr $ B.unlines $
            map (\(a,b) -> a <> "\t" <> B.pack (show b)) chrSizes
        mkAtacCDS count peaks mat
        getConnection output chr count
  where
    isGzip x = ".gz" `T.isSuffixOf` T.pack x

-- | Create a sparse matrix that can be used as the input for "make_atac_cds"
-- in Cicero.
-- Specifically, the file is a tab-delimited text file with three columns.
-- The first column is the peak coordinates in the form
-- "chr10_100013372_100013596", the second column is the cell name, and the
-- third column is an integer that represents the number of reads from that
-- cell overlapping that peak. The file should not have a header line.
-- For example:
-- chr10_100002625_100002940	cell1	1
-- chr10_100006458_100007593	cell2	2
-- chr10_100006458_100007593	cell3	1
mkAtacCDS :: FilePath
          -> [BED3]         -- ^ Peak list
          -> SpMatrix Int   -- ^ Cell by Peak matrix
          -> IO ()
mkAtacCDS output peaks mat = runResourceT $ runConduit $
    streamRows mat .| concatMapC procRow .| unlinesAsciiC .| sinkFile output
  where
    procRow (cell, xs) = map f xs
      where
        f (i, c) = B.intercalate "\t" [colnames V.! i, cell, fromJust $ packDecimal c]
    colnames = V.fromList $ map f peaks
      where
        f x = B.intercalate "_"
            [x^.chrom, fromJust $ packDecimal $ x^.chromStart, fromJust $ packDecimal $ x^.chromEnd]

-- |
--        ddrtree_coord1	ddrtree_coord2
-- cell1    -0.7084047      -0.7232994
-- cell2    -4.4767964       0.8237284
-- cell3     1.4870098      -0.4723493
getConnection :: FilePath  -- ^ Output
              -> FilePath  -- ^ Chromosome size file
              -> FilePath  -- ^ Count matrix
              -> IO (FilePath, FilePath)
getConnection dir chr cds = R.runRegion $ do
    _ <- [r| library(cicero)
        input_cds <- make_atac_cds(cds_hs, binarize = TRUE)

        set.seed(2017)
        input_cds <- detect_genes(input_cds)
        input_cds <- estimate_size_factors(input_cds)
        input_cds <- preprocess_cds(input_cds, method = "LSI")
        input_cds <- reduce_dimension(input_cds, reduction_method = 'UMAP', 
                                      preprocess_method = "LSI")

        embeding <- reducedDims(input_cds)$UMAP
        #embeding <- read.table(embed_hs)
        cicero_cds <- make_cicero_cds(input_cds, reduced_coordinates = embeding, k = 50)
        conns <- run_cicero(cicero_cds, chr_hs)
        saveRDS(cicero_cds, file=output_cds_hs)
        saveRDS(conns, file=output_conn_hs)
        #write.table(conns, file=output_hs)
    |]
    return (output_cds, output_conn)
  where
    output_cds = dir <> "/cds.rds"
    output_conn = dir <> "/connection.rds"

outputConn :: FilePath -> FilePath -> IO ()
outputConn output input = R.runRegion $ do
    [r| library(cicero)
        conns <- readRDS(input_hs)
        idx <- conns[,3] > 0.001 & !is.na(conns[,3])
        conns <- conns[idx,]
        write.table(conns, file=output_hs, quote=F, row.names=F, sep = "\t")
    |]
    return ()

subsetPeak :: [BED3]     -- smaller set
           -> [BED3]     -- master set
           -> SpMatrix a  -- matrix
           -> ([BED3], SpMatrix a)
subsetPeak peaks master mat = (map snd kept, deleteCols (map fst removed) mat)
  where
    (kept, removed) = partition f $ zip [0..] master
    f (_, x) = tree `isIntersected` x
    tree = bedToTree const $ zip peaks $ repeat ()

ciceroBuilder = do
    node "Read_Input" [| \() -> do
        dir <- lookupConfig "matrix_dir"
        let f suffix x = (fst $ T.breakOn suffix $ snd $ T.breakOnEnd "/" x, x)
        mats <- liftIO $ fmap (filter (".mat.gz" `T.isSuffixOf`)) $
            shelly $ lsT $ fromText $ T.pack dir
        let mats' = map (f ".mat.gz") mats
        peaks <- liftIO $ fmap (filter (".narrowPeak.gz" `T.isSuffixOf`)) $
            shelly $ lsT $ fromText $ T.pack dir
        return $ flip map peaks $ \p ->
            let (a,b) = f ".narrowPeak.gz" p
            in (a, fromJust $ lookup a mats', b)
        |] $ return ()

    nodePar "Submatrix" [| \(nm, mat, peak) -> do
        dir <- lookupConfig "output_dir"
        peakFl <- lookupConfig "peak"
        let output1 = dir <> "/" <> T.unpack nm <> ".narrowPeak"
            output2 = dir <> "/" <> T.unpack nm <> ".mat.gz"
        liftIO $ do
            shelly $ mkdir_p $ fromText $ T.pack dir
            p <- runResourceT $ runConduit $ streamBedGzip (T.unpack peak) .| sinkList
            m <- mkSpMatrix id $ T.unpack mat
            master <- runResourceT $ runConduit $ streamBedGzip peakFl .| sinkList
            let (p', m') = subsetPeak p master m
            writeBed output1 p'
            saveMatrix output2 id m'
            return (nm, output1, output2)
        |] $ return ()

    nodePar "Cicero" [| \(nm, peakFl, matFl) -> do
        genome <- lookupConfig "genome_index"
        dir <- lookupConfig "output_dir"
        let output = dir <> "/" <> T.unpack nm
        liftIO $ do
            shelly $ mkdir_p $ fromText $ T.pack output
            r <- cicero output genome peakFl matFl
            return (nm, r)
        |] $ return ()
    nodePar "Output_Cicero" [| \(nm, (_, conn)) -> do
        dir <- lookupConfig "output_dir"
        let output = dir <> "/" <> T.unpack nm <> "/conns.txt"
        liftIO $ outputConn output conn
        return output
        |] $ return ()
    node "Cicero_Stat" [| \inputs -> do
        dir <- (<> "/Cicero/") <$> lookupConfig "output_dir"
        let output = dir <> "cicero_stat.pdf"
            breaks = 1000
        liftIO $ do
            shelly $ mkdir_p dir
            vec <- UM.replicate breaks (0 :: Int32)
            let source x = sourceFile x .| linesUnboundedAsciiC .| (dropC 1 >> mapC f)
                f x = readDouble $ last $ B.split '\t' x
                g x = UM.modify vec (+1) $ truncate $ x / 1.0001 * fromIntegral breaks
            runResourceT $ runConduit $ mapM_ source inputs .| mapM_C g
            ys <- U.toList <$> U.unsafeFreeze vec
            let xs = take breaks [0, 1/1000 .. 1] :: [Double]
            R.runRegion $ do
                myTheme
                [r| library("ggplot2")
                    df <- data.frame(x=xs_hs, y=ys_hs)
                    fig <- ggplot(df, aes(x, y)) + geom_col() +
                        myTheme() + myFill() +
                        labs(x = "", y = "")
                    ggsave(output_hs, fig, width=55, height=45, unit="mm", useDingbats=F)
                |]
                return ()
        |] $ return ()
    path ["Read_Input", "Submatrix", "Cicero", "Output_Cicero", "Cicero_Stat"]

    node "Subsample_Merge" [| \() -> do
        peakFl <- lookupConfig "peak"
        peakDir <- lookupConfig "cluster_dir"
        dir <- lookupConfig "output_dir"
        let output = dir <> "/merged.mat.gz"
        liftIO $ do
            files <- fmap (filter (".mat.gz" `T.isSuffixOf`)) $ shelly $
                lsT $ fromText $ T.pack peakDir
            gen <- create
            mat' <- fmap concatMatrix <$> forM files $ \x -> do
                m <- mkSpMatrix id $ T.unpack x
                sampleRows 500 m gen
            saveMatrix output id mat'
            return ("merged", peakFl, output)
        |] $ return ()
    node "Merged_Cicero" [| \(nm, peakFl, matFl) -> do
        genome <- lookupConfig "genome_index"
        dir <- lookupConfig "output_dir"
        let output = dir <> "/" <> T.unpack nm
        liftIO $ do
            shelly $ mkdir_p $ fromText $ T.pack output
            r <- cicero output genome peakFl matFl
            return (nm, r)
        |] $ return ()
    node "Merged_Output_Cicero" [| \(nm, (_, conn)) -> do
        dir <- lookupConfig "output_dir"
        let output = dir <> "/" <> T.unpack nm <> "/conns.txt"
        liftIO $ outputConn output conn
        return output
        |] $ return ()
    path ["Subsample_Merge", "Merged_Cicero", "Merged_Output_Cicero"]


    node "Output_Cicero_PE" [| \(fls, proFl) -> do
        outdir <- (<> "/Cicero_plain/") <$> lookupConfig "output_dir"
        geneFl <- lookupConfig "gene_expr"
        liftIO $ do
            shelly $ mkdir_p outdir
            geneExpr <- DF.readTable geneFl
            promoters <- bedToTree (<>) . map (\x -> (x, [x])) <$> (readBed proFl :: IO [BED])
            let header = "cCRE\tinteracting locus\tCicero_score\tGene Name\tDistance"
                toBed x = let [chr, x'] = B.split ':' x
                              [s, e] = B.split '-' x'
                          in BED3 chr (readInt s) $ readInt e
                dist a b' = let a' = toBed a
                                mid x = (x^.chromStart + x^.chromEnd) `div` 2
                            in if a'^.chrom /= b'^.chrom
                                then -1
                                else abs $ mid a' - mid b'
                change x = let [a,b,c] = B.split '_' x in a <> ":" <> b <> "-" <> c
            let allGenes = Set.fromList $ DF.rowNames geneExpr
            forM fls $ \fl -> do
                let output = outdir <> T.unpack clName <> ".tsv"
                    clName = reverse (filter (not . T.null) $ T.splitOn "/" $ T.pack fl) !! 1
                    f [cre1,cre2,v] =
                        let gene1 = concatMap snd $ queryIntersect (toBed cre1) promoters
                            gene2 = concatMap snd $ queryIntersect (toBed cre2) promoters
                            mkLink a b sc genes = flip map genes' $ \g -> 
                                [a, b, sc, fromJust $ g^.name, fromJust $ packDecimal $ dist a g]
                              where
                                genes' = filter (\x -> geneExpr DF.!
                                    (T.map toUpper $ fst $ T.breakOn ":" $ T.pack $ B.unpack $ fromJust $ x^.name, clName) > 1) genes
                        in nubSort $ mkLink cre1 cre1 "1" gene1 <> mkLink cre2 cre2 "1" gene2 <> 
                            mkLink cre1 cre2 v gene2 <> mkLink cre2 cre1 v gene1
                runResourceT $ runConduit $ sourceFile fl .|
                    linesUnboundedAsciiC .| (dropC 1 >> mapC ((\[a,b,c] -> [change a, change b, c]) . B.words)) .|
                    filterC ((>0.2) . readDouble . last) .| concatMapC f .|
                    (yield header >> mapC (B.intercalate "\t")) .|
                    unlinesAsciiC .| sinkFile output
                return output
        |] $ return ()
    ["Output_Cicero", "Get_Promoters"] ~> "Output_Cicero_PE"

    node "Output_Cicero_Merged_PE" [| \(input, proFl) -> do
        outdir <- (<> "/Cicero_merged/") <$> lookupConfig "output_dir"
        peakDir <- lookupConfig "cluster_dir"
        geneFl <- lookupConfig "gene_expr"
        liftIO $ do
            shelly $ mkdir_p outdir
            files <- fmap (filter (".narrowPeak.gz" `T.isSuffixOf`)) $ shelly $
                lsT peakDir
            geneExpr <- DF.readTable geneFl
            promoters <- bedToTree (<>) . map (\x -> (x, [x])) <$> (readBed proFl :: IO [BED])
            let header = "cCRE\tinteracting locus\tCicero_score\tGene Name\tDistance"
                toBed x = let [chr, x'] = B.split ':' x
                              [s, e] = B.split '-' x'
                          in BED3 chr (readInt s) $ readInt e
                dist a b' = let a' = toBed a
                                mid x = (x^.chromStart + x^.chromEnd) `div` 2
                            in if a'^.chrom /= b'^.chrom
                                then -1
                                else abs $ mid a' - mid b'
                change x = let [a,b,c] = B.split '_' x in a <> ":" <> b <> "-" <> c

            links <- runResourceT $ runConduit $ sourceFile input .|
                linesUnboundedAsciiC .| (dropC 1 >> mapC ((\[a,b,c] -> [change a, change b, c]) . B.words)) .|
                filterC ((>0.2) . readDouble . last) .| sinkList

            forM files $ \fl -> do
                let output = outdir <> T.unpack clName <> ".tsv"
                    clName = fst $ T.breakOn ".narrowPeak.gz" $ snd $ T.breakOnEnd "/" fl
                    f [cre1,cre2,v] =
                        let gene1 = concatMap snd $ queryIntersect (toBed cre1) promoters
                            gene2 = concatMap snd $ queryIntersect (toBed cre2) promoters
                            mkLink a b sc genes = flip map genes' $ \g -> 
                                [a, b, sc, fromJust $ g^.name, fromJust $ packDecimal $ dist a g]
                              where
                                genes' = filter (\x -> geneExpr DF.!
                                    (T.toUpper $ fst $ T.breakOn ":" $ T.pack $ B.unpack $ fromJust $ x^.name, clName) > 1) genes
                        in nubSort $ mkLink cre1 cre1 "1" gene1 <> mkLink cre2 cre2 "1" gene2 <> 
                            mkLink cre1 cre2 v gene2 <> mkLink cre2 cre1 v gene1
                peaks <- runResourceT $ runConduit $ streamBedGzip (T.unpack fl) .| sinkList :: IO [BED]
                let tree = bedToTree const $ zip peaks $ repeat ()
                runResourceT $ runConduit $ yieldMany links .|
                    filterC (all (isIntersected tree . toBed) . take 2) .|
                    concatMapC f .|
                    (yield header >> mapC (B.intercalate "\t")) .|
                    unlinesAsciiC .| sinkFile output
                return output
        |] $ return ()
    ["Merged_Output_Cicero", "Get_Promoters"] ~> "Output_Cicero_Merged_PE"
 