{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell   #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE QuasiQuotes       #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE GADTs #-}
import HEA

import Statistics.Quantile
import qualified Taiji.Utils.DataFrame as DF
import Bio.RealWorld.GENCODE
import Bio.RealWorld.ENCODE
import Bio.RealWorld.ID
import Control.Monad.State.Strict
import Control.Arrow
import qualified Data.Vector as V
import AI.Clustering.Hierarchical hiding (normalize)
import Data.Either
import Control.Parallel.Strategies (parMap, rdeepseq)
import qualified Data.Text as T
import qualified Data.Text.IO as T
import Bio.Data.Bed.Types
import Shelly hiding (FilePath, path)
import           Data.Conduit.Zlib           (gzip, ungzip, multiple)
import Data.Int
import Bio.Data.Bed
import Control.Monad.ST (runST)
import qualified Data.Matrix as Mat
import qualified Data.ByteString.Char8 as B
import qualified Data.HashMap.Strict as M
import Data.List.Ordered (nubSort)
import Bio.Utils.Functions
import Taiji.Utils.Plot
import Statistics.Sample (mean, meanVarianceUnb)
import Data.Binary
import System.Random.MWC.Distributions (uniformShuffle)
import System.Random.MWC (create)
import           Network.HTTP.Conduit hiding (path)
import Bio.Pipeline.CallPeaks

import Taiji.Prelude
import qualified Language.R                        as R
import           Language.R.QQ
import Language.R.HExp

import FilterPeak (peakBuilder)
import PeakCall

readDataSheet :: FilePath -> IO [B.ByteString]
readDataSheet fl = map (f . B.split '\t') . tail . B.lines <$> B.readFile fl
  where
    f xs = xs!!6

computePRs :: [NarrowPeak] -> [BED3] -> [(Double, (Double, Double))]
computePRs test gold = flip map fdrs $ \fdr -> 
    let test' = filter ((>=cutoff) . fromJust . (^.npQvalue)) test
        cutoff = negate $ logBase 10 fdr
    in (fdr, pr test' gold)
  where
    fdrs = [0.001, 0.005, 0.01, 0.02, 0.03, 0.04, 0.05, 0.06, 0.07, 0.08, 0.09, 0.1]

pr :: [NarrowPeak] -> [BED3] -> (Double, Double)
pr test gold =
    let tp = runIdentity $ runConduit $ yieldMany test .| intersectBed gold .| lengthC
        fn = length gold - (runIdentity $ runConduit $ yieldMany gold .| intersectBed test .| lengthC)
    in (fromIntegral tp / fromIntegral (length test), 1 - fromIntegral fn / fromIntegral (length gold))

lookupMeta :: B.ByteString -> IO ((T.Text, Int), String)
lookupMeta x = queryById (EncodeAcc x) >>= \case
    Left e -> error e
    Right r -> do
        let [i] = as $ r |@ "biological_replicates"
            e = snd $ T.breakOnEnd "/" $ T.init $ as $ r |@ "dataset"
            link = as $ r |@ "href"
        return ((e, i), link)

        {-
        let life = as $ r |@ "replicates" |! 0 |@ "library" |@ "biosample" |@ "life_stage"
            cl = as $ r |@ "biosample_ontology" |@ "classification"
            organ = as $ r |@ "biosample_ontology" |@ "organ_slims"
            cells = as $ r |@ "biosample_ontology" |@ "cell_slims"
            -}
        --return (x, (life, cl, organ, cells))

-- | Download data from ENCODE portal to a given directory.
download :: String
         -> FilePath  -- ^ Output dir
         -> IO FilePath
download acc dir = do
     request <- parseRequest url
     manager <- newManager tlsManagerSettings
     runResourceT $ do
         response <- http request manager
         let filename = T.unpack $ snd $ T.breakOnEnd "filename=" $ T.pack $
                B.unpack $ fromJust $ lookup "Content-Disposition" $
                responseHeaders response
         runConduit $ responseBody response .| sinkFileBS (dir ++ "/" ++ filename)
         return $ dir ++ "/" ++ filename
  where
    url = "https://www.encodeproject.org/" <> acc

subsample :: Int -> FilePath -> FilePath -> IO ()
subsample nReads input output = shelly $ escaping False $
    run_ "zcat" [T.pack $ show input, "|", "shuf", "-n", T.pack $ show nReads, ">", T.pack output]

callpeaks :: Double -> FilePath -> FilePath -> IO ()
callpeaks fdr input output = do
    let fl = location .~ input $ emptyFile :: File '[] 'Bed
    _ <- callPeaks output fl Nothing opt
    return ()
  where
    opt = mode .~ NoModel (-100) 200 $
        cutoff .~ QValue fdr $
        callSummits .~ False $
        gSize .~ Just "hs" $ def

--------------------------------------------------------------------------------
-- Workflow
--------------------------------------------------------------------------------

build "wf" [t| SciFlow ENV |] $ do
    peakBuilder
    peakCallBuilder

    node "Read_Data" [| \() -> do
        input <- lookupConfig "datasheet"
        dhs <- lookupConfig "dhs"
        dir <- (<> "/download/") <$> lookupConfig "output_dir"
        liftIO $ do
            let f xs = ((snd $ T.breakOnEnd "_" $ xs!!0, read $ T.unpack $ xs!!1), T.unpack $ xs!!2)
            files <- M.fromList . map (f . T.splitOn "\t") . T.lines <$> T.readFile dhs
            shelly $ mkdir_p dir
            ids <- readDataSheet input
            dat <- fmap catMaybes $ forM ids $ \i -> do
                (k, link) <- lookupMeta i
                let base = "/projects/ren-transposon/home/kai/ENCODE_DNase/"
                return $ case M.lookup k files of
                    Nothing -> Nothing
                    Just r -> Just (k, link, base <> r)
            forM dat $ \(k, link, r) -> do
                fl <- download link dir
                return (k, fl, r)
        |] $ return ()
    nodePar "Get_Depth" [| \(k, fl, bed) -> liftIO $ do
        n <- runResourceT $ runConduit $ sourceFile bed .|
            multiple ungzip .| linesUnboundedAsciiC .| lengthC :: IO Double
        return (k, fl, bed, n)
        |] $ return ()

    uNode "Filter_Data" [| \input -> do
        let input' = take 10 $ filter (\x -> x^._4 > 100000000 && x^._4 < 110000000) input
            fdrs = [0.001, 0.005, 0.01, 0.025, 0.05, 0.1]
            sampleSize = [2000000, 5000000, 10000000, 25000000, 50000000, 100000000]
        return $ flip concatMap sampleSize $ \s ->
            flip concatMap fdrs $ \fdr -> flip map input' $ \(k, gold, test, _) ->
                (s, fdr, k, gold, test)
        |]

    nodePar "Call_Peaks" [| \(n, fdr, (exp, rep), gold, test) -> do
        dir <- (<> "/Peaks/") <$> lookupConfig "output_dir"
        liftIO $ do
            shelly $ mkdir_p dir
            withTempDir (Just "./") $ \tmp -> do
                let out = tmp <> "/" <> show n <> ".bed.gz"
                    peakFl = printf "%s/%s_rep%d_%d_%f.bed.gz" dir (T.unpack exp)
                        rep n fdr
                subsample n test out
                _ <- callpeaks fdr out peakFl
                return ((exp, rep), n, fdr, gold, peakFl)
        |] $ return ()
    uNode "Compute_PR_Prep" [| \input -> return $ groupBy ((==) `on` (^._1)) $ sortBy (comparing (^._1)) input |]
    nodePar "Compute_PR" [| \input -> do
        dir <- (<> "/Benchmark/") <$> lookupConfig "output_dir"
        liftIO $ shelly $ mkdir_p dir
        let output = printf "%s/%s_rep%d.pdf" dir (T.unpack exp) rep :: String 
            (exp, rep) = head input ^. _1
        (depth, fdr, precision, recall) <- fmap unzip4 $ liftIO $ forM input $ \(_, s, fdr, gold, peakFl) -> do
            gold' <- runResourceT $ runConduit $ streamBedGzip gold .| sinkList 
            test' <- runResourceT $ runConduit $ streamBedGzip peakFl .| sinkList 
            let (a,b) = pr test' gold'
            return (fromIntegral s :: Int32, fdr, a, b)
        liftIO $ R.runRegion $ do
            myTheme
            [r| library("ggplot2")
                df <- data.frame(depth=depth_hs, fdr=fdr_hs, precision=precision_hs, recall=recall_hs)
                fig <- ggplot(df, aes(recall, precision)) +
                    geom_point(aes(colour=factor(fdr), shape=factor(depth))) +
                    coord_fixed() +
                    myTheme()
                ggsave(output_hs, fig, width=100, height=100, unit="mm", useDingbats=F)
            |]
            return ()
        |] $ return ()
    path ["Read_Data", "Get_Depth", "Filter_Data", "Call_Peaks", "Compute_PR_Prep", "Compute_PR"]

main :: IO ()
main = mainFun wf