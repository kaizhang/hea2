{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell   #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE QuasiQuotes       #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE DataKinds #-}

module FilterPeak (peakBuilder) where

import HEA
import Data.BBI.BigWig

import qualified Data.Conduit.List as L
import Statistics.Quantile
import qualified Data.IntSet as IS
import qualified Taiji.Utils.DataFrame as DF
import qualified Data.HashMap.Strict as HM
import Data.Int
import Bio.RealWorld.GENCODE
import Bio.Seq.IO
import Control.Monad.State.Strict
import Control.Arrow
import qualified Data.Vector as V
import qualified Data.Vector.Unboxed as U
import qualified Data.Vector.Unboxed.Mutable as UM
import AI.Clustering.Hierarchical hiding (normalize)
import Data.Either
import Control.DeepSeq
import Control.Parallel.Strategies (parMap, rdeepseq)
import qualified Data.Text as T
import qualified Data.Text.IO as T
import Bio.Data.Bed.Types
import Shelly hiding (FilePath, path)
import Data.Int
import Data.Conduit.Internal (zipSources)
import Bio.Data.Bed
import Bio.Data.Bed.Utils
import qualified Data.Matrix as Mat
import qualified Data.ByteString.Char8 as B
import qualified Data.HashMap.Strict as M
import qualified Data.HashSet as S
import Data.List.Ordered (nubSort)
import Bio.Utils.Functions
import Taiji.Utils.Plot
import Statistics.Sample (mean, meanVarianceUnb)
import Statistics.Correlation (pearson)
import Statistics.Distribution.Normal (standard)
import Statistics.Distribution (complCumulative)
import Data.Binary
import Data.Conduit.Internal (zipSinks, zipSources)
import System.Random.MWC.Distributions (uniformShuffle)
import System.Random.MWC (create, uniformR)
import Bio.RealWorld.ENCODE
import Bio.RealWorld.ID
import           Data.Conduit.Zlib           (gzip, ungzip, multiple)
import Control.Concurrent.Async (forConcurrently)

import Taiji.Prelude
import qualified Language.R                        as R
import           Language.R.QQ
import Data.Vector.SEXP (toList)
import Language.R.HExp
import Taiji.Utils.Matrix

randomRegion :: Int
             -> Int  -- half-window size
             -> BEDTree ()  -- exclude
             -> [BED3] -- genome
             -> IO [BED3]
randomRegion num window tree genome = do
    g <- create
    replicateM num $ sampleRegion g
  where
    genome' = V.fromList genome
    n = V.length genome'
    sampleRegion g = do
        i <- uniformR (0, n-1) g
        let BED3 chr s e = genome' V.! i
            go = do
                x <- uniformR (s+50000, e-50000) g
                let region = BED3 chr (x - window) (x + window)
                if tree `isIntersected` region
                    then go 
                    else return region
        go

computeFDR :: ([Double], [Double]) -> IO [Double]
computeFDR (fg, bg) = R.runRegion $ do
    fdr <-
        [r| library("fitdistrplus")
           fit <- fitdist(bg_hs, "beta", method="mme")
           shape1 <- fit$estimate[[1]]
           shape2 <- fit$estimate[[2]]
           pvalues <- pbeta(fg_hs, shape1, shape2, lower.tail=F)
           p.adjust(pvalues, method = "BH")
        |]
    R.unSomeSEXP fdr $ \x -> case hexp x of
        Real x' -> return $ toList x'

proportion :: Int -> ConduitT (Row Int) Void (ResourceT IO) (U.Vector Double)
proportion n = do
    (vec, n) <- mapC (second (map (second (const 1)))) .| zipSinks (colSumC n) lengthC
    return $ U.map (/(fromIntegral n)) vec

changeSize :: NarrowPeak -> NarrowPeak
changeSize pk = chromStart .~ max 0 (summit - halfWindowSize) $
        chromEnd .~ summit + halfWindowSize $
        npPeak .~ Just halfWindowSize $ pk
  where
    summit = pk^.chromStart + fromJust (pk^.npPeak)
    halfWindowSize = 5

mkCountMat :: Monad m
           => [BED3]    -- ^ a list of regions
           -> ConduitT (B.ByteString, [BED]) (Row Int) m ()
mkCountMat regions = mapC $ second $ countEachCell bedTree
  where
    bedTree = bedToTree (++) $ zip regions $ map return [0::Int ..]
    countEachCell :: BEDTree [Int] -> [BED] -> [(Int, Int)]
    countEachCell beds = HM.toList . foldl' f HM.empty
      where
        f m bed = foldl' (\x k -> HM.insertWith (+) k (1::Int) x) m $
            concat $ intersecting beds query
          where
            query = case bed^.strand of
                Just False -> BED3 (bed^.chrom) (bed^.chromEnd - 1) (bed^.chromEnd)
                _ -> BED3 (bed^.chrom) (bed^.chromStart) (bed^.chromStart + 1)
{-# INLINE mkCountMat #-}

groupCells :: Monad m => ConduitT BED (B.ByteString, [BED]) m ()
groupCells = L.groupBy ((==) `on` (^.name)) .|
    mapC (\xs -> (fromJust $ head xs^.name, xs))
{-# INLINE groupCells #-}

peakBuilder = do
    node "Background_Region" [| \input -> do
        dir <- (<> "/PeakFilter/") <$> lookupConfig "output_dir"
        blacklistFl <- lookupConfig "black_list"
        genomeFl <- lookupConfig "genome"
        let output = dir <> "random_region.bed.gz"
        liftIO $ do
            chr <- fmap (filter ((/="chrM") . fst) . filter (("chr" `B.isPrefixOf`) . fst)) $
                withGenome genomeFl $ return . getChrSizes
            let genome = map (\(a,b) -> BED3 a 0 b) chr
            shelly $ mkdir_p dir
            peaks <- runResourceT $ runConduit $
                (streamBedGzip input >> streamBed blacklistFl) .| sinkList :: IO [BED3]
            let mergedPeaks = runIdentity $ runConduit $ mergeBed peaks .| sinkList
                tree = bedToTree const $ zip mergedPeaks $ repeat ()
            regions <- randomRegion 1000000 200 tree genome
            runResourceT $ runConduit $ yieldMany regions .| sinkFileBedGzip output
            return output
        |] $ return ()
    path ["Merge_Peaks", "Background_Region"]

    uNode "Count_Matrix_Prep" [| \(mergedPeaks, background, peaks, beds) -> return $
        zip4 peaks beds (repeat mergedPeaks) $ repeat background
        |]
    ["Merge_Peaks", "Background_Region", "Adaptive_Peak_Call", "Cutoff_Analysis_Prep"] ~> "Count_Matrix_Prep"
    nodePar "Count_Matrix" [| \(peakFl, bedFl, peakList, background) -> do
        dir <- (<> "/PeakFilter/") <$> lookupConfig "output_dir"
        let prefix = dir <> T.unpack (fst $ T.breakOn ".bed.gz" $ snd $ T.breakOnEnd "/" bedFl) 
            output1 = prefix <> "_fg.txt"
            output2 = prefix <> "_bg.txt"
        liftIO $ do
            peaks <- runResourceT $ runConduit $ streamBedGzip peakFl .| sinkList :: IO [BED3]
            peaks' <- runResourceT $ runConduit $ streamBedGzip peakList .| intersectBed peaks .| sinkList :: IO [BED3]
            p1 <- runResourceT $ runConduit $ streamBedGzip (T.unpack bedFl) .| groupCells .|
                mkCountMat peaks' .| proportion (length peaks')
            B.writeFile output1 $ B.unlines $ map toShortest $ U.toList p1
            bg <- runResourceT $ runConduit $ streamBedGzip background .| sinkList :: IO [BED3]
            p2 <- runResourceT $ runConduit $ streamBedGzip (T.unpack bedFl) .| groupCells .|
                mkCountMat bg .| proportion (length bg)
            B.writeFile output2 $ B.unlines $ map toShortest $ U.toList p2
            return (output1, output2)
        |] $ return ()
    nodePar "Compute_FDR" [| \(fg, bg) -> do
        dir <- (<> "/PeakFilter/") <$> lookupConfig "output_dir"
        let prefix = dir <> T.unpack (fst $ T.breakOn ".txt" $ snd $ T.breakOnEnd "/" $ T.pack fg) 
            output = prefix <> "_fdr.txt"
        liftIO $ do
            fg' <- map readDouble . B.lines <$> B.readFile fg
            bg' <- map readDouble . B.lines <$> B.readFile bg
            fdr <- computeFDR (fg', bg')
            B.writeFile output $ B.unlines $ map toShortest fdr
            return output
        |] $ return ()
    path ["Count_Matrix_Prep", "Count_Matrix", "Compute_FDR"]

    node "Filter_Peaks" [| \(merged, fdrs, peaks) -> do
        dir <- (<> "/Final_Peaks/") <$> lookupConfig "output_dir"
        liftIO $ do
            shelly $ mkdir_p dir
            forM (zip fdrs peaks) $ \(fdrFl, peakFl) -> do
                let output = dir <> T.unpack (snd $ T.breakOnEnd "/" $ T.pack peakFl) 
                fdr <- map readDouble . B.lines <$> B.readFile fdrFl
                peaks <- runResourceT $ runConduit $ streamBedGzip peakFl .| sinkList :: IO [BED3]
                peaks' <- runResourceT $ runConduit $ streamBedGzip merged .| intersectBed peaks .| sinkList :: IO [NarrowPeak]
                let peaks'' = if (length fdr /= length peaks')
                        then error ""
                        else map snd $ filter ((<=0.01) . fst) $ zip fdr peaks'
                runResourceT $ runConduit $ yieldMany peaks'' .|
                    filterC (("chr" `B.isPrefixOf`) . (^.chrom)) .|
                    sinkFileBedGzip output
                return output
        |] $ return ()
    ["Merge_Peaks", "Compute_FDR", "Adaptive_Peak_Call"] ~> "Filter_Peaks"
    node "Final_Merge_Peaks" [| \peaks -> do
        dir <- lookupConfig "output_dir"
        let output = dir <> "/merged_peaks.bed.gz"
        liftIO $ withTempDir (Just "./") $ \tmp -> do
            let f m x = force $ M.insert (x^.chrom, x^.chromStart, x^.chromEnd) x m
                tmp1 = tmp <> "/tmp1.bed"
                tmp2 = tmp <> "/tmp2.bed"
            runResourceT $ runConduit $ (mapM_ streamBedGzip peaks :: ConduitT () NarrowPeak (ResourceT IO) ()) .|
                sinkFileBed tmp1
            shelly $ escaping False $ bashPipeFail bash_ "cat" $
                [T.pack tmp1, "|", "sort", "-k1,1", "-k2,2n", "-k3,3n", ">", T.pack tmp2]
            ps <- fmap (zipWith (\i p -> name .~ Just (B.pack $ show i) $ p) [1 :: Int ..]) $
                runResourceT $ runConduit $ streamBed tmp2 .|
                mergeSortedBedWith nub .| concatC .| sinkList :: IO [NarrowPeak]
            runResourceT $ runConduit $ yieldMany ps .| sinkFileBedGzip output
        return output
        |] $ return ()
    path ["Filter_Peaks", "Final_Merge_Peaks"]

    node "Output_Cluster_Peak" [| \(peakFls, peaklist) -> do
        dir <- (<> "/Cluster_peak/") <$> lookupConfig "output_dir"
        liftIO $ do
            shelly $ mkdir_p dir
            peaks <- runResourceT $ runConduit $ streamBedGzip peaklist .| sinkList :: IO [BED3]
            forM_ peakFls $ \fl -> do
                let output = dir <> T.unpack (snd $ T.breakOnEnd "/" $ T.pack fl)
                runResourceT $ runConduit $ (streamBedGzip fl :: ConduitT () NarrowPeak (ResourceT IO) ()) .|
                    intersectBed peaks .| sinkFileBedGzip output
        |] $ return ()
    ["Adaptive_Peak_Call", "Final_Merge_Peaks"] ~> "Output_Cluster_Peak"

  {-
    node "TT" [| \() -> liftIO $ do
        let matFl = "/mnt/silencer2/home/kai/tscc/project/Atlas/output/SCATACSeq/Feature/Sample/muscle_SM-ADA6L_rep1_peak.mat.gz"
            clFl = "/mnt/silencer2/home/kai/tscc/project/Atlas/output/SCATACSeq/temp/Pre/Cluster/muscle_SM-ADA6L_rep1_clusters.bin"
            peaklistFl = "/mnt/silencer2/home/kai/tscc/project/Atlas/output/SCATACSeq/temp/Peak/merged.narrowPeak.gz"
            peaksFl = "/mnt/silencer2/home/kai/tscc/project/Atlas/output/SCATACSeq/temp/Peak/muscle_SM-ADA6L_rep1/C11.narrowPeak.gz"
        mat <- mkSpMatrix id matFl
        cl <- fromJust . lookup "C11" . map (\x -> (_cluster_name x, x)) <$> decodeFile clFl
        print $ length $ _cluster_member cl
        peaklist <- runResourceT $ runConduit $ streamBedGzip peaklistFl .| sinkList
        peaks <- runResourceT $ runConduit $ streamBedGzip peaksFl .| mapC changeSize .| sinkList
        computeProp cl peaks peaklist mat >>= U.mapM_ print
        |] $ return ()
        -}