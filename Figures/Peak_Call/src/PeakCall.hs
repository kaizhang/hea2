{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell   #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE QuasiQuotes       #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE DataKinds #-}

module PeakCall (peakCallBuilder) where

import HEA
import Data.BBI.BigWig

import Statistics.Quantile
import qualified Data.IntSet as IS
import qualified Taiji.Utils.DataFrame as DF
import Data.Int
import Bio.RealWorld.GENCODE
import Control.Monad.State.Strict
import Control.Arrow
import qualified Data.Vector as V
import qualified Data.Vector.Unboxed as U
import qualified Data.Vector.Unboxed.Mutable as UM
import AI.Clustering.Hierarchical hiding (normalize)
import Data.Either
import Control.DeepSeq
import Control.Parallel.Strategies (parMap, rdeepseq)
import qualified Data.Text as T
import qualified Data.Text.IO as T
import Bio.Data.Bed.Types
import Shelly hiding (FilePath, path)
import Data.Int
import Data.Conduit.Internal (zipSources)
import Bio.Data.Bed
import Bio.Data.Bed.Utils
import qualified Data.Matrix as Mat
import qualified Data.ByteString.Char8 as B
import qualified Data.HashMap.Strict as M
import qualified Data.HashSet as S
import Data.List.Ordered (nubSort)
import Bio.Utils.Functions
import Taiji.Utils.Plot
import Statistics.Sample (mean, meanVarianceUnb)
import Statistics.Correlation (pearson)
import Statistics.Distribution.Normal (standard)
import Statistics.Distribution (complCumulative)
import Data.Binary
import Data.Conduit.Internal (zipSinks, zipSources)
import System.Random.MWC.Distributions (uniformShuffle)
import System.Random.MWC (create, uniformR)
import Bio.RealWorld.ENCODE
import Bio.RealWorld.ID
import           Data.Conduit.Zlib           (gzip, ungzip, multiple)
import Control.Concurrent.Async (forConcurrently)

import Taiji.Prelude
import qualified Language.R                        as R
import           Language.R.QQ
import Language.R.HExp
import Taiji.Utils.Matrix

cutoffAnalysis :: FilePath -> FilePath -> IO (Int, FilePath)
cutoffAnalysis output input = withTempDir (Just "./") $ \tmpdir -> do
    shelly $ run_ "macs2" ["callpeak", "-f", "BED", "--outdir", T.pack tmpdir, "--keep-dup", "all",
        "-t", T.pack input, "-g", "hs", "--nomodel", "--shift", "-100", "--extsize", "200", "--cutoff-analysis"]
    n <- runResourceT $ runConduit $ sourceFile input .|
        multiple ungzip .| linesUnboundedAsciiC .| lengthC :: IO Int
    let file = tmpdir <> "/NA_cutoff_analysis.txt"
    shelly $ cp file output
    return (n, output)

plotCutoff :: FilePath -> (Int, FilePath) -> IO ()
plotCutoff output (n, input) = R.runRegion $ do
    myTheme
    [r| library("ggplot2")
        library("cowplot")
        data <- read.table(input_hs, header=T)
        df <- subset(data.frame(fdr=10**(-data[,2]), npeaks=data[,3]/1000, psize=data[,5]), fdr <= 0.99)
        fig1 <- ggplot(df, aes(fdr, npeaks)) +
            geom_line() +
            ggtitle(depth_hs) +
            ylab("# peaks (thousand)") +
            myTheme()
        fig2 <- ggplot(df, aes(fdr, psize)) +
            geom_line() +
            myTheme()
        fig <- plot_grid(fig1, fig2, ncol=1, labels = "")
        ggsave(output_hs, fig, width=80, height=100, unit="mm", useDingbats=F)
    |]
    return ()
  where
    depth = printf "%.2fM" (fromIntegral n / 1000000 :: Double) :: String

mergePeaks :: FilePath -> ConduitT NarrowPeak NarrowPeak (ResourceT IO) ()
mergePeaks tmpdir = do
    mapC resize .| sinkFileBed tmp1
    liftIO $ shelly $ escaping False $ bashPipeFail bash_ "cat" $
        [T.pack tmp1, "|", "sort", "-k1,1", "-k2,2n", "-k3,3n", ">", T.pack tmp2]
    streamBed tmp2 .| mergeSortedBedWith iterativeMerge .| concatC
  where
    tmp1 = tmpdir <> "/tmp1"
    tmp2 = tmpdir <> "/tmp2"
    iterativeMerge [] = []
    iterativeMerge peaks = bestPeak : iterativeMerge rest
      where
        rest = filter (\x -> sizeOverlapped x bestPeak == 0) peaks
        bestPeak = maximumBy (comparing (^.npSignal)) peaks
    resize pk = chromStart .~ max 0 (summit - halfWindowSize) $
        chromEnd .~ summit + halfWindowSize$
        npPeak .~ Just halfWindowSize $ pk
      where
        summit = pk^.chromStart + fromJust (pk^.npPeak)
    halfWindowSize = 200

peakCallBuilder = do
    uNode "Cutoff_Analysis_Prep" [| \() -> do
        bedDir <- lookupConfig "cluster_bed"
        liftIO $ shelly $ lsT bedDir
        |]

    nodePar "Cutoff_Analysis" [| \fl -> do
        dir <- (<> "/Cutoff/") <$> lookupConfig "output_dir"
        let output = dir <> T.unpack (fst $ T.breakOn ".bed.gz" $ snd $ T.breakOnEnd "/" fl) <> ".txt"
        liftIO $ do
            shelly $ mkdir_p dir
            cutoffAnalysis output $ T.unpack fl
        |] $ return ()
    nodePar "Plot_Cutoff_Analysis" [| \(n, input) -> do
        dir <- (<> "/Cutoff/") <$> lookupConfig "output_dir"
        let output = dir <> T.unpack (fst $ T.breakOn ".txt" $ snd $ T.breakOnEnd "/" $ T.pack input) <> ".pdf"
        liftIO $ do
            shelly $ mkdir_p dir
            plotCutoff output (n, input)
        |] $ return ()
    path ["Cutoff_Analysis_Prep", "Cutoff_Analysis", "Plot_Cutoff_Analysis"]

    nodePar "Adaptive_Peak_Call" [| \fl -> do
        dir <- (<> "/Peaks/") <$> lookupConfig "output_dir"
        let output = dir <> T.unpack (fst $ T.breakOn ".bed.gz" $ snd $ T.breakOnEnd "/" fl) <> ".bed.gz"
        liftIO $ do
            shelly $ mkdir_p dir
            n <- runResourceT $ runConduit $ sourceFile (T.unpack fl) .|
                multiple ungzip .| linesUnboundedAsciiC .| lengthC :: IO Int
            let fdr | n > 100000000 = 0.001
                    | n > 50000000 = 0.01
                    | n > 25000000 = 0.025
                    | n > 5000000 = 0.05
                    | n <= 5000000 = 0.1
                    | otherwise = undefined
            _ <- callPeaks output (location .~ T.unpack fl $ emptyFile :: File '[] 'Bed)
                Nothing $ mode .~ NoModel (-100) 200 $
                    cutoff .~ QValue fdr $
                    callSummits .~ True $
                    gSize .~ Just "hs" $ def
            return output
        |] $ return ()
    node "Merge_Peaks" [| \fls -> do
        dir <- (<> "/Peaks/") <$> lookupConfig "output_dir"
        let output = dir <> "merged_peaks.bed.gz"
        liftIO $ withTempDir (Just "./") $ \tmpdir -> runResourceT $ runConduit $
            mapM_ streamBedGzip fls .| mergePeaks tmpdir .| sinkFileBedGzip output
        return output
        |] $ return ()
    path ["Cutoff_Analysis_Prep", "Adaptive_Peak_Call", "Merge_Peaks"]
