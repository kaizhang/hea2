{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell   #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE QuasiQuotes       #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE GADTs #-}
import qualified Taiji.Utils.DataFrame as DF
import qualified Data.Vector as V
import qualified Data.Text as T
import qualified Data.Text.IO as T
import qualified Data.ByteString.Char8 as B
import qualified Data.HashMap.Strict as M
import Data.List.Ordered (nubSort)
import Taiji.Utils.Plot
import System.Environment
import Data.Maybe

readEvidence :: FilePath   -- ^ Terms
             -> FilePath   -- ^ Annotation
             -> [T.Text]
             -> IO (DF.DataFrame Int)
readEvidence fl1 fl2 tfs = do
    m <- M.fromList . map ((\xs -> (head xs, xs!!1)) . T.splitOn "\t") . T.lines <$>
        T.readFile fl1
    let f xs = (head xs, mapMaybe (\x -> M.lookup x m) $ T.splitOn " " $ xs!!6)
    anno <- filter ((`elem` tfs) . fst) . filter (not . null . snd) .
        map (f . T.splitOn "\t") . T.lines <$> T.readFile fl2
    let colnames = nubSort $ concatMap snd anno
    return $ DF.mkDataFrame (map fst anno) colnames $ flip map anno $ \(_, xs) -> 
        map (\x -> if x `elem` xs then 1 else 0) colnames

main = do
    [f1,f2, f3] <- getArgs
    tfs <- tail . map (head . T.splitOn "\t") . T.lines <$> T.readFile f3
    readEvidence f1 f2 tfs >>=
        DF.writeTable "out.tsv" (T.pack . show) 
