{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell   #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE QuasiQuotes       #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE GADTs #-}

module ChromVar where

import HEA

import qualified Taiji.Utils.DataFrame as DF
import qualified Data.Vector as V
import qualified Data.Text as T
import Bio.Data.Bed.Types
import Bio.Data.Bed
import Control.Monad.ST (runST)
import qualified Data.Matrix as Mat
import qualified Data.ByteString.Char8 as B
import qualified Data.HashMap.Strict as M
import Data.List.Ordered (nubSort)
import Bio.Utils.Functions
import Taiji.Utils.Plot
import Data.Binary
import System.Random.MWC.Distributions (uniformShuffle)
import System.Random.MWC (create)
import Shelly (shelly, run_)

import Taiji.Prelude hiding (groupBy)
import qualified Language.R                        as R
import           Language.R.QQ
import Language.R.HExp

driver_gene :: [(String, String)]
driver_gene = 
    [ ("PAX7", "Stl")
    , ("NKX2-2", "")
    , ("FOXE1", "Fol")
    , ("NR5A1", "Adc")
    , ("ATOH1", "Gbl")
    , ("PDX1", "Dut")
    , ("SPIC", "Mac")
    , ("SPI1", "Mac")
    , ("TBX21", "TLy")
    , ("NR1I3", "Hpc")
    , ("HNF1A", "Hpc")
    , ("NKX2-5", "Cam")
    , ("MYF6", "Sk1/2")
    , ("PTF1A", "Acn")
    , ("FOXL2", "Grn")
    , ("POU2F2", "Bly")
    , ("STAT5A", "Adi")
    ]

plotCells :: [(Double, Double, Double)]
          -> R.R s (R.SomeSEXP s)
plotCells points = do
    myTheme
    [r| library("ggplot2")
        library("ggrastr")
        library("RColorBrewer")
        library("gridExtra")
        rwb <- colorRampPalette(colors = c("white", brewer.pal(7,"Reds")))
        df <- data.frame(x=xs_hs, y=ys_hs, z=zs_hs)
        ggplot(df, aes(x,y)) +
            geom_point_rast(size=0.2, alpha=0.5, stroke=0, shape=20, aes(colour = z)) +
            myTheme() + myFill() +
            labs(x = "UMAP-1", y = "UMAP-2", fill = "Dose (mg)") +
            theme(
                plot.margin=unit(c(2,0,2,2),"mm"),
                panel.grid.major = element_blank(),
                axis.ticks = element_blank(),
                axis.text = element_blank(),
                axis.line = element_blank(),
                panel.border = element_blank(),
                legend.position = c(0.9, 0.1) 
            ) + scale_colour_gradientn(colours =rwb(99))
                
    |]
  where
    (xs, ys, zs) = unzip3 points

plotCells2 :: [(Double, Double, Double)]
          -> R.R s (R.SomeSEXP s)
plotCells2 points = do
    myTheme
    [r| library("ggplot2")
        library("ggrastr")
        library("RColorBrewer")
        library("gridExtra")
        rwb <- colorRampPalette(colors = rev(brewer.pal(7,"RdBu")))
        df <- data.frame(x=xs_hs, y=ys_hs, z=zs_hs)
        ggplot(df, aes(x,y)) +
            geom_point_rast(size=0.15, alpha=0.8, stroke=0, shape=20, aes(colour = z)) +
            myTheme() + myFill() +
            labs(x = "UMAP-1", y = "UMAP-2", fill = "Dose (mg)") +
            theme(
                plot.margin=unit(c(2,0,2,2),"mm"),
                panel.grid.major = element_blank(),
                axis.ticks = element_blank(),
                axis.text = element_blank(),
                axis.line = element_blank(),
                panel.border = element_blank(),
                legend.position = c(0.9, 0.1) 
            ) + scale_colour_gradientn(colours =rwb(99))
                
    |]
  where
    (xs, ys, zs) = unzip3 points



chromvar = do
    node "Submatrix" [| \() -> do
        chromVarFl <- lookupConfig "chromvar"
        dir <- lookupConfig "output_dir"
        let output = dir <> "/chromvar.tsv"
        liftIO $ do
            df <- DF.readTable chromVarFl

            DF.writeTable output (T.pack . show) $ df `DF.csub` (map (T.pack . fst) driver_gene)
            return output
        |] $ return ()
    node "ChromVAR" [| \chromVarFl -> do
        clFl <- lookupConfig "cluster_file"
        dir <- lookupConfig "output_dir"
        let output = dir <> "/output.pdf"
        liftIO $ do
            df <- DF.readTable chromVarFl
            clusters' <- decodeFile clFl :: IO [CellCluster]

            let points = flip concatMap clusters' $ \cl -> flip map (_cluster_member cl) $ \x ->
                    let (cx,cy) = _cell_2d x
                    in (cx, cy, df DF.! (T.pack $ B.unpack $ _cell_barcode x, "TBX21"))

            R.runRegion $ do
                fig <- plotCells2 points
                [r| ggsave(output_hs, fig_hs, width=60, height=60, unit="mm", useDingbats=F)
                |]
                return ()
        |] $ return ()
    ["Submatrix"] ~> "ChromVAR"

    node "Promoter_Acc" [| \() -> do
        clFl <- lookupConfig "cluster_file"
        dir <- lookupConfig "output_dir"
        expr <- lookupConfig "expression"
        let output = dir <> "/output2.pdf"
        liftIO $ do
            df <- DF.map (log . (+0.01)) <$> DF.readTable expr
            clusters' <- decodeFile clFl :: IO [CellCluster]

            let points = flip concatMap clusters' $ \cl -> flip map (_cluster_member cl) $ \x ->
                    let (cx,cy) = _cell_2d x
                    in (cx, cy, df DF.! (T.pack $ B.unpack $ _cell_barcode x, "TBX21"))

            R.runRegion $ do
                fig <- plotCells2 points
                [r| ggsave(output_hs, fig_hs, width=60, height=60, unit="mm", useDingbats=F)
                |]
                return ()
        |] $ return ()



