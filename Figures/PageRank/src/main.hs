{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell   #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE QuasiQuotes       #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE GADTs #-}
import HEA

import qualified Taiji.Utils.DataFrame as DF
import qualified Data.Vector as V
import qualified Data.Text as T
import qualified Data.Text.IO as T
import Bio.Data.Bed.Types
import Bio.Data.Bed
import Control.Monad.ST (runST)
import qualified Data.Matrix as Mat
import qualified Data.ByteString.Char8 as B
import qualified Data.Map.Strict as M
import Data.List.Ordered (nubSort)
import Bio.Utils.Functions
import Taiji.Utils.Plot
import Data.Binary
import System.Random.MWC.Distributions (uniformShuffle)
import System.Random.MWC (create)
import Shelly (shelly, run_)
import ChromVar

import Taiji.Prelude hiding (groupBy)
import qualified Language.R                        as R
import           Language.R.QQ
import Language.R.HExp

qnorm :: DF.DataFrame Double -> DF.DataFrame Double
qnorm df = DF.fromMatrix (DF.rowNames df) (DF.colNames df) $ quantileNormalization $ DF._dataframe_data df

filterRanks :: DF.DataFrame (Double, Double) -- ^ Ranks and pvalue
            -> DF.DataFrame (Double, Double)
filterRanks input = 
    let tfs = nubSort $ flip concatMap (Mat.toColumns $ DF._dataframe_data input) $ \pval ->
            V.toList $ fst $ V.unzip $ filterFDR 0.01 $
            V.zip (V.fromList $ DF.rowNames input) $ V.map snd pval
    in input `DF.rsub` tfs

cells :: [T.Text]
cells = ["Bly", "Tly.1", "Mst", "Grn", "Stl", "Enc.1", "Gbl.1", "Eep.1", "Krt",
    "Bas.1", "Lue", "Nec", "Gcf", "Prt", "Acn", "Dut", "Agb", "Skm1", "Fol",
    "Cam", "Adc", "Hpc", "Mac.1", "Pal", "Mes", "Adi", "Fib.1", "Swn.1", "End.1",
    "Smm.1"]

entropy :: V.Vector Double -> Double
entropy v = e (normalize v) -- / (logBase 2 $ fromIntegral $ V.length v)
  where
    e xs = negate $ V.sum $ V.map (\p -> p * logBase 2 p) xs
    normalize xs = V.map (/s) xs'
      where
        s = V.sum xs'
        xs' = V.map (+pseudoCount) xs
        pseudoCount = 0

diagonize :: DF.DataFrame Double -> DF.DataFrame Double
diagonize = DF.reorderRows f
  where
    f xs = map (\x -> (x, M.findWithDefault undefined x xs')) names
      where
        xs' = M.fromList xs
        names = map fst $ sortBy (comparing snd) $ map (\(a,b) -> (a, g' b)) xs
        g' xs = (i, negate v)
          where
            v = xs V.! i
            i = V.maxIndex xs
            --z = scale xs

plotRanks :: FilePath
          -> [String]
          -> DF.DataFrame Double
          -> IO()
plotRanks output idx df = R.runRegion $ do
    valMatrix <- toRMatrix df
    [r| library("ggplot2")
        library("ComplexHeatmap")
        library("RColorBrewer")
        library("viridis")
        rwb <- colorRampPalette(colors = rev(brewer.pal(7,"RdBu")))
        h1 <- Heatmap( valMatrix_hs, 
            col=rwb(99),
            cluster_rows=F, cluster_columns=F,
            show_row_names=F, row_names_side="left",
            show_column_names=T,
            column_names_gp = gpar(fontsize=5),
            row_names_gp = gpar(fontsize=5),
            left_annotation = rowAnnotation(foo=anno_mark(
                at=pos_hs,labels=txt_hs,side="left",
                link_width=unit(0.5, "points"),
                link_gp=gpar(lwd=0.4),
                labels_gp=gpar(fontsize=5)
            )),
            heatmap_legend_param = list(
                title = "zscore of ranking score",
                title_gp = gpar(fontsize = 6),
                #title_position = "left-top",
                direction = "horizontal",
                labels_gp = gpar(fontsize = 5)
            )
        )

        pdf(output_hs, width=10.0, height=12.0)
        draw(h1, heatmap_legend_side = "bottom")
        dev.off()
    |]
    return ()
  where
    (pos, txt) = unzip $ mapMaybe (\i -> case M.lookup (T.pack i) (DF._dataframe_row_names_idx df) of
        Nothing -> Nothing
        Just t -> Just (fromIntegral (t + 1) :: Double, i)
        ) idx
    names = map (T.unpack . head . T.splitOn ".") $ DF.rowNames df

-- Take the top TFs in each cluster
topTFs :: Int -> DF.DataFrame Double -> DF.DataFrame Double
topTFs n df = df `DF.rsub` tfs
  where
    tfs = nubSort $ flip concatMap (Mat.toColumns $ DF._dataframe_data $
        DF.mapRows scale df) $ \v -> take n $ reverse $ fst $ unzip $
            sortBy (comparing snd) $ zip (DF.rowNames df) $ V.toList v

orderTFs :: DF.DataFrame Double -> [(T.Text, [T.Text])]
orderTFs df = zip (DF.colNames df) $ flip map
    (Mat.toColumns $ DF._dataframe_data $ DF.mapRows scale df) $ \v ->
        reverse $ fst $ unzip $ sortBy (comparing snd) $
        zip (DF.rowNames df) $ V.toList v

orderTFs' :: Double -> DF.DataFrame Double -> [(T.Text, [T.Text])]
orderTFs' cutoff df = zip (DF.colNames df) $ flip map
    (Mat.toColumns $ DF._dataframe_data $ DF.mapRows scale df) $ \v ->
        reverse $ fst $ unzip $ filter ((>=cutoff) . snd) $
        sortBy (comparing snd) $ zip (DF.rowNames df) $ V.toList v

phenotypeMap :: [(T.Text, T.Text)]
phenotypeMap = [ ("Adi", "adipose tissue phenotype")
      , ("Cam", "cardiovascular system phenotype")
      , ("Mac", "immune system phenotype")
      , ("Tly", "immune system phenotype")
      , ("Bly", "immune system phenotype")
      , ("Mst", "immune system phenotype")
      , ("Hpc", "liver/biliary system phenotype")
      , ("Sk1", "muscle phenotype")
      , ("Sk2", "muscle phenotype")
      , ("PA2", "respiratory system phenotype")
      , ("Enc", "digestive/alimentary phenotype")
      , ("GCf", "digestive/alimentary phenotype")
      , ("Prt", "digestive/alimentary phenotype")
      , ("Gbl", "digestive/alimentary phenotype")
      , ("ABD" , "endocrine/exocrine gland phenotype")
      , ("Fol" , "endocrine/exocrine gland phenotype")
      , ("AdC" , "endocrine/exocrine gland phenotype")
      ]

--      , ("Fib", "growth/size/body region phenotype")

-- cellular phenotype
-- craniofacial phenotype
-- embryo phenotype
-- hematopoietic system phenotype
-- homeostasis/metabolism phenotype
-- integument phenotype
-- neoplasm
-- nervous system phenotype
-- pigmentation phenotype
-- reproductive system phenotype

barPlot :: FilePath -> [(String, Double, Double)] -> IO ()
barPlot output input = R.runRegion $ do
    myTheme
    [r| library("ggplot2")
        df <- data.frame(name=names_hs, val=dat_hs, label=labels_hs)
        fig <- ggplot(df, aes(x=reorder(name, val), y=val)) +
            geom_col(aes(fill=label), position="dodge") +
            coord_flip() +
            myTheme() +
            labs(x = "", y = "% validated")
        ggsave(output_hs, fig, width=60, height=50, unit="mm", useDingbats=F)
    |]
    return ()
  where
    (names, dat, labels) = unzip3 $ flip concatMap input $ \(nm, bg, fg) -> 
        [ (nm, bg, "Background" :: String)
        , (nm, fg, "Prediction") ]

driver_tf :: [(String, String)]
driver_tf = 
    [ ("PAX7", "Stl")
    , ("NKX2-5", "Cam")
    , ("TBX21", "TLy")
    , ("SPI1", "Mac")
    , ("FOXL2", "Grn")
    , ("MYOD1", "Sk1/2")
    , ("HNF1A", "Hpc")
    , ("PAX5", "Bly")
    , ("FOXE1", "Fol")
    , ("CEBPA", "Adi")
    , ("PTF1A", "Acn")
    , ("PDX1", "Dut")
    , ("CDX2", "")
    , ("ATOH1", "Gbl")
    , ("NR5A1", "Adc")
    , ("MITF", "Mst")
    , ("SOX15", "Eep")
    , ("TP63", "epi")
    , ("FOXA1", "Lue")
    , ("NKX2-2", "")
    , ("SPDEF", "Agb")
    , ("SOX10", "Swn")
    , ("ERG", "End")
    , ("PRRX1", "Fib")
    , ("SOX9", "Paneth cells")
    , ("GFI1B", "Tuft cells")
    ]



build "wf" [t| SciFlow ENV |] $ do
    --node "Read_Evidence" [| \() -> do
    --  phe <- lookupConfig "phenotype"
    --  anno <- DF.readTable phe
    --  |]

{-
    node "Driver_TFs" [| \() -> do
        rankFl <- lookupConfig "ranks"
        pvalFl <- lookupConfig "pvalues"
        annoFl <- lookupConfig "annotation"
        phenotypeFl <- lookupConfig "phenotype"
        dir <- lookupConfig "output_dir"
        let output = dir <> "/Fig3_Validation.pdf"
        liftIO $ do
            anno <- readAnno annoFl
            df <- changeName anno <$> readRanks rankFl pvalFl
            phenotype <- DF.readTable phenotypeFl
            let tfs = M.toList $ fmap nubSort $ M.fromListWith (++) $
                    flip mapMaybe (orderTFs' 2 df) $ \(nm, xs) ->
                        case lookup (fst $ T.breakOn "." nm) phenotypeMap of
                            Nothing -> Nothing
                            Just x -> Just (x, filter (`elem` tfList) xs)
                tfList = DF.rowNames phenotype
            gen <- create
            res <- forM tfs $ \(ph, fg) -> do
                bg <- V.toList . V.take (length fg) <$> uniformShuffle (V.fromList $ tfList) gen
                let r1 = sum $ flip map fg $ \x -> phenotype DF.! (x, ph)
                    r2 = sum $ flip map bg $ \x -> phenotype DF.! (x, ph)
                    n = length fg
                return (T.unpack $ fst $ T.breakOn " phenotype" ph, r1 / fromIntegral n, r2 / fromIntegral n, n)
            barPlot output $ map (\(a,b,c,n) ->
                (printf "%s (n = %d)" a n
                , c * 100
                , b * 100) ) res
        |] $ return ()
        -}


    node "Plot_Ranks" [| \() -> do
        rankFl <- lookupConfig "ranks"
        pvalFl <- lookupConfig "pvalues"
        frankFl <- lookupConfig "fetal_ranks"
        fpvalFl <- lookupConfig "fetal_pvalues"
        fannoFl <- lookupConfig "fetal_annotation"
        annoFl <- lookupConfig "annotation"
        dir <- lookupConfig "output_dir"
        let output = dir <> "/PageRank.pdf"
            add x xs = flip map xs $ \(a,b) -> (a, x <> b)
        liftIO $ do
            anno <- add "Adult " <$> readAnno annoFl
            fanno <- add "Fetal " <$> readAnno fannoFl
            df1 <- fmap (changeName anno) $ DF.zip <$> DF.readTable rankFl <*> DF.readTable pvalFl
            df2 <- fmap (changeName fanno) $ DF.zip <$> DF.readTable frankFl <*> DF.readTable fpvalFl
            let df' = diagonize $ DF.orderDataFrame id $
                    DF.filterCols (\_ x -> V.maximum x > 2.34) $
                    DF.filterRows (\_ x -> V.maximum x > 2.34) df
                df = DF.mapRows scale $ DF.map (logBase 2 . (+0.0000001)) $
                    qnorm $ DF.map fst $ filterRanks $ DF.cbind [df1, df2]
            DF.writeTable "ranks.tsv" (T.pack . show) df'
            forM_ (zip (DF.colNames df') $ Mat.toColumns $ DF._dataframe_data df') $ \(nm, vec) -> do
                let r = T.intercalate "," $ map fst $
                        sortBy (flip (comparing snd)) $
                        filter ((>2.34) . snd) $ zip (DF.rowNames df') $ V.toList vec
                T.putStrLn $ nm <> "\t" <> r
            --let cutoff = head $ drop 149 $ sort $ map entropy $
            --        Mat.toRows $ DF._dataframe_data df
                --DF.filterRows (\_ v -> entropy v <= cutoff) df
            plotRanks output (map fst driver_tf) $
                diagonize $ DF.orderDataFrame id $
                DF.map (\x -> max (-3.5) $ min 3.5 x) df'

            DF.writeTable "1.tsv" (T.pack . show) $ DF.orderDataFrame id $
                DF.map (\x -> max (-3.5) $ min 3.5 x) $
                DF.filterCols (\_ x -> V.maximum x > 1) $
                DF.filterRows (\_ x -> V.maximum x > 1) df
        |] $ return ()

    chromvar

main :: IO ()
main = mainFun wf