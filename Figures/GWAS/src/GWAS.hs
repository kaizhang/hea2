{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell   #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE QuasiQuotes       #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE PartialTypeSignatures #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE GADTs #-}

module GWAS (gwasBuilder, averageDF) where

import           Data.Colour       
import Data.Colour.Names
import HEA
import qualified Data.Text as T
import qualified Data.Text.IO as T
import           Data.Conduit.Zlib           (gzip, ungzip, multiple)
import qualified Data.Vector.Unboxed as U
import qualified Data.ByteString.Char8 as B
import AI.Clustering.Hierarchical hiding (normalize)
import Bio.Utils.Functions
import Bio.RealWorld.GENCODE
import Data.Monoid
import qualified Data.Matrix as Mat
import Control.Concurrent.Async (mapConcurrently, forConcurrently)
import Data.Char (isDigit)
import Control.Arrow
import Bio.Data.Bed
import Bio.Data.Bed.Utils
import Bio.Data.Bed.Types
import Bio.Data.Bam
import Data.Either
import Shelly (mkdir_p, shelly, lsT)
import Taiji.Prelude
import qualified Data.Map.Strict as M
import qualified Data.Set as Set
import qualified Data.HashSet as S
import Control.DeepSeq
import Statistics.Correlation (pearson)
import Statistics.Sample
import Data.List.Split (chunksOf)
import Data.Binary
import Data.List.Ordered (nubSortBy, nubSort)
import Data.Int (Int32)
import qualified Data.Vector as V
import qualified Taiji.Utils.DataFrame as DF
import GHC.Generics (Generic)
import Graph
import IGraph
import IGraph.Algorithms
import System.Random.MWC.Distributions
import System.Random.MWC
import System.IO

import Taiji.Prelude hiding (groupBy)
import qualified Language.R                        as R
import Data.Vector.SEXP (toList)
import           Language.R.QQ
import Language.R.HExp
import qualified Language.R.HExp as H

data GWAS = GWAS
    { _gwas_pmid :: T.Text
    , _gwas_chr :: T.Text
    , _gwas_pos :: Int
    , _gwas_snp :: T.Text
    , _gwas_pvalue :: Double
    , _gwas_trait :: T.Text
    , _gwas_sample_size :: Int
    } deriving (Show, Generic)

instance Binary GWAS
instance Binary BED3

diagonizeDF :: DF.DataFrame Double -> DF.DataFrame Double
diagonizeDF = DF.reorderRows f
  where
    f xs = map (\x -> (x, M.findWithDefault undefined x xs')) names
      where
        xs' = M.fromList xs
        names = map fst $ sortBy (comparing snd) $ map (\(a,b) -> (a, g b)) xs
        g xs = (i, negate v)
          where
            v = z V.! i
            i = V.maxIndex z
            z = scale xs

plotHeatmap :: FilePath
            -> (T.Text -> T.Text)
            -> ([DF.DataFrame Double], [DF.DataFrame Double])
            -> M.Map T.Text Double
            -> M.Map T.Text Double
            -> [T.Text]
            -> IO ()
plotHeatmap output fn (df1, df2) fc1 fc2 highlight = R.runRegion $ do
    liftIO $ mapM_ (mapM_ print . DF.rowNames) df1'
    let plot w colors dfs fc marks = do
            mat <- toRMatrix $ DF.rbind dfs
            [r| split <- factor(split_hs, levels = groups_hs)
                colors <- c(colors_hs)[1:length(groups_hs)]
                names(colors) <- groups_hs
                tissueCat <- matrix(tissueCat_hs, byrow=T, nrow=nrow_hs)
                tissueCat[tissueCat == "NULL"] <- NA
                ha <- HeatmapAnnotation(
                    foo = anno_mark(at = cidx_hs, labels = cmarks_hs, side="top", lines_gp = gpar(lwd=0.5), labels_gp=gpar(fontsize=5)),
                    bar = anno_block(gp = gpar(lwd=0.3, fill = colors))
                    )
                if (length(idx_hs) == 0) {
                    la <- rowAnnotation(
                        cats = anno_simple(tissueCat, width = unit(1.5, "cm"),
                            col = colors, na_col = "white")
                        #percent = anno_simple(percent_hs, width = unit(0.12, "cm"),
                        #    col = c("a" = "white", "b" = "#c6dbef", "c" = "#4292c6", d = "#08306b"))
                        )
                } else {
                    la <- rowAnnotation(
                        foo = anno_mark(at = idx_hs, labels = labels_hs, side="left", lines_gp = gpar(lwd=0.5), labels_gp=gpar(fontsize=5)),
                        cats = anno_simple(tissueCat, width = unit(1.5, "cm"),
                            col = colors, na_col = "white")
                        #percent = anno_simple(percent_hs, width = unit(0.12, "cm"),
                        #    col = c("a" = "white", "b" = "#c6dbef", "c" = "#4292c6", d = "#08306b"))
                        )
                }
                Heatmap(mat_hs, width = w_hs, col=c("#fef0d9", "#d7301f"), use_raster=T,
                    column_split = split, column_gap = unit(0.0, "mm"),
                    row_split = rsplit_hs, column_title = NULL,
                    show_row_names = F, show_column_names = F,
                    show_row_dend = F, show_column_dend = F,
                    cluster_rows=F, cluster_columns=F,
                    heatmap_legend_param = list(
                        title = "log(1+accessibility)",
                        title_gp = gpar(fontsize = 6),
                        title_position = "leftcenter-rot",
                        #direction = "horizontal",
                        labels_gp = gpar(fontsize = 5)
                    ),
                    top_annotation = ha,
                    left_annotation = la
                )
            |]
          where
            (cmarks, cidx) = unzip $
                map (\x -> (fst $ head x, snd $ x !! (length x `div` 2))) $
                groupBy ((==) `on` fst) $ zip split [1 :: Int32 ..]
            groups = map head $ group split
            split = map (T.unpack . fn) $ DF.colNames $ head dfs
            rsplit = concat $ zipWith (\x i -> let n = fst $ DF.dim x in replicate n $ i : '-' : show n) dfs ['a'..]
            enrichments = map (\x -> M.findWithDefault undefined x fc) $ DF.rowNames df
            percent = flip map (computeEnrich df) $ \x -> case () of
                _ | x == 0 -> "a" :: String
                  | x <= 5 -> "b"
                  | x <= 50 -> "c"
                  | x <= 100 -> "d"
                  | otherwise -> undefined
            tissueCat = flip concatMap (Mat.toLists $ DF._dataframe_data df) $ \row ->
                let s = S.fromList $ map (fn . fst) $ filter ((>0) . snd) $ zip (DF.colNames df) row
                    nms = map head $ group $ map fn $ DF.colNames df
                in map (\x -> if x `S.member` s then T.unpack x else "NULL") nms
            df = DF.rbind dfs
            nrow = fromIntegral $ fst $ DF.dim df :: Int32
            ridx = M.fromList $ zip (DF.rowNames df) [1 :: Int32 ..]
            idx = map (\x -> M.findWithDefault undefined x ridx) marks
            labels = flip map marks $ \x -> 
                let (a, b) = T.breakOnEnd "+" x
                --in T.unpack $ T.init a <> " (" <> b <> ")"
                in T.unpack $ T.init a

    [r| library("ComplexHeatmap")
        library("RColorBrewer")
        library("viridis")
        library(circlize)
        library(scales)
        pdf(output_hs, width=8, height=7)
    |]
    hm1 <- plot (1 :: Double) colors1 df1' fc highlight
    hm2 <- plot (1.5 :: Double) colors2 df2' fc []
    [r| draw(hm1_hs + hm2_hs)
        dev.off()
    |]
    return ()
  where
    fc = M.intersectionWith (-) fc1 fc2
    (df1', df2') = unzip $ zipWith reOrder df1 df2
      where
        colOrder1 = organizeDF $ DF.rbind df1
        colOrder2 = organizeDF $ DF.rbind df2
        reOrder x y =
            --let names = DF.rowNames $ DF.reorderRows (DF.orderByHClust id) $ DF.cbind [x, y]
            --in ((x `DF.rsub` names) `DF.csub` colOrder1, (y `DF.rsub` names) `DF.csub` colOrder2)
            let names = if sum (Mat.toList $ DF._dataframe_data x) == 0
                    then DF.rowNames $ diagonizeDF $ y `DF.csub` colOrder2
                    else DF.rowNames $ diagonizeDF $ x `DF.csub` colOrder1
                x' = (x `DF.csub` colOrder1) `DF.rsub` names
                y' = (y `DF.csub` colOrder2) `DF.rsub` names
            in (x', y')
    computeEnrich d = flip map (Mat.toLists $ DF._dataframe_data d) $ \x ->
        let m = fromIntegral $ length $ nubSort $ map (fn . fst) $ filter ((>0) . snd) $ zip (DF.colNames d) x
        in m / n * 100
      where
        n = fromIntegral $ length $ nubSort $ map fn $ DF.colNames d :: Double
    colors1 :: [String]
    colors1 = [ "#ffe1fe","#79d400","#f929ff","#bbe300","#0252f0","#99ff69"
              , "#63009f","#01fe99","#aa00af","#01b73f","#eb00ab","#8cff95"
              , "#a669ff","#ffbb1f","#012e99","#edff8f","#74007b","#019f53"
              , "#dc0077","#00e2d1","#ff334c","#01936b","#db004d" ]
    colors2 :: [String]
    colors2 = ["#01a7ae", "#f06200","#027fdf","#d29500","#8991ff","#b2a300","#e08aff","#628200","#ff9ff4","#005120","#ff6f5b","#0098e5","#ffd36f","#002656","#d1ffc5","#810051","#b1fff5","#5e0d00","#e1fbff","#0a001c","#ffc57a","#002415","#ffafe5","#3b2c00","#cbb8ff","#664e00","#90bbff","#ffa26e","#0282aa","#ff8fa7","#01716e","#540025","#ffcbc1"]
    organizeDF d = concat $ map (f . fst) $ flatten $ hclust Ward (V.fromList grp) (euclidean `on` snd)
      where
        f xs = flatten $ hclust Ward (V.fromList xs) (hamming `on` (d `DF.cindex`))
        grp = map (\x -> (x, averageDF $ d `DF.csub` x)) $ groupBy ((==) `on` fn) $ sortBy (comparing fn) $ DF.colNames d

averageDF :: DF.DataFrame Double -> V.Vector Double
averageDF df = V.map (/n) $ foldl1' (V.zipWith (+)) cols
  where
    cols = Mat.toColumns $ DF._dataframe_data df
    n = fromIntegral $ length cols

enrichment :: [(Int, Int)] -> Int -> Int -> IO [Double]
enrichment input white' total' = R.runRegion $ do
    ps <- [r| total <- total_hs
              white <- white_hs
              f <- function(selectedWhite, selected) {
                  #e <- log2((selectedWhite / selected) / (white / total))
                  if (selectedWhite == 0) {
                      return(0)
                  } else {
                      return(phyper(selectedWhite - 1, white, total - white, selected, lower.tail=F, log.p=T) / log(10))
                  }
              }
              mapply(f, selectedWhite_hs, selected_hs)
           |]
    R.unSomeSEXP ps $ \x -> case H.hexp x of
        H.Real x'  -> return $ toList x'
  where
    (selectedWhite, selected) = unzip $ map (\(x, y) -> (fromIntegral x :: Double, fromIntegral y :: Double)) input
    white = fromIntegral white' :: Double
    total = fromIntegral total' :: Double

enrichment' :: [(Int, Int, Int, Int)] -> IO [Double]
enrichment' input = R.runRegion $ do
    ps <- [r| f <- function(selectedWhite, selected, white, total) {
                  if (selectedWhite == 0) {
                      return(0)
                  } else {
                      return(phyper(selectedWhite - 1, white, total - white, selected, lower.tail=F, log.p=T) / log(10))
                  }
              }
              mapply(f, selectedWhite_hs, selected_hs, white_hs, total_hs)
           |]
    R.unSomeSEXP ps $ \x -> case H.hexp x of
        H.Real x'  -> return $ toList x'
  where
    (selectedWhite, selected, white, total) = unzip4 $ map (\(a,b,c,d) ->
        ( fromIntegral a :: Double
        , fromIntegral b :: Double
        , fromIntegral c :: Double
        , fromIntegral d :: Double )) input


readGWASAssoc :: FilePath -> IO [GWAS]
readGWASAssoc fl = mapMaybe (f . T.splitOn "\t") . tail . T.lines <$> T.readFile fl
  where
    readSampleSize input = h . M.fromListWith (+) . g $ input
      where
        g x = let ws = dropWhile (not . T.all isDigit) $
                      filter (\x -> x == "cases" || T.all isDigit x) $ T.words $ T.filter (\w -> w /= ',' && w /= '~') x
                  go (y1:y2:ys) = if T.all isDigit y1
                      then if y2 == "cases"
                          then (True, readInt $ B.pack $ T.unpack y1) : go ys
                          else (False, readInt $ B.pack $ T.unpack y1) : go (y2:ys)
                      else go (y2:ys)
                  go [y1] = (False, readInt $ B.pack $ T.unpack y1) : go []
                  go [] = []
              in go ws
        h m = case M.lookup True m of
            Just x -> x
            Nothing -> M.findWithDefault (error $ show input) False m
    f xs 
        | xs!!12 == "" = Nothing
        | otherwise = Just GWAS 
            { _gwas_pmid = xs!!1
            , _gwas_chr = "chr" <> (xs!!11)
            , _gwas_pos = readInt $ B.pack $ T.unpack $ xs!!12
            , _gwas_snp = xs!!23
            , _gwas_pvalue = readDouble $ B.pack $ T.unpack $ xs!!27
            , _gwas_trait = T.map (\x -> if x == '–' then '-' else x) $ xs!!7
            , _gwas_sample_size = readSampleSize $ xs!!8
            }

filterGWAS :: [GWAS] -> [GWAS]
filterGWAS gwas = runIdentity $ runConduit $ 
    mergeBedWith iterativeMerge gwas' .| concatC .| mapC (^._data) .| sinkList
  where
    gwas' = map (\x -> BEDExt (BED3 (B.pack $ T.unpack $ _gwas_chr x) (_gwas_pos x - halfWindowSize) (_gwas_pos x + halfWindowSize)) x) gwas
    iterativeMerge [] = []
    iterativeMerge peaks = bestPeak : iterativeMerge rest
      where
        rest = filter (\x -> sizeOverlapped x bestPeak == 0) peaks
        bestPeak = minimumBy (comparing (\x -> _gwas_pvalue $ x^._data)) peaks
    halfWindowSize = 2500

mkEnrichTable :: [(T.Text, FilePath)] -> IO (DF.DataFrame Double)
mkEnrichTable input = do
    (rows, res) <- fmap unzip $ forM input $ \(i, fl) -> do
        r <- map (\x -> (T.pack $ B.unpack $ x^._1, x^._4)) <$> filterResult fl
        return (i, r)
    let cols = nubSort $ concatMap (map fst) res
        dat = flip map res $ \x -> map (\y -> fromMaybe undefined $ lookup y x) cols
    return $ DF.mkDataFrame rows cols dat
  where
    filterResult fl = map (f . B.split '\t') . B.lines <$> B.readFile fl
      where
        f [a,b,c,d] = (a, readDouble b, negate $ readDouble c, readDouble d)

genNullGWAS :: GenIO -> [[GWAS]] -> IO (M.Map Int [[GWAS]])
genNullGWAS gen gwasGroup = fmap (M.fromListWith (<>) . concat) $ replicateM 1000 $ do
    v <- uniformShuffle gwas gen
    return $ nubSortBy (comparing fst) $ map (\x -> (length x, [x])) $ group' sizes $ V.toList v
  where
    group' (x:xs) ys = take x ys : group' xs (drop x ys)
    group' _ _ = []
    sizes = nubSort $ map length gwasGroup
    gwas = V.fromList $ concat gwasGroup

--parMapDF :: (Double -> Double) -> DF.DataFrame Double -> DF.DataFrame Double
--parMapDF f df = 

getContext :: [Gene] -> ([BED3], [BED3])
getContext genes = (nubSort . concat) *** (nubSort . concat) $ unzip $
    flip map genes $ \Gene{..} -> 
        let tss | geneStrand = geneLeft : map transLeft geneTranscripts
                | otherwise = geneRight : map transRight geneTranscripts
            within2000 = map (\x -> BED3 geneChrom (x-2000) (x+2000)) tss
            within200 = map (\x -> BED3 geneChrom (x-200) (x+200)) tss
        in (within200, within2000)

empiricalFDR :: [Double]  -- ^ null distribution
             -> [Double]  -- ^ real distribution
             -> M.Map Double Double
empiricalFDR nullDistr realDistr = M.fromList $
    map (\(p, (fp, tp)) -> (fromJust p, fromIntegral fp / fromIntegral tp)) $
    filter (isJust . fst) $ scanl f (Nothing, (0, 0)) $ groupBy ((==) `on` fst) $
    sortBy (comparing fst) $ zip nullDistr (repeat False) <> zip realDistr (repeat True)
  where
    f (_, (fp, tp)) xs =
        let (tp', fp') = count xs
        in if tp' /= 0
            then (Just $ fst $ head xs, (fp + fp', tp + tp'))
            else (Nothing, (fp + fp', tp))
    count xs = let (a, b) = partition snd xs in (length a, length b)

adjustP :: [Double] -> IO [Double]
adjustP ps = R.runRegion $ do
    r <- [r| p.adjust(10^ps_hs, method = "fdr") |]
    R.unSomeSEXP r $ \x -> case H.hexp x of
        H.Real x'  -> return $ toList x'

gwasBuilder = do
    node "Read_GWAS" [| \() -> do
        gwasFl <- lookupConfig "gwas_catalog"
        dir <- (<> "/GWAS_catalog/") <$> lookupConfig "output_dir"
        let output = dir <> "/gwas_summary.txt"
            output2 = dir <> "/gwas.bin"
            output3 = dir <> "/gwas_info.txt"
        liftIO $ do
            shelly $ mkdir_p dir
            let f x = (_gwas_trait x, _gwas_pmid x)
            allGwas <- groupBy ((==) `on` f) . sortBy (comparing f) <$> readGWASAssoc gwasFl

            let gwas = filter ((>=10) . length) $ map filterGWAS $
                    filter ((>=20000) . _gwas_sample_size . head) allGwas
            withFile output WriteMode $ \h -> do
                hPutStrLn h $ printf "Number of GWAS studies downloaded: %d" (length allGwas)
                hPutStrLn h $ printf "Number of GWAS studies after filtering: %d" (length gwas)
                hPutStrLn h $ printf "Number of SNPs: %d" (length $ nubSort $ map _gwas_snp $ concat gwas)
            T.writeFile output3 $ T.unlines $ map (T.intercalate "\t") $
                sortBy (comparing ((read :: String -> Int) . T.unpack . (!!1))) $ flip map gwas $ \x -> 
                    let gwasNames = _gwas_trait (head x) <> "+" <> _gwas_pmid (head x)
                    in [gwasNames, T.pack $ show $ length x, T.pack $ show $ _gwas_sample_size $ head x]
            encodeFile output2 gwas
            return output2
        |] $ return ()
    node "Gen_Null_GWAS" [| \gwasFl -> do
        dir <- (<> "/GWAS_catalog/Null_GWAS/") <$> lookupConfig "output_dir"
        liftIO $ do
            shelly $ mkdir_p dir
            gen <- create
            gwas <- decodeFile gwasFl :: IO [[GWAS]]
            nullGwas <- genNullGWAS gen gwas
            fmap M.fromList $ forM (M.toList nullGwas) $ \(n, x) -> do
                let output = dir <> show n <> ".bin"
                encodeFile output x
                return (n, output)
        |] $ return ()
    path ["Read_GWAS", "Gen_Null_GWAS"]

        {-
    uNode "HEA_Prep" [| \(gwas, nullGwas) -> do
        cluster <- lookupConfig "all_cluster_peak"
        liftIO $ do
            peaks <- fmap (filter (".narrowPeak.gz" `T.isSuffixOf`)) $ shelly $ lsT cluster
            let peakFls = flip map peaks $ \fl -> 
                    let nm = fst $ T.breakOn ".narrowPeak.gz" $ snd $ T.breakOnEnd "/" fl
                    in (nm, T.unpack fl)
            return (gwas, nullGwas, peakFls)
        |]
    namespace "HEA" $ enrichmentAnalysis "/GWAS_catalog/HEA/"
    ["Read_GWAS", "Gen_Null_GWAS"] ~> "HEA_Prep"
    path ["HEA_Prep", "HEA_Enrichment"]
    path ["HEA_Prep", "HEA_Null_Enrichment_Prep"]

    node "HEA_Summary" [| \(gwasFl, fdrFl) -> do
        creFl <- lookupConfig "cre"
        liftIO $ do
            fdr <- DF.readTable fdrFl
            print $ length $ filter (<=0.01) $ Mat.toList $ DF._dataframe_data fdr

            let f x = (_gwas_trait x <> "+" <> _gwas_pmid x) `elem` DF.rowNames fdr
                g x = BED3 (B.pack $ T.unpack $ _gwas_chr x) (_gwas_pos x) (_gwas_pos x + 1)
            gwas <- nubSort . map g . filter f . concat <$> (decodeFile gwasFl :: IO [[GWAS]])
            cres <- runResourceT $ runConduit $ streamBedGzip creFl .| sinkList :: IO [BED3]
            n <- runConduit $ yieldMany gwas .| intersectBed cres .| lengthC
            print n
            print $ fromIntegral n / fromIntegral (length gwas)
        |] $ return ()
    ["Read_GWAS", "HEA_FDR"] ~> "HEA_Summary"

    node "EpiMap_Summary" [| \(gwasFl, fdrFl) -> do
        creFl <- lookupConfig "EpiMap_cre"
        liftIO $ do
            fdr <- DF.readTable fdrFl
            print $ length $ filter (<=0.01) $ Mat.toList $ DF._dataframe_data fdr

            let f x = (_gwas_trait x <> "+" <> _gwas_pmid x) `elem` DF.rowNames fdr
                g x = BED3 (B.pack $ T.unpack $ _gwas_chr x) (_gwas_pos x) (_gwas_pos x + 1)
            gwas <- nubSort . map g . filter f . concat <$> (decodeFile gwasFl :: IO [[GWAS]])
            cres <- runResourceT $ runConduit $ streamBedGzip creFl .| sinkList :: IO [BED3]
            n <- runConduit $ yieldMany gwas .| intersectBed cres .| lengthC
            print n
            print $ fromIntegral n / fromIntegral (length gwas)
        |] $ return ()
    ["Read_GWAS", "EpiMap_FDR"] ~> "EpiMap_Summary"
    -}


    node "Lift_EpiMap" [| \() -> do
        epimap <- lookupConfig "EpiMap"
        hg19ToHg38 <- lookupConfig "hg19_to_hg38"
        dir <- (<> "/EpiMap/") <$> lookupConfig "output_dir"
        liftIO $ do
            shelly $ mkdir_p dir
            peakFls <- fmap (filter (".bed.gz" `T.isSuffixOf`)) $ shelly $ lsT $ epimap
            forM peakFls $ \fl -> do
                let output = dir <> "/" <> T.unpack nm <> ".bed.gz"
                    nm = fst $ T.breakOn "_hg19_enhancer_list.bed.gz" $ snd $ T.breakOnEnd "/" fl
                beds <- runResourceT $ runConduit $ sourceFile (T.unpack fl) .|
                    multiple ungzip .| linesUnboundedAsciiC .|
                    (dropC 1 >> mapC fromLine) .| sinkList :: IO [BED3]
                beds' <- liftOver hg19ToHg38 beds
                runResourceT $ runConduit $ yieldMany beds' .| sinkFileBedGzip output
                return (nm, output)
        |] $ return ()
        {-
    namespace "EpiMap" $ enrichmentAnalysis "/GWAS_catalog/EpiMap/"
    ["Read_GWAS", "Gen_Null_GWAS", "Lift_EpiMap"] ~> "EpiMap_Enrichment"
    ["Read_GWAS", "Gen_Null_GWAS", "Lift_EpiMap"] ~> "EpiMap_Null_Enrichment_Prep"

    uNode "CRM_Prep" [| \(gwas, nullGwas) -> liftIO $ do
        peaks <- fmap (filter (".bed" `T.isSuffixOf`)) $ shelly $ lsT "../CRE_Module/output/modules/"
        let peakFls = flip map peaks $ \fl -> 
                let nm = fst $ T.breakOn ".bed" $ snd $ T.breakOnEnd "/" fl
                in (nm, T.unpack fl)
        return (gwas, nullGwas, peakFls)
        |]
    namespace "CRM" $ enrichmentAnalysis "/GWAS_catalog/CRM/"
    ["Read_GWAS", "Gen_Null_GWAS"] ~> "CRM_Prep"
    path ["CRM_Prep", "CRM_Enrichment"]
    path ["CRM_Prep", "CRM_Null_Enrichment_Prep"]
    -}

    node "Merged_Peaks" [| \() -> do
        dir <- (<> "/GWAS_catalog/") <$> lookupConfig "output_dir"
        creFl1 <- lookupConfig "cre"
        creFl2 <- lookupConfig "EpiMap_cre"
        let output = dir <> "/background.bed.gz"
        liftIO $ do
            peaks1 <- runResourceT $ runConduit $ streamBedGzip creFl1 .| sinkList :: IO [BED3]
            peaks2 <- runResourceT $ runConduit $ streamBedGzip creFl2 .| sinkList :: IO [BED3]
            runResourceT $ runConduit $ mergeBed (peaks1 <> peaks2) .| sinkFileBedGzip output
            return output
        |] $ return ()
    uNode "HEA2_Prep" [| \(gwas, nullGwas, creFl) -> do
        cluster <- lookupConfig "all_cluster_peak"
        liftIO $ do
            peaks <- fmap (filter (".narrowPeak.gz" `T.isSuffixOf`)) $ shelly $ lsT cluster
            let peakFls = flip map peaks $ \fl -> 
                    let nm = fst $ T.breakOn ".narrowPeak.gz" $ snd $ T.breakOnEnd "/" fl
                    in (nm, T.unpack fl)
            return (gwas, nullGwas, creFl, peakFls)
        |]
    namespace "HEA2" $ enrichmentAnalysis3 "/GWAS_catalog/HEA2/"
    ["Read_GWAS", "Gen_Null_GWAS", "Merged_Peaks"] ~> "HEA2_Prep"
    path ["HEA2_Prep", "HEA2_Enrichment_Prep"]

    node "HEA3_Prep" [| \(gwas, nullGwas, creFl) -> do
        dir <- (<> "/GWAS_catalog/HEA3/") <$> lookupConfig "output_dir"
        cluster <- lookupConfig "all_cluster_peak"
        gencode <- lookupConfig "gencode"
        liftIO $ do
            shelly $ mkdir_p dir
            (within200, _) <- getContext <$> readGenes gencode
            let tree = bedToTree undefined $ zip within200 $ repeat ()
            peaks <- fmap (filter (".narrowPeak.gz" `T.isSuffixOf`)) $ shelly $ lsT cluster
            peakFls <- forM peaks $ \fl -> do
                let nm = fst $ T.breakOn ".narrowPeak.gz" $ snd $ T.breakOnEnd "/" fl
                    output = dir <> T.unpack nm <> ".bed.gz"
                runResourceT $ runConduit $ streamBedGzip (T.unpack fl) .|
                    filterC (\x -> not $ isIntersected tree (x :: BED3)) .| sinkFileBedGzip output
                return (nm, output)
            return (gwas, nullGwas, creFl, peakFls)
        |] $ return ()
    namespace "HEA3" $ enrichmentAnalysis3 "/GWAS_catalog/HEA3/"
    ["Read_GWAS", "Gen_Null_GWAS", "Merged_Peaks"] ~> "HEA3_Prep"
    path ["HEA3_Prep", "HEA3_Enrichment_Prep"]

    node "HEA4_Prep" [| \(gwas, nullGwas, creFl) -> do
        dir <- (<> "/GWAS_catalog/HEA4/") <$> lookupConfig "output_dir"
        cluster <- lookupConfig "all_cluster_peak"
        gencode <- lookupConfig "gencode"
        liftIO $ do
            shelly $ mkdir_p dir
            (within200, within2000) <- getContext <$> readGenes gencode
            let tree = bedToTree undefined $ zip (within200 <> within2000) $ repeat ()
            peaks <- fmap (filter (".narrowPeak.gz" `T.isSuffixOf`)) $ shelly $ lsT cluster
            peakFls <- forM peaks $ \fl -> do
                let nm = fst $ T.breakOn ".narrowPeak.gz" $ snd $ T.breakOnEnd "/" fl
                    output = dir <> T.unpack nm <> ".bed.gz"
                runResourceT $ runConduit $ streamBedGzip (T.unpack fl) .|
                    filterC (\x -> not $ isIntersected tree (x :: BED3)) .| sinkFileBedGzip output
                return (nm, output)
            return (gwas, nullGwas, creFl, peakFls)
        |] $ return ()
    namespace "HEA4" $ enrichmentAnalysis3 "/GWAS_catalog/HEA4/"
    ["Read_GWAS", "Gen_Null_GWAS", "Merged_Peaks"] ~> "HEA4_Prep"
    path ["HEA4_Prep", "HEA4_Enrichment_Prep"]

    namespace "EpiMap2" $ enrichmentAnalysis3 "/GWAS_catalog/EpiMap2/"
    ["Read_GWAS", "Gen_Null_GWAS", "Merged_Peaks", "Lift_EpiMap"] ~> "EpiMap2_Enrichment_Prep"
 
    {-
    uNode "HEA2_Prep" [| \(gwas, creFl) -> do
        cluster <- lookupConfig "all_cluster_peak"
        liftIO $ do
            peaks <- fmap (filter (".narrowPeak.gz" `T.isSuffixOf`)) $ shelly $ lsT cluster
            let peakFls = flip map peaks $ \fl -> 
                    let nm = fst $ T.breakOn ".narrowPeak.gz" $ snd $ T.breakOnEnd "/" fl
                    in (nm, T.unpack fl)
            return (gwas, creFl, peakFls)
        |]
    namespace "HEA2" $ enrichmentAnalysis2 "/GWAS_catalog/HEA2/"
    ["Read_GWAS", "Merged_Peaks"] ~> "HEA2_Prep"
    path ["HEA2_Prep", "HEA2_Enrichment"]

    namespace "EpiMap2" $ enrichmentAnalysis2 "/GWAS_catalog/EpiMap2/"
    ["Read_GWAS", "Merged_Peaks", "Lift_EpiMap"] ~> "EpiMap2_Enrichment"

    uNode "CRM2_Prep" [| \gwas -> do
        creFl <- lookupConfig "cre"
        liftIO $ do
            peaks <- fmap (filter (".bed" `T.isSuffixOf`)) $ shelly $ lsT "../CRE_Module/output/modules/"
            let peakFls = flip map peaks $ \fl -> 
                    let nm = fst $ T.breakOn ".bed" $ snd $ T.breakOnEnd "/" fl
                    in (nm, T.unpack fl)
            return (gwas, creFl, peakFls)
        |]
    namespace "CRM2" $ enrichmentAnalysis2 "/GWAS_catalog/CRM2/"
    path ["Read_GWAS", "CRM2_Prep", "CRM2_Enrichment"]
    -}

    node "Combined_FDR" [| \(input1, input2) -> do
        dir <- (<> "/GWAS_catalog/") <$> lookupConfig "output_dir"
        adultAnnoFl <- lookupConfig "cluster_annotation"
        fetalAnnoFl <- lookupConfig "fetal_annotation"
        let output1 = dir <> "/fold_change.tsv"
            output2 = dir <> "/pvalue.tsv"
            output3 = dir <> "/fdr.tsv"
        liftIO $ do
            anno1 <- map (\(a,b) -> ("A_" <> a, "Adult " <> b)) <$> readAnno adultAnnoFl
            anno2 <- map (\(a,b) -> ("F_" <> a, "Fetal " <> b)) <$> readAnno fetalAnnoFl
            let input = input1 <> input2
                df = changeName (anno1 <> anno2) $
                    DF.filterRows (\_ v -> V.minimum (V.map (^._3) v) <= 0.001) $
                    DF.transpose $ DF.mkDataFrame (map fst input) (map fst $ snd $ head input) $ 
                    map (map snd . snd) input
            DF.writeTable output1 (T.pack . show) $ DF.map (^._1) df
            DF.writeTable output2 (T.pack . show) $ DF.map (^._2) df
            DF.writeTable output3 (T.pack . show) $ DF.map (^._3) df
            return (output1, output2, output3)
        |] $ return ()
    node "Plot" [| \(input1, input2, input3) -> do
        metadata <- lookupConfig "EpiMap_metadata"
        heaData <- lookupConfig "hea_metadata"
        liftIO $ do
            epiMapGroup <- map ((\xs -> (xs!!0, xs!!1)) . T.splitOn "\t") . tail . T.lines <$> T.readFile metadata
            heaGroup <- map ((\xs -> (xs!!1, xs!!2)) . T.splitOn "\t" . T.strip) . T.lines <$> T.readFile heaData
            df <- DF.map (\x -> if x <= 0.001 then 1 else 0) <$> DF.readTable input3
            let isHEA x = not $ "BSS" `T.isPrefixOf` x
                hea = filter isHEA $ DF.colNames df
                epimap = filter (not . isHEA) $ DF.colNames df
                heaDF = df `DF.csub` hea
                epiDF = df `DF.csub` epimap
                heaTraits = DF.rowNames $ DF.filterRows (\_ v -> V.any (>0) v) heaDF
                epiTraits = DF.rowNames $ DF.filterRows (\_ v -> V.any (>0) v) epiDF
                sharedTraits = S.toList $ S.fromList heaTraits `S.intersection` S.fromList epiTraits
                heaSpecific = S.toList $ S.fromList heaTraits `S.difference` S.fromList sharedTraits
                epiSpecific = S.toList $ S.fromList epiTraits `S.difference` S.fromList sharedTraits
                nameMap = M.fromList $ epiMapGroup <> heaGroup
                lookupNameFn x = M.findWithDefault (error $ show x) (fst $ T.breakOn "_" x) nameMap

            fc <- {-DF.map (\x -> if x <= 1 then 0 else logBase 2 x) <$>-} DF.map negate <$> DF.readTable input2
            let heaFC = M.fromList $ zip (DF.rowNames fc) $ map V.maximum $
                    Mat.toRows $ DF._dataframe_data $ fc `DF.csub` hea
                epiFC = M.fromList $ zip (DF.rowNames fc) $ map V.maximum $
                    Mat.toRows $ DF._dataframe_data $ fc `DF.csub` epimap

            mapM_ T.putStrLn sharedTraits
            putStrLn "+++"
            mapM_ T.putStrLn heaSpecific
            putStrLn "+++"
            mapM_ T.putStrLn epiSpecific

            plotHeatmap "1.pdf" lookupNameFn
                ( [ heaDF `DF.rsub` sharedTraits
                  , heaDF `DF.rsub` heaSpecific
                  , heaDF `DF.rsub` epiSpecific ]
                , [ epiDF `DF.rsub` sharedTraits
                  , epiDF `DF.rsub` heaSpecific
                  , epiDF `DF.rsub` epiSpecific ]
                )
                heaFC
                epiFC
                [ "Heel bone mineral density+30048462"
                , "Mean corpuscular volume+19862010"
                , "Lung function (FEV1/FVC)+30595370"
                , "Platelet distribution width+32888494"
                , "Atrial fibrillation+30061737"
                , "Thyroid stimulating hormone levels+32769997"
                , "HDL cholesterol levels+30698716"
                , "Educational attainment (MTAG)+30038396"
                , "Asthma+30929738"
                , "Peak expiratory flow+30804560"
                , "Hair color+30595370"
                , "Colorectal cancer+31089142"
                , "Inflammatory bowel disease+28067908"
                , "Total testosterone levels+32042192"
                , "Alzheimer's disease (late onset)+30617256"
                , "Rheumatoid arthritis+33310728"
                , "Intake of sweets+31005972"
                , "Blood urea nitrogen levels+31152163"
                , "Diastolic blood pressure+29403010"
                , "Triglyceride levels+32203549"
                , "Basal cell carcinoma+27539887"
                , "Prostate cancer+29892016"
                , "Waist-hip ratio+30575882"
                , "Monocyte count+27863252"

                , "Factor VIII levels+30586737"
                , "Left ventricle wall thickness+28394258"
                , "Cholesterol, total+19060911"
                , "Bipolar disorder+33169155"
                , "Sunburns+30595370"
                , "Type 2 diabetes+30054458"
                , "Systolic blood pressure+30595370"
                , "Medication use (diuretics)+31015401"
                , "Cognitive performance+30038396"
                , "Corneal hysteresis+32716492"
                , "Reaction time+29844566"
                , "Calcium levels+20705733"
                , "Diverticular disease+30177863"
                , "Childhood body mass index+26604143"
                , "Body mass index+30595370"
                , "Neutrophil percentage of granulocytes+27863252"

                -- unclear which single cell type this should be associated with
                -- - for Epimap, it associates with one iPSC and one immune cell cancer dataset
                , "Waist-to-hip ratio adjusted for BMI (additive genetic model)+30778226"

                -- unclear which single cell type this should be associated with -
                -- for Epimap, it associates with one embryonic adrenal gland dataset
                , "Coffee consumption (cups per day)+31837886"

                -- unclear which single cell type this should be associated with -
                -- for Epimap, it associates with many seemingly unrelated datasets
                , "Birth length (MTAG)+31681408"

                -- unclear which single cell type this should be associated with -
                -- for Epimap, it associates with one hematopoietic stem cell dataset)
                , "Medication use (adrenergics, inhalants)+31015401"

                -- unclear which single cell type this should be associated with -
                -- for Epimap, it associates with one liver dataset)
                , "Medication use (HMG CoA reductase inhibitors)+31015401"

                -- unclear which single cell type this should be associated with -
                -- for Epimap, it associates with one neural progenitor dataset
                , "Eudaimonic well-being+30279531"

                -- unclear which single cell type this should be associated with -
                -- for Epimap, it associates with a large number of immune cell datasets
                , "Life satisfaction+27089181"

                -- ((unclear which single cell type this should be associated with - for Epimap, it associates with a single peyer’s patch intestinal dataset)
                , "Parental longevity (father's age at death)+31484785"
                -- (This is enriched in one EpiMap Kidney dataset, of which there are very many - maybe this would be enriched in adult kidney, if we had it?)
                , "Chronic kidney disease+31152163"
                -- (This is enriched in EpiMap Kidney datasets, maybe this would be enriched in adult kidney if we had it?)
                , "Estimated glomerular filtration rate+30604766" 
                -- (This is an eye trait, but is weirdly enriched in reproductive tissues from EpiMap and not eye tissues or eye cell types)
                , "Optic cup area+28073927" 
                -- (This is enriched in one immune cell cancer dataset from EpiMap but not melanocytes from HEA or Epimap (both are close but non significant))
                , "Low tan response+29739929"
                ]
            {-
            let (names, values, groups) = unzip3 $ flip concatMap sharedTraits $ \x -> 
                    let cats1 = nubSort $
                            map (lookupNameFn . fst) $
                            filter ((>0) . snd) $ zip (DF.colNames heaDF) $
                            V.toList $ heaDF `DF.rindex` x
                        cats2 = nubSort $
                            map (lookupNameFn . fst) $
                            filter ((>0) . snd) $ zip (DF.colNames epiDF) $
                            V.toList $ epiDF `DF.rindex` x
                    in [ (T.unpack x, fromIntegral (length cats1) / n1 :: Double, "HEA" :: String)
                       , (T.unpack x, fromIntegral (length cats2) / n2, "EpiMap") ]
                n1 = fromIntegral $ length $ nubSort $ map snd heaGroup
                n2 = fromIntegral $ length $ nubSort $ map snd epiMapGroup
                order = map (^._1) $ sortBy (flip (comparing (^._2))) $
                    filter (\x -> x^._3 == "EpiMap") $ zip3 names values groups
            R.runRegion $ do
                myTheme
                [r| library("ggplot2")
                    df <- data.frame(x=names_hs, y=values_hs * 100, group=groups_hs)
                    df$x <- factor(df$x, levels = order_hs)
                    fig <- ggplot(df, aes(x,y, group=group, colour=group)) +
                        geom_point() +
                        #geom_line() +
                        coord_flip() +
                        myTheme()
                    ggsave("2.pdf", fig, width=60, height=150, unit="mm", useDingbats=F)
                |]
                return ()
            -}
        |] $ return ()
    ["HEA4_Enrichment", "EpiMap2_Enrichment"] ~> "Combined_FDR"
    path ["Combined_FDR", "Plot"]
 

enrichmentAnalysis :: String -> Builder ()
enrichmentAnalysis prefix = do
    node "Enrichment" [| \(gwasFl, _, peakFls) -> do
        dir <- (<> prefix) <$> lookupConfig "output_dir"
        let output = dir <> "/enrichment_pvalue.tsv"
        liftIO $ do
            shelly $ mkdir_p dir
            gwas <- decodeFile gwasFl :: IO [[GWAS]]
            let gwasNames = flip map gwas $ \x -> _gwas_trait (head x) <> "+" <> _gwas_pmid (head x)
                totalBg = length $ nubSort $ map _gwas_snp $ concat gwas
            res <- forM peakFls $ \(_, peakFl) -> do
                peaks <- ( runResourceT $ runConduit $ if ".gz" `T.isSuffixOf` T.pack peakFl
                    then streamBedGzip peakFl .| sinkList 
                    else streamBed peakFl .| sinkList ) :: IO [BED3]
                let tree = bedToTree const $ zip peaks $ repeat ()
                    intersections = flip map gwas $ \g ->
                        let snp = flip map g $ \x -> BED (B.pack $ T.unpack $ _gwas_chr x)
                                (_gwas_pos x) (_gwas_pos x + 1) (Just $ B.pack $ T.unpack $ _gwas_snp x) Nothing Nothing
                        in ( S.fromList $ runIdentity $ runConduit $ yieldMany snp .|
                            filterC (isIntersected tree) .| mapC (fromJust . (^.name)) .| sinkList, length g )
                    overlapBg = S.size $ S.unions $ map fst intersections
                enrichment (map (\(s, n) -> (S.size s, n)) intersections) overlapBg totalBg
            DF.writeTable output (T.pack . show) $ DF.orderDataFrame id $ DF.mkDataFrame (map fst peakFls) gwasNames res
            return output
        |] $ return ()
    uNode "Null_Enrichment_Prep" [| \(_, nullGwas, peaks) -> do
        return $ zip (chunksOf 10 $ map snd peaks) (repeat nullGwas)
        |]
    nodePar "Null_Enrichment" [| \(peakFls, gwasFl) -> liftIO $ do
        gwas <- decodeFile gwasFl :: IO [[GWAS]]
        let totalBg = length $ nubSort $ map _gwas_snp $ concat gwas
        fmap concat $ forM peakFls $ \peakFl -> do
            peaks <- runResourceT $ runConduit $ streamBedGzip peakFl .|
                sinkList :: IO [BED3]
            let tree = bedToTree const $ zip peaks $ repeat ()
                intersections = flip map gwas $ \g ->
                    let snp = flip map g $ \x -> BED (B.pack $ T.unpack $ _gwas_chr x)
                            (_gwas_pos x) (_gwas_pos x + 1) (Just $ B.pack $ T.unpack $ _gwas_snp x) Nothing Nothing
                    in ( S.fromList $ runIdentity $ runConduit $ yieldMany snp .|
                        filterC (isIntersected tree) .| mapC (fromJust . (^.name)) .| sinkList, length g )
                overlapBg = S.size $ S.unions $ map fst intersections
            enrichment (map (\(s, n) -> (S.size s, n)) intersections) overlapBg totalBg
        |] $ return ()
    path ["Null_Enrichment_Prep", "Null_Enrichment"]

    node "FDR" [| \tableFl -> do
        dir <- (<> prefix) <$> lookupConfig "output_dir"
        adultAnnoFl <- lookupConfig "cluster_annotation"
        fetalAnnoFl <- lookupConfig "fetal_annotation"
        let output = dir <> "/enrichment_fdr.tsv"
        liftIO $ do
            anno1 <- map (\(a,b) -> ("A_" <> a, "Adult " <> b)) <$> readAnno adultAnnoFl
            anno2 <- map (\(a,b) -> ("F_" <> a, "Fetal " <> b)) <$> readAnno fetalAnnoFl
            df <- DF.readTable tableFl
            fdr <- adjustP $ Mat.toList $ DF._dataframe_data df
            let df' = DF.orderDataFrame id $ changeName (anno1 <> anno2) $
                    DF.filterRows (\_ v -> V.minimum v <= 0.01) $ DF.transpose $
                    df{DF._dataframe_data = Mat.fromLists $ chunksOf (snd $ DF.dim df) fdr}
            DF.writeTable output (T.pack . show) df'
            return output
        |] $ return ()
    ["Enrichment"] ~> "FDR"


enrichmentAnalysis2 :: String -> Builder ()
enrichmentAnalysis2 prefix = do
    node "Enrichment" [| \(gwasFl, creFl, peakFls) -> do
        dir <- (<> prefix) <$> lookupConfig "output_dir"
        let output = dir <> "/enrichment_pvalue.tsv"
        liftIO $ do
            shelly $ mkdir_p dir
            cre <- runResourceT $ runConduit $ streamBedGzip creFl .| sinkList :: IO [BED3]
            let creTree = bedToTree const $ zip cre [0 :: Int ..]
            gwas <- decodeFile gwasFl :: IO [[GWAS]]
            let gwasNames = flip map gwas $ \x -> _gwas_trait (head x) <> "+" <> _gwas_pmid (head x)
                totalBg = length cre
            res <- forM peakFls $ \(_, peakFl) -> do
                peaks <- fmap S.fromList $ runResourceT $ runConduit $
                    (if ".gz" `T.isSuffixOf` T.pack peakFl then streamBedGzip peakFl else streamBed peakFl) .|
                    concatMapC (\x -> map snd $ queryIntersect (x :: BED3) creTree) .|
                    sinkList
                let totalFg = S.size peaks
                    (stats, fcs) = unzip $ flip map gwas $ \g ->
                        let snp = flip map g $ \x -> BED3 (B.pack $ T.unpack $ _gwas_chr x) (_gwas_pos x) (_gwas_pos x + 1)
                            overlapCRE = S.fromList $ concatMap (map snd . (`queryIntersect` creTree)) snp
                            overlapBg = S.size overlapCRE
                            overlapFg = S.size $ overlapCRE `S.intersection` peaks
                        in ( (overlapFg, totalFg, overlapBg, totalBg)
                           , (fromIntegral overlapFg / fromIntegral totalFg) / (fromIntegral overlapBg / fromIntegral totalBg) )
                enrichment' stats
            DF.writeTable output (T.pack . show) $ DF.mkDataFrame (map fst peakFls) gwasNames res
            return output
        |] $ return ()
    node "FDR" [| \tableFl -> do
        dir <- (<> prefix) <$> lookupConfig "output_dir"
        adultAnnoFl <- lookupConfig "cluster_annotation"
        fetalAnnoFl <- lookupConfig "fetal_annotation"
        let output = dir <> "/enrichment_fdr.tsv"
        liftIO $ do
            anno1 <- map (\(a,b) -> ("A_" <> a, "Adult " <> b)) <$> readAnno adultAnnoFl
            anno2 <- map (\(a,b) -> ("F_" <> a, "Fetal " <> b)) <$> readAnno fetalAnnoFl
            df <- DF.transpose <$> DF.readTableWith (read . B.unpack) tableFl :: IO (DF.DataFrame Double)
            fdr <- mapM adjustP $ Mat.toLists $ DF._dataframe_data df
            let df' = DF.orderDataFrame id $ changeName (anno1 <> anno2) $
                    DF.filterRows (\_ v -> V.minimum v <= 0.01) $
                    df{DF._dataframe_data = Mat.fromLists fdr}
            DF.writeTable output (T.pack . show) df'
            return output
        |] $ return ()
    ["Enrichment"] ~> "FDR"

enrichmentAnalysis3 :: String -> Builder ()
enrichmentAnalysis3 prefix = do
    uNode "Enrichment_Prep" [| \(gwasFl, nullGwas, creFl, peakFls) -> return $
        flip map peakFls $ \peak -> (gwasFl, nullGwas, creFl, peak)
        |]
    nodePar "Enrichment" [| \(gwasFl, nullGwasFl, creFl, (nm, peakFl)) -> liftIO $ do
        cre <- runResourceT $ runConduit $ streamBedGzip creFl .| sinkList :: IO [BED3]
        let creTree = bedToTree const $ zip cre [0 :: Int ..]
        gwas <- decodeFile gwasFl :: IO [[GWAS]]
        let gwasNames = flip map gwas $ \x -> _gwas_trait (head x) <> "+" <> _gwas_pmid (head x)
            totalBg = length cre

        peaks <- fmap S.fromList $ runResourceT $ runConduit $
            (if ".gz" `T.isSuffixOf` T.pack peakFl then streamBedGzip peakFl else streamBed peakFl) .|
            concatMapC (\x -> map snd $ queryIntersect (x :: BED3) creTree) .|
            sinkList
        let totalFg = S.size peaks
            getStat g = 
                let snp = flip map g $ \x -> BED3 (B.pack $ T.unpack $ _gwas_chr x) (_gwas_pos x) (_gwas_pos x + 1)
                    overlapCRE = S.fromList $ concatMap (map snd . (`queryIntersect` creTree)) snp
                    overlapBg = S.size overlapCRE
                    overlapFg = S.size $ overlapCRE `S.intersection` peaks
                in (overlapFg, totalFg, overlapBg, totalBg)
            getFc (a,b,c,d) = (fromIntegral a / fromIntegral b) / (fromIntegral c / fromIntegral d) :: Double

        nullGwas' <- fmap force $ forM nullGwasFl $ \x -> do
            r <- decodeFile x :: IO [[GWAS]]
            fmap (force . sort) $ enrichment' $ map getStat r
        let f n p = let nullDistr = M.findWithDefault undefined n nullGwas'
                    in fromIntegral (length $ fst $ span (<p) nullDistr) / fromIntegral (length nullDistr) :: Double
            stats = map getStat gwas
            fcs = map getFc stats
        ps <- enrichment' stats
        return (nm, zip gwasNames $ zip3 fcs ps $ zipWith f (map length gwas) ps)
        |] $ return ()
    path ["Enrichment_Prep", "Enrichment"]
