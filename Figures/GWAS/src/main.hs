{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell   #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE QuasiQuotes       #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE PartialTypeSignatures #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE GADTs #-}
import           Data.Colour       
import AI.Clustering.Hierarchical hiding (normalize)
import Data.Colour.Names
import HEA
import Data.Int (Int32)
import qualified Data.Text as T
import qualified Data.Text.IO as T
import qualified Data.Vector.Unboxed as U
import qualified Data.ByteString.Char8 as B
import Bio.Utils.Functions
import Data.Monoid
import qualified Data.Matrix as Mat
import Control.Concurrent.Async (mapConcurrently)
import Control.Arrow
import Bio.Data.Bed
import Bio.Data.Bed.Utils
import Bio.Data.Bed.Types
import Bio.Data.Bam
import Data.Char (toUpper)
import Data.Either
import Shelly (mkdir_p, shelly, lsT, fromText)
import Taiji.Prelude
import qualified Data.Map.Strict as M
import qualified Data.Set as Set
import Control.DeepSeq
import Statistics.Correlation (pearson)
import Statistics.Sample
import Data.Binary
import Data.List.Ordered (nubSortBy, nubSort)
import qualified Data.Vector as V
import qualified Taiji.Utils.DataFrame as DF
import GHC.Generics (Generic)
import Graph
import GWAS
import IGraph
import IGraph.Algorithms

import qualified Data.Matrix.Dynamic as Dyn
import qualified Data.Matrix.Static.Dense as D
import qualified Data.Matrix.Static.Sparse as S
import qualified Data.Matrix.Static.Generic as G
import Data.Matrix.Static.IO (fromMM')

import Taiji.Prelude hiding (groupBy)
import Data.Vector.SEXP (toList)
import qualified Language.R.HExp as H
import qualified Language.R                        as R
import           Language.R.QQ
import Language.R.HExp

jitterPlot :: FilePath
           -> Double
           -> [(String, String, Double)]
           -> IO ()
jitterPlot output fdr input = R.runRegion $ do
    myTheme
    [r| library("ggplot2")
        df <- data.frame(trait = trait_hs, group = label_hs, val = ps_hs)
        fig <- ggplot(df, aes(group, val)) + geom_jitter() +
            myTheme() + myFill() +
            facet_wrap(vars(trait), ncol=3, scales="free_y")
        ggsave(output_hs, fig, width=180, height=60, unit="mm", useDingbats=F)
    |]
    return ()
  where
    (trait, label, ps) = unzip3 input

plotSingleGWAS :: FilePath -> Double
               -> [(String, String)]
               -> [ ( String    -- ^ trait name
                    , [(String, String, Double)] )]
               -> IO ()
plotSingleGWAS output fdr colorScheme input = R.runRegion $ do
    myTheme
    [r| library("ggplot2")
        library("ggrepel")
        df <- data.frame(trait=trait_hs, lineage=factor(lineage_hs, levels=level_hs), cell=cell_hs, p=p_hs, display=display_hs)
        df$cell = factor(df$cell, levels=cellOrder_hs)
        fig <- ggplot(df, aes(cell, p)) + 
            geom_hline(yintercept=fdr_hs, linetype="dashed", color = "grey50", size=0.2) +
            geom_point(shape=20, aes(color=lineage, size=p>=fdr_hs)) +
            scale_size_manual(values=c(0.2,0.8)) +
            geom_text_repel(aes(label=cell),
                data=subset(df, display==1),
                size=1.7, segment.size = 0.2, segment.color = "grey50"
            ) +
            myTheme() + myFill() +
            theme( axis.text.x=element_blank(), axis.ticks.x=element_blank(),
                panel.grid.major= element_blank(), panel.grid.minor = element_blank() ) +
            labs(x = "222 cell types", y = "-log10(P)") +
            facet_wrap(vars(trait), ncol=1, scales="free_y") +
            scale_color_manual(values = colors_hs) +
            guides(size = FALSE, color=guide_legend(nrow=8,byrow=F))
        ggsave(output_hs, fig, width=70, height=225, unit="mm", useDingbats=F)
    |]
    return ()
  where
    cellOrder = map (^._2) $ sortBy (comparing (^._1)) $ snd $ head input
    (level, colors) = unzip colorScheme
    (trait, lineage, cell, p, display) = unzip5 $ flip concatMap input $ \(trait, dat) ->
        let top3 = head $ drop 3 $ reverse $ sort $ map (^._3) dat
        in flip map dat $ \(lineage, cell, p) -> (trait, lineage, cell, p, if p > top3 && p >= fdr then 1 :: Int32 else 0)

diagonizeDF :: DF.DataFrame Double -> DF.DataFrame Double
diagonizeDF = DF.reorderRows f
  where
    f xs = map (\x -> (x, M.findWithDefault undefined x xs')) names
      where
        xs' = M.fromList xs
        names = map fst $ sortBy (comparing snd) $ map (\(a,b) -> (a, g b)) xs
        g xs = (i, negate v)
          where
            v = z V.! i
            i = V.maxIndex z
            z = scale xs

plotGWAS2 :: FilePath
          -> (T.Text -> T.Text)
          -> [(String, String)]
          -> Set.Set T.Text
          -> DF.DataFrame Double
          -> IO ()
plotGWAS2 output fn colorScheme traits df = R.runRegion $ do
    pvalues <- toRMatrix df'
    [r| library("ggplot2")
        library("ComplexHeatmap")
        split <- factor(split_hs, levels = groups_hs)
        colors2 <- structure(c("#fef0d9", "#fdcc8a", "#fc8d59", "#d7301f"), names = c("0", "1", "2", "3"))
        ha <- HeatmapAnnotation(
            foo = anno_mark(at = cidx_hs, labels = cmarks_hs, side="top", lines_gp = gpar(lwd=0.5), labels_gp=gpar(fontsize=5)),
            bar = anno_block(gp = gpar(lwd=0.3, fill = colors_hs))
        )
        la <- rowAnnotation(
            foo = anno_mark(at = ridx_hs, labels = labels_hs, side="left", lines_gp = gpar(lwd=0.5), labels_gp=gpar(fontsize=5))
        )
        h1 <- Heatmap( pvalues_hs, use_raster=T, raster_quality=10,
            column_split = split, column_gap = unit(0.0, "mm"), column_title = NULL,
            col=colors2, name = "mat",
            cluster_rows=F, cluster_columns=F,
            show_row_names=F, show_row_dend=F, show_column_dend=F, show_column_names=F,
            heatmap_legend_param = list(
                title = "Significance level of LDSC Coefficient",
                title_gp = gpar(fontsize=6),
                #title_position = "left-top",
                direction = "horizontal",
                labels_gp = gpar(fontsize=5)
            ),
            top_annotation = ha,
            left_annotation = la
        )
        pdf(output_hs, width=4.5, height=10)
        draw(h1, heatmap_legend_side="bottom")
        dev.off()
    |]
    return ()
  where
    df' = let d = DF.map (min 6) df in diagonizeDF $ d `DF.csub` organizeDF d
    (labels', ridx) = unzip $ filter (\(x, i) -> i `mod` 4 == 0 || x `Set.member` traits) $
        zip (DF.rowNames df') [1 :: Int32 ..]
    labels = map T.unpack labels'
    colors = map (\x -> fromJust $ lookup x colorScheme) groups
    (cmarks, cidx) = unzip $
        map (\x -> (fst $ head x, snd $ x !! (length x `div` 2))) $
        groupBy ((==) `on` fst) $ zip split [1 :: Int32 ..]
    groups = map head $ group split
    split = map (T.unpack . fn) $ DF.colNames df'
    h = fromIntegral $ fst $ DF.dim df :: Double
    w = fromIntegral $ snd $ DF.dim df :: Double
    organizeDF d = concat $ map (f . fst) $ flatten $ hclust Ward (V.fromList grp) (euclidean `on` snd)
      where
        f xs = flatten $ hclust Ward (V.fromList xs) (euclidean `on` (d `DF.cindex`))
        grp = map (\x -> (x, averageDF $ d `DF.csub` x)) $
            groupBy ((==) `on` fn) $ sortBy (comparing fn) $ DF.colNames d

diseases = 
    [ ("Coronary artery disease", "CAD")
    , ("Chronic obstructive pulmonary disease", "COPD")
    , ("Type 1 diabetes", "T1D")
    , ("Type 2 diabetes", "T2D")
    , ("Osteoarthritis", "OA")
    , ("Atrial fibrillation", "AF")
    , ("Ulcerative colitis", "UC")
    , ("Rheumatoid arthritis", "")
    , ("Stroke", "")
    , ("Autoimmune vitiligo", "")
    , ("Crohns disease", "")
    , ("Divertilular disease", "")
    , ("Carpal tunnel syndrome", "")
    , ("Asthma child onset", "")
    , ("Varicose veins", "")
    , ("Alzheimers disease", "")
    , ("Selective IgA deficiency", "")
    , ("Alzheimers disease", "")
    , ("Atopic dermatitis", "")
    , ("Breast cancer", "")
    , ("Multiple sclerosis", "")
    , ("Primary biliary cirrhosis", "")
    , ("Gout", "")
    , ("Hypertension", "")
    , ("Chronic kidney disease", "")
    , ("Primary sclerosing cholangitis", "")
    , ("Systemic lupus erythematosus", "") ]

data SNP = SNP
    { _snp_location :: BED3
    , _snp_id :: B.ByteString
    , _snp_index :: B.ByteString
    , _snp_log_bayes :: Maybe Double
    , _snp_ppa :: Double
    , _snp_target :: B.ByteString
    , _snp_overlap_cre :: Maybe B.ByteString
    , _snp_tf :: Maybe [(B.ByteString, Double)]
    } deriving (Show, Generic)

instance Binary SNP

mkTFNode :: B.ByteString -> NodeAttr
mkTFNode x = defaultNodeAttributes
    { _nodeLabel=B.unpack x
    , _nodeType = "TF"
    , _displayLabel=False
    , _nodeColour=opaque green }
  where
    list = ["MEIS3"]

mkGeneNode :: B.ByteString -> NodeAttr
mkGeneNode x = defaultNodeAttributes
    { _nodeLabel=B.unpack x
    , _nodeType = "Gene"
    , _nodeColour=opaque gray
    , _displayLabel=display }
  where
    display | x `elem` list = True
            | otherwise = False
    list = ["TGFB1", "IRF8", "SCN10A", "SCN5A"]

mkTraitNode :: B.ByteString -> NodeAttr
mkTraitNode x = defaultNodeAttributes
    { _nodeLabel=B.unpack x
    , _nodeType = "Trait"
    , _nodeColour=opaque red }

mkCellNode :: B.ByteString -> NodeAttr
mkCellNode x = defaultNodeAttributes
    { _nodeLabel=B.unpack x
    , _nodeType = "Cell"
    , _nodeColour=opaque purple }

readGeneType :: FilePath -> IO (M.Map B.ByteString B.ByteString)
readGeneType fl = M.fromList . map (f . last) . filter (\x -> x!!2 == "gene") .
    map (B.split '\t') . dropWhile ((=='#') . B.head) . B.lines <$> B.readFile fl
  where
    f x = let r = map (\[a,b] -> (a,b)) $ filter ((==2) . length) $
                  map (filter (not . B.null) . B.words) $ B.split ';' x
          in (B.tail $ B.init $ fromJust $ lookup "gene_name" r, B.tail $ B.init $ fromJust $ lookup "gene_type" r)

plotOverlapPPA :: FilePath -> [SNP] -> IO ()
plotOverlapPPA output snps = R.runRegion $ do
    myTheme
    [r| library("ggplot2")
        df <- data.frame(label=label_hs, val=val_hs)
        fig <- ggplot(df, aes(x=val, color=label)) +
            geom_density() +
            #geom_histogram(color="#e9ecef", alpha=0.6, position = 'identity') +
            myTheme()
        ggsave(output_hs, fig, width=52, height=40, unit="mm", useDingbats=F)
    |]
    return ()
  where
    (label, val) = unzip $ zip (repeat ("overlap" :: String)) o ++ zip (repeat "nonoverlap") no
    f = map _snp_ppa
    (o, no) = (f *** f) $ partition (isJust . _snp_overlap_cre) $ filter ((>0.1) . _snp_ppa) snps

showSNP :: SNP -> B.ByteString
showSNP snp = B.intercalate "\t"
    [ _snp_target snp, _snp_id snp
    , B.pack $ show $ _snp_ppa snp,
    fromMaybe "." $ _snp_overlap_cre snp,
    fromMaybe "." $ fmap (B.intercalate "," . map (\(x,y) -> x <> ":" <> B.pack (show y))) $ _snp_tf snp ]

readSNP :: FilePath -> IO [SNP]
readSNP fl = validate . map (f . B.split '\t') . B.lines <$> B.readFile fl
  where
    validate xs = let r = M.fromListWithKey (\k x _ -> error $ show x) $ map (\x -> ((_snp_id x, _snp_index x), x)) $ filter (\x -> _snp_ppa x > 0.1) xs
                  in r `seq` xs
    f xs = SNP bed (xs!!3) (xs!!4) b (readDouble $ xs!!6) target Nothing Nothing
      where
        b = case xs!!5 of
            "." -> Nothing
            x -> Just $ readDouble x
        bed = BED3 (head xs) (readInt $ xs!!1) (readInt $ xs!!2) 
    target = B.pack $ T.unpack $ fst $ T.breakOn ".99credset" $
        snd $ T.breakOnEnd "/" $ T.pack fl

readInteractions :: FilePath
                 -> IO [( B.ByteString, [(B.ByteString, [(B.ByteString, Double)])] )]
readInteractions fl = filter (not . null . snd) . map (f . B.split '\t') . B.lines <$>
    B.readFile fl
  where
    f xs = (xs!!3, map g $ filter ('|' `B.elem`) $ drop 4 xs)
      where
        g y = let [a, b] = B.split '|' y
              in (fst $ B.break (=='=') a, filter ((>0.015) . snd) $ map h $ B.split ';' b)
        h x = let [a,b] = B.split ',' x
              in (fst $ B.break (==':') a, readDouble b)

readCRE :: FilePath    -- ^ Master peak list
        -> [(T.Text, FilePath)]  -- ^ cluster peaks
        -> IO [BED]
readCRE fl clusters = do
    peaks <- fmap concat $ forM clusters $ \(nm, x) ->
        runResourceT $ runConduit $ streamBedGzip x .| sinkList 
    runResourceT $ runConduit $ streamBedGzip fl .| intersectBedWith g peaks .| sinkList
  where
    g :: BED3 -> [NarrowPeak] -> BED
    g x [] = undefined
    g x pk = name .~ Just nm $ asBed (x^.chrom) (minimum $ map (^.chromStart) pk) (maximum $ map (^.chromEnd) pk) 
      where
        nm = (x^.chrom) <> ":" <> B.pack (show $ x^.chromStart) <> "-" <>
            B.pack (show $ x^.chromEnd)

build "wf" [t| SciFlow ENV |] $ do
    namespace "GWAS" gwasBuilder
    node "Plot_GWAS" [| \() -> do
        ldsc_coeff <- lookupConfig "ldsc_coeff"
        ldsc_pvalue <- lookupConfig "ldsc_pvalue"
        ldsc_metadata <- lookupConfig "ldsc_metadata"
        adultAnnoFl <- lookupConfig "cluster_annotation"
        fetalAnnoFl <- lookupConfig "fetal_annotation"
        metadata <- lookupConfig "hea_metadata"
        dir <- lookupConfig "output_dir"
        let output = dir <> "/GWAS.pdf"
        liftIO $ do
            heaGroup <- M.fromList . map ((\xs -> (xs!!1, xs!!2)) .
                T.splitOn "\t" . T.strip) . T.lines <$>
                T.readFile metadata
            let lookupNameFn x = M.findWithDefault (error $ show x) x heaGroup
            traitId2Name <- do
                let f xs = (head xs, (xs!!2) <> " (" <> (xs!!1) <> ")")
                map (f . B.split '\t') . tail . B.lines <$> B.readFile ldsc_metadata
            let adjustP :: [Double] -> IO [Double]
                adjustP ps = R.runRegion $ do
                    r <- [r| p.adjust(ps_hs, method = "fdr") |]
                    R.unSomeSEXP r $ \x -> case H.hexp x of
                        H.Real x'  -> return $ toList x'
            anno1 <- map (\(a,b) -> ("A_" <> a, "Adult " <> b)) <$> readAnno adultAnnoFl
            anno2 <- map (\(a,b) -> ("F_" <> a, "Fetal " <> b)) <$> readAnno fetalAnnoFl
            coeff <- DF.readTable ldsc_coeff
            pvalue <- DF.readTable ldsc_pvalue
            pvalue' <- adjustP $ Mat.toList $ DF._dataframe_data pvalue
            let fdr = pvalue{DF._dataframe_data = Mat.fromList (DF.dim pvalue) pvalue'}
                df = DF.transpose $ changeName traitId2Name $ DF.transpose $
                    changeName (anno1 <> anno2) $
                    DF.orderDataFrame id $
                    DF.filterRows (\_ v -> V.maximum v >= p3) $
                    DF.map (negate . logBase 10) pvalue
                vec = sortBy (comparing snd) $ Mat.toList $ DF._dataframe_data $ DF.zip pvalue fdr 
                p1 = negate $ logBase 10 $ fst $ head $ dropWhile ((<=0.001) . snd) vec
                p2 = negate $ logBase 10 $ fst $ head $ dropWhile ((<=0.01) . snd) vec
                p3 = negate $ logBase 10 $ fst $ head $ dropWhile ((<=0.1) . snd) vec
                colorScheme :: [(String, String)]
                colorScheme = zip (nubSort $ map (T.unpack . lookupNameFn) $ DF.colNames df)
                    ["#000000", "#d70000", "#8c3cff", "#028800", "#00acc7", "#98ff00", "#ff7fd1", "#6c004f", "#ffa530", "#00009d", "#867068", "#004942", "#4f2a00", "#00fdcf", "#bcb7ff", "#95b47a", "#c004b9", "#2566a2", "#280041", "#dcb3af", "#fef590", "#50455b", "#a47c00"]
            printf "P value at FDR 0.1%%: %f\n" p1
            printf "P value at FDR 1%%: %f\n" p2
            printf "P value at FDR 10%%: %f\n" p3
            DF.writeTable (dir <> "/LDSC.tsv") (T.pack . show) df
            let traits :: [T.Text]
                traits =
                    [ "Heightz (UKB)"
                    , "Thyroid stimulating hormone (30367059)"
                    , "Atrial fibrillation (30061737)"
                    , "Type 2 diabetes (30297969)"
                    , "Major depressive disorder (30718901)"
                    , "Coronary artery disease (28714975)"
                    , "Allergy eczema (UKB)"

                    --, "Intelligence (29942086)"
                    --, "Autism (30804558)"

                    -- COVID
                    --, "COVID19 hospitalized (33307546)"
                    --, "COVID19 very severe (33307546)"
                    ]
                selected = df `DF.rsub` traits
            plotSingleGWAS (dir <> "/selected_GWAS.pdf") p2 colorScheme $ flip map (zip (DF.rowNames selected) $
                Mat.toLists $ DF._dataframe_data selected) $ \(trait, xs) ->
                (T.unpack trait, zipWith (\a b -> (T.unpack $ lookupNameFn a, T.unpack a, b)) (DF.colNames selected) xs)
            let neuralCells = filter ((=="Neuronal") . lookupNameFn) $ DF.colNames df
                df' = (df `DF.csub` neuralCells) `DF.rsub`
                    [ "Morning person (UKB)" :: T.Text
                    , "Intelligence (29942086)" ]
                dat = flip concatMap (zip (DF.rowNames df') (Mat.toLists $ DF._dataframe_data df')) $ \(t, xs) -> 
                    flip map (zip xs (DF.colNames df')) $ \(x, nm) ->
                        if "Fetal" `T.isPrefixOf` nm
                            then (T.unpack t, "Fetal", x)
                            else (T.unpack t, "Adult", x)
            jitterPlot (dir <> "/feta_vs_adult.pdf") p2 dat
            plotGWAS2 output lookupNameFn colorScheme (Set.fromList traits) $ DF.map (\x -> if x >= p1 then 3 else if x >= p2 then 2 else if x >= p3 then 1 else 0) df
        |] $ return ()

    node "Read_DeltaSVM" [| \() -> do
        dir <- lookupConfig "output_dir"
        motifFl <- lookupConfig "deltaSVM_result"
        let output = dir ++ "/pbSNP.bed"
        liftIO $ do
            let f xs = let (chr:s:_) = B.split '_' $ head xs
                       in ( BED3 chr (readInt s - 1) (readInt s)
                          , [(xs!!1, readDouble $ xs!!5)] )
            snp <- runResourceT $ runConduit $ sourceFile motifFl .|
                linesUnboundedAsciiC .|
                (dropC 1 >> mapC (B.split '\t')) .|
                filterC (\x -> x!!4 == "Y" && x!!6 /= "None") .|
                mapC f .| sinkList
            let res = map (\(a,b) -> BEDExt a b) $ M.toList $ M.fromListWith (++) snp
            writeBed output res
            return output
        |] $ return ()
    node "LiftOver" [| \() -> do
        dir <- lookupConfig "output_dir"
        pkFl <- lookupConfig "cre"
        chain <- lookupConfig "hg38_to_hg19"
        liftIO $ do
            let output = dir <> "/peaks_hg19.bed.gz"
                -- Expand CRE by 50 bp
                f x = let nm = (x^.chrom) <> ":" <> B.pack (show $ x^.chromStart) <> "-" <>
                                B.pack (show $ x^.chromEnd)
                      in name .~ Just nm $ chromStart %~ (subtract 50) $ chromEnd %~ (+50) $ x
            peaks <- runResourceT $ runConduit $
                streamBedGzip pkFl .| mapC f .| sinkList :: IO [NarrowPeak]
            peaks' <- liftOver chain peaks
            runResourceT $ runConduit $ yieldMany peaks' .| sinkFileBedGzip output
            return output
        |] $ return ()
    node "Read_SNP" [| \(motifFl, peakFl) -> do
        dir <- lookupConfig "output_dir"
        let output = dir <> "/snp.bin"
        snpFls <- lookupConfig "snp"
        liftIO $ do
            shelly $ mkdir_p $ fromText $ T.pack dir
            fls <- fmap (filter (".bed" `T.isSuffixOf`)) $ shelly $ lsT $ fromText $ T.pack snpFls
            snp <- concat <$> mapM (readSNP . T.unpack) fls
            beds <- runResourceT $ runConduit $
                streamBedGzip peakFl .| sinkList :: IO [NarrowPeak]
            motifs <- readBed motifFl :: IO [BEDExt BED3 [(B.ByteString, Double)]]
            let f x [] = [x]
                f x ys = flip map ys $ \y -> _data %~ (\x' -> x'{_snp_overlap_cre=y^.name}) $ x
                g x [] = [x^._data]
                g x ys = flip map ys $ \y -> (x^._data){_snp_tf=Just (y^._data)}
                getCore x = chromStart .~ (x^.chromStart) + 50 $ chromEnd .~ (x^.chromEnd) - 50 $ x
            snp' <- runConduit $ yieldMany snp .|
                mapC (\x -> BEDExt (_snp_location x) x) .|
                intersectBedWith f beds .| concatC .| intersectBedWith g motifs .| concatC .| sinkList 
            encodeFile output snp'
            return output
        |] $ return ()
    node "SNP_Overlap" [| \input -> do
        dir <- lookupConfig "output_dir"
        let output = dir ++ "/snp_peak_overlap.pdf"
            output2 = dir ++ "/snp_ppa.pdf"
        liftIO $ do
            snp <- decodeFile input
            printf "SNP Total (PPA > 0.1): %d\n" $ length $ filter (\x -> _snp_ppa x > 0.1) snp
            printf "SNP overlapping: %d\n" $ length $
                filter (\x -> isJust (_snp_overlap_cre x) && _snp_ppa x > 0.1) snp
            printf "No. traits: %d\n" $ length $ nubSort $ map _snp_target $ filter (\x -> _snp_ppa x > 0.1) snp
            printf "Disrupting motifs: %d\n" $ length $
                filter (\x -> isJust (_snp_overlap_cre x) && _snp_ppa x > 0.1 && isJust (_snp_tf x)) snp
            let header = "Disease or trait\tSNP_hg19\tPPA\tOverlapping cCRE_hg38\tDisrupted motifs"
            B.writeFile "1.tsv" $ B.unlines $ (header:) $ map showSNP $ 
                filter (\x -> isJust (_snp_overlap_cre x) && _snp_ppa x > 0.1) snp
            plotOverlapPPA output2 snp
            let (sa, sb) = partition ((>0.1) . fst) $ M.elems $ M.fromListWith (\a b -> if fst a > fst b then a else b) $
                    map (\x -> (_snp_id x, (_snp_ppa x, _snp_overlap_cre x))) snp
                frac x = (m :: Double, fromIntegral (length x) :: Double)
                  where
                    m = fromIntegral $ length $ filter (isJust . snd) x
                (m1, n1) = frac sa
                (m2, n2) = frac sb
            R.runRegion $ do
                myTheme
                [r| library("ggplot2")
                    m1 <- m1_hs
                    n1 <- n1_hs
                    m2 <- m2_hs
                    n2 <- n2_hs
                    p <- fisher.test(matrix(c(m1,n1,m2,n2), nrow=2, byrow=T))$p.value
                    df <- data.frame(name=c(">0.1", "<=0.1"), val=c(m1/n1*100, m2/n2*100))
                    fig <- ggplot(df, aes(name, val)) +
                        geom_col() +
                        ggtitle(p) +
                        myTheme() + myFill() +
                        labs(x = "PPA", y = "% overlap with cCRE")
                    ggsave(output_hs, fig, width=42, height=55, unit="mm", useDingbats=F)
                |]
                return ()
       |] $ return ()
    ["Read_DeltaSVM", "LiftOver"] ~> "Read_SNP"
    path ["Read_SNP", "SNP_Overlap"]

    node "SNP_Output" [| \input -> do
        dir <- lookupConfig "output_dir"
        annoFl <- lookupConfig "cluster_annotation"
        accFl <- lookupConfig "accessibility"
        interFl <- lookupConfig "cicero_interaction"
        interFl' <- lookupConfig "pcHiC_interaction"
        let output = dir ++ "/snp.tsv"
        liftIO $ do
            anno <- readAnno annoFl
            acc <- changeName anno <$> DF.readTable accFl
            interCicero <- M.fromList <$> readInteractions interFl
            interHiC <- M.fromList <$> readInteractions interFl'
            snp <- decodeFile input :: IO [SNP]

            print $ length $ filter (\x -> _snp_ppa x > 0.1) snp

            let f x = ((showSNP' x <> "\t") <>) $ fromMaybe ".\t." $ do
                    cre <- _snp_overlap_cre x
                    let r1 = fromMaybe "." $ B.intercalate "," . nubSort . concatMap (map fst . snd) <$>
                            M.lookup cre interCicero
                        r2 = fromMaybe "." $ B.intercalate "," . nubSort . concatMap (map fst . snd) <$>
                            M.lookup cre interHiC
                    return $ r1 <> "\t" <> r2
                snp' = sortBy (flip (comparing _snp_ppa)) $
                    filter (\x -> isJust (_snp_overlap_cre x) && isJust (_snp_tf x) && _snp_ppa x > 0.1) snp
                maxAcc cre = B.pack $ T.unpack $ T.intercalate "," $ map fst $ take 3 $
                    sortBy (flip (comparing snd)) $ zip (DF.colNames acc) $
                    V.toList $ acc `DF.rindex` T.pack (B.unpack $ fst $ B.break (=='(') cre)
                header = "Trait\tSNP (hg19)\tindex variant (hg19)\tPPA\tOverlapping CRE (hg38)\tTop 3 cell types with maximum accessibility\tGain of motif\tLoss of motif\tTarget genes (Cicero)\tTarget genes (pcHiC)"
                showSNP' snp = B.intercalate "\t" [ _snp_target snp, _snp_id snp
                    , _snp_index snp, B.pack $ show $ _snp_ppa snp,
                    fromMaybe "." $ _snp_overlap_cre snp,
                    fromMaybe "." $ fmap maxAcc $ _snp_overlap_cre snp,
                    g $ fromMaybe "." $ fmap (B.intercalate "," . map fst . filter ((>0) . snd)) $ _snp_tf snp,
                    g $ fromMaybe "." $ fmap (B.intercalate "," . map fst . filter ((<0) . snd)) $ _snp_tf snp
                    ]
                  where g x = if B.null x then "." else x

            {-
            printf "All SNP (PPA > 0.1): %d\n" $ length $ filter (\x -> _snp_ppa x > 0.1) snp
            printf "SNP overlaped CRE: %d\n" $ length $ filter (\x -> isJust (_snp_overlap_cre x) && _snp_ppa x > 0.1) snp
            printf "SNP disrupt motif: %d\n" $ length snp'
            printf "SNP have gene: %d\n" $ length snpWithGene
            printf "Final SNP: %d\n" $ length $ mapMaybe f snp'
            -}

            B.writeFile output $ B.unlines $ (header:) $ map f $ sortBy (flip (comparing _snp_ppa)) $ filter (\x -> _snp_ppa x > 0.1) snp
       |] $ return ()
    path ["Read_SNP", "SNP_Output"]

    node "SNP_Stat" [| \input -> do
        annoFl <- lookupConfig "cluster_annotation"
        interFl <- lookupConfig "pcHiC_interaction"
        gwas <- lookupConfig "gwas"
        accFl <- lookupConfig "accessibility"
        annoFl <- lookupConfig "cluster_annotation"
        liftIO $ do
            snp <- decodeFile input :: IO [SNP]
            inter <- M.fromListWith undefined <$> readInteractions interFl
            anno <- readAnno annoFl
            acc <- changeName anno <$> DF.readTable accFl
            let snp_ppa = filter (\x -> _snp_ppa x > 0.1) snp
                snp_cre = filter (\x -> isJust $ _snp_overlap_cre x) snp_ppa
                count label xs (f, g) = 
                    let (rows, vals) = unzip $ M.toList $ fmap f $ M.fromListWith (<>) $ map (\x -> (_snp_target x, g x)) xs
                    in DF.mkDataFrame (map (T.pack . B.unpack) rows) [label] $ map return vals
                f x = fromMaybe [] $ do
                    cre <- _snp_overlap_cre x
                    r <- M.lookup cre inter
                    return $ concatMap (map fst . snd) r
                topACC cre = map fst $ take 1 $ sortBy (flip (comparing snd)) $
                    zip (DF.colNames acc) $ V.toList $ acc `DF.rindex` cre
            DF.writeTable "1.tsv" (T.pack . show) $ DF.cbind
                [ count "# variants PPA > 1" snp_ppa (getSum, const $ Sum 1)
                , count "# CREs associated with variants" snp_cre (getSum, const $ Sum 1)
                , count "# cell types with maximum CRE accessibility" snp_cre (length . nubSort, topACC . T.pack . B.unpack . fromJust . _snp_overlap_cre)
                , count "# genes associated with CREs" snp_ppa (length . nubSort, f)
                , count "# TFs disrupted by variants" snp_ppa (length . nubSort . map fst, fromMaybe [] . _snp_tf)
                ]

            DF.writeTable "details.tsv" id $ DF.cbind
                [ count "# cell types with maximum CRE accessibility" snp_cre (T.intercalate "," . nubSort, topACC . T.pack . B.unpack . fromJust . _snp_overlap_cre)
                , count "# genes associated with CREs" snp_ppa (T.pack . B.unpack . B.intercalate "," . nubSort, f)
                , count "# TFs disrupted by variants" snp_ppa (T.pack . B.unpack . B.intercalate "," . nubSort . map fst, fromMaybe [] . _snp_tf)
                ]
       |] $ return ()
    path ["Read_SNP", "SNP_Stat"]



main :: IO ()
main = mainFun wf