{-# LANGUAGE OverloadedStrings #-}
import System.Environment
import           Data.Conduit.Zlib           (gzip, ungzip, multiple)
import Conduit
import Control.Monad
import qualified Data.ByteString.Char8 as B
import qualified Data.HashMap.Strict as M
import qualified Data.Vector.Mutable as VM
import qualified Data.Vector as V
import Data.Maybe

readDB :: FilePath -> ConduitT () (B.ByteString, B.ByteString) (ResourceT IO) ()
readDB fl = sourceFile fl .| multiple ungzip .| linesUnboundedAsciiC .|
    filterC ((/= '#') . B.head) .| concatMapC (f . B.split '\t')
  where
    f xs = flip map (B.split ',' $ xs!!4) $ \s -> 
      (B.intercalate ":" [head xs, xs!!1, xs!!3, s], xs!!2)

main = do
    [dbFl, input] <- getArgs
    input' <-  B.lines <$> B.readFile input
    let query = M.fromList $ zip input' $ repeat Nothing
    res <- runResourceT $ runConduit $ readDB dbFl .| foldlC (\acc (i, x) -> M.adjust (const $ Just x) i acc) query
    forM_ input' $ \x -> print $ fromMaybe "." $ fromJust $ M.lookup x res