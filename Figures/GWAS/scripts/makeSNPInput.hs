{-# LANGUAGE OverloadedStrings #-}
import Control.Monad
import System.Environment
import Data.List.Ordered
import qualified Data.ByteString.Char8 as B
import Data.Maybe

readSNP :: FilePath -> IO [B.ByteString]
readSNP fl = map ((!!3) . B.split '\t') . B.lines <$> B.readFile fl

main :: IO ()
main = do
    inputs <- getArgs
    xs <- concat <$> mapM readSNP inputs
    B.putStr $ B.unlines $ nubSort $ flip mapMaybe xs $ \x ->
            let [a,b,c,d] = B.split ':' x
                x' = B.intercalate "_" ["chr" <> a, b, c, d]
            in if (B.length c > 1 || B.length d > 1) then Nothing else Just x'
    
