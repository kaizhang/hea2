{-# LANGUAGE OverloadedStrings #-}
import Taiji.Utils.Matrix
import Bio.Utils.Misc
import Bio.Data.Bed
import System.Environment
import Conduit
import Control.Lens
import qualified Data.ByteString.Char8 as B
import Shelly
import Data.List
import Control.Monad
import Data.Function
import qualified Data.Text as T
import qualified Data.Map.Strict as M
import Data.Conduit.Zlib (multiple, ungzip, gzip)
import Control.Concurrent.Async (mapConcurrently)
import Data.List.Split

remapGenes :: [B.ByteString] -> ([B.ByteString], M.Map Int Int)
remapGenes xs = (map fst genes, M.fromList $ flip concatMap (zip genes [0..]) $ \((_, xs), i) -> zip xs $ repeat i)
  where
    genes = M.toList $ M.fromListWith (<>) $ zip (map (fst . B.break (==':')) xs) $ map return [0..]

procMat :: M.Map Int Int -> Row Int -> Row Int
procMat m (a, xs) = (a, M.toList $ M.fromListWith max $ map f xs)
  where
    f (i, x) = let i' = M.findWithDefault undefined i m
               in (i', x)

main = do
    [outputDir, geneFl, input] <- getArgs
    let outputMat = outputDir <> "/matrix.mtx.gz"
        outputColIndx = outputDir <> "/features.txt.gz"
        outputRowIndx = outputDir <> "/barcodes.txt.gz"
    shelly $ mkdir_p $ fromText $ T.pack outputDir
    (genes, idxMap) <- remapGenes . B.lines <$> B.readFile geneFl
    mat <- mapRows (procMat idxMap) <$> mkSpMatrix readInt input
    saveMatrixMM outputMat mat
    runResourceT $ runConduit $ streamRows mat .| mapC fst .| unlinesAsciiC .|
        gzip .| sinkFile outputRowIndx
    runResourceT $ runConduit $ yieldMany genes .| unlinesAsciiC .|
        gzip .| sinkFile outputColIndx