{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell   #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE QuasiQuotes       #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE RankNTypes #-}
import HEA
import System.Environment

import Statistics.Quantile
import Data.Conduit.Zlib
import Control.Arrow
import qualified Taiji.Utils.DataFrame as DF
import qualified Data.Vector as V
import qualified Data.Vector.Mutable as VM
import qualified Data.Text as T
import Bio.Data.Bed.Types
import Bio.Data.Bed
import Control.Monad.ST (runST)
import qualified Data.Matrix as Mat
import qualified Data.Map.Strict as Map
import qualified Data.ByteString.Char8 as B
import qualified Data.HashMap.Strict as M
import Data.List.Ordered (nubSort)
import Bio.Utils.Functions
import Taiji.Utils
import Shelly hiding (FilePath, path)
import Data.Binary
import System.Random.MWC.Distributions (uniformShuffle)
import System.Random.MWC (create)
import Statistics.Sample (mean)
import Data.Int

import Taiji.Prelude
import qualified Language.R                        as R
import           Language.R.QQ
import Language.R.HExp
import qualified Data.Vector.SEXP as S

chamberMap :: [(B.ByteString, String)]
chamberMap =
    [ ("CARE181125_3B","RV")
    , ("CARE181125_3C","LA")
    , ("CARE181125_3D","LV")
    , ("CARE181213_2A","RA")
    , ("CARE181213_2B","RV")
    , ("CARE190307_10A","RA")
    , ("CARE190307_10B","RV")
    , ("CARE190307_10C","LA")
    , ("CARE190307_10D","LV")
    , ("CARE190331_11B","RV")
    , ("CARE190331_11D","LV")
    , ("CARE191122_2C","LA")
    , ("CARE191122_2D","LV")
    , ("CARE191122_3D","LV")
    ]

getClusters :: FilePath   -- ^ Cluster
            -> [T.Text] -- ^ Matrix
            -> IO [[T.Text]]
getClusters clFl names = do
    cls <- map (map readInt . B.split ',') . B.lines <$> B.readFile clFl
    return $ (map . map) (\i -> idx V.! i) cls
  where
    idx = V.fromList names

--------------------------------------------------------------------------------
-- Plot Cluster
--------------------------------------------------------------------------------

-- | Random sample 30,000 cells.
sampleCell :: [CellCluster] -> IO [CellCluster]
sampleCell clusters
    | ratio >= 1 = return clusters
    | otherwise = do
        gen <- create
        forM clusters $ \c -> do
            s <- sampling gen ratio $ V.fromList $ _cluster_member c
            return $ c {_cluster_member = V.toList s}
  where
    n = foldl1' (+) $ map (length . _cluster_member) clusters
    ratio = 10000 / fromIntegral n :: Double
    sampling gen frac v = V.take n' <$> uniformShuffle v gen
      where
        n' = max 100 $ truncate $ frac * fromIntegral (V.length v)

plotCellsChamber :: FilePath -> [CellCluster]  -- ^ major clusters
                -> IO ()
plotCellsChamber output clusters = R.runRegion $ do
    myTheme
    [r| library("ggplot2")
        library("ggrastr")
        library("gridExtra")
        library("ggdendro")
        df <- data.frame(cls=cls_hs, x=xs_hs, y=ys_hs)
        fig <- ggplot(df, aes(x,y)) +
            geom_point(size=0.5, alpha=1, stroke = 0, shape=20, aes(colour = factor(cls))) +
            myTheme() + myFill() +
            labs(x = "UMAP-1", y = "UMAP-2") +
            theme(
                plot.margin=unit(c(2,0,2,2),"mm"),
                panel.grid.major = element_blank(),
                axis.ticks = element_blank(),
                axis.text = element_blank(),
                axis.line = element_blank(),
                panel.border = element_blank(),
                legend.position = "top"
            ) + scale_color_manual(values=colors_hs)

        ggsave(output_hs, fig, width=90, height=90, unit="mm", useDingbats=F)
    |]
    return ()
  where
    (cls, xs, ys) = unzip3 $ flip concatMap clusters $ \cl -> map f $ _cluster_member cl
    f x = let (a,b) = _cell_2d x
              nm | "sample" `B.isInfixOf` _cell_barcode x = "Fetal"
                 | otherwise = "Adult" :: String
          in (nm, a, b)
    colors :: [String]
    colors =
        [ "#f3776e"
        , "#7caf41"
        , "#1cbdc3"
        , "#a781ba" ]


plotCells :: FilePath -> [CellCluster]  -- ^ major clusters
          -> IO ()
plotCells output clusters = R.runRegion $ do
    myTheme
    [r| library("ggplot2")
        library("ggrastr")
        library("gridExtra")
        library("ggdendro")
        df <- data.frame(cls=cls_hs, x=xs_hs, y=ys_hs)
        fig <- ggplot(df, aes(x,y)) +
            #geom_point_rast(size=0.1, alpha=1, stroke = 0, shape=20, aes(colour = factor(cls))) +
            geom_point(size=0.5, alpha=1, stroke = 0, shape=20, aes(colour = factor(cls))) +
            #geom_text(data=data.frame(cx=cx_hs, cy=cy_hs),
            #    aes(cx, cy, label = label_hs), size=1.7
            #) +
            myTheme() + myFill() +
            labs(x = "UMAP-1", y = "UMAP-2") +
            theme(
                plot.margin=unit(c(2,0,2,2),"mm"),
                panel.grid.major = element_blank(),
                axis.ticks = element_blank(),
                axis.text = element_blank(),
                axis.line = element_blank(),
                panel.border = element_blank(),
                legend.position = "none"
            ) + scale_color_manual(values=colors_hs)

        ggsave(output_hs, fig, width=90, height=90, unit="mm", useDingbats=F)
    |]
    return ()
  where
    (cls, xs, ys) = unzip3 $ flip concatMap clusters $ \cl ->
        uncurry (zip3 (repeat $ B.unpack $ _cluster_name cl)) $ unzip $
            map _cell_2d $ _cluster_member cl
    (label, cx, cy) = unzip3 $ flip map clusters $ \cl ->
        let (x,y) = getCentroid $ map _cell_2d $ _cluster_member cl
        in (B.unpack $ _cluster_name cl, x, y)
    getCentroid ps = let (x,y) = unzip ps
                     in ( median medianUnbiased $ V.fromList x
                        , median medianUnbiased $ V.fromList y )
    colors :: [String]
    colors =
        [ "#c7d347", "#9945ce", "#69c855", "#5b54ae", "#c5903a", "#c6478c", "#6bceac",
            "#d24d3a", "#70a6bf", "#763830", "#c0ca93", "#43304f", "#ca9289", "#4a6037", "#ba94ca"]
            {-
        [ "#FB9600"
        , "#901102"
        , "#FB7E7A"
        , "#115093"
        , "#797941"
        , "#920DFF"
        , "#7C79FF"
        , "#199150"
        , "#1A00FF" ]
        -}

main :: IO ()
main = do
    [clFl, output] <- getArgs
    clusters <- decodeFile clFl >>= sampleCell
    plotCells output clusters
    --plotCellsChamber output clusters