{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE QuasiQuotes       #-}
import Bio.Data.Bed
import qualified Data.ByteString.Char8 as B
import Data.Conduit.Zlib (multiple, ungzip)
import Statistics.Sample (mean)
import System.Environment
import Data.Conduit.List (groupBy)
import qualified Data.Vector.Unboxed as U
import Conduit
import Data.Int (Int32)
import Taiji.Prelude hiding (groupBy)
import Control.Monad.ST (runST)
import Control.DeepSeq (force)

getPeaks :: [FilePath] -> IO [BED3]
getPeaks input = do
    Just res <- runResourceT $ runConduit $ yieldMany input .| foldMC merge Nothing
    return $ map fst $ take 10000 $ sortBy (flip (comparing snd)) res
  where
    merge Nothing fl = fmap Just $
        runResourceT $ runConduit $ sourceFile fl .| multiple ungzip .|
            linesUnboundedAsciiC .| mapC (f . B.split '\t') .| sinkList
    merge (Just xs) fl = do
        dat <- runResourceT $ runConduit $ sourceFile fl .| multiple ungzip .|
            linesUnboundedAsciiC .| mapC (f . B.split '\t') .| sinkList
        return $ force $ Just $ zipWith (\(x1, y1) (x2, y2) -> (x1, y1 + y2)) xs dat
    f [a,b] = let [chr, a'] = B.split ':' a
                  [s,e] = B.split '-' a'
              in (BED3 chr (readInt s) (readInt e), readDouble b)

main = do
    input <- getArgs
    getPeaks input >>= writeBed "out.bed"