{-# LANGUAGE OverloadedStrings #-}
import Taiji.Utils.Matrix
import Bio.Utils.Misc
import Bio.Data.Bed
import System.Environment
import Conduit
import Control.Lens
import qualified Data.ByteString.Char8 as B
import Shelly
import Data.List
import Control.Monad
import Data.Function
import qualified Data.Text as T
import qualified Data.Map.Strict as M
import Data.Conduit.Zlib (multiple, ungzip, gzip)
import Control.Concurrent.Async (mapConcurrently)
import Data.List.Split
import HEA

main = do
    [dir, annot, outdir] <- getArgs
    anno <- readAnno annot
    files <- shelly $ lsT dir
    shelly $ mkdir_p outdir
    forM_ files $ \fl -> do
        let cl = B.pack $ T.unpack $ fst $ T.breakOn ".tsv.gz" $ snd $ T.breakOnEnd "/" fl
            nm = T.pack $ B.unpack $ fromJust $ lookup cl anno
        shelly $ run_ "cp" [fl, T.pack outdir <> "/" <> nm <> ".tsv.gz"]