{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE FlexibleContexts #-}

import Bio.Data.Bed
import System.Environment
import qualified Data.Text as T
import qualified Data.ByteString.Char8 as B
import Data.List.Ordered (nubSort)
import Shelly

import Taiji.Prelude

readPeaks :: FilePath -> ConduitT i NarrowPeak (ResourceT IO) ()
readPeaks dir = do
    files <- shelly $ lsT dir
    forM_ files $ \fl -> do
        let j = fst $ T.breakOn ".narrowPeak.gz" $ snd $ T.breakOnEnd "/" fl
            nm = B.pack $ T.unpack $ i <> "." <> j
        streamBedGzip (T.unpack fl) .| mapC (rename nm)
  where
    i = snd $ T.breakOnEnd "/" $ T.pack dir
    rename nm x = name .~ Just nm $ x

mergePeaks :: FilePath -> ConduitT NarrowPeak NarrowPeak (ResourceT IO) ()
mergePeaks tmpdir = do
    mapC resize .| sinkFileBed tmp1
    liftIO $ shelly $ escaping False $ bashPipeFail bash_ "cat" $
        [T.pack tmp1, "|", "sort", "-k1,1", "-k2,2n", "-k3,3n", ">", T.pack tmp2]
    streamBed tmp2 .| mergeSortedBedWith (\x -> map (annotatePeak x) $ iterativeMerge x) .| concatC
  where
    tmp1 = tmpdir <> "/tmp1"
    tmp2 = tmpdir <> "/tmp2"
    annotatePeak peaks pk = name .~ nm $ pk
      where
        nm = Just $ B.intercalate "+" $ nubSort $ concatMap (B.split '+' . fromJust . (^.name)) overlapped
        overlapped = filter (\x -> sizeOverlapped x pk > 0) peaks
    iterativeMerge [] = []
    iterativeMerge peaks = bestPeak : iterativeMerge rest
      where
        rest = filter (\x -> sizeOverlapped x bestPeak == 0) peaks
        bestPeak = maximumBy (comparing (fromJust . (^.npPvalue))) peaks
    resize pk = chromStart .~ max 0 (summit - halfWindowSize) $
        chromEnd .~ summit + halfWindowSize$
        npPeak .~ Just halfWindowSize $ pk
      where
        summit = pk^.chromStart + fromJust (pk^.npPeak)
    halfWindowSize = 200

main :: IO ()
main = do
    (output:dirs) <- getArgs
    withTempDir (Just "./") $ \tmpdir -> runResourceT $ runConduit $
        mapM_ readPeaks dirs .| mergePeaks tmpdir .| sinkFileBedGzip output