{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE QuasiQuotes       #-}
import Bio.Data.Bed
import qualified Data.ByteString.Char8 as B
import Data.Conduit.Zlib (multiple, ungzip)
import Statistics.Sample (meanVarianceUnb, mean)
import System.Environment
import Bio.Utils.Functions (filterFDR)
import qualified Data.Conduit.List as L
import qualified Data.Vector.Unboxed as U
import qualified Data.Vector as V
import Conduit
import Data.Int (Int32)
import Taiji.Prelude
import Control.Monad.ST (runST)
import Control.DeepSeq (force)

import qualified Language.R                        as R
import Data.Vector.SEXP (toList)
import           Language.R.QQ
import Language.R.HExp

count :: Monad m => [BED3] -> ConduitT BED (B.ByteString, Int, Int) m ()
count regions = groupCells .| filterC ((>1000) . length . snd) .| mapC f
  where
    f (nm, beds) = 
        let beds' = filter (isIntersected bedSet) beds
        in force (nm, length beds', overlapAnalysis beds')
    bedSet = bedToTree const $ zip regions $ repeat ()

groupCells :: Monad m => ConduitT BED (B.ByteString, [BED]) m ()
groupCells = L.groupBy ((==) `on` (^.name)) .|
    mapC (\xs -> (fromJust $ head xs^.name, xs))
{-# INLINE groupCells #-}

overlapAnalysis :: [BED] -> Int
overlapAnalysis xs = runIdentity $ runConduit $ mergeBedWith length xs .| filterC (>=2) .| lengthC

binAverage :: [(Int, Int)] -> [(Double, (Double, Double))]
binAverage xs = map (\x -> let (a, b) = unzip x in (ave a, ave' b)) $
    filter (not . null) $ go step [] $ sortBy (comparing fst) xs
  where
    go limit acc (x:xs)
        | fst x < limit = go limit (x:acc) xs
        | otherwise = acc : go (limit + step) [] (x:xs)
    go _ acc _ = [acc]
    step = 100
    ave x = mean $ U.fromList $ map fromIntegral x
    ave' x = meanVarianceUnb $ U.fromList $ map fromIntegral x

computePValues :: [(Double, Double)]
               -> [(B.ByteString, Int, Int)]
               -> IO [(B.ByteString, Double)]
computePValues trainData testData = R.runRegion $ do
    pvalues <-
        [r| data <- data.frame(x = trainX_hs, y = trainY_hs)
            model <- loess(y ~ x, data, control=loess.control(surface="direct"))
            new_data <- data.frame(x = testX_hs, y = testY_hs)
            lambda <- predict(model, new_data)
            getP <- function(y, l) { return(ppois(q=y, lambda=max(0.1, l), lower.tail = F)) }
            mapply(getP, new_data$y, lambda)
        |]
    p <- R.unSomeSEXP pvalues $ \x -> case hexp x of
        Real x' -> return $ toList x'
    return $ zip names p
  where
    (trainX, trainY) = unzip trainData
    (names, testX', testY') = unzip3 testData
    testX = map fromIntegral testX' :: [Int32]
    testY = map fromIntegral testY' :: [Int32]

streamFile :: FilePath -> (B.ByteString, FilePath) -> ConduitT () BED (ResourceT IO) ()
streamFile dir (prefix, fl) = streamBedGzip (dir <> "/" <> fl) .| mapC f
  where
    f x = name %~ (fmap (prefix' <>)) $ x
    prefix' = prefix <> "_1+"

main = do
    [fl] <- getArgs
    --regions <- readBed bed
    {-
    runResourceT $ runConduit $ mapM_ (streamFile dir) list_of_files .|
        count regions .| mapC (\(a,b,c) -> B.intercalate "\t" [a, B.pack $ show b, B.pack $ show c]) .|
        unlinesAsciiC .| sinkFile "o.count"
    -}
    dat <- map (f . B.split '\t') . B.lines <$> B.readFile fl
    forM_ (groupBy ((==) `on` g) dat) $ \res -> do
        let r = binAverage $ map (\(_,x,y) -> (x,y)) res
        forM_ r $ \(a, (b,c)) -> do
            putStrLn $ unwords $ [show a, show b, show c]

        --ps <- V.fromList <$> computePValues r res
        --print $ fromIntegral (V.length (filterFDR 0.05 ps)) / fromIntegral (V.length ps)
    --filter (<0.05) ps (fromIntegral $ length ps)
    --B.writeFile "o.txt" $ B.unlines $ flip map r $ \(b,c) -> B.intercalate "\t" [B.pack $ show b, B.pack $ show c]
  where
    f [a,b,c] = (a, readInt b, readInt c)
    g (x, _, _) = fst $ B.breakEnd (=='+') x


list_of_files :: [(B.ByteString, FilePath)]
list_of_files =
    [ ("adipose_omentum_SM-ADYHB", "data/HEA/adipose_omentum_SM-ADYHB_rep1_fragments.bed.gz")
    , ("adipose_omentum_SM-CHZRM", "data/HEA/adipose_omentum_SM-CHZRM_rep1_fragments.bed.gz")
    , ("adipose_omentum_SM-CSSD4", "data/HEA/adipose_omentum_SM-CSSD4_rep1_fragments.bed.gz")
    , ("adipose_omentum_SM-IOBHJ", "data/HEA/adipose_omentum_SM-IOBHJ_rep1_fragments.bed.gz")
    , ("adrenal_gland_SM-A8WNO", "data/HEA/adrenal_gland_SM-A8WNO_rep1_fragments.bed.gz")
    , ("artery_aorta_SM-C1MLC", "data/HEA/artery_aorta_SM-C1MLC_rep1_fragments.bed.gz")
    , ("artery_aorta_SM-C1PX3", "data/HEA/artery_aorta_SM-C1PX3_rep1_fragments.bed.gz")
    , ("artery_aorta_SM-CR89M", "data/HEA/artery_aorta_SM-CR89M_rep1_fragments.bed.gz")
    , ("artery_aorta_SM-JF1NU", "data/HEA/artery_aorta_SM-JF1NU_rep1_fragments.bed.gz")
    , ("artery_tibial_SM-CHLWW", "data/HEA/artery_tibial_SM-CHLWW_rep1_fragments.bed.gz")
    , ("artery_tibial_SM-IOBHK", "data/HEA/artery_tibial_SM-IOBHK_rep1_fragments.bed.gz")
    , ("colon_sigmoid_SM-AZPYO", "data/HEA/colon_sigmoid_SM-AZPYO_rep1_fragments.bed.gz")
    , ("colon_sigmoid_SM-JF1O8", "data/HEA/colon_sigmoid_SM-JF1O8_rep1_fragments.bed.gz")
    , ("colon_transverse_SM-A9HOW", "data/HEA/colon_transverse_SM-A9HOW_rep1_fragments.bed.gz")
    , ("colon_transverse_SM-A9VP4", "data/HEA/colon_transverse_SM-A9VP4_rep1_fragments.bed.gz")
    , ("colon_transverse_SM-ACCQ1", "data/HEA/colon_transverse_SM-ACCQ1_rep1_fragments.bed.gz")
    , ("colon_transverse_SM-BZ2ZS", "data/HEA/colon_transverse_SM-BZ2ZS_rep1_fragments.bed.gz")
    , ("colon_transverse_SM-CSSDA", "data/HEA/colon_transverse_SM-CSSDA_rep1_fragments.bed.gz")
    , ("esophagus_ge_junction_SM-CTD24", "data/HEA/esophagus_ge_junction_SM-CTD24_rep1_fragments.bed.gz")
    , ("esophagus_ge_junction_SM-IOERG", "data/HEA/esophagus_ge_junction_SM-IOERG_rep1_fragments.bed.gz")
    , ("esophagus_mucosa_SM-A9HOR", "data/HEA/esophagus_mucosa_SM-A9HOR_rep1_fragments.bed.gz")
    , ("esophagus_mucosa_SM-A9VPA", "data/HEA/esophagus_mucosa_SM-A9VPA_rep1_fragments.bed.gz")
    , ("esophagus_mucosa_SM-AZPYJ", "data/HEA/esophagus_mucosa_SM-AZPYJ_rep1_fragments.bed.gz")
    , ("esophagus_muscularis_SM-A8CPH", "data/HEA/esophagus_muscularis_SM-A8CPH_rep1_fragments.bed.gz")
    , ("esophagus_muscularis_SM-CSSCV", "data/HEA/esophagus_muscularis_SM-CSSCV_rep1_fragments.bed.gz")
    , ("esophagus_muscularis_SM-IOBHM", "data/HEA/esophagus_muscularis_SM-IOBHM_rep1_fragments.bed.gz")
    , ("esophagus_muscularis_SM-IQYD1", "data/HEA/esophagus_muscularis_SM-IQYD1_rep1_fragments.bed.gz")
    , ("heart_atrial_appendage_SM-IOBHN", "data/HEA/heart_atrial_appendage_SM-IOBHN_rep1_fragments.bed.gz")
    , ("heart_atrial_appendage_SM-JF1NX", "data/HEA/heart_atrial_appendage_SM-JF1NX_rep1_fragments.bed.gz")
    , ("heart_lv_SM-IOBHO", "data/HEA/heart_lv_SM-IOBHO_rep1_fragments.bed.gz")
    , ("heart_lv_SM-JF1NY", "data/HEA/heart_lv_SM-JF1NY_rep1_fragments.bed.gz")
    , ("liver_SM-A8WNZ", "data/HEA/liver_SM-A8WNZ_rep1_fragments.bed.gz")
    , ("lung_SM-A62E9", "data/HEA/lung_SM-A62E9_rep1_fragments.bed.gz")
    , ("lung_SM-A8WNH", "data/HEA/lung_SM-A8WNH_rep1_fragments.bed.gz")
    , ("lung_SM-ACCPU", "data/HEA/lung_SM-ACCPU_rep1_fragments.bed.gz")
    , ("lung_SM-JF1NZ", "data/HEA/lung_SM-JF1NZ_rep1_fragments.bed.gz")
    , ("mammary_tissue_SM-IOBHL", "data/HEA/mammary_tissue_SM-IOBHL_rep1_fragments.bed.gz")
    , ("mammary_tissue_SM-JF1NV", "data/HEA/mammary_tissue_SM-JF1NV_rep1_fragments.bed.gz")
    , ("muscle_SM-ADA6L", "data/HEA/muscle_SM-ADA6L_rep1_fragments.bed.gz")
    , ("muscle_SM-C1MKW", "data/HEA/muscle_SM-C1MKW_rep1_fragments.bed.gz")
    , ("muscle_SM-C1PWV", "data/HEA/muscle_SM-C1PWV_rep1_fragments.bed.gz")
    , ("muscle_SM-IOBHP", "data/HEA/muscle_SM-IOBHP_rep1_fragments.bed.gz")
    , ("muscle_SM-JF1O9", "data/HEA/muscle_SM-JF1O9_rep1_fragments.bed.gz")
    , ("nerve_tibial_SM-CHLWU", "data/HEA/nerve_tibial_SM-CHLWU_rep1_fragments.bed.gz")
    , ("nerve_tibial_SM-CP2V6", "data/HEA/nerve_tibial_SM-CP2V6_rep1_fragments.bed.gz")
    , ("nerve_tibial_SM-IOBHQ", "data/HEA/nerve_tibial_SM-IOBHQ_rep1_fragments.bed.gz")
    , ("ovary_SM-IOBHR", "data/HEA/ovary_SM-IOBHR_rep1_fragments.bed.gz")
    , ("pancreas_SM-ADRUQ", "data/HEA/pancreas_SM-ADRUQ_rep1_fragments.bed.gz")
    , ("pancreas_SM-IOBHS", "data/HEA/pancreas_SM-IOBHS_rep1_fragments.bed.gz")
    , ("pancreas_SM-JF1NS", "data/HEA/pancreas_SM-JF1NS_rep1_fragments.bed.gz")
    , ("pancreas_SM-JF1O6", "data/HEA/pancreas_SM-JF1O6_rep1_fragments.bed.gz")
    , ("skin_SM-IOBHU", "data/HEA/skin_SM-IOBHU_rep1_fragments.bed.gz")
    , ("skin_SM-JF1O1", "data/HEA/skin_SM-JF1O1_rep1_fragments.bed.gz")
    , ("skin_sun_exposed_SM-ADYHK", "data/HEA/skin_sun_exposed_SM-ADYHK_rep1_fragments.bed.gz")
    , ("skin_sun_exposed_SM-IOBHT", "data/HEA/skin_sun_exposed_SM-IOBHT_rep1_fragments.bed.gz")
    , ("skin_sun_exposed_SM-IQYCP", "data/HEA/skin_sun_exposed_SM-IQYCP_rep1_fragments.bed.gz")
    , ("skin_sun_exposed_SM-JF1NT", "data/HEA/skin_sun_exposed_SM-JF1NT_rep1_fragments.bed.gz")
    , ("small_intestine_SM-A62GO", "data/HEA/small_intestine_SM-A62GO_rep1_fragments.bed.gz")
    , ("small_intestine_SM-ADA5F", "data/HEA/small_intestine_SM-ADA5F_rep1_fragments.bed.gz")
    , ("small_intestine_SM-JF1O2", "data/HEA/small_intestine_SM-JF1O2_rep1_fragments.bed.gz")
    , ("stomach_SM-CHLWL", "data/HEA/stomach_SM-CHLWL_rep1_fragments.bed.gz")
    , ("stomach_SM-IOBHV", "data/HEA/stomach_SM-IOBHV_rep1_fragments.bed.gz")
    , ("stomach_SM-JF1NP", "data/HEA/stomach_SM-JF1NP_rep1_fragments.bed.gz")
    , ("stomach_SM-JF1O3", "data/HEA/stomach_SM-JF1O3_rep1_fragments.bed.gz")
    , ("thyroid_SM-C1MKY", "data/HEA/thyroid_SM-C1MKY_rep1_fragments.bed.gz")
    , ("thyroid_SM-IOBHW", "data/HEA/thyroid_SM-IOBHW_rep1_fragments.bed.gz")
    , ("thyroid_SM-JF1O4", "data/HEA/thyroid_SM-JF1O4_rep1_fragments.bed.gz")
    , ("uterus_SM-A87A2", "data/HEA/uterus_SM-A87A2_rep1_fragments.bed.gz")
    , ("uterus_SM-IOBHX", "data/HEA/uterus_SM-IOBHX_rep1_fragments.bed.gz")
    , ("vagina_SM-A9HOS", "data/HEA/vagina_SM-A9HOS_rep1_fragments.bed.gz")
    , ("heart_lv_CARE181125_3D", "data/heart/CARE181125_3D_rep1_fragments.bed.gz")
    , ("heart_lv_CARE190307_10D", "data/heart/CARE190307_10D_rep1_fragments.bed.gz")
    , ("heart_lv_CARE190331_11D", "data/heart/CARE190331_11D_rep1_fragments.bed.gz")
    , ("heart_lv_CARE191122_2D", "data/heart/CARE191122_2D_rep1_fragments.bed.gz")
    , ("heart_lv_CARE191122_3D", "data/heart/CARE191122_3D_rep1_fragments.bed.gz")
    , ("heart_la_CARE190307_10C", "data/heart/CARE190307_10C_rep1_fragments.bed.gz")
    , ("heart_la_CARE181125_3C", "data/heart/CARE181125_3C_rep1_fragments.bed.gz")
    , ("heart_la_CARE191122_2C", "data/heart/CARE191122_2C_rep1_fragments.bed.gz")
    , ("heart_rv_CARE181125_3B", "data/heart/CARE181125_3B_rep1_fragments.bed.gz")
    , ("heart_ra_CARE181213_2A", "data/heart/CARE181213_2A_rep1_fragments.bed.gz")
    , ("heart_rv_CARE181213_2B", "data/heart/CARE181213_2B_rep1_fragments.bed.gz")
    , ("heart_rv_CARE190307_10B", "data/heart/CARE190307_10B_rep1_fragments.bed.gz")
    , ("heart_rv_CARE190331_11B", "data/heart/CARE190331_11B_rep1_fragments.bed.gz")
    , ("heart_ra_CARE190307_10A", "data/heart/CARE190307_10A_rep1_fragments.bed.gz")
    , ("Human_brain_1", "data/brain/Human_brain_1_rep1_fragments.bed.gz")
    , ("Human_brain_2", "data/brain/Human_brain_2_rep1_fragments.bed.gz")
    , ("islet_CB1", "data/islet/islet_CB1_rep1_fragments.bed.gz")
    , ("islet_CB2", "data/islet/islet_CB2_rep1_fragments.bed.gz")
    , ("islet_CB3", "data/islet/islet_CB3_rep1_fragments.bed.gz")
    , ("LungMap_D122", "data/lung/LungMap_D122_rep1_fragments.bed.gz")
    , ("LungMap_D175", "data/lung/LungMap_D175_rep1_fragments.bed.gz")
    , ("LungMap_D231", "data/lung/LungMap_D231_rep1_fragments.bed.gz") ]
