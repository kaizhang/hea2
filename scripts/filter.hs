import Bio.Data.Bed
import Bio.Utils.Functions
import qualified Data.Vector as V
import System.Environment
import Taiji.Prelude

main = do
    [fl] <- getArgs
    peaks <- runResourceT $ runConduit $ streamBedGzip fl .| sinkList
    print $ minimum $ map (fromJust . (^.npPvalue)) peaks
    --print $ V.length $ filterFDR 0.001 $ V.fromList $ map (\x -> (x, f $ fromJust $ x^.npPvalue)) peaks
  where
    f x = 10**(negate x)
