{-# LANGUAGE OverloadedStrings #-}
import qualified Data.ByteString.Char8 as B
import qualified Data.Set as S
import qualified Data.Map.Strict as M
import Conduit
import Data.Conduit.Zlib
import HEA
import System.Environment
import Data.ByteString.Lex.Integral (packDecimal)
import Bio.Data.Bed
import Text.Printf
import Data.List.Ordered
import qualified Data.Text as T

readCellCRE :: [FilePath] -> IO [(BED3, [Int])]
readCellCRE fls = runResourceT $ runConduit $ mapM_ f (zip [1..] fls) .| sinkList
  where
    f (i, fl) = streamBedGzip fl .| mapC (\x -> (x :: BED3, [i]))

main = do
    creFl:inputs <- getArgs
    anno1 <- map (\(a,b) -> ("A_" <> a, b)) <$> readAnno "../annotation.tsv"
    anno2 <- map (\(a,b) -> ("F_" <> a, "Fetal " <> b)) <$> readAnno "../fetal_annotation.tsv"
    let anno = anno1 <> anno2

    cre <- runResourceT $ runConduit $ sourceFile creFl .| multiple ungzip .|
        linesUnboundedAsciiC .| (dropC 1 >> mapC fromLine) .| sinkList :: IO [BED3]
    tree <- bedToTree (<>) <$> readCellCRE inputs
    let m = flip map cre $ \x -> nubSort $ concatMap snd $ queryIntersect x tree
        header =
            [ "%%MatrixMarket matrix coordinate pattern general"
            , "%", B.pack $ printf "%d %d %d" (length m) (length inputs) (sum $ map length m) ]
        result = flip concatMap (zip [1..] m) $ \(i, xs) ->
            map (\j -> B.unwords [fromJust $ packDecimal i, fromJust $ packDecimal j]) xs
    B.writeFile "out.mat" $ B.unlines $ header <> result

    B.writeFile "col.txt" $ B.unlines $ flip map inputs $ \x ->
        let stage = if "Fetal_Peaks" `T.isInfixOf` T.pack x
                then "F" else "A"
            i = B.pack $ T.unpack $ fst $ T.breakOn ".bed" $ snd $ T.breakOnEnd "/" $ T.pack x
        in fromJust $ lookup (stage <> "_" <> i) anno