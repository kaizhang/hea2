{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE QuasiQuotes       #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE RecordWildCards #-}
module HEA
  ( abbrGo
  , abbreviation
  , changeName
  , readAnno
  , mainFun
  , module Control.Workflow
  , module Data.Maybe
  , asks
  , liftIO
  , ENV
  , lookupConfig
  , liftOver
  , crossmap
  , GREAT(..)
  , great
  , great'
  , writeGREAT
  , filterGREAT
  , umap
  , sampling
  , sampling'
  , showBed

  , goEnrich

  , cat20
  , color26
  , myTheme

  , toRMatrix
  , toRSpMatrix
  ) where

import qualified Data.ByteString.Char8 as B
import qualified Data.HashMap.Strict as M
import qualified Data.Map.Strict as Map
import Bio.Data.Bed.Types
import Bio.Data.Bed
import qualified Data.Vector as V
import Bio.Utils.Functions
import qualified Data.Text as T
import Data.Binary
import Data.Conduit.Internal (zipSources)
import GHC.Generics (Generic)
import qualified Data.Matrix as Mat
import Data.Yaml
import Control.Monad.Trans.Reader (ask)
import Taiji.Prelude
import System.Random.MWC
import System.Random.MWC.Distributions (uniformShuffle)
import Data.Int (Int32)
import Taiji.Utils.Plot (npg)
import qualified Taiji.Utils.DataFrame as DF
import Taiji.Utils
import Data.Maybe
import Shelly (shelly, run_)

import Control.Workflow.Coordinator.Remote (Remote, RemoteConfig(..), getDefaultRemoteConfig)
import           Control.Workflow
import Control.Workflow.Main
import Data.Proxy (Proxy(..))
import Data.Binary.Instances ()

import Data.Vector.SEXP (toList)
import qualified Language.R                        as R
import qualified Language.R.HExp as H
import           Language.R.QQ

abbrGo :: T.Text -> T.Text
abbrGo txt' | T.length txt > 30 = T.unwords $ map f $ T.words txt
          | otherwise = txt
  where
    txt = T.replace "independent" "indep." $
        T.replace "dependent" "dep." $
        T.replace "platelet-derived growth factor receptor-beta" "PDGFR-beta" $
        T.replace "transforming growth factor beta" "TGF-beta" txt'
    f x = fromMaybe x $ lookup x table
    table = [ ("regulation", "reg.")
            , ("inactivation", "inact.")
            , ("adhesion", "adh.")
            , ("proliferation", "prolifer.")
            , ("signaling", "sig.")
            , ("development", "dev.")
            , ("negative", "neg.")
            , ("postive", "pos.")
            , ("process", "proc.")
            , ("involved", "involv.")
            , ("establishment", "establish.")
            , ("dichotomous", "dichot.")
            , ("branching", "branch.")
            , ("subdivision", "subdiv.")
            , ("depolarization", "depolar.")
            , ("relaxation", "relax.")
            , ("membrane", "membr.")
            , ("muscle", "mus.")
            ]

liftOver :: (BEDLike b, BEDConvert b)
         => FilePath  -- chainfile
         -> [b]   -- from
         -> IO [b]
liftOver chain beds = withTempDir Nothing $ \dir -> do
    let tmp1 = dir <> "/tmp1.bed"
        tmp2 = dir <> "/tmp2.bed"
    writeBed tmp1 beds 
    shelly $ run_ "liftOver" ["-bedPlus=3", T.pack tmp1, T.pack chain, T.pack tmp2, "/dev/null"]
    readBed tmp2

crossmap :: FilePath -> FilePath -> FilePath -> IO ()
crossmap chain input output = do
    shelly $ run_ "CrossMap.py" ["bigwig", T.pack chain, T.pack input, T.pack output]

myTheme :: R.R s ()
myTheme = do
    [r| 
    myTheme <<- function(base_size=6, base_family="Helvetica") {
        library(grid)
        library(ggthemes)
        (theme_foundation(base_size=base_size, base_family=base_family)
        + theme(plot.title = element_text(face = "bold",
                size = 7, hjust = 0.5),
            text = element_text(),
            panel.background = element_rect(colour = NA),
            plot.background = element_rect(colour = NA),
            panel.border = element_rect(colour = NA),
            axis.title = element_text(size = rel(1)),
            axis.title.y = element_text(angle=90,vjust =2),
            axis.title.x = element_text(vjust = -0.2),
            axis.text = element_text(), 
            axis.line = element_line(colour="black"),
            axis.ticks = element_line(),
            panel.grid.major = element_line(colour="#f0f0f0", size=0.2),
            panel.grid.minor = element_blank(),
            legend.key = element_rect(colour = NA),
            legend.position = "bottom",
            legend.direction = "horizontal",
            legend.key.size= unit(0.2, "cm"),
            legend.margin = unit(0, "cm"),
            legend.title = element_text(face="italic"),
            plot.margin=unit(c(5,5,5,5),"mm"),
            strip.background=element_rect(colour="#f0f0f0",fill="#f0f0f0"),
            strip.text = element_text(face="bold")
            ))
        }

        myFill <<- function(...){
            library(scales)
            discrete_scale("fill","Publication",manual_pal(values = npg_hs), ...)
        }
        myColour <<- function(...){
            library(scales)
            discrete_scale("colour","Publication",manual_pal(values = npg_hs), ...)
        }
    |]
    return ()

toRMatrix :: DF.DataFrame Double -> (forall s. R.R s (R.SomeSEXP s)) 
toRMatrix df = [r|
        mat <- matrix(dat_hs, row_hs, byrow=T)
        rownames(mat) <- rownames_hs
        colnames(mat) <- colnames_hs
        mat
    |]
  where
    dat = concat $ Mat.toLists $ DF._dataframe_data df
    colnames = map T.unpack $ DF.colNames df
    rownames = map T.unpack $ DF.rowNames df
    row = fromIntegral $ Mat.rows $ DF._dataframe_data df :: Double

umap :: Int   -- ^ neighbors
     -> DF.DataFrame Double -> IO (DF.DataFrame Double)
umap n df = do
    res <- R.runRegion $ do
        mat <- toRMatrix df
        embedding <- [r| library("uwot")
            set.seed(123)
            umap(mat_hs, n_neighbors = neighbors_hs, min_dist = 0.01, metric = "euclidean")
        |]
        d1 <- [r| embedding_hs[,1] |] 
        d2 <- [r| embedding_hs[,2] |] 
        R.unSomeSEXP d1 $ \x -> R.unSomeSEXP d2 $ \y ->
            case (H.hexp x, H.hexp y) of
                (H.Real x', H.Real y') -> return [toList x', toList y']
    return $ DF.mkDataFrame (DF.rowNames df) ["D1", "D2"] $ transpose res
  where
    neighbors = fromIntegral n :: Int32

toRSpMatrix :: SpMatrix Double -> (forall s. R.R s (R.SomeSEXP s)) 
toRSpMatrix mat = do
    (nm, dat) <- fmap unzip $ liftIO $ runResourceT $ runConduit $
        zipSources (iterateC succ (0::Int32)) (streamRows mat) .|
        mapC f .| sinkList
    let (i, j, x) = unzip3 $ concat dat
    [r| library("Matrix")
        sparseMatrix(i=i_hs, j=j_hs, x=x_hs, index1=F,
            dims=c(n_hs, m_hs),
            dimnames = list(nm_hs, NULL)
        )
    |]
  where
    f (i, (nm, xs)) = (B.unpack nm, map (\(j,x) -> (i,fromIntegral j :: Int32, x)) xs)
    n = fromIntegral $ _num_row mat :: Int32
    m = fromIntegral $ _num_col mat :: Int32

cat20 :: [String]
cat20 = [ "#1f77b4", "#aec7e8", "#ff7f0e", "#ffbb78", "#2ca02c", "#98df8a"
    , "#d62728", "#ff9896", "#9467bd", "#c5b0d5", "#8c564b", "#c49c94"
    , "#e377c2", "#f7b6d2", "#7f7f7f", "#c7c7c7", "#bcbd22", "#dbdb8d", "#17becf", "#9edae5"]

color26 :: [String]
color26 = ["#5c3200", "#0b0097", "#c8ff34", "#ff82ff", "#64d600", "#4e0057", "#62ff7d",
    "#e5006c", "#02e89e", "#e30040", "#019d0d", "#004a9d", "#ffee4a", "#280028",
    "#edffa6", "#2a0013", "#b0ffd4", "#ff6c28", "#017d9e", "#a27c00", "#dcafff",
    "#004809", "#abd5ff", "#322300", "#ffedcb", "#01a170"]

showBed :: BEDLike b => b -> B.ByteString
showBed x = B.concat [x^.chrom, ":", B.pack (show $ x^.chromStart), "-", B.pack (show $ x^.chromEnd)]


{-
colorMap :: 
colorMap 
#393b79
#5254a3
#6b6ecf
#9c9ede
#637939
#8ca252
#b5cf6b
#cedb9c
#8c6d31
#bd9e39
#e7ba52
#e7cb94
#843c39
#ad494a
#d6616b
#e7969c
#7b4173
#a55194
#ce6dbd
#de9ed6
-}

abbreviation :: [(T.Text, T.Text)]
abbreviation =
    [ ("ovary", "ov")
    , ("pancreas", "pc")
    , ("liver", "lv")
    , ("adrenal_gland", "ag")
    , ("thyroid", "tr")
    , ("artery_aorta", "aa")
    , ("artery_tibial", "at")
    , ("mammary_tissue", "mm")
    , ("adipose_omentum", "ap")
    , ("uterus", "ur")
    , ("colon_transverse", "ct")
    , ("nerve_tibial", "nt")
    , ("heart_lv", "hl")
    , ("lung", "lu")
    , ("colon_sigmoid", "cs")
    , ("heart_atrial_appendage", "ha")
    , ("vagina", "vg")
    , ("esophagus_muscularis", "em")
    , ("stomach", "st")
    , ("esophagus_ge_junction", "eg")
    , ("esophagus_mucosa", "ec")
    , ("small_intestine", "in")
    , ("muscle", "mu")
    , ("skin_sun_exposed", "ss")
    , ("skin", "sk")
    , ("colon", "cn")
    ]

readAnno :: FilePath -> IO [(B.ByteString, B.ByteString)]
readAnno fl = do
    anno <- map ((\(a:_:b:_) -> (a, b)) . B.split '\t') . B.lines <$>
        B.readFile fl
    return $ concatMap f $ M.toList $ M.fromListWith (++) $ map (\(a,b) -> (b, [a])) anno
  where
    f (x, [y]) = [(y, x)]
    f (x, ys) = zipWith g ys [1..]
      where
        g y i = (y, x <> "#" <> B.pack (show i))

changeName :: [(B.ByteString, B.ByteString)] -> DF.DataFrame a -> DF.DataFrame a
changeName anno df = df{DF._dataframe_col_names = names, DF._dataframe_col_names_idx = idx}
  where
    f x = case lookup (B.pack $ T.unpack x) anno of
        Nothing -> x
        Just x' -> T.pack $ B.unpack x'
    names = V.map f $ DF._dataframe_col_names df
    idx = Map.fromList $ zip (V.toList names) [0..]

data GREAT = GREAT
    { _great_term_id :: Int
    , _great_term_name :: B.ByteString
    , _great_enrichment :: Double
    , _great_pvalue :: Double
    } deriving (Show, Generic)


instance Binary GREAT

goEnrich :: FilePath -> [(String, Double)] -> IO ()
goEnrich output genes = R.runRegion $ do
    [r| library("topGO")
        val <- ys_hs
        names(val) <- xs_hs
        select <- function(x) {return(x>0.5)}
        sampleGOdata <- new("topGOdata", description = "Simple session", ontology = "BP",
            allGenes = val, nodeSize = 10, 
            mapping = "org.Hs.eg.db",
            ID="SYMBOL",
            annot = annFUN.org, geneSel=select )

        result <- runTest(sampleGOdata, algorithm = "elim", statistic = "fisher")
        allRes <- GenTable(sampleGOdata, pvalue= result,
            orderBy = "pvalue", ranksOf = "pvalue", topNodes = 50)
        write.table(allRes, file=output_hs, sep="\t", quote=F, row.names=F)
    |]
    return ()
  where
    (xs,ys) = unzip genes

writeGREAT :: FilePath -> [GREAT] -> IO ()
writeGREAT output terms = B.writeFile output $ B.unlines $ header : map f terms
  where
    header = "GO_ID\tGO_Name\t_Fold_enrichment\t-log10(P-value)"
    f GREAT{..} = B.intercalate "\t"
        [ B.pack $ show _great_term_id
        , _great_term_name
        , B.pack $ show _great_enrichment
        , B.pack $ show $ negate $ logBase 10 _great_pvalue ]

{-
goSummary :: [GREAT] -> IO ()
goSummary = 
    [r| library("tm")
        docs <- Corpus(VectorSource(text_hs))
    |]
-}

-- | Apply standard GREAT filtering
filterGREAT :: [GREAT] -> [GREAT]
filterGREAT = fst . unzip . sortBy (comparing snd) . V.toList .
    filterFDR 0.01 . V.fromList . map (\x -> (x, _great_pvalue x)) .
    filter ((>=2) . _great_enrichment)

great :: FilePath -> IO [GREAT]
great input = withTemp Nothing $ \tmp -> do
    if ".gz" `T.isSuffixOf` T.pack input
        then runResourceT $ runConduit $ streamBedGzip input .| filterC f .| sinkFileBed tmp
        else runResourceT $ runConduit $ streamBed input .| filterC f .| sinkFileBed tmp
    (readBed tmp :: IO [BED3]) >>= \case
        [] -> return []
        _ -> do
            R.runRegion $ do
                _ <- [r| library(rGREAT)
                        bed <- as.data.frame(read.table(tmp_hs, header = FALSE, sep="\t", stringsAsFactors=FALSE, quote=""))
                        job <- submitGreatJob(bed, species = "hg38", version = "4.0", request_interval = 10)
                        print(job)
                        tb <- getEnrichmentTables(job, ontology = c("GO Biological Process"))
                        write.table(tb, file=tmp_hs, sep="\t", quote=F, row.names=F)
                |]
                return ()
            getGO tmp
  where
    f :: BED3 -> Bool
    f x = "chr" `B.isPrefixOf` (x^.chrom)
    getGO :: FilePath -> IO [GREAT]
    getGO input = map (f . B.split '\t') . tail . B.lines <$> B.readFile input
      where
        f xs = GREAT (readInt $ snd $ B.breakEnd (==':') $ xs!!0)
            (xs!!1) (readDouble $ xs!!5) (readDouble $ xs!!7) 

-- | Promoter only
great' :: FilePath -> FilePath -> IO [GREAT]
great' input assoc = withTemp Nothing $ \tmp -> do
    if ".gz" `T.isSuffixOf` T.pack input
        then runResourceT $ runConduit $ streamBedGzip input .| filterC f .| sinkFileBed tmp
        else runResourceT $ runConduit $ streamBed input .| filterC f .| sinkFileBed tmp
    (readBed tmp :: IO [BED3]) >>= \case
        [] -> return []
        _ -> do
            R.runRegion $ do
                _ <- [r| library(rGREAT)
                        bed <- as.data.frame(read.table(tmp_hs, header = FALSE, sep="\t", stringsAsFactors=FALSE, quote=""))
                        job <- submitGreatJob(bed, species = "hg38",
                            version = "4.0", request_interval = 10)
                        print(job)
                        tb <- getEnrichmentTables(job, ontology = c("GO Biological Process"))
                        write.table(tb, file=tmp_hs, sep="\t", quote=F, row.names=F)
                        res <- plotRegionGeneAssociationGraphs(job)
                        write.table(res, file=assoc_hs, sep="\t", quote=F, row.names=F)
                |]
                return ()
            getGO tmp
  where
    f :: BED3 -> Bool
    f x = "chr" `B.isPrefixOf` (x^.chrom)
    getGO :: FilePath -> IO [GREAT]
    getGO input = map (f . B.split '\t') . tail . B.lines <$> B.readFile input
      where
        f xs = GREAT (readInt $ snd $ B.breakEnd (==':') $ xs!!0)
            (xs!!1) (readDouble $ xs!!5) (readDouble $ xs!!7) 

sampling :: Int -> [a] -> GenIO -> IO [a]
sampling n xs gen = uniformShuffle (V.fromList xs) gen >>= return . take n . V.toList

sampling' :: Int -> [[a]] -> GenIO -> IO [[a]]
sampling' n xs gen = sequence $ zipWith (\a b -> sampling a b gen) nSample xs
  where
    nSample = let ws = map (sqrt . fromIntegral . length) xs
               in map (\x -> max 1 $ truncate $ x / sum ws * fromIntegral n) ws

-------------------------------------------------------------------------------
-- Workflow related functions
-------------------------------------------------------------------------------

getCoordConfig :: String -> Int -> FilePath -> IO RemoteConfig
getCoordConfig ip port fl = do
    config <- getDefaultRemoteConfig ["remote", "--ip", ip, "--port", show port]
    settings <- decodeFileThrow fl :: IO (M.HashMap String Value)
    return config
        { _remote_parameters = str <$> M.lookup "submit_params" settings
        , _submission_cmd = str $ M.lookupDefault "sbatch" "submit_command" settings
        , _cpu_format = str $ M.lookupDefault "--ntasks-per-node=%d" "submit_cpu_format" settings
        , _memory_format = str $ M.lookupDefault "--mem=%d000" "submit_memory_format" settings
        }
  where
    str (String x) = T.unpack x
    str _ = error "Expecting string"

commands = [ runParser getCoordConfig
           , deleteParser
           , showParser
           , viewParser
           , remoteParser (Proxy :: Proxy Remote) ]

type ENV = Map.Map String Value

lookupConfig :: String -> ReaderT ENV IO String
lookupConfig x = asks (Map.findWithDefault errMsg x) >>= \case
    String s -> return $ T.unpack s
    _ -> error "Not a String"
  where
    errMsg = error $ "Option not found: " <> x

mainFun :: SciFlow ENV -> IO ()
mainFun = defaultMain "" "" commands


-- | Creates count table 'Counts'
counts :: (Ord a, Ord b) => [a] -> [b] -> Counts a b
counts xs = foldl' f mempty . zipWith ((,)) xs
    where f cs@(Counts cxy cx cy) p@(x,y) = 
            cs { joint       = Map.insertWith (+) p 1 cxy
               , marginalFst = Map.insertWith (+) x 1 cx
               , marginalSnd = Map.insertWith (+) y 1 cy }

-- | A count
type Count = Double
-- | Count table
data Counts a b = 
  Counts 
  { joint :: !(Map.Map ((,) a b) Count) -- ^ Counts of both components
  , marginalFst :: !(Map.Map a Count) -- ^ Counts of the first component
  , marginalSnd :: !(Map.Map b Count) -- ^ Counts of the second component
  } 

instance (Ord a, Ord b) => Monoid (Counts a b) where
    mempty = Counts Map.empty Map.empty Map.empty
    c `mappend` k = 
        Counts { joint = unionPlus (joint c) (joint k)
               , marginalFst = unionPlus (marginalFst c) (marginalFst k)
               , marginalSnd = unionPlus (marginalSnd c) (marginalSnd k)
               }

instance (Ord a, Ord b) => Semigroup (Counts a b) where
    c <> k = 
        Counts { joint = unionPlus (joint c) (joint k)
               , marginalFst = unionPlus (marginalFst c) (marginalFst k)
               , marginalSnd = unionPlus (marginalSnd c) (marginalSnd k)
               }

unionPlus :: (Num a, Ord k) => Map.Map k a -> Map.Map k a -> Map.Map k a
unionPlus m = 
    Map.foldlWithKey' (\z k v -> Map.insertWith (+) k v z) m

-- | The binomial coefficient: C^n_k = PROD^k_i=1 (n-k-i)\/i
choice :: (Enum b, Fractional b) => b -> b -> b
choice n k = foldl' (*) 1 [n-k+1 .. n] / foldl' (*) 1 [1 .. k]
{-# SPECIALIZE choice :: Double -> Double -> Double #-}